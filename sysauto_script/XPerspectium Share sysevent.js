function pspShareSysevent() {
	var pspRepl = new PerspectiumReplicator();
	
	var sgr = new GlideRecord("sys_trigger");
	sgr.addQuery("name", "XPerspectium Share sysevent");
	sgr.query();
	if (!sgr.next()) {
		return; // no scheduled job, no run, which is unlikely
	}
		
	var tGr = new GlideRecord("sysevent");
		
	var gdt = new GlideDateTime(sgr.sys_updated_on.toString());
	gdt.addSeconds(-10);
		
	var eq = "sys_created_onBETWEEN" + gdt.getValue() + "@javascript:gs.minutesAgoEnd(-1)";
		
	tGr.addEncodedQuery(eq);
	tGr.orderBy("sys_created_on");
	tGr.query();
	var kount = 0;
	while(tGr.next()) {
		pspRepl.shareRecord(tGr);
	}
}

pspShareSysevent();