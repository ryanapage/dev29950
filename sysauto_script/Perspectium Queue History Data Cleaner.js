var pspUtil = new PerspectiumUtil();
var daysRetained = pspUtil.getPspPropertyValue("com.perspectium.queue_history.days_retained", "35");
var encodedQuery = "u_timestamp<=javascript:gs.daysAgoStart(" + daysRetained + ")";

var pspDC = new PerspectiumDataCleaner();
pspDC.cleanTable("u_psp_queue_history", encodedQuery);