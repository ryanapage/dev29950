var pspUtil = new PerspectiumUtil();
var pspDC = new PerspectiumDataCleaner();
pspDC.cleanTable("psp_out_message","state=sent^sys_updated_on<javascript:gs.minutesAgoStart(15)");
pspDC.cleanTable("psp_in_message","state=received^ORstate=skipped^sys_updated_on<javascript:gs.minutesAgoStart(15)");
pspDC.cleanTable("u_psp_refresh_history_set","u_state=processed^ORu_state=skipped");

// delete error messages older than 1 day
var e = pspUtil.getPspPropertyValue("com.perspectium.outbound.delete_error_messages", "false");
if (e == "true") {
	psp.cleanTable("psp_out_message","state=error^sys_created_on<javascript:gs.daysAgoStart(0)");
}