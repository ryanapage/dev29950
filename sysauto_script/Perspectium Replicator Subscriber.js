var pspUtil = new PerspectiumUtil();
try {
    var e = pspUtil.getPspPropertyValue("com.perspectium.enable_inbound", "true");
    if (e == "true") {
        var p = new PerspectiumReplicator();
    	p.fetchFromQueue();
    }
}
catch(e) {
    var logger = new PerspectiumLogger();
    logger.logError("error = " + e, "Perspectium Replicator Subscriber");
}