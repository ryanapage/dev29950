var pspUtil = new PerspectiumUtil();
var msgValue = "Perspectium-Version: " + pspUtil.getPspPropertyValue("com.perspectium.update_set.version", "unknown");
var instanceKey = gs.getProperty("instance_name", "unregistered");
	
// create and put heartbeat message into outbound queue
var pspM = new PerspectiumMessage("monitor", "heartbeat", instanceKey, "status", msgValue, "", "", "component_type=servicenow");
pspM.enqueue();