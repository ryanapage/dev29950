var dt = new GlideDateTime();
var rand = getRandomIntInclusive(0,10);
dt.addSeconds(rand * 60);

gs.info("Process in " + rand + " minutes");
gs.eventQueueScheduled("sn_appauthor.check.config.update","","","", dt);

function getRandomIntInclusive(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}