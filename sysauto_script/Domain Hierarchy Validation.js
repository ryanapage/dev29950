if (new GlideDomainPathProvider().isInstalled()) {
	GlideDomainPathUpdater.get().updateDomainPaths(false);
	var pathValidator = new GlideDomainPathValidator();
	pathValidator.setUpdateProgress(false);
	pathValidator.validateDomainPaths(false, true);
}