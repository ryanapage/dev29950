try {
    var pspO = new PerspectiumOutbound("u_psp_attachment_out_message");
	pspO.processOutbound();
}
catch(e) {
    var logger = new PerspectiumLogger();
    logger.logError("error = " + e, "Perspectium Attachments and Audit Outbound Processing");
}

try {
	var pspO2 = new PerspectiumOutbound("u_psp_audit_out_message");
	pspO2.processOutbound();
}
catch(e) {
    var logger = new PerspectiumLogger();
    logger.logError("error = " + e, "Perspectium Attachments and Audit Outbound Processing");
}