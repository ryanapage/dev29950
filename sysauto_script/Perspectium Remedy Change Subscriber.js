var psp = new Perspectium();
var gr = new GlideRecord("u_psp_queues");
var gdt = new GlideDateTime();
var gtime1 = new GlideTime();
gtime1.setValue("00:00:30");
gdt.subtract(gtime1);
var target_sys = "";
gr.addQuery("u_name", "psp.in.siam.client.remedy");
gr.query();
if (gr.next())
	target_sys = gr.sys_id;

var dbGR = new GlideRecord('sys_db_object');
dbGR.addQuery('name', 'u_psp_receipt_status');
dbGR.query();

var receiptStatus = "";

if (dbGR.next()) {
	var statusGR = new GlideRecord('u_psp_receipt_status');
	statusGR.u_name = "remedy";
	statusGR.u_value = "getChanges";
	var statusSysId = statusGR.insert();
	receiptStatus = ",ReceiptStatusId=" + statusSysId;
}

this.psp.createPSPOut("siam", "remedy", gs.getProperty('instance_name'), "getChanges", "", target_sys, "", "cipher=3,SIAM_common_format=change,SIAM_provider=remedy,RemedyAPIForm=CHG:ChangeInterface,set_id=,query_field=Submit Date,psp_flag=last,psp_flag=first,SIAM_message_id=,query_time=" + gdt.getValue().replace(" ", "T") + receiptStatus, "", "ready", "");