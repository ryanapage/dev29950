try {
	var pspRHS = new PerspectiumRefreshHistorySet();
	pspRHS.refreshRecords();
}
catch(e) {
    var logger = new PerspectiumLogger();
    logger.logError("error = " + e, "Perspectium Refresh History Set");
}