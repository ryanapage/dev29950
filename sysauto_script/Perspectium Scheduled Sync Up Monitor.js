/*
 * If the Sync Up has been locked (syncing) for longer than 60 minutes (waitTime) 
 * after it was scheduled we suspect it may not have released the lock properly 
 * and it won't be re-scheduled.  Log an error/alert and reset it.
 *
 * Can change params allowReset or waitTime
 */

var allowReset = true;
var waitTime = 60;

var logger = new PerspectiumLogger();
var gr = new GlideRecord('psp_replicate_conf');
gr.addQuery('u_syncing', 'true');
gr.addQuery('u_auto_scheduled_sync_reset', 'false');
gr.addNotNullQuery('u_last_sync_time');
gr.query();
while(gr.next()){
	checkDynamicShare(gr);
}

function checkDynamicShare(gr){
	var interval = parseInt(gr.u_sync_interval, 10) + waitTime;
	if(gr.u_last_sync_time < gs.minutesAgo(interval)){
		var r = "";
		if(allowReset){
			gr.u_syncing = false;
			gr.u_auto_scheduled_sync_reset = true;
			gr.update();
			r = " Resetting";
		}
		logger.logError("Dynamic Share: " + gr.table_name + " - [ " + gr.sys_id + " ] may have their Scheduled Sync Up stuck." + r, "Perspectium Scheduled Sync Up Monitor");
	}
}