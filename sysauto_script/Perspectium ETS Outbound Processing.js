try {
    var pspEO = new PerspectiumETSOutbound();
	pspEO.processOutbound();
}
catch(e) {
    var logger = new PerspectiumLogger();
    logger.logError("error = " + e, "Perspectium ETS Outbound Processing");
}