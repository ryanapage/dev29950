var logger = new PerspectiumLogger();
var pspUtil = new PerspectiumUtil();
var lastUpdated;
var currentTime;
var diffTime;
var timeThreshold = pspUtil.getPspPropertyValue("com.perspectium.replicator.check_long_running_bulk_share", 12);
var gr = new GlideRecord('psp_bulk_share');
gr.addQuery('status', 'Running');
gr.query();

while (gr.next()) {
	lastUpdated  = gr.getValue("sys_updated_on");
	currentTime = new GlideDateTime();
	diffTime = parseInt(gs.dateDiff(lastUpdated, currentTime, true));
	diffTime = (diffTime / 60) / 60;
	
	if (diffTime > timeThreshold) {
		logger.logError("Bulk share \"" + gr.getValue("u_name") + "\" <" + gr.sys_id + "> is running but has not replicated data in " + timeThreshold +" hours. It was last updated on " + lastUpdated + " (GMT)", "PerspectiumReplicator.BulkShareMonitor");
	}
}




