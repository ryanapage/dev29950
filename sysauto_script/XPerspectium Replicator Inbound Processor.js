var pspS = new Perspectium();
var pspUtil = new PerspectiumUtil();
try {
    var e = pspUtil.getPspPropertyValue("com.perspectium.enable_inbound", "true");
    if (e == "true") {
		var igr = new GlideRecord("psp_in_message");
		igr.addQuery("state", "ready");
		igr.orderBy("sys_created_on");
		igr.query();
		while(igr.next()) {
			if (pspS.processPSPInMessage(igr)) {
				igr.state = "received";
			}

			igr.update();
		}
		
		// insert all history set into table for scheduled job to process
		pspS.pspRHS.insertIntoTable();
    }
}
catch(e) {
    var logger = new PerspectiumLogger();
    logger.logError("error = " + e, "Perspectium Replicator Inbound Processor");
}

