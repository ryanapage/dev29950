var psp = new Perspectium();
var gr = new GlideRecord("u_psp_queues");
var gdt = new GlideDateTime();
var gtime1 = new GlideTime();
gtime1.setValue("00:01:00");
gdt.subtract(gtime1);
var target_sys = "";
gr.addQuery("u_name", "psp.in.siam.client.remedy");
gr.query();
if (gr.next())
	target_sys = gr.sys_id;

var dbGR = new GlideRecord('sys_db_object');
dbGR.addQuery('name', 'u_psp_receipt_status');
dbGR.query();

var receiptStatus = "";

if (dbGR.next()) {
	var statusGR = new GlideRecord('u_psp_receipt_status');
	statusGR.u_name = "remedy";
	statusGR.u_value = "getComments";
	var statusSysId = statusGR.insert();
	receiptStatus = ",ReceiptStatusId=" + statusSysId;
}

var queryString = "'Incident Number' LIKE \"INC%\"";
this.psp.createPSPOut("siam", "remedy", gs.getProperty('instance_name'), "getIncidents", "", target_sys, "", "cipher=3,SIAM_common_format=incident,SIAM_provider=remedy,query_field=Last Modified Date,RemedyAPIForm=HPD:IncidentInterface,set_id=,psp_flag=last,psp_flag=first,SIAM_message_id=,query_time=" + gdt.getValue().replace(" ", "T") + ",remedy_query_string=" + queryString + receiptStatus, "", "ready", "");