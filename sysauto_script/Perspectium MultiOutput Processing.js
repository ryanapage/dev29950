try {
    var psp = new Perspectium();
    psp.processMultiOutput();
}
catch(e) {
    var logger = new PerspectiumLogger();
    logger.logError("error = " + e, "Perspectium MultiOutput Processing");
}