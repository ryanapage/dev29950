(function(){
	// This scheduled job checks for the integrity of the source records updated by Discovery NOT within the last 6 months.
	
	start();
	
	function start() {
		var gr = new GlideRecord("sys_object_source");
		gr.addQuery("name", ["ServiceNow", "Service-now"]);
		gr.addQuery("last_scan", "<", new GlideDateTime(gs.monthsAgo(6)));
		gr.query();
		
		while (gr.next()) {
			var grt = new GlideRecord(gr.target_table);
			if (grt.get(gr.target_sys_id))
				continue;
			
			gr.deleteRecord();
		}
	}

}());

