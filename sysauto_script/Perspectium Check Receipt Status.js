var pspUtil = new PerspectiumUtil();
var pspLogger = new PerspectiumLogger();
var alertThreshold = pspUtil.getPspPropertyValue('com.perspectium.receipt.error_alert_threshold', '10'); // in mins
var autoDelete = pspUtil.getPspPropertyValue('com.perspectium.receipt.status_auto_delete', 'true');

var p = new GlideDateTime(gs.minutesAgo(alertThreshold)).getValue();
var gr = new GlideRecord('u_psp_receipt_status');
gr.addQuery('sys_created_on' , '<', p);
gr.addQuery('u_processed', false);
gr.query();

while(gr.next()) {
	if (gr.u_status == 'sent') {
		var error = 'Receipt was not received for record [' + gr.u_record + '] on table [' + gr.u_record_table + ']';
		var name = 'PerspectiumReceipt';
		pspLogger.logError(error, name);
	}
	
	if (autoDelete == true || autoDelete == 'true') {
		gr.deleteRecord();
	} else {
		gr.u_processed = true;
		gr.update();
	}
}