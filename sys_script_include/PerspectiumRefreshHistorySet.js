var PerspectiumRefreshHistorySet = Class.create();
PerspectiumRefreshHistorySet.prototype = {
    initialize: function() {
		this.tableRecords = {};
    },
	
	add: function(tableName, tableSysId) {
		// if we already have sys_ids to refresh for this table, we'll check if the sys_id is in here to avoid duplicates
		if (tableName in this.tableRecords) {
			var tableSysIds = this.tableRecords[tableName];
			if (tableSysIds.indexOf(tableSysId) >= 0)
				return;
			this.tableRecords[tableName] = tableSysIds + "," + tableSysId;
		} 	
		// no sys ids yet for this table so we'll set it as the first one
		else {
			this.tableRecords[tableName] = tableSysId;
		}		
	},
	
	insertIntoTable: function() {
		for (var t in this.tableRecords) {
			var rgr = new GlideRecord("u_psp_refresh_history_set");
			rgr.u_state = "ready";
			rgr.u_table_name = t;
			rgr.u_sys_id = this.tableRecords[t];
			rgr.insert();
		}
	},
	
	refreshRecords: function() {
		var rgr = new GlideRecord("u_psp_refresh_history_set");
		rgr.addQuery("u_state", "ready");
		rgr.query();
		while (rgr.next()) {
			var success = true;
			var tableSysIds = String(rgr.u_sys_id).split(",");
			for (var t = 0; t < tableSysIds.length; t++) {
				var hgr = new GlideRecord(rgr.u_table_name);
				if (!hgr.get(tableSysIds[t])) {
					continue;
				}
				try{
					var hs = new GlideHistorySet(hgr);
					hs.refresh();
				}
				catch(e){
					var logger = new PerspectiumLogger();
					logger.logDebug("Skipped history set for: " + hgr.getTableName() + " - " + tableSysIds[t], "PerspectiumRefreshHistorySet.refreshRecords");
					success = false;
					rgr.u_state = "skipped";
					rgr.update();
				}
			}
			if(success){
				rgr.u_state = "processed";
				rgr.update();
			}
		}
	},
	
	validateExistingSubscribeConfigurations: function() {
		var gr = new GlideRecord('psp_replicate_conf');
		gr.addQuery('sync_direction', 'Subscribe');
		gr.addQuery('u_refresh_history_set', true);
		gr.query();
		
		while(gr.next()) {
			var dgr = new GlideRecord('sys_dictionary');
			dgr.addQuery('name', gr.table_name);
			dgr.addQuery('internal_type', 'Collection');
			dgr.addQuery('audit', false);
			dgr.query();
			
			if(dgr.next()) {
				gr.u_refresh_history_set = false;
				gr.update();
			}
		}
	},

    type: 'PerspectiumRefreshHistorySet'
};