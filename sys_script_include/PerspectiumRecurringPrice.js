var PerspectiumRecurringPrice = Class.create();
PerspectiumRecurringPrice.prototype = {
 	initialize: function() {
		
        this.logger = new PerspectiumLogger();
    },

    addRecurringPrices : function(source, target, pspTag /* optional */) {
        var targetId = target.sys_id;
		var targetTable = target.getTableName();
        this.logger.logDebug('Adding recurring price for: ' + targetTable + "," + targetId, "PerspectiumRecurringPrice.addRecurringPrices");

        if (source == null || source.u_recurring_prices.isNil()) {
            this.logger.logDebug('No recurring prices found', "PerspectiumRecurringPrice.addRecurringPrices");
            return;
        }

        var xmldoc = null;
        try {
             xmldoc = new XMLDocument(source.u_recurring_prices);
        } catch(err){
            this.logger.logDebug('Error creating XMLDocument for recurring prices: ' + err, "PerspectiumRecurringPrice.addRecurringPrices");
            return;
        }

        if (xmldoc == null) {
           this.logger.logDebug('XMLDocument is null', "PerspectiumRecurringPrice.addRecurringPrices");
           return;
        }

        var nodelist = xmldoc.getNodes("//recurring_price");
        if (nodelist == null) {
            this.logger.logDebug('No recurring price nodes found', "PerspectiumRecurringPrice.addRecurringPrices");
            return;
        }

        this.logger.logDebug('Found ' + nodelist.getLength() + ' recurring prices', "PerspectiumRecurringPrice.addRecurringPrices");
        for (var i=0; i < nodelist.getLength(); i++) {
            var nodeItem = nodelist.item(i);
            this.addRecurringPrice(nodeItem, targetTable, targetId, pspTag);
        }
    },
	
	_getValue : function(nodeItem, nodeName) {        
		var xml_util = (typeof GlideXMLUtil != 'undefined') ? new GlideXMLUtil() : new Packages.com.glide.util.XMLUtil();
		var it = xml_util.childElementIterator(nodeItem);
        while (it.hasNext()) {
            var el = it.next();
            var nn = el.getNodeName();
            var nodeValue = xml_util.getText(el);
			
			if( nodeValue == null)
				nodeValue = "";
			
			if(nn == nodeName) {
				return String(nodeValue);
			}
		}
											  
		return "";
	},
	
	_setValues : function(ct, targetId, nodeItem) {
		ct.request = targetId;	
		
		ct.recurring_frequency = this._getValue(nodeItem, "recurring_frequency");
		ct.recurring_price = this._getValue(nodeItem, "recurring_price_value");		
	},

    addRecurringPrice : function(nodeItem, targetTable, targetId, pspTag /* optional */) {
        var StringUtil = (typeof GlideStringUtil != 'undefined') ? new GlideStringUtil() : new Packages.com.glide.util.StringUtil();
				
		// first see if we can find item by sys_id
		var ct = new GlideRecord("sc_recurring_rollup");
		ct.addQuery("request", targetId);
		ct.query();
		if (!ct.next()) {
			ct.initialize();
			this._setValues(ct, targetId, nodeItem);
			ct.insert();
		} else {
			this._setValues(ct, targetId, nodeItem);
			ct.update();
		}
		
		// if pspTag exists, go and tag it
		if (pspTag) {
			var pspUtil = new PerspectiumUtil();
			pspUtil.addTag(ct, pspTag);
		}
    },

    type: 'PerspectiumRecurringPrice'
};