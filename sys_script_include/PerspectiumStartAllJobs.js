var PerspectiumStartAllJobs = Class.create();
var STATUS_SUCCESS = 200;
var STATUS_FAILURE = 500;
var status = { // JSON object to be returned
	code: -1, // status code
	errors: []
};

PerspectiumStartAllJobs.prototype = Object.extendsObject(AbstractAjaxProcessor,{
	
	startAllJobs: function() {
        var pspGR = new GlideRecord("sysauto");
        pspGR.addEncodedQuery("nameSTARTSWITHPerspectium");
		pspGR.query();
        
		while (pspGR.next()) {
            if (pspGR.getValue("active") == true) {
                gs.print("Observer already started !");
            } else {
                pspGR.active = "true";
                pspGR.update();
                gs.print(pspGR.name + " started");
            }
        }

        // activate Observer script actions if installed
        try {
            var pE = new GlideRecord("psp_event_subscription");
            pE.query();
            while (pE.next()) {
                pE.setForceUpdate(true);
                pE.update();
            }
        } catch (e) {
			status.errors.push("Observer script action failed: " + e);
        }

        var psp = new Perspectium();
        psp.updateStatus("status", "started");
        var key = gs.getProperty("instance_name", "unregistered");
        var str = '{"topic":"monitor", "type":"label", "key":"' + key + '", "name":"Perspectium^PSP", "value":"Scheduled Jobs Started"}';

        psp.postStats("input", str);

        this.synchDynShareRules();
		
		status.code = STATUS_SUCCESS;
		return JSON.stringify(status);
    },

    synchDynShareRules: function() {
        var psp = new PerspectiumReplicator();
        psp.synchDynShareRules();
    },

    type: 'PerspectiumStartAllJobs'
});