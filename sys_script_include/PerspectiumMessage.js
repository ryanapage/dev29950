var PerspectiumMessage = Class.create();
PerspectiumMessage.prototype = {
    initialize: function(topic, type, key, name, value, target_queue, extra, attributes, shareGR, set_id, state, checkOutboundMax, outboundTable, record_sys_id) {
		this.logger = new PerspectiumLogger();
		this.pspUtil = new PerspectiumUtil();
			
		if(topic && topic != null && topic != ""){
			this.topic = topic.toString();
		}
		
		if(type && type != null && type != ""){
			this.type = type.toString();
		}
		
		if(key && key != null && key != ""){
			this.key = key.toString();
		}
		
		if(name && name != null && name != ""){
			this.name = name.toString();
		}
		
		// we'll set value if passed in blank as some messages contain blank value
		if(value && value != null){
			this.value = value.toString();
		}
		else
			this.value = "";
		
		if (target_queue && target_queue != null && target_queue != ""){
			this.target_queue = target_queue;
		}
		
		if (extra && extra != null && extra != ""){
			this.extra = extra.toString();
		}

		// always initialize attributes map so this object isn't undefined
		this.attributesMap = {};
		
		if (attributes && attributes != null && attributes != ""){
			this.attributes = attributes;
			
			// store attributes into map to easily reference each value
			var attrs = attributes.split(",");
			for(var a in attrs){
				var attr = attrs[a];
				// name=value for each attribute
				var attrStrs = attr.split("=");
				if(attrStrs.length != 2)
					continue;
				
				// store the value of this attribute into the map using the attribute name as the map property name
				// if the property already exists such as psp_flag for first and last, we'll append it separating by "-"
				if (this.attributesMap[attrStrs[0]]) {
					this.attributesMap[attrStrs[0]] = this.attributesMap[attrStrs[0]] + "-" + attrStrs[1];
				} else {
					this.attributesMap[attrStrs[0]] = attrStrs[1]; 
				}
			}
		}
		
		if (set_id && set_id != null && set_id != ""){
			set_id = set_id.toString();
			this.set_id = set_id;	
			
			if (this.getAttribute("set_id") == null) {
				this.appendAttribute("set_id", set_id);
			}			
		}
		
		if (shareGR && shareGR != null && shareGR != ""){		
			this.shareGR = shareGR.toString();
		}
				
		if (state && state != null && state != "") {		
			this.state = state.toString();
		}
		else {
			this.state = "ready";
		}
		
		if (outboundTable && outboundTable != null && outboundTable != "") {
			this.outboundTable = outboundTable.toString();
		}
			
		// see if parameter to check outbound messages max passed in
		// check for undefined since a value of false will fail if(checkOutboundMax)
		if (typeof checkOutboundMax != 'undefined' && checkOutboundMax != null && checkOutboundMax != "") {
			this.checkOutboundMax = checkOutboundMax;
		}
		else {
			this.checkOutboundMax = true;
		}
		
		if (record_sys_id && record_sys_id != null && record_sys_id != ""){
			this.record_sys_id = record_sys_id.toString();
		}
				
    },
		
	setTopic: function(topic){
		this.topic = topic.toString();	
	},
	
	getTopic: function() {
		return this.topic;	
	},
	
	setType: function(type){
		this.type = type.toString();
	},
	
	getType: function() {
		return this.type;		
	},
	
	setKey: function(key){
		this.key = key.toString();
	},
	
	getKey: function() {
		return this.key;		
	},
	
	setName: function(name){
		this.name = name.toString();		
	},
	
	getName: function() {
		return this.name;	
	},
	
	setValue: function(value){
		this.value = value.toString();		
	},
	
	getValue: function() {
		return this.value;	
	},
	
	setTargetQueue: function(target_queue){
		this.target_queue = target_queue.toString();
	},

	setExtra: function(extra){
		this.extra = extra.toString();
	},
		
	getExtra: function() {
		return this.extra;	
	},
	
	setAttributes: function(attributes){
		this.attributes = attributes.toString();				
	},
	
	getAttributes: function() {
		return this.attributes;
	},
	
	setSetId: function(set_id){
		this.set_id = set_id.toString();	
	},
	
	setShareGr: function(shareGR){
		this.shareGR = shareGR.toString();
	},

	setState: function(state) {
		this.state = state.toString();	
	},
	
	setOutboundTable: function(outboundTable) {
		this.outboundTable = outboundTable;	
	},
	
	getAttribute: function(name){
		if(!this.attributesMap){
			return null;
		}
		
		if (this.attributesMap[name]) {
			return this.attributesMap[name];
		}
		
		return null;
	},
	
	setRecordSysId: function(record_sys_id){
		this.record_sys_id = record_sys_id.toString();
	},
	
	getRecordSysId: function(){
		return this.record_sys_id;
	},
	
	// to add an attribute to the attributes string
	appendAttribute: function(attrKey, attrVal){
		if(!this.attributes)
			this.attributes = "";
		
		if(this.attributes.length > 0)
			this.attributes += ",";
		
		this.attributes += attrKey + "=" + attrVal;
		// also store in attributes map for next time we check getAttribute()
		this.attributesMap[attrKey] = attrVal;
	},
		
	enqueue: function(){				
		// see if we should check for outbound messages max
		if (this.checkOutboundMax == "true" || this.checkOutboundMax == true) {			
			// if so we'll check psp setting in case user disabled
			var pspUtil = new PerspectiumUtil();
			var outboundEnableMax = pspUtil.getPspPropertyValue("com.perspectium.outbound.enable_max", "false");
			if (outboundEnableMax == "true" || outboundEnableMax == true) {		
				var count = new GlideAggregate("psp_out_message");
				count.addAggregate('COUNT');
				count.query();
				var totalOutboundMessages = 0;
				if (count.next()) {
					totalOutboundMessages = count.getAggregate('COUNT');
				}
		
				var outboundMaxMessages = parseInt(pspUtil.getPspPropertyValue("com.perspectium.outbound.max_messages", "1000000"));
				if (totalOutboundMessages >= outboundMaxMessages) {
					var maxErrorMessage = "Maximum number of messages (" + outboundMaxMessages + ") has been reached and no more messages will be placed into the outbound queue (skipped " + this.name + " " + this.topic + " message";
					if (this.shareGR) {
						var shareType = "bulk share ";
						if (this.shareGR.getTableName() == "psp_replicate_conf") {
							shareType = "dynamic share ";	
						}
						
						maxErrorMessage += " from " + shareType + this.shareGR.sys_id;
					}
				
					maxErrorMessage += ")";
					
					this.logger.logError(maxErrorMessage, "PerspectiumMessage.enqueue", this.shareGR);
					
					// try to delete sent messages so we have room for next time
					var grPOut = new GlideRecord("psp_out_message");
					pspQOut = grPOut.addQuery("state", "sent");
					grPOut.deleteMultiple(); // deletes sent records that are older than 15 minutes
					return;	
				}
			}
		}
				
		// create message and put into outbound table		
		var grPSPOut;
		if (this.outboundTable && this.outboundTable != null && this.outboundTable != ""
		   && this.outboundTable != "psp_out_message") {			
			var enableMax = this.pspUtil.getPspPropertyValue("com.perspectium." + this.outboundTable + ".enable_max", false);
			if (enableMax == true || enableMax == "true") {
				var recordLimit = this.pspUtil.getPspPropertyValue("com.perspectium." + this.outboundTable + ".record_limit", 8000);
				var limit = this.checkOutboundLimit(this.outboundTable, parseInt(recordLimit), this.name, this.topic);
				if (limit == false || limit == 'false')
					return;
			}
			
			grPSPOut = new GlideRecord(this.outboundTable);
			grPSPOut.initialize();
			// any new outbound table we create will have all u_ custom fields
			grPSPOut.setValue("u_topic", this.topic);
			grPSPOut.setValue("u_type", this.type);
			grPSPOut.setValue("u_key", this.key);
			grPSPOut.setValue("u_name", this.name);
			grPSPOut.setValue("u_value", this.value);
			grPSPOut.setValue("u_state", this.state);			
		}
		else {
			grPSPOut = new GlideRecord("psp_out_message");
			grPSPOut.initialize();
			grPSPOut.setValue("topic", this.topic);
			grPSPOut.setValue("type", this.type);
			grPSPOut.setValue("key", this.key);
			grPSPOut.setValue("name", this.name);
			grPSPOut.setValue("value", this.value);
			grPSPOut.setValue("state", this.state);
		}
		
		if(this.shareGR) {
			grPSPOut.setValue("u_source_table", this.shareGR.getTableName());
			grPSPOut.setValue("u_source", this.shareGR.sys_id);
			
			// if we didn't explicitly set sys_id we'll use the share config's sys_id
			if (this.getAttribute("set_id") == null) {
				this.appendAttribute("set_id", this.shareGR.sys_id);
			}
		}

        if(this.target_queue && this.target_queue != null && this.target_queue != "") {
        	grPSPOut.setValue("u_target_queue", this.target_queue);
        }
		
		if (this.extra && this.extra != null && this.extra != "") {
			grPSPOut.setValue("u_extra", this.extra);
		}
		
		if (this.attributes && this.attributes != null && this.attributes != "") {
			grPSPOut.setValue("u_attributes", this.attributes);
		}
		
		// set sequence based on epoch ms
		var currentMS = (new Date).getTime();
		grPSPOut.setValue("u_sequence", String(currentMS));
						
        var sysID = grPSPOut.insert();
		if(sysID == null || sysID == ''){
			var errMsg = "Error inserting outbound message with name: " + this.name;
			if (this.record_sys_id){
				errMsg += ", sys_id: " + this.record_sys_id;
			}
			errMsg += ", value: " + this.value;
			this.logger.logError(errMsg, "PerspectiumMessage.enqueue", this.shareGR);
		}
		
        return sysID;		
	},
	
	checkOutboundLimit: function(outboundTable, recordLimit, name, topic) {
		var count = new GlideAggregate(outboundTable);
		count.addAggregate('COUNT');
		count.query();
		var totalOutboundMessages = 0;
		if (count.next())
			totalOutboundMessages = count.getAggregate('COUNT');
		
		if (parseInt(totalOutboundMessages) < recordLimit) {				
			return true;
		}
			
		this.logger.logWarn("Maximum number of messages (" + recordLimit + ") has been reached and no more messages will be placed into the outbound queue (skipped " + name + " " + topic + " message)", "PerspectiumMessage.checkOutboundLimit");
		
		// try to delete sent messages so we have room for next time
		var grPOut = new GlideRecord(outboundTable);
		grPOut.addQuery("state", "sent");
		grPOut.addEncodedQuery("sys_created_on<=javascript:gs.minutesAgoStart(15)");
		grPOut.deleteMultiple(); // deletes sent records that are older than 15 minutes
		return false;	
	},
	
	encodeMessageJSON: function () {
		var json = new JSON();
		
		// if we didn't explicitly set sys_id we'll use the share config's sys_id			
		if(this.shareGR && this.shareGR != null && this.shareGR != "" && this.getAttribute("set_id") == null) {			
			this.appendAttribute("set_id", this.shareGR.sys_id);
		}
		
		var str = new Packages.java.lang.StringBuffer();
		
        str.append('{"topic":"' + this.topic + '",');
        str.append('"type":"' + this.type + '",');
        str.append('"key":"' + this.key + '",');
		
		// set psp_timestamp to be the current time this message is being encoded since it would be the time message is being sent out
		//var curDateTime = gs.nowDateTime();		
		var curDateTime = gs.nowNoTZ();
		
        str.append('"psp_timestamp":"' + curDateTime + '",');		
        str.append('"name":' + json.encode(String(this.name)) + ',');
				
		// message set monitor messages we'll json encode value since we're storing json in value field 
		str.append('"value":');
		if (this.topic == "monitor" && this.type == "message" || this.attributesMap['cipher'] == '0') {			
			str.append(json.encode(String(this.value)));
		}
		// replicator, etc. don't need to since those are base64 encoded
		else {
			str.append('"' + this.value + '"');
		}
				
		if (this.attributes && this.attributes != null && this.attributes != "") {
			str.append(',"attributes":' + json.encode(String(this.attributes)));
		}
		
		if (this.extra && this.extra != null && this.extra != "") {
        	str.append(',"extra":' + json.encode(String(this.extra)));
		}
		
		str.append('}');
				
		return str.toString();
    },
	
	
    type: 'PerspectiumMessage'
};