var PerspectiumTables = Class.create();
PerspectiumTables.prototype = Object.extendsObject(AbstractAjaxProcessor, {
	
	getShareTables: function() {
		var bulkShareArr = [];
		var bs = new GlideRecord('psp_bulk_share');
		bs.query();
		while (bs.next()) {
			bulkShareArr.push(bs.getValue('table_name')); 
			if ((bs.getValue('u_include_child_only') == 1) || (bs.getValue('u_include_child_tables') == 1)) {
				var table = new TableUtils(bs.getValue('table_name'));
				var extensions = table.getAllExtensions();
				bulkShareArr = (bulkShareArr.concat(extensions));
			} 
		}	
		var dynamicShareArr = [];
		var ds = new GlideRecord('psp_replicate_conf');
		ds.addQuery('sync_direction', 'Share');
		ds.query();
		while (ds.next()) {
			dynamicShareArr.push(ds.getValue('table_name')); 
			if (ds.getValue('u_share_base_table_only') == 0) {
				var dynamicTables = new TableUtils(ds.getValue('table_name'));
				var dynamicExtensions = dynamicTables.getAllExtensions();
				dynamicShareArr = (dynamicShareArr.concat(dynamicExtensions));
			}
		}	
		var finalArray = this.findDuplicates(dynamicShareArr.concat(bulkShareArr));
		var arrayUtil = new ArrayUtil();
		finalArray = arrayUtil.unique(finalArray);
		
		return finalArray.toString();
	},
	
	findDuplicates: function(tables) {
		var i;
		var length = tables.length;
		var result = [];
		var obj = {}; 
		for (i = 0; i < length ; i++) {
			obj[tables[i]] = 0;
		}
		for (i in obj) {
			result.push(i);
		}

		return result;
},
	
 type: 'PerspectiumTables'
	
});