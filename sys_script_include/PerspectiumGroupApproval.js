var PerspectiumGroupApproval = Class.create();
PerspectiumGroupApproval.prototype = {
 	initialize: function() {
        this.logger = new PerspectiumLogger();
    },

    addGroupApprovals : function(source, target, pspTag /* optional */) {
        var targetId = target.sys_id;
		var targetTable = target.getTableName();
        this.logger.logDebug('Adding group approval for: ' + targetTable + "," + targetId, "PerspectiumGroupApproval.addGroupApprovals");

        if (source == null || source.u_group_approvals.isNil()) {
            this.logger.logDebug('No group approval found', "PerspectiumGroupApproval.addApprovers");
            return;
        }

        var xmldoc = null;
        try {
             xmldoc = new XMLDocument(source.u_group_approvals);
        } catch(err){
            this.logger.logDebug('Error creating XMLDocument for group approval: ' + err, "PerspectiumGroupApproval.addGroupApprovals");
            return;
        }

        if (xmldoc == null) {
           this.logger.logDebug('XMLDocument is null', "PerspectiumGroupApproval.addGroupApprovals");
           return;
        }

        var nodelist = xmldoc.getNodes("//group_approval");
        if (nodelist == null) {
            this.logger.logDebug('No group approval nodes found', "PerspectiumGroupApproval.addGroupApprovals");
            return;
        }

        this.logger.logDebug('Found ' + nodelist.getLength() + ' group approval', "PerspectiumGroupApproval.addGroupApprovals");
        for (var i=0; i < nodelist.getLength(); i++) {
            var nodeItem = nodelist.item(i);
            this.addGroupApproval(nodeItem, targetTable, targetId, pspTag);
        }
    },
	
	_getValue : function(nodeItem, nodeName) {        
		var xml_util = (typeof GlideXMLUtil != 'undefined') ? new GlideXMLUtil() : new Packages.com.glide.util.XMLUtil();
		var it = xml_util.childElementIterator(nodeItem);
        while (it.hasNext()) {
            var el = it.next();
            var nn = el.getNodeName();
            var nodeValue = xml_util.getText(el);
			
			if( nodeValue == null)
				nodeValue = "";
			
			if(nn == nodeName) {
				return nodeValue;
			}
		}
											  
		return "";
	},
	
	_setValues : function(ct, targetId, nodeItem) {
		ct.parent = targetId;
		ct.approval = this._getValue(nodeItem, "approval");
		ct.approval_user = this._getValue(nodeItem, "approval_user");		
		ct.assignment_group = this._getValue(nodeItem, "assignment_group");
		ct.short_description = this._getValue(nodeItem, "short_description");
	},
	
    addGroupApproval : function(nodeItem, targetTable, targetId, pspTag /* optional */) {
        var StringUtil = (typeof GlideStringUtil != 'undefined') ? new GlideStringUtil() : new Packages.com.glide.util.StringUtil();
		
		var approval = this._getValue(nodeItem, "approval");		
		if (approval == null)
			return; // no value, cannot search
		
		// first see if we can find sysapproval_approver by approver and task
		var sa = new GlideRecord("sysapproval_group");
		sa.addQuery("parent", targetId);
		sa.addQuery("approval", approval);
		sa.query();
		if (!sa.next()) {
			sa.initialize();
			this._setValues(sa, targetId, nodeItem);
			sa.insert();
		} else {
			this._setValues(sa, targetId, nodeItem);
			sa.update();
		}
		
		// if pspTag exists, go and tag it
		if (pspTag) {
			var pspUtil = new PerspectiumUtil();
			pspUtil.addTag(sa, pspTag);
		}
    },
	
    type: 'PerspectiumGroupApproval'
};