var Perspectium = Class.create();

Perspectium.prototype = {
    initialize: function () {	
		this.mappedTable = {};
		this.recordSysId = '';
		this.recordTargetTable = '';
		this.instance_name = gs.getProperty("instance_name", "unregistered");
		this.logger = new PerspectiumLogger();
		this.pspUtil = new PerspectiumUtil();
		this.maxBytes = parseInt(this.pspUtil.getPspPropertyValue("com.perspectium.output_bytes_limit", "50000000")); // max bytes to cap the send string limit of 16MB for Java String
		this.qUser = this.pspUtil.getPspPropertyValue("com.perspectium.quser","");
		if (this.qUser == "${server_generated}") {
			//this.qUser = gs.getProperty("instance_name", "guest");
			this.qUser = gs.getProperty("instance_name", "");
		}
		
		this.qPassword = this.pspUtil.getPspPropertyValue("com.perspectium.qpassword","");
		if (this.qPassword == "${server_generated}") {
			//this.qPassword = gs.getProperty("instance_name", "");
			this.qPassword = gs.getProperty("instance_name", "guest");
		}
		
		// force to JavaScript string so endsWith() can work properly
		this.qInputURL = String(this.pspUtil.getPspPropertyValue("com.perspectium.snc_input",""));
		if (this.qInputURL == "${server_generated}") {
			//var i = gs.getProperty("instance_name", "unregistered");
			//this.qInputURL = "https://" + i + ".perspectium.net/";
			this.qInputURL = "";
		}

		if (this.qInputURL != "" && !this.endsWith(this.qInputURL, "/")) {
			this.qInputURL += "/";
		}
							
		this.defaultInputQueue = this.pspUtil.getPspPropertyValue("com.perspectium.input_queue","psp.in.servicenow");
		this.qPassphrase = this.pspUtil.getPspPropertyValue("com.perspectium.qpassphrase", "");		
		this.proxyHost = this.pspUtil.getPspPropertyValue("com.perspectium.proxy_host", "unset");
		this.proxyPort = this.pspUtil.getPspPropertyValue("com.perspectium.proxy_port", "8080");		
		this.single_output_processor = this.pspUtil.getPspPropertyValue("com.perspectium.single_output_processor", "true");
		this.rowLimitStr = this.pspUtil.getPspPropertyValue("com.perspectium.output_row_limit", "4000");
		this.messageMinutesPast = this.pspUtil.getPspPropertyValue("com.perspectium.outbound.minutes.past", 10);
		
		this.outboundEnableMax = this.pspUtil.getPspPropertyValue("com.perspectium.outbound.enable_max", "false");
		this.outboundMaxMessages = parseInt(this.pspUtil.getPspPropertyValue("com.perspectium.outbound.max_messages", "1000000"));
		this.skipSysDomainPath = this.pspUtil.getPspPropertyValue("com.perspectium.replicator.skip_sys_domain_path", "false");
		this.debug = this.pspUtil.getPspPropertyValue("com.perspectium.debug", "false");
		this.performanceLogging = this.pspUtil.getPspPropertyValue("com.perspectium.performance_logging", "false");
		this.messageStatsMod = "Sharing";
		this.encryptionMode = this.pspUtil.getPspPropertyValue("com.perspectium.replicator.encryption_mode", "encrypted");
		
		this.pspRHS = new PerspectiumRefreshHistorySet();
		
		this.glideVersion = gs.getProperty('glide.war').substring(0, 7);
    },
    
    getStats : function(type) {
		var glide_url = gs.getProperty("glide.servlet.uri");
		
		if (!this.endsWith(glide_url, "/")) {
			glide_url += "/";
		}
		
        return this.getStatsDirect(glide_url + type);
    },
    
    getStatsDirect : function(url) {
        var getMethod = new Packages.org.apache.commons.httpclient.methods.GetMethod(url);
        var httpClient = this.getHttpClient();
        
        var result = httpClient.executeMethod(getMethod);
        if (result != 200) {
			getMethod.releaseConnection();
            throw new Error("HTTP GET failed: " + result + " for URL: " + url);
		}
        
        var rstr = getMethod.getResponseBodyAsString();
		getMethod.releaseConnection();
		
		return rstr;
    },
	
	getQUser : function() {
		return this.qUser;
	},
	
	getQPassword : function() {
		return this.qPassword;
	},
	
	getQInputURL : function() {
		return this.qInputURL;
	},
    
    postStats : function(type, str, instance_name, cluster_names) {
		if (this.getQUser() == "" || this.getQPassword() == "" || this.getQInputURL() == "") {
			this.logger.logError("Unable to run as the target server or credentials are not configured properly. Please check your Perspectium properties", "Perspectium.postStats");
			return;	
		}
		
		var snc_input = this.getQInputURL();
		if (!this.endsWith(snc_input, "/")) {
			snc_input += "/";
		}
        var postMethod = new Packages.org.apache.commons.httpclient.methods.PostMethod(snc_input + type);
        
        postMethod.setRequestHeader("psp_quser", this.getQUser());
        postMethod.setRequestHeader("psp_qpassword", this.getQPassword());
        postMethod.setRequestHeader("psp_input_queue", this.defaultInputQueue);
        
        if (instance_name == undefined) {
            postMethod.setRequestHeader("psp_instance", gs.getProperty("instance_name", "unregistered"));
        } else {
            postMethod.setRequestHeader("psp_instance", instance_name);
        }
        
        if (cluster_names != undefined) {
            postMethod.setRequestHeader("psp_cluster", cluster_names);
        }
        
        var httpClient = this.getHttpClient();
        postMethod.addRequestHeader("Content-Type", "text/html");
        postMethod.setRequestEntity(new Packages.org.apache.commons.httpclient.methods.StringRequestEntity(str));
        
        var result = httpClient.executeMethod(postMethod);
        if (result != 200) {
			var str = "HTTP POST failed: " + result;
			this.logger.logWarn(str);
            throw new Error(str);
		}
		postMethod.releaseConnection();
        
        return result;
    },
    
    updateStatus: function (type, value) {
        var name = "com.perspectium." + type;
        var pspGR = new GlideRecord("sys_status");
        pspGR.addQuery("name", name);
        pspGR.query();
        if (pspGR.next()) {
            pspGR.name = name;
            pspGR.value = value;
            pspGR.update();
        } else {
            pspGR.initialize();
            pspGR.name = name;
            pspGR.value = value;
            pspGR.insert();
        }
    },
    
    encodeGlideRecordJSON: function (grTemp, bOmitEmptyFields) {
        var json = new JSON();
        var grA = new GlideRecord(grTemp.getTableName());
        grA.newRecord();
        var fields = new GlideRecordUtil().getFields(grA);
        
        var i=0;
        var o = {};
        while(fields[i]) {
			columnName = fields[i];
			if(bOmitEmptyFields == true){
				if(grTemp.getValue(columnName) === "" || grTemp.getValue(columnName) === null){
					i++;
					continue;
				}
			}
            o[columnName] = grTemp.getValue(columnName);
            i++;
        }
        var enc = json.encode(o);
        return enc;
    },
    
    encodePSPOutJSON: function (grPSPOut) {		
		var json = new JSON();
		var str = new Packages.java.lang.StringBuffer();
		var pspM = new PerspectiumMessage(grPSPOut.topic.toString(), grPSPOut.type.toString(), grPSPOut.key.toString(), grPSPOut.name.toString(), grPSPOut.value.toString(), "", "", grPSPOut.u_attributes.toString(), "", "", "", false, "", "");
		
        str.append('{"topic":"' + grPSPOut.topic.toString() + '",');
        str.append('"type":"' + grPSPOut.type.toString() + '",');
        str.append('"key":"' + grPSPOut.key.toString() + '",');
        str.append('"psp_timestamp":"' + grPSPOut.sys_created_on.toString() + '",');
        str.append('"name":' + json.encode(grPSPOut.name.toString()) + ',');
		
		// message set monitor messages we'll json encode value since we're storing json in value field 
		str.append('"value":');
		if ((grPSPOut.topic == "monitor" && grPSPOut.type == "message") || (pspM.attributesMap['cipher'] != undefined && pspM.attributesMap['cipher'] == 0)) {
			str.append(json.encode(grPSPOut.value.toString()));
		}
		// replicator, etc. don't need to since those are base64 encoded
		else {
			str.append('"' + grPSPOut.value.toString() + '"');
		}
		
		str.append(',');		
		str.append('"attributes":' + json.encode(grPSPOut.u_attributes.toString()) + ',');
		str.append('"extra":' + json.encode(grPSPOut.u_extra.toString()) + '}');
                
		return str.toString();
    },
    
    postMulti: function(arrOut, target_queue) {
		if (this.getQUser() == "" || this.getQPassword() == "" || this.getQInputURL() == "") {
			this.logger.logError("Unable to run as the target server or credentials are not configured properly. Please check your Perspectium properties", "Perspectium.postMulti");
			return;	
		}
        var snc_input = this.getQInputURL();
        var quser = this.getQUser();
        var qpassword = this.getQPassword();
		var qpassphrase = this.qPassphrase;
        var input_queue = this.defaultInputQueue;
        var instance = gs.getProperty("instance_name", "unregistered");

        if (target_queue && target_queue != "default" && target_queue != "") {
           var qgr = new GlideRecord("u_psp_queues");
           if (qgr.get(target_queue)) {
               var encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();

               snc_input = qgr.u_endpoint_url.toString();
               quser = qgr.u_queue_user;
               qpassword = encrypter.decrypt(qgr.u_queue_password);
               input_queue = qgr.u_name;
               //instance = qgr.u_instance;
           }
        }
		
		if (!this.endsWith(snc_input, "/")) {
			snc_input += "/";
		}

        this.logger.logDebug("posting to: " + snc_input + ", " + input_queue + " (" + arrOut.length() + " bytes)", "Perspectium.postMulti");

        var postMethod = new Packages.org.apache.commons.httpclient.methods.PostMethod(snc_input + "multiinput");
        postMethod.setRequestHeader("psp_quser", quser);
        postMethod.setRequestHeader("psp_qpassword", qpassword);
        postMethod.setRequestHeader("passphrase", qpassphrase);
        postMethod.setRequestHeader("psp_input_queue", input_queue);
        postMethod.setRequestHeader("psp_instance", instance);
		var httpClient = this.getHttpClient();				
        postMethod.addRequestHeader("Content-Type", "text/json");
        postMethod.setRequestEntity(new Packages.org.apache.commons.httpclient.methods.StringRequestEntity(arrOut));
        var result = httpClient.executeMethod(postMethod);
		postMethod.releaseConnection();
        return result;
    },
	
	postOne: function(postBody, target_queue, bSynchronous, queueName) {
		if (this.getQUser() == "" || this.getQPassword() == "" || this.getQInputURL() == "") {
			this.logger.logError("Unable to run as the target server or credentials are not configured properly. Please check your Perspectium properties", "Perspectium.postOne");
			return;	
		}
		
        var snc_input = this.getQInputURL();
        var quser = this.getQUser();
        var qpassword = this.getQPassword();
		var qpassphrase = this.qPassphrase;
        var input_queue = this.defaultInputQueue;
        var instance = gs.getProperty("instance_name", "unregistered");

        if (target_queue && target_queue != "default" && target_queue != "") {
           var qgr = new GlideRecord("u_psp_queues");
           if (qgr.get(target_queue)) {
               var encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();

               snc_input = qgr.u_endpoint_url.toString();
               quser = qgr.u_queue_user;
               qpassword = encrypter.decrypt(qgr.u_queue_password);
               input_queue = qgr.u_name;
               //instance = qgr.u_instance;
           }
        }
		
		if (!this.endsWith(snc_input, "/")) {
			snc_input += "/";
		}

		// if a queue name was passed in we used that regardless of the default queue or target queue
		// such as when sending messageset messages so they don't get routed to the target queue
		if(queueName && queueName != null && queueName != ""){
			input_queue = queueName;
		}
				
        //this.logger.logDebug("posting to: " + snc_input + ", " + input_queue + " (" + postBody.length + " bytes)", "Perspectium.postOne");
		
        var postMethod = new Packages.org.apache.commons.httpclient.methods.PostMethod(snc_input + "input");
        postMethod.setRequestHeader("psp_quser", quser);
        postMethod.setRequestHeader("psp_qpassword", qpassword);
        postMethod.setRequestHeader("passphrase", qpassphrase);
        postMethod.setRequestHeader("psp_input_queue", input_queue);
        postMethod.setRequestHeader("psp_instance", instance);
		var httpClient = this.getHttpClient();				
        postMethod.addRequestHeader("Content-Type", "text/json");
		if(bSynchronous){
			this.logger.logDebug("bSynchronous: " + bSynchronous, "Perspectium.postOne");
			postMethod.setRequestHeader("synchronous", "true");
		}
        postMethod.setRequestEntity(new Packages.org.apache.commons.httpclient.methods.StringRequestEntity(postBody));
        var result = httpClient.executeMethod(postMethod);
		postMethod.releaseConnection();
        return result;
    },
	
	getHttpClient: function() {		
		var httpClient;
		if (typeof GlideHTTPClient != 'undefined') {
			httpClient = new GlideHTTPClient();
			
			httpClient.setHttpTimeout(600000);
			
			if (this.proxyHost != "unset") {
				httpClient.setupProxy(this.proxyHost, this.proxyPort);
			}
		} else {
			httpClient = new Packages.org.apache.commons.httpclient.HttpClient();
			
			if (this.proxyHost != "unset") {
				httpClient.getHostConfiguration().setProxy(this.proxyHost, this.proxyPort);
			}			
		}
						
		return httpClient;
	},

    processMultiOutput: function(encodedQuery) {
        // do the targetted queues first, in order
        var qgr = new GlideRecord("u_psp_queues");
        qgr.addQuery("u_active", "true");
        qgr.addQuery("u_direction", "share");
        qgr.orderBy("u_order");
        qgr.query();
        while (qgr.next()) {
			try {
				this.processMultiTargetedOutput(qgr.sys_id, encodedQuery);
			}
			catch(e) {
				this.logger.logError("Error processing shared queue " + qgr.u_name + " on " + qgr.u_endpoint_url + ": " + e, "Perspectium.processMultiOutput");
			}
        }
		
		// only share out messages from default queue if default is not disabled
		var gr = new GlideRecord("u_psp_properties");
		gr.addQuery("u_name", "com.perspectium.outbound.default_disabled");
		gr.query();
		gr.next();
		
		if (gr.u_value.toString() === "false") { 
			try {
				this.processMultiTargetedOutput("default", encodedQuery);
			}
			catch(e) {
				this.logger.logError("Error processing " + this.getQInputURL() + ": " + e, "Perspectium.processMultiOutput");
			}
		}
    },
    
    processMultiTargetedOutput: function(target_queue, encodedQuery) {
		var single_output_processor = this.single_output_processor;
		
		if ("true" != single_output_processor) {
			var lock = new GlideMutex.get("processMultiTargetedOutput");
			while (lock.toString()+'' == "undefined") {
   			this.logger.logDebug("waiting for lock: processMultiTargetedOutput (" + target_queue + ")", "Perspectium.processMultiTargetedOutput");
   			gs.sleep(1000); // sleep for 1000 milliseconds
   			lock = new GlideMutex.get("Perspectium Post Out");
			}
   		 this.logger.logDebug("lock acquired: processMultiTargetedOutput (" + target_queue + ")", "Perspectium.processMultiTargetedOutput");
		}
		
        var grPSPOut = new GlideRecord("psp_out_message");
        grPSPOut.addQuery("state", "ready");
        if (target_queue == "default") {
          grPSPOut.addNullQuery("u_target_queue");
        } else if (target_queue) {
          grPSPOut.addQuery("u_target_queue", target_queue);
        }

		if(encodedQuery && encodedQuery != null && encodedQuery != ""){
		   grPSPOut.addEncodedQuery(encodedQuery);
		}
		
		var rowLimitStr = this.rowLimitStr;
		
		var sdt = new Date();
        grPSPOut.setLimit(parseInt(rowLimitStr));
        grPSPOut.orderBy("sys_created_on");
		grPSPOut.orderBy("u_sequence");
        grPSPOut.query();
				
        var objJSON = new JSON();
        var arrID = [];
        var arrOut = "";
        var arrJSON = [];
		var tBytes = 0;
		var recTypes = [];
		var recordCount = {}; 
		var numRecords = 0;
		
		// clear out messageset messages to start clean each time we process multioutput for a queue
		this.messageSetMessages = {};
			
        while(grPSPOut.next()) {
			// first record we'll check if it's older than our past minutes setting and alert if necessary
			if(tBytes == 0){
				this.checkMessagePastLimit(grPSPOut);
			}
				
			if ("true" != single_output_processor) {
				// dont change to processing if only 1 output processor job
	    		grPSPOut.state = "processing";
	    		grPSPOut.update();
			}
			
			var grKey = grPSPOut.key;
	    	if (grKey != this.instance_name && grKey.indexOf(this.instance_name + ".") != 0) {
				// may have been cloned, so skip
				grPSPOut.state = "skipped";
				grPSPOut.u_state_info = "Message skipped due to mismatch between instance and key of message";
				grPSPOut.update();
				
				this.logger.logDebug("skipping mismatched key: " + grPSPOut.key, "Perspectium.processMultiTargetedOutput");
				continue;
	    	}
			
			if (grPSPOut.name.contains("Perspectium^PSP"))
				recTypes.push("monitor");
			else if (recTypes.indexOf(grPSPOut.name.toString()) === -1)
				recTypes.push(grPSPOut.name.toString());
			
			try {
				var jj = this.encodePSPOutJSON(grPSPOut);
				var llength = jj.length();

				// if 1 record already is over big, mark it as error and move on
				if (llength > this.maxBytes) {
				    // mark it as error
					var estr = "outbound record (" + grPSPOut.sys_id.toString() + ") of length " + llength + " is more than " + this.maxBytes;
					this.logger.logError(estr, "Perspectium.processMultiTargetedOutput");
					//grPSPOut.state = "error";
					grPSPOut.u_state_info = estr;
					grPSPOut.update();
					
					// for errors we'll call mark function so we can update messageset
					var eArrID = [];
					eArrID.push(grPSPOut.sys_id.toString());
					this.markPSPOut(eArrID, "error");
					
					continue;
				}

				tBytes += llength;
				if (tBytes > this.maxBytes) {
					this.logger.logDebug("maximum " + this.maxBytes + " bytes reached (" + tBytes + ") at " + arrJSON.length + " records, backing up to " + (tBytes - jj.length()) + ", breaking", "Perspectium.processMultiTargetedOutput");
					break;
				}
				
				// mark what table the record came from
				if (grPSPOut.topic.toString() == "replicator") {
					var tname = grPSPOut.name.toString().split(".")[0];
					numRecords += 1;
					
					if (recordCount[tname]) {
						var tmp = recordCount[tname];
						recordCount[tname] = tmp+1;
					} else {
						recordCount[tname] = 1;
					}
				}				
				
				arrJSON.push(jj);
				arrID.push(grPSPOut.sys_id.toString());
			} catch (e) {
				this.logger.logError("unable to process " + grPSPOut.sys_id.toString() + " " + e, "Perspectium.processMultiTargetedOutput");
	    		//grPSPOut.state = "error";
				grPSPOut.u_state_info = e + "";
	    		grPSPOut.update();
				
				// for errors we'll call mark function so we can update messageset
				var eArrID = [];
				eArrID.push(grPSPOut.sys_id.toString());
				this.markPSPOut(eArrID, "error");

				
			}
        }
		
		if ("true" != single_output_processor) {
			lock.release();
   		 this.logger.logDebug("lock released: processMultiTargetedOutput (" + target_queue + ")", "Perspectium.processMultiTargetedOutput");
		}
		
		if (arrJSON.length == 0) {
			//this.logger.logDebug("nothing to send", "Perspectium.processMultiTargetedOutput");
			return 200;
		}
		
		var csecs = this._getTimeDiffSecs(sdt);
		var rsecs = arrJSON.length/csecs;
				
        this.logger.logDebug(arrJSON.length + " records (" + target_queue + ") COLLECTED in " + csecs + " secs (" + rsecs.toFixed(2) + " recs/s)", "Perspectium.processMultiTargetedOutput");
		this.addMessageStats(arrJSON.length + " outbound records COLLECTED and ready to be sent to " + target_queue, csecs, this.messageStatsMod, recTypes.toString(), "COLLECTED", numRecords);
        
		sdt = new Date();
		
		var sbuf = new Packages.java.lang.StringBuffer();
		
		sbuf.append("[");
		sbuf.append(arrJSON.join(","));
		sbuf.append("]");
		arrOut = sbuf.toString();
		
		csecs = this._getTimeDiffSecs(sdt);
		rsecs = arrJSON.length/csecs;
		
		var seconds = parseFloat(csecs);
		
		var profiling = '\n';
		var firstTime = true;
		for (var table in recordCount) {
			profiling += table + " = " + String(recordCount[table]) + '\n';
		}		
		
        this.logger.logDebug(arrJSON.length + " records (" + target_queue + ") ENCODED in " + csecs + " secs (" + rsecs.toFixed(2) + " recs/s)", "Perspectium.processMultiTargetedOutput");
		this.addMessageStats(arrJSON.length + " outbound records ENCODED and ready to be sent to " + target_queue + ". " + profiling, csecs, this.messageStatsMod, recTypes.toString(), "ENCODED", numRecords);

		sdt = new Date();
        var result = this.postMulti(arrOut, target_queue);
		csecs = this._getTimeDiffSecs(sdt);
		rsecs = arrJSON.length/csecs;
		seconds = parseFloat(csecs);
		
        this.logger.logDebug(arrJSON.length + " records (" + target_queue + ") POSTED in " + csecs + " secs (" + rsecs.toFixed(2) + " recs/s)", "Perspectium.processMultiTargetedOutput");
		this.addMessageStats(arrJSON.length + " outbound records POSTED to " + target_queue + ". " + profiling, csecs, this.messageStatsMod, recTypes.toString(), "POSTED", numRecords);
		
        if (result != 200) {
			if ("true" != single_output_processor) {
				// mark only if records were changed to processing
	    		this.markPSPOut(arrID, "ready");
			}
			
			if (result == 401) {
				// if no target queue specified, sets the default disabled property to true.
				if (target_queue == "default") { 	
					var grPSPProp = new GlideRecord("u_psp_properties");
					grPSPProp.addQuery("u_name", "com.perspectium.outbound.default_disabled");
					grPSPProp.query(); 
					if (grPSPProp.next()) { 
						grPSPProp.setValue("u_value", "true");
						grPSPProp.update();		
					}
				} else {
					// gets the gr of the target queue, and sets it to inactive
					var grPSPQueues = new GlideRecord("u_psp_queues");
					grPSPQueues.addQuery("sys_id", target_queue); 
					grPSPQueues.query(); 
					if (grPSPQueues.next()) { 
						grPSPQueues.setValue("u_active", "false");		
						grPSPQueues.update();
					}
				}
				
				this.logger.logDebug("Unable to process outbound messages. Please check credentials.");
			}			
			
            throw new Error("HTTP POST failed: " + result);
        } else {
			sdt = new Date();			
			
	    	this.markPSPOut(arrID, "sent");
			csecs = this._getTimeDiffSecs(sdt);
			rsecs = arrID.length/csecs;
			seconds = parseFloat(csecs);
			
        	this.logger.logDebug(arrJSON.length + " records (" + target_queue + ") UPDATED to 'sent' in " + csecs + " secs (" + rsecs.toFixed(2) + " recs/s)", "Perspectium.processMultiTargetedOutput");
			this.addMessageStats(arrJSON.length + " outbound records UPDATED and sent to " + target_queue + ". " + profiling, csecs, this.messageStatsMod, recTypes.toString(), "UPDATED", numRecords);
        }
		
		// send out messageset messages for all replicator messages processed in this job
		// by going through each messageset message and create outbound message for it
		var instanceKey = gs.getProperty("instance_name", "unregistered");

		for(var m in this.messageSetMessages){				
			// create JSON string that contains all message set info which will be stored into value field of outbound message
			var msgJson = this.messageSetMessages[m].encodeMessageJSON(instanceKey, "snc-shared");
			if(!msgJson || msgJson == null || msgJson == "")
				continue;
			
			var pm = new PerspectiumMessage("monitor", "message", instanceKey, instanceKey, msgJson, "", "", "", "", m);			
			pm.enqueue();		
		}
		
		return result;
    },
	
	addMessageStats: function (name, value, mod, recs, action, records) {
		// only add the records if debug property is true
		if (this.performanceLogging == false || this.performanceLogging == "false")
			return;
		
		// skip 0 records
		if (name.startsWith("0 record"))
			return;

		var gr = new GlideRecord("u_psp_performance_stats");
		gr.u_name = name;
		gr.u_value = value;
		gr.u_module = mod;
		gr.u_record_types = recs;
		gr.u_action = action;
		gr.u_records = records;
		gr.u_duration = value;

		gr.insert();
	},
	
	checkMessagePastLimit: function(grPSPOut){
		// get current datetime and substract our minutes past setting from it
		var cgdt = new GlideDateTime();
		var gt = new GlideTime();
		var minutesPast = this.messageMinutesPast;
		if(isNaN(minutesPast)){
			this.logger.logError("com.perspectium.outbound.minutes.past system property " + minutesPast + " is not a number", "Perspectium.checkMessagePastLimit");
			return;	
		}
		
		var hoursPast = this.leftPad(Math.floor(Math.abs(minutesPast) / 60));  
		var minPast = this.leftPad(Math.abs(minutesPast) % 60);  
		
		gt.setValue(hoursPast + ":" + minPast + ":00");
		cgdt.subtract(gt);

		// check if we have sent a message already
		// we'll use the minutes past setting so if property dateime is newer than it we won't log error message again
		var lastPastSent = this.pspUtil.getPspPropertyValue("com.perspectium.outbound.last_past_sent_time");
		var loggedPrevError = false;
		// compare if the last sent is greater than our time check
		if(lastPastSent != null){			
			var lgdt = new GlideDateTime(lastPastSent);
			// last sent time is greater than our past setting time so we won't log error again since we already logged it
			if(lgdt.compareTo(cgdt) > 0){
				loggedPrevError = true;   
			}
		}
			
		if(!loggedPrevError){
			var pgdt = new GlideDateTime(grPSPOut.sys_created_on);	
							   
			// compare past setting time with message's datetime and if message's datetime is older we'll log error
			if(cgdt.compareTo(pgdt) > 0){
				this.logger.logError("Trying to send outbound message " + grPSPOut.sys_id + " that was created more than " + minutesPast + " minutes ago", "Perspectium.checkMessagePastLimit");

				// set datetime setting to now GMT that we just sent out message
				// use GMT since var lgdt = new GlideDateTime(lastPastSent) above is in GMT
				this.pspUtil.setPspPropertyValue("com.perspectium.outbound.last_past_sent_time", gs.nowNoTZ());
			}	
			
		}
	},
	
	_getTimeDiffSecs: function(startTime) {
		var endTime = new Date();
		var csecs = (endTime.getTime() - startTime.getTime()) / 1000;
		if (csecs <= 0) {
			csecs = 0.0001;
		}
		
		return csecs;
	},
	
    markPSPOut: function (arrID, state) {
        var grPSPOut = new GlideRecord("psp_out_message");
        grPSPOut.addQuery("sys_id", "IN", arrID.toString());
		
		// Delete the records immediately instead of marking them sent
		var deleteImmediately = this.pspUtil.getPspPropertyValue("com.perspectium.outbound.delete_immediately", "false");
		if((deleteImmediately == true || deleteImmediately == "true") && state == "sent"){
			grPSPOut.deleteMultiple();
			return;
		}
		
        grPSPOut.query();
        while(grPSPOut.next()) {
            grPSPOut.state = state;
            grPSPOut.setWorkflow(false);
            grPSPOut.update();
			
			var processMessageSet = this.pspUtil.getPspPropertyValue("com.perspectium.outbound.process_message_set", "false");						
			if (grPSPOut.topic != "replicator" || processMessageSet == "false" || processMessageSet == false){
				continue;	
			}
			
			// for each replicator message we'll update messageset for it
			// first create a message object to be able to get each attribute in message's attributes string
			var pspMsg = new PerspectiumMessage(grPSPOut.topic, grPSPOut.type, grPSPOut.key, grPSPOut.name, grPSPOut.value, "", grPSPOut.u_extra, grPSPOut.u_attributes, "", "");
			var messageSetId = pspMsg.getAttribute("set_id");
			if(messageSetId == null){
				continue;	
			}
			
			// create messageset message for this set_id if it doesn't exist
			if(!this.messageSetMessages[messageSetId]){
				this.messageSetMessages[messageSetId] = new PerspectiumMessageSetMessage(messageSetId);
			}

			// increment count based on state of message
			if(state == "sent"){
				this.messageSetMessages[messageSetId].incSuccess();
			}
			else if(state == "error"){
				this.messageSetMessages[messageSetId].incFailure();	
			}

			var messagePspFlag = pspMsg.getAttribute("psp_flag");
			if(messagePspFlag == null){
				continue;	
			}
			
			// update if we received first or last message
			if(messagePspFlag.indexOf("first") >= 0){
				//this.messageSetMessages[messageSetId].setStarted(gs.nowNoTZ());
				// so we can have accurate time messages were put in outbound in ready state to when they were actually sent
				// we'll use the datetime the message was created in outbound as our starting time for when this message set starting sending out				
				this.messageSetMessages[messageSetId].setStarted(grPSPOut.sys_created_on);	
				this.messageSetMessages[messageSetId].setFirst(true);				
			}
			
			if(messagePspFlag.indexOf("last") >= 0){
				this.messageSetMessages[messageSetId].setFinished(gs.nowNoTZ());		
				this.messageSetMessages[messageSetId].setLast(true);
			}
			
        }
    },
    
    createPSPOut: function(topic, type, key, name, value, target_queue, extra, attributes, shareGR, state, record_sys_id) {
		// check if setting enabled where we check if we reach max # of messages to put in outbound
		if (this.outboundEnableMax == "true" || this.outboundEnableMax == true) {		
			var count = new GlideAggregate("psp_out_message");
			count.addAggregate('COUNT');
			count.query();
			var totalOutboundMessages = 0;
			if (count.next()) {
				totalOutboundMessages = count.getAggregate('COUNT');
			}
			
			if (totalOutboundMessages >= this.outboundMaxMessages) {				
				var maxErrorMessage = "Maximum number of messages (" + this.outboundMaxMessages + ") has been reached and no more messages will be placed into the outbound queue (skipped " + name + " " + topic + " message";
				if (shareGR) {
					var shareType = "bulk share ";
					if (shareGR.getTableName() == "psp_replicate_conf") {
						shareType = "dynamic share ";	
					}
					
					maxErrorMessage += " from " + shareType + shareGR.sys_id;
				}
			
				maxErrorMessage += ")";
				this.logger.logError(maxErrorMessage, "Perspectium.createPSPOut");
				
				// try to delete sent messages so we have room for next time
				var grPOut = new GlideRecord("psp_out_message");
				pspQOut = grPOut.addQuery("state", "sent");
				grPOut.deleteMultiple(); // deletes sent records that are older than 15 minutes

				return;	
			}
		}
				
		var pspM = new PerspectiumMessage(topic, type, key, name, value, target_queue, extra, attributes, shareGR, "", state, false, "", record_sys_id);
		return pspM.enqueue();
    },
	
	isSubscribed: function(tableName, action, cgr) {
		
		// try querying with just table name first to get the table's subscribe config
		// in cases where both a child table and a base table have configs
		// so we don't return the base table config
		var qc = this.getSubscribeConfiguration(tableName, action, true, cgr);
		if (qc != null)
			return qc;
				
		// check for global subscribe next
		qc = this.getSubscribeConfiguration("global", action, true, cgr);
		if (qc != null)
			return qc;
		
		// no table config exists so try checking for base table
		return this.getSubscribeConfiguration(tableName, action, false, cgr); 	
	},
	
    getSubscribeConfiguration: function(tableName, action, checkTableOnly, cgr) {
        // if global subscribe on and we have a sys_history_line record we don't want to subscribe to it as refreshHistorySet() is meant to handle refreshing
		this.logger.logDebug("getSubscribeConfiguration for " + tableName);
		var arrTableType = cgr.getValue("name").split(".");
    	var cgrTableName = arrTableType[0];
		if(tableName == "global" && cgrTableName == 'sys_history_line')
			return null;
				
		var tUtils = new TableUtils(tableName);
        var tables = tUtils.getTables();
        
        var action_str = "unknown";
        if (action == "insert")
            action_str = "action_create";
        
        if (action == "update")
            action_str = "action_update";
        
        if (action == "delete")
            action_str = "action_delete";
        
        pGr = new GlideRecord("psp_replicate_conf");
        pGr.addQuery("sync_direction", "subscribe");
        pGr.addQuery("active", true);
        
        if (action == "bulk") {
            var pGrOr = pGr.addQuery('action_create', true);
            pGrOr.addOrCondition('action_update', true);
        } else {
            pGr.addQuery(action_str, true);
        }

		// query by table name first so we get specific table's subscribe config
		// so if configs for both child and base tables it doesn't return the base one
		if(checkTableOnly && cgr.u_source_table_name.isNil())
			pGr.addQuery("table_name", tableName);		
        
		pGr.query();
        while(pGr.next()) {
	    	// to make sure we have the right one we check other configuration properties like condition when we find matching configuration
			if (!pGr.u_source_table_name.isNil() && pGr.u_source_table_name == tableName 
				&& this.checkMatchingSubscribeConfiguration(tableName, cgr, pGr))
				return pGr;
						
			if(pGr.table_name.toString() == tableName && this.checkMatchingSubscribeConfiguration(tableName, cgr, pGr))
				return pGr;
			
			
            if(tables.contains(pGr.table_name.toString()) && this.checkMatchingSubscribeConfiguration(tableName, cgr, pGr))
                return pGr;
        }
        
        return null;
    },
	
	checkMatchingSubscribeConfiguration: function(tableName, cgr, qc){
		if (!qc.u_source_table_name.isNil()) {
			tableName = qc.table_name;
		}
		this.logger.logDebug("checkMatchingSubscribeConfiguration for table " + tableName, "Perspectium.checkMatchingSubscribeConfiguration", qc);
		
		if (qc.condition.isNil() && qc.u_condition_script.isNil()) {
			return true; // nothing to test
		}

		var grRepl = this.decrypt(tableName, cgr.value.toString(), qc.u_field_prefix, cgr.type, cgr.u_subscribed_queue);
			
		//Determine if the GlideRecord is valid
		if(grRepl && grRepl != null && !grRepl.isValid()) {
			var str = "Skipping " + tableName + " as it is not a valid GlideRecord";
			this.logger.logWarn(str, "Perspectium.checkMatchingSubscribeConfiguration", qc);
			cgr.state = "skipped";
			cgr.u_state_info = str;
			return false;
		}
		
		var filter = (typeof GlideFilter != 'undefined') ? GlideFilter : Packages.com.glide.script.Filter;
		if (!qc.condition.isNil() && !filter.checkRecord(grRepl, qc.condition.toString())) {
			var str = "Skipping subscribe configuration with condition " + qc.condition.toString() + " as it does not match for subscribing to " + tableName;
			this.logger.logDebug(str, "Perspectium.checkMatchingSubscribeConfiguration", qc);
			cgr.state = "skipped";
			cgr.u_state_info = str;
			return false;
		}
		
		if (!qc.u_condition_script.isNil()) {
			var evaluator = (typeof GlideController != 'undefined') ? GlideController : Packages.com.glide.script.GlideController;
			evaluator.putGlobal("qcurrent", cgr);
			var oldCurrent = current;
			current = grRepl;
			var answer = evaluator.evaluateString(qc.u_condition_script);
			current = oldCurrent;
			var str = "after condition script, answer == " + answer;
			evaluator.removeGlobal('qcurrent');
			this.logger.logDebug(str, "Perspectium.checkMatchingSubscribeConfiguration", qc);
			if (answer == false) {
				cgr.state = "skipped";
				cgr.u_state_info = str;
				return false;
			}
		}
		
		return true;
	},
	
	processETS: function(cgr, qc){
		
		this.decryptData(cgr.value, cgr.type, cgr.u_subscribed_queue, cgr.messageCipher);
		//split xml records
		var arrTableType = cgr.getValue("name").split(".");
    	var tableName = arrTableType[0];
		var tableTag = ("</" + tableName + ">");
		var splitRec = this.decodeData.split(tableTag);
				
		for(var i = 1 ; i<splitRec.length; i++) { 
			splitRec[i] =  splitRec[i] + tableTag;	
		    var gr = this.deserialize(tableName, splitRec[i], null);
			this.processETSRecord(gr, cgr, qc);
		}
		return true;	
	},
	
    decrypt: function(tableName, eData, prefix, dataType, subscribedQueue, messageCipher) {
	
        this.decryptData(eData, dataType, subscribedQueue, messageCipher);
        return this.deserializer.deserialize(tableName, this.decodeData, prefix);
    },
	
	decryptData: function( eData, dataType, subscribedQueue, messageCipher ){
		//this.logger.logDebug(tableName + " : " + dataType, "Perspectium.decrypt");
		var decrypter = new PerspectiumEncryption();
		if (subscribedQueue && !subscribedQueue.isNil() && !subscribedQueue.u_record_encryption_key.isNil()) {
			var encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
			var qKey = encrypter.decrypt(subscribedQueue.u_record_encryption_key);
				
			this.logger.logDebug("using subscribed queue key: " + qKey, "Perspectium.decrypt");
			decrypter = new PerspectiumEncryption(qKey);			
		}
		
		this.deserializer = this; 
		if (dataType != null && dataType == "salesforce" ) {
			this.deserializer = new PerspectiumSFDC();
			// no cipher specified will be AES128 as that's what Salesforce app sends by default
			if(!messageCipher || (messageCipher && messageCipher != null && messageCipher == "2")){			
				this.decodeData = decrypter.decryptAESString(eData);
			}
			else{
				this.decodeData = decrypter.decryptString(eData)
			}
		}
		else if (messageCipher && messageCipher != null && messageCipher == "2") {
			this.decodeData = decrypter.decryptAESString(eData);			
		}
		else {
			this.decodeData = decrypter.decryptString(eData);
		}
		
		if (this.decodeData == null) {
			// decryption failed
			this.logger.logDebug("decryption failed", "Perspectium.decrypt");
			return null;
		}
        
      //this.logger.logDebug(this.decodeData, "Perspectium.decrypt");
        
	},
    
    deserialize: function(tableName, xmlData, prefix) {
        var gr = new GlideRecord(tableName.toLowerCase());
        gr.initialize();
				
        var xml_util = (typeof GlideXMLUtil != 'undefined') ? new GlideXMLUtil() : new Packages.com.glide.util.XMLUtil();
        var doc = xml_util.parse(xmlData);
        if (!doc) {
            this.logger.logWarn("deserializing: XML unparseable", "Perspectium.deserialize");
            return null;
        }
        
        var root = doc.getDocumentElement();
        this.logger.logDebug("Root name: " + root.getNodeName(), "Perspectium.deserialize");
        
        // look through our children for data or related-list elements...
        var it = xml_util.childElementIterator(root);
        while (it.hasNext()) {
            var el = it.next();
            
            var n = el.getNodeName().toLowerCase();
            var v = xml_util.getText(el);

			if ( (v == null || v=="") && el.getTextContent() != "" ) {
				var d = xml_util.newDocument(el);
				v = xml_util.toFragmentString(d);
				this.logger.logDebug("v after toString: [" + v + "]");
			}
			
            if (v == null)
                continue;
			
			if (prefix && !prefix.isNil() && n.indexOf(prefix) != 0) {
				n = prefix + n;
			}

			var el = gr.getElement(n);
			if (el == null) {
				this.logger.logDebug("element name not found: " + n, "Perspectium.deserialize");
				continue;
			}
			
			try {
				// use isNumber instead ?
				if (el.getED().getInternalType() == "currency") {
					gr.setValue(n, parseInt(v));
				} else if (el.getED().getInternalType().indexOf("journal") >= 0) {
					// for journal fields that are part of the record (such as when sharing with business rule when of before)
					// have to setDisplayValue() for it to save properly as setValue() will not save journal entry
					el.setDisplayValue(String(v));
				} else {		  
					// so cyrillic characters appear correctly when replicated over
					// set as JavaScript string so content isn't a JavaObject
					el.setValue(String(v));
				}
			} catch (e) {
				this.logger.logWarn("error deserializing " + n + " with value " + v + " " + el.getED().getInternalType() + " and error " + e, "Perspectium.deserialize");
			} finally {
				continue;
			}
        }
        
        return gr;
    },

    sendOutboundCounter: function(name, kount) {
        /*this.logger.logDebug(name, "Perspectium.sendOutboundCounter");
		if (kount) {
			this.createPSPOut("counterout", "add", gs.getProperty("instance_name", "unregistered"), name, kount + "");
		} else {
			this.createPSPOut("counterout", "add", gs.getProperty("instance_name", "unregistered"), name, "1");
		}*/
		this.sendCounter(name, kount, "counterout");
    },

    sendCounter: function(name, kount, counterType) {
        this.logger.logDebug(name, "Perspectium.sendCounter");
		if (kount) {
			this.createPSPOut(counterType, "add", gs.getProperty("instance_name", "unregistered"), name, kount + "");
		} else {
			this.createPSPOut(counterType, "add", gs.getProperty("instance_name", "unregistered"), name, "1");
		}
    },
	
	processPSPInMessage: function(cgr) {
		var COUNTER_OUT = "counterout";
		var REPLICATOR = "replicator";
		var HEARTBEAT = "heartbeat";
		var MONITOR = "monitor";
		var SIAM = "siam";
		
		// if have an inbound table map use that to map to the target table so it doesn't get skipped
		// in processReplInMessage
		var tt = this.mapTable(cgr);
		if (tt != "undefined") {
			return this.processMappedInMessage(cgr, tt);
		}
		
		switch(String(cgr.topic)) {
			case COUNTER_OUT:
				return this.processCounterOutMessage(cgr);
			case MONITOR:
				if (cgr.type == HEARTBEAT) {
					return this.processHeartbeatMessage(cgr);
				} else if (cgr.type == REPLICATOR) {
					return this.processReportingMessage(cgr);
				} else {
					return this.processMonitorInMessage(cgr);
				}
			case REPLICATOR:
				return this.processReplInMessage(cgr);
			case SIAM:
				if (cgr.name == "getObjectSchema_response") {
					return this.processSiamTableCreateMessage(cgr);					
				}
				if (cgr.name == "getObjectDataCount_response") {
					return this.processSiamDataCountMessage(cgr);
				}
		}
				
		var str = "unhandled topic: " + cgr.topic + ", type: " + cgr.type;
		this.logger.logWarn(str, "Perspectium.processPSPInMessage");
        cgr.state = "skipped";
		cgr.u_state_info = str;
		return false;
	},
	
	/**
	 * When a reporting message is inbounded, duplicate it to it's own table.
	 **/
	processCounterOutMessage: function(cgr) {
		var gr = new GlideRecord('u_psp_counter_out');
		gr.u_topic = cgr.topic;
		gr.u_type = cgr.type;
		gr.u_key = cgr.key;
		gr.u_name = cgr.name;
		gr.u_value = cgr.value;
		gr.u_attributes = cgr.u_attributes;

		try {
			gr.insert();
			return true;
		} catch (e) {
			var error = "Could not duplicate record into u_psp_counter_out table."
			this.logger.logWarn(error, "Perspectium.processCounterOutMessage");
			return false;
		}
	},

	/**
	 * When a reporting message is inbounded, duplicate it to it's own table.
	 **/
	processReportingMessage: function(cgr) {
		var gr = new GlideRecord('u_psp_reporting_stats');
		gr.u_name = cgr.name;
		gr.u_value = cgr.value;
		try {
			gr.insert();
			return true;
		} catch (e) {
			var error = "Could not duplicate record into u_psp_reporting_stats table."
			this.logger.logWarn(error, "Perspectium.processReportingMessage");
			return false;
		}
	},

	/**
	 * When a heartbeat message is inbounded, duplicate it to it's own table.
	 **/
	processHeartbeatMessage: function(cgr) {
		var gr = new GlideRecord('u_psp_heartbeat');
		gr.u_topic = cgr.topic;
		gr.u_type = cgr.type;
		gr.u_key = cgr.key;
		gr.u_name = cgr.name;
		gr.u_value = cgr.value;
		gr.u_attributes = cgr.u_attributes;
		try {
			gr.insert();
			return true;
		} catch (e) {
			var error = "Could not duplicate record into u_psp_heartbeat table."
			this.logger.logWarn(error, "Perspectium.processHeartbeatMessage");
			return false;
		}
	},
	
	/**
	 * Process SIAM create table message
	 **/
	processSiamTableCreateMessage: function(cgr) {
		var schTab = JSON.parse(GlideStringUtil.base64Decode(cgr.value.toString()));
		var si = "var pspA = new PerspectiumApttus(); pspA.getObjectDataCount('"+schTab.Name.toString()+"');";
		return this.createTable(schTab,'apttus',si);
	},
	
	processSiamDataCountMessage: function(cgr) {
		odc_resp = JSON.parse(GlideStringUtil.base64Decode(cgr.value.toString()));
		params = this.parseQueryString(odc_resp.queryString);
		var nEntries = odc_resp.entryCount;
	    var numPages = Math.floor(nEntries/300)+1;
		var pspA = new PerspectiumApttus();
							
		for(i=0; i < numPages; i++) {
			messCmd = "object="+params.object+"&offset="+i+"&limit=300&startDate="+params.startDate+"&endDate="+params.endDate;
			pspA.getObjectData(messCmd);
		}
							
        return true;				
	},
		
	getMapKey: function(cgr) {
		return cgr.topic.toString()+cgr.type.toString();
	},
	
	mapTable: function(cgr) {
		//this function is only used for inbound table maps
		var key = this.getMapKey(cgr);
		if (key in this.mappedTable) {
			return this.mappedTable[key];	
		} else {
			var tgr = new GlideRecord("u_psp_table_map");
			tgr.addQuery("u_topic", cgr.topic);
			tgr.addQuery("u_type", cgr.type);
			tgr.addQuery("u_direction", "inbound");
			tgr.query();
			if (tgr.next()) {
				this.mappedTable[key] = tgr.u_target_table;
				return tgr.u_target_table;	
			}
			
			this.mappedTable[key] = "undefined";
		}
		
		return "undefined";
	},
	
	processMappedInMessage: function(cgr, tt) {
		var pspM = new PerspectiumMessage(cgr.topic, cgr.type, cgr.key, cgr.name, cgr.value, "", cgr.u_extra, cgr.u_attributes);
		var messageCipher = pspM.getAttribute("cipher");
		
		var grRepl = this.decrypt(tt, cgr.value.toString(), "u_", cgr.type, cgr.u_subscribed_queue, messageCipher);		
		if (grRepl == null || !grRepl.isValid()) {
			var str = "unable to decrypt source, please check decryption key";
			this.logger.logWarn(str, "Perspectium.processMappedInMessage");
        	cgr.state = "error";
			cgr.u_state_info = str;
			return false;			
		}		

		mapSysId = grRepl.insert();
        
        // grab map target information
        var gr = new GlideRecord(tt);
        gr.addQuery('sys_id', mapSysId);
        gr.query();
    
        if (gr.next()) {
			if (!gr.isValidField('sys_target_sys_id') || !gr.isValidField('sys_target_table')) {
				return true;
			}
			
            this.recordSysId = gr.sys_target_sys_id;
            this.recordTargetTable = gr.sys_target_table;
        }

		return true;
	},
	
	processMonitorInMessage: function(cgr) {
		// push into import set
		var igr = new GlideRecord("u_psp_imp_alert");
		if (!igr.isValid()) {
			var str = "import set table unknown: u_psp_imp_alert";
			this.logger.logWarn(str, "Perspectium.processMonitorInMessage");
        	cgr.state = "skipped";
			cgr.u_state_info = str;
			return false;			
		}
		
		igr.initialize();
		igr.u_topic = cgr.topic;
		igr.u_type = cgr.type;
		igr.u_key = cgr.key;
		igr.u_name = cgr.name;
		igr.u_value = cgr.value;
		igr.u_description = cgr.u_description;
		igr.u_timestamp = cgr.u_timestamp;
		igr.u_extra = cgr.u_extra;
		igr.u_priority = cgr.u_priority;
		
		igr.insert();
		return true;
	},

    processReplInMessage: function(cgr) {
	    // cgr is updated in PerspectiumReplicator.fetchFromQueue
		//this.logger.logDebug("name = " + cgr.name, "Perspectium.processReplInMessage");
		
		if (cgr.type == "error") {
			return this.processReplInError(cgr);
		}

    	var arrTableType = cgr.getValue("name").split(".");
    	var tableName = arrTableType[0];
    	var action = arrTableType[1];

        //this.logger.logDebug(tableName + "." + action, "Perspectium.processReplInMessage");

		cgr.u_state_info = '';
        var qc = this.isSubscribed(tableName, action, cgr);
        
    	if (qc == null) {
			var str = "not subscribed: " + tableName + ", " + action;
            this.logger.logDebug(str, "Perspectium.processReplInMessage", qc);        
            cgr.state = "skipped";
			
			// use error message from isSubscribed() if set
			if(cgr.u_state_info != null && cgr.u_state_info.isNil())
				cgr.u_state_info = str;
			
            return false;
    	}
		this.logger.logDebug("Subscribed to " + qc.table_name + " with prefix " + qc.u_field_prefix, "Perspectium.processReplInMessage", qc);			
		cgr.state = "";
		cgr.u_state_info = "";
		
		if (!qc.u_source_table_name.isNil()) {
			tableName = qc.table_name;
		}
		
		cgr.u_source_table = qc.getTableName();
		cgr.u_source = qc.sys_id;
						
		// reset for bytes counter
		var pspM = new PerspectiumMessage(cgr.topic, cgr.type, cgr.key, cgr.name, cgr.value, "", cgr.u_extra, cgr.u_attributes);
		var messageCipher = pspM.getAttribute("cipher");
		var messageAttributes = pspM.getAttributes();
	
		if (String(messageAttributes).indexOf("ets=true") >= 0) {
			this.processETS(cgr, qc);
			return true;
		}
		
		this.decodeData = '';    	
		var grRepl = this.decrypt(tableName, cgr.value.toString(), qc.u_field_prefix, cgr.type, cgr.u_subscribed_queue, messageCipher);
		if (grRepl == null || !grRepl.isValid()) {
			var str = "unable to decrypt source, please check decryption key";
			this.logger.logError(str, "Perspectium.processReplInMessage", qc);
        	cgr.state = "error";
			cgr.u_state_info = str;
			return false;			
		}
					
        //this.sendOutboundCounter(tableName + "." + action);
		this.logger.logDebug(qc.table_name + " copy_empty_fields: " + qc.u_copy_empty_fields, "Perspectium.processReplInMessage"); 
		this.logger.logDebug(qc.table_name + " run_business_rules: " + qc.u_run_business_rules, "Perspectium.processReplInMessage"); 

		return this.processReplInMessageAction(grRepl, cgr, qc, tableName, action);
    },
	
	processETSRecord : function(grRepl, cgr, qc){
		
    	var arrTableType = cgr.getValue("name").split(".");
    	var tableName = arrTableType[0];
    	var action = arrTableType[1];

		return this.processReplInMessageAction(grRepl, cgr, qc, tableName, action);
	},
	
	
	processReplInMessageAction: function(grRepl, cgr, qc, tableName, action ){
		  if (action == "bulk") {     
            this.logger.logDebug("BULK grRepl sys_id=" + grRepl.getValue("sys_id"), "Perspectium.processReplInMessage", qc);
			
            var grDest = new GlideRecord(tableName);
			
			this.logger.logDebug("grRepl " + grRepl.getValue("sys_id"), "Perspectium.processReplInMessage");
			
            if(grRepl.sys_id.isNil() || !grDest.get(grRepl.getValue("sys_id"))) {
				// if record has a sys_class_name we'll also get base table to check if user updated record by changing class (such as from cmdb_ci_netgear to cmdb_ci_ip_switch) 
				// since record with sys_id will exist in base table and we want to still do an update() and not an insert()
				if(!grRepl.sys_class_name.isNil()){			    	
					var tUtils = new TableUtils(tableName);
					var baseTableName = tUtils.getAbsoluteBase();
					var baseGr = new GlideRecord(baseTableName);
					
					if(baseGr.get(grRepl.getValue("sys_id"))){
						// base record exist so get record from the old class to do update() 
						// that way it'll update the class name as well
						var newGrDest = new GlideRecord(baseGr.sys_class_name);
						if(newGrDest.get(grRepl.getValue("sys_id"))){
							this.logger.logDebug("BULK update where class changing from " + baseGr.sys_class_name + " to " + grRepl.sys_class_name + " for " + grRepl.getValue("sys_id"), "Perspectium.processReplInMessage", qc);
							return this.processReplInMessageUpdate(grRepl, newGrDest, qc, cgr);	
						}					
					}
				}
				
                this.logger.logDebug("BULK insert" + tableName, "Perspectium.processReplInMessage", qc);
                action = "insert";
            } else {								
                this.logger.logDebug("BULK update " + tableName + " " + grRepl.getValue("sys_id"), "Perspectium.processReplInMessage", qc);
				return this.processReplInMessageUpdate(grRepl, grDest, qc, cgr);
            }
        }
    
    	if(action == "insert") {
			grRepl = this.enhanceReplRecord(grRepl, tableName);
			if (grRepl == null) {
				cgr.state = "error";
				cgr.u_state_info = "error inserting record into table";				
				return false;
			}
			
			// for jakarta+ keep the sys_class_path the way it is as otherwise record can be lost in domain-separated instances
			/*if (grRepl.isValidField('sys_class_path')) {
				try {
					grRepl.sys_class_path = GlideDBObjectManager.get().getClassPath(grRepl.getTableName());
				}
				catch (e) {
					cgr.state = "error";
					var errorStr = "error inserting " + grRepl.getValue("sys_id") + " into " + tableName + " due to sys_class_path - " + grRepl.getLastErrorMessage();
					cgr.u_state_info = errorStr;				
					this.logger.logError(errorStr, "Perspectium.processReplInMessage", qc);
					return false;
				}
			}
			
			// pre-kingston journal fields are stored under base table
			if (tableName == 'sys_journal_field' && this.glideVersion < 'glide-k') {			
				var tUtils = new TableUtils(grRepl.name);
				grRepl.name = tUtils.getAbsoluteBase();			
			}
			
			// set sys_class_name if it exists to the name of the table we're trying to insert
			// otherwise if subscribe has a source table specified and the incoming record has this source table as its sys_class_name
			// the record will be inserted as the sys_class_name record instead
			if(!grRepl.sys_class_name.isNil()){
				grRepl.sys_class_name = grRepl.getTableName();
			}
			*/
			
            this.logger.logDebug("INSERT " + grRepl.getTableName(), "Perspectium.processReplInMessage", qc);
	    	if (!grRepl.sys_id.isNil()) {
				grRepl.setNewGuidValue(grRepl.getValue("sys_id"));
            }

            if (qc.u_run_business_rules != true) {
                grRepl.setWorkflow(false);
            }
			
			grRepl.autoSysFields(!qc.u_override_system_fields);
			if (!qc.u_before_script.isNil()) {
				var checkIgnore = this.executeBeforeScript(qc.u_before_script, cgr, grRepl, grRepl);
				if(checkIgnore) {
					this.logger.logDebug(grRepl.sys_class_name +  " sys_id<" + grRepl.sys_id +"> ignored by executeBeforeScript", "Perspectium.processReplInMessage", qc);
					return false;
				}
			}

			// Set psp_subscribed_record to true to skip Dynamic Share Business Rule
			grRepl.psp_subscribed_record = true;			
			try{
				var insertSysId = grRepl.insert();	
				if(insertSysId == null || insertSysId == ''){
					cgr.state = "error";
					var errorStr = "error inserting " + grRepl.getValue("sys_id") + " into " + tableName + " - " + grRepl.getLastErrorMessage();
					cgr.u_state_info = errorStr;				
					this.logger.logError(errorStr, "Perspectium.processReplInMessage", qc);
					return false;				
				}
				
				this.recordSysId = insertSysId;	
                this.recordTargetTable = grRepl.getTableName();
            	//this.logger.logDebug("insert result = " + grRepl.insert(), "Perspectium.processReplInMessage");
			}
			catch(e){
				cgr.state = "error";
				var errorStr = "error inserting " + grRepl.getValue("sys_id") + " into " + tableName + " - " + e;
				cgr.u_state_info = errorStr;				
				this.logger.logError(errorStr, "Perspectium.processReplInMessage", qc);
				return false;
			}
						
			if (qc.u_refresh_history_set == true) {
				this.refreshHistorySet(grRepl);
			}

			return true;
    	}
    
    	if(action == "delete") {
            this.logger.logDebug("DELETE", "Perspectium.processReplInMessage", qc);
			var grDest = new GlideRecord(tableName);
			
			// run script first in case we have an ignore that we don't need to delete record 
			// bc we're doing something else with delete
			if (!qc.u_before_script.nil()) {
				var checkIgnore = this.executeBeforeScript(qc.u_before_script, cgr, grDest, grRepl);
				if(checkIgnore) {
					var str = grRepl.sys_class_name +  " record ignored by executeBeforeScript";  
					if (!grRepl.sys_id.isNil()) {
						str = "sys_id<" + grRepl.sys_id +"> " + str;
					}
						
					this.logger.logDebug(str, "Perspectium.processReplInMessage", qc);
					cgr.state = "skipped";
					cgr.u_state_info = "Ignored by Subscribe Configuration's Before Subscribe Script";
					return false;
				}
			}

			if (grRepl.sys_id.isNil()) {
				var str = "Cannot DELETE as no sys_id found in record";
				this.logger.logWarn(str, "Perspectium.processReplInMessage", qc);
				cgr.state = "skipped";
				cgr.u_state_info = str;
				return false;
			}
			
            if(!grDest.get("sys_id", grRepl.getValue("sys_id"))) {
				var str = "sys_id not found for DELETE " + grRepl.getValue("sys_id");
				this.logger.logWarn(str, "Perspectium.processReplInMessage", qc);
				cgr.state = "skipped";
				cgr.u_state_info = str;
				return false;
	    	}

            if (qc.u_run_business_rules != "true") {
                grDest.setWorkflow(false);
            }
			
            grDest.deleteRecord();
            return true;
    	}
    
    	if(action == "update") {
            this.logger.logDebug("UPDATE", "Perspectium.processReplInMessage", qc);
            var grDest = new GlideRecord(tableName);
            if(!grDest.get("sys_id", grRepl.getValue("sys_id"))) {
				var str = "sys_id not found for UPDATE " + grRepl.getValue("sys_id");
            	this.logger.logWarn(str, "Perspectium.processReplInMessage", qc);
				cgr.state = "skipped";
				cgr.u_state_info = str;
            	return false;
            }
        
            return this.processReplInMessageUpdate(grRepl, grDest, qc, cgr);
    	}
    
		var str = "invalid record " + cgr.sys_id;
    	this.logger.logWarn(str, "Perspectium.processReplInMessage", qc);
    	cgr.state = "error";
		cgr.u_state_info = str;
    	return false;
		
	},

	processReplInError: function(cgr) {			
		var pspMsg = new PerspectiumMessage(cgr.topic, cgr.type, cgr.key, cgr.name, cgr.value, "", cgr.u_extra, cgr.u_attributes, "", "");
        var messageSetId = pspMsg.getAttribute("set_id");
		var target_queue = "";
		var shareGR = null;
		if (messageSetId != null) {
			// see if we can get the dynamic/bulk share config
            shareGR = new GlideRecord("psp_bulk_share");
			shareGR.addQuery("sys_id", messageSetId);
			shareGR.query();
			if (!shareGR.next()) {
	            shareGR = new GlideRecord("psp_replicate_conf");
				shareGR.addQuery("sys_id", messageSetId);
				shareGR.query();
				if (!shareGR.next()) {
					shareGR = null;
				}
			}
        }

		if (shareGR != null) {
			target_queue = shareGR.u_target_queue;	
		}
		
		// only save extra as we don't need attributes so new ones can be created 
		this.createPSPOut(cgr.topic, cgr.type, cgr.key, cgr.name, cgr.value, target_queue, cgr.u_extra, "", shareGR, "error") ;
		return true;
	},
	
	executeBeforeScript: function(script, cgr, gr, ingr) {											
		var gc = (typeof GlideController != 'undefined') ? GlideController : Packages.com.glide.script.GlideController;
		var oldCurrent = current;
		var ignore = false;
		var decrypter = new PerspectiumEncryption();
		var decodeData = decrypter.decryptString(cgr.value);

		if(decodeData){
			var xml_util = (typeof GlideXMLUtil != 'undefined') ? new GlideXMLUtil() : new Packages.com.glide.util.XMLUtil();
			var qcurrentxml = xml_util.parse(decodeData);
			gc.putGlobal('qcurrentxml', qcurrentxml);
			gc.putGlobal('xml_util', xml_util);
		}
		else {
			this.logger.logDebug("qcurrentxml not put into global as record cannot be decrypted for inbound message " + cgr.sys_id, "Perspectium.executeBeforeScript");
		}
        gc.putGlobal('current', gr);
        gc.putGlobal('repl_gr', ingr);
		gc.putGlobal('qcurrent', cgr);
		gc.putGlobal('ignore', ignore);
		
		// get gliderecord as its currently in the system if user needs to compare in before subscribe script
		var arrTableType = cgr.getValue("name").split(".");
    	var tableName = arrTableType[0];
    	var action = arrTableType[1];
        // check table exists in case we're doing a source table transform in the replicator subscribe config and the source table doesn't exists in this instance
		var tableUtils = new TableUtils(tableName);		
		var gr_before = new GlideRecord(tableName);
		if(tableUtils.tableExists() && gr_before.get(ingr.sys_id)){
			gc.putGlobal('gr_before', gr_before);										
		}		
		// if record or table doesn't exist we'll use current record about to be inserted 				 
		else{
			gc.putGlobal('gr_before', gr);
		}							   
									   
        var rc = gc.evaluateString(script);
        ignore = gc.getGlobal('ignore');
        gc.removeGlobal('current');
        gc.removeGlobal('repl_gr');
		gc.removeGlobal('qcurrent');
		gc.removeGlobal('gr_before');
		gc.removeGlobal('qcurrentxml');
		gc.removeGlobal('xml_util');
		current = oldCurrent;
		return ignore;	
	},
	
	executeBeforeShareScript: function(script, gr, jsonParams) {
		var gc = (typeof GlideController != 'undefined') ? GlideController : Packages.com.glide.script.GlideController;
		var oldCurrent = current;
		var ignore = false;

        gc.putGlobal('current', gr);
        gc.putGlobal('ignore', ignore);
		if(jsonParams != null){
			for(var param in jsonParams){
			   this.logger.logDebug("Adding Before Share Script paramter " + param);
			   gc.putGlobal(param, jsonParams[param]);
			}			
		}
        var rc = gc.evaluateString(script);
		if(jsonParams != null){
			for(var param in jsonParams){
			   jsonParams[param] = gc.getGlobal(param);
			   gc.removeGlobal(param);
			}			
		}		
		ignore = gc.getGlobal('ignore');
        gc.removeGlobal('ignore');
        gc.removeGlobal('current');
		current = oldCurrent;
		
		return ignore;	
	},
	
	refreshHistorySet: function(hgr) {
		// add to list of table records we want to refresh in scheduled job
		this.pspRHS.add(hgr.getTableName(), hgr.sys_id);
	},

    processReplInMessageUpdate: function (grRepl, grDest, qc, cgr) {
		// create XML doc of incoming source XML in case we need to check source to verify fields exist for empty fields
		var xml_util = (typeof GlideXMLUtil != 'undefined') ? new GlideXMLUtil() : new Packages.com.glide.util.XMLUtil();
		var doc = new XMLDocument(this.decodeData);
		
        var arrFields = grRepl.getFields();
        for (var i = 0; i < arrFields.size(); i++) {
            var glideElement = arrFields.get(i);			
			var el = grDest.getElement(glideElement.getName());
			// fields from servicenow will be all lower case but also check for upper case if record was shared from a database like Oracle
			var existsInIncoming = (doc != null && (doc.getElementByTagName(glideElement.getName()) || doc.getElementByTagName(glideElement.getName().toUpperCase())));

			// skip field so we don't error on updating entire record
			if(el == null){
				this.logger.logDebug("skipping null element " + glideElement.getName() + " in table " + grDest.sys_class_name, "Perspectium.processReplInMessageUpdate", qc);
				continue;
			}

			if(!existsInIncoming){
				this.logger.logDebug(glideElement.getName() + " does not exist in incoming XML so we skip this column", "Perspectium.processReplInMessageUpdate");
				continue;
			}
					
			var val = grRepl.getValue(glideElement.getName());				
			if (!glideElement.hasValue() && qc.u_copy_empty_fields == true) {
				// for journal fields we don't need to update value if blank as this will result in a blank entry added in sys_journal_field
				if (el.getED().getInternalType().indexOf("journal") >= 0) {
					continue;
				}
				
				// check if this element exists in the incoming XML as we don't want to clear out fields that exists on this table here but not in incoming record				
				if (existsInIncoming) {
					this.logger.logDebug(glideElement.getName() + " exists in incoming XML so we set field empty", "Perspectium.processReplInMessageUpdate");										
					val = "NULL";					
				}
			}
	
			if(grDest.getValue(glideElement.getName()) == val || (this.skipSysDomainPath && glideElement.getName() == "sys_domain_path"))
				continue;
			
			this.recordSysId = grRepl.sys_id;
            this.recordTargetTable = grRepl.getTableName();
			
	    	this.logger.logDebug("UPDATE on  "+ grDest.sys_class_name +  " for sys_id<" + grDest.sys_id +"> Name = " + glideElement.getName() + " | Value = " + val, "Perspectium.processReplInMessageUpdate", qc);			
					
			// for journal fields that are part of the record (such as when sharing with business rule when of before)
			// have to setDisplayValue() for it to save properly as setValue() will not save journal entry
			if (el.getED().getInternalType().indexOf("journal") >= 0) {
				el.setDisplayValue(String(val));
			}
			else {
				el.setValue(val);		
			}
        }

		var tableName = grDest.getTableName();
		grDest = this.enhanceReplRecord(grDest, tableName);
		if (grDest == null) {
			cgr.state = "error";
			cgr.u_state_info = "error updating record";				
			return false;			
		}

		// for jakarta+ keep the sys_class_path the way it is as otherwise record can be lost in domain-separated instances
		/*if (grDest.isValidField('sys_class_path')) {
			try {
				grDest.sys_class_path = GlideDBObjectManager.get().getClassPath(grDest.getTableName());
			}
			catch (e) {
				cgr.state = "error";
				var errorStr = "error updating " + grDest.sys_id + " and its sys_class_path - " + e;
				cgr.u_state_info = errorStr;				
				this.logger.logError(errorStr, "Perspectium.processReplInMessageUpdate", qc);
				return false;
			}
		}
		
		// pre-kingston journal fields are stored under base table
		var tableName = grDest.getTableName();
		if (tableName == 'sys_journal_field' && this.glideVersion < 'glide-k') {			
			var tUtils = new TableUtils(grDest.name);
			grDest.name = tUtils.getAbsoluteBase();			
		}
		
		// set sys_class_name if it exists to the name of the table we're trying to update
		// otherwise if subscribe has a source table specified and the incoming record has this source table as its sys_class_name
		// the record will be updated and become the sys_class_name record instead
		if(!grDest.sys_class_name.isNil()){
			grDest.sys_class_name = grDest.getTableName();
		}*/
				
        if (qc.u_run_business_rules != true) {
            grDest.setWorkflow(false);
        }
		
		grDest.autoSysFields(!qc.u_override_system_fields);
		
		if (!qc.u_before_script.isNil()) {
			var checkIgnore = this.executeBeforeScript(qc.u_before_script, cgr, grDest, grRepl);
			if(checkIgnore) {
				this.logger.logDebug(grDest.sys_class_name +  " sys_id<" + grDest.sys_id +"> ignored by executeBeforeScript", "Perspectium.processReplInMessageUpdate", qc);
				return false;
			}
		}
	
		// Set psp_subscribed_record to true to skip Dynamic Share Business Rule
		grDest.psp_subscribed_record = true;
		try{
			var updateSysId = grDest.update();
			
			if(updateSysId == null || updateSysId == ''){
				cgr.state = "error";
				var errorStr = "error updating " + grDest.sys_id + " - " + grDest.getLastErrorMessage();
				cgr.u_state_info = errorStr;				
				this.logger.logError(errorStr, "Perspectium.processReplInMessageUpdate", qc);
				return false;				
			}
		}
		catch(e){
			
			// on failure try updateLazy() in case it's erroring because table such as sys_history_line has an update column
			// which will cause update() to thrown exception TypeError: 0 is not a function
			if(!grDest.updateLazy()){	
				cgr.state = "error";
				var errorStr = "error updating " + grDest.sys_id + " - " + e;
				cgr.u_state_info = errorStr;				
				this.logger.logError(errorStr, "Perspectium.processReplInMessageUpdate", qc);
				return false;
			}
		}
	
		if (qc.u_refresh_history_set == true) {
			this.refreshHistorySet(grDest);
		}
		
        return true;
    },
	
	enhanceReplRecord: function(grRepl, tableName) {
		// for jakarta+ keep the sys_class_path the way it is as otherwise record can be lost in domain-separated instances
		if (grRepl.isValidField('sys_class_path')) {
			try {
				grRepl.sys_class_path = GlideDBObjectManager.get().getClassPath(grRepl.getTableName());
			}
			catch (e) {
				var errorStr = "error inserting " + grRepl.getValue("sys_id") + " into " + tableName + " due to sys_class_path - " + grRepl.getLastErrorMessage();
				this.logger.logError(errorStr, "Perspectium.enhanceReplRecord");
				return null;
			}
		}

		// pre-kingston journal fields are stored under base table
		if (tableName == 'sys_journal_field' && this.glideVersion < 'glide-k') {			
			var tUtils = new TableUtils(grRepl.name);
			grRepl.name = tUtils.getAbsoluteBase();			
		}

		// set sys_class_name if it exists to the name of the table we're trying to insert
		// otherwise if subscribe has a source table specified and the incoming record has this source table as its sys_class_name
		// the record will be inserted as the sys_class_name record instead
		if (!grRepl.sys_class_name.isNil()) {
			grRepl.sys_class_name = grRepl.getTableName();
		}	
		
		return grRepl;
	},
	
	leftPad : function(number) {    
    	return ((number < 10 && number >= 0) ? '0' : '') + number;  
	},
	
	endsWith : function(str, suffix) {
    	return str.indexOf(suffix, str.length - suffix.length) !== -1;
	},
	
    parseQueryString: function(qStr) {
	    var obj = {};
        function sliceUp(x) { x.replace('?', '').split('&').forEach(splitUp); }
        function splitUp(x) { var str = x.split('='); obj[str[0]] = decodeURIComponent(str[1]); }
        try { (!qStr ? sliceUp(location.search) : sliceUp(qStr)); } catch(e) {}
            return obj;
	},
	
	createTable: function(schTab,tbPfx,si) {

		// get import set table so we can make it as the parent table that this new table extends
		var igr = new GlideRecord('sys_db_object');
		igr.addQuery('name', 'sys_import_set_row');
		igr.query();

		if (!igr.next()) {
			// can't find import set table
			return false;
		}

		var tableName = String(tbPfx+"_"+schTab.Name.toString()).toLowerCase();

		// create table
		var table = new GlideRecord('sys_db_object');
		table.initialize();
		table.name = tableName;
		table.super_class = igr.sys_id;
		tableSysId = table.insert();

		var fieldList = schTab.Fields;

		for (var fieldKey in fieldList) {
			var field = new GlideRecord('sys_dictionary');
			field.initialize();
			field.name = "u_"+tableName;
			field.column_label = fieldList[fieldKey].Name;

			if(fieldList[fieldKey].Length) {
				field.max_length = fieldList[fieldKey].Length;
			} else {
				field.max_length = 255;
			}

			field.internal_type = 'string';
			field.insert();
		}

		gs.log("Created table: u_"+tableName);

		var uigr = new GlideRecord("sys_ui_action");
		uigr.active = true;
		uigr.name = "Import Data";
		uigr.list_banner_button = true;
		uigr.table = "u_"+tableName;
		uigr.action_name = "sysverb_import_"+tableName;

		uigr.script = si;
		uigr.insert();

		return true;
	},
    
    type: 'Perspectium'
};