var PerspectiumApprover = Class.create();
PerspectiumApprover.prototype = {
 	initialize: function() {
        this.logger = new PerspectiumLogger();
    },

    addApprovers : function(source, target, pspTag /* optional */) {
        var targetId = target.sys_id;
		var targetTable = target.getTableName();
        this.logger.logDebug('Adding change task for: ' + targetTable + "," + targetId, "PerspectiumApprover.addApprovers");

        if (source == null || source.u_approvers.isNil()) {
            this.logger.logDebug('No approvers found', "PerspectiumApprover.addApprovers");
            return;
        }

        var xmldoc = null;
        try {
             xmldoc = new XMLDocument(source.u_approvers);
        } catch(err){
            this.logger.logDebug('Error creating XMLDocument for approvers: ' + err, "PerspectiumApprover.addApprovers");
            return;
        }

        if (xmldoc == null) {
           this.logger.logDebug('XMLDocument is null', "PerspectiumApprover.addApprovers");
           return;
        }

        var nodelist = xmldoc.getNodes("//approver");
        if (nodelist == null) {
            this.logger.logDebug('No approver nodes found', "PerspectiumApprover.addApprovers");
            return;
        }

        this.logger.logDebug('Found ' + nodelist.getLength() + ' approvers', "PerspectiumApprover.addApprovers");
        for (var i=0; i < nodelist.getLength(); i++) {
            var nodeItem = nodelist.item(i);
            this.addApprover(nodeItem, targetTable, targetId, pspTag);
        }
    },
	
	_getValue : function(nodeItem, nodeName) {        
		var xml_util = (typeof GlideXMLUtil != 'undefined') ? new GlideXMLUtil() : new Packages.com.glide.util.XMLUtil();
		var it = xml_util.childElementIterator(nodeItem);
        while (it.hasNext()) {
            var el = it.next();
            var nn = el.getNodeName();
            var nodeValue = xml_util.getText(el);
			
			if( nodeValue == null)
				nodeValue = "";
			
			if(nn == nodeName) {
				return nodeValue;
			}
		}
											  
		return "";
	},
	
	_setValues : function(ct, aId, targetId, nodeItem, targetTable) {
		ct.approver = aId;
		ct.sysapproval = targetId;
		ct.comments = this._getValue(nodeItem, "comments");
		ct.assignment_group = this._getValue(nodeItem, "assignment_group");
		ct.state = this._getValue(nodeItem, "state");
		ct.document_id = targetId;
		ct.source_table = targetTable;
	},
	
//         <approver>
//             <created>2017-09-28 06:07:46</created>
//             <approver_name>Bernard Laboy</approver_name>
//             <assignment_group_name/>
//             <approver>ee826bf03710200044e0bfc8bcbe5de6</approver>
//             <state>requested</state>
//             <comments/>
//             <approver_email>bernard.laboy@example.com</approver_email>
//             <assignment_group/>
//         </approver>

    addApprover : function(nodeItem, targetTable, targetId, pspTag /* optional */) {
        var StringUtil = (typeof GlideStringUtil != 'undefined') ? new GlideStringUtil() : new Packages.com.glide.util.StringUtil();
		
		var aEmail = this._getValue(nodeItem, "approver_email");
		if (!aEmail || aEmail == '')
			return;
		
		// locate the user by email
		var sgr = new GlideRecord("sys_user");
		sgr.addQuery("email", aEmail);
		sgr.query();
		if (!sgr.next()) {
			// create ? no, exit for now
			this.logger.logError('Approver not found: ' + aName + ' not adding sysapproval_approver', "PerspectiumApprover.addApprover");
			return;
		}
		
		var aId = sgr.sys_id;
		if (aId == null)
			return; // no sys_id, cannot search
		
		// first see if we can find sysapproval_approver by approver and task
		var sa = new GlideRecord("sysapproval_approver");
		sa.addQuery("approver", aId);
		sa.addQuery("sysapproval", targetId);
		sa.query();
		if (!sa.next()) {
			sa.initialize();
			this._setValues(sa, aId, targetId, nodeItem, targetTable);
			sa.insert();
		} else {
			this._setValues(sa, aId, targetId, nodeItem, targetTable);
			sa.update();
		}
		
		// if pspTag exists, go and tag it
		if (pspTag) {
			var pspUtil = new PerspectiumUtil();
			pspUtil.addTag(taskCI, pspTag);
		}
    },
	
    type: 'PerspectiumApprover'
};