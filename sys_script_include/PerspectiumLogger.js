var PerspectiumLogger = Class.create();
PerspectiumLogger.prototype = {	
	
    initialize: function() {
		this.pspUtil = new PerspectiumUtil();
		this.debug = this.pspUtil.getPspPropertyValue("com.perspectium.debug", "false");
		this.instance_name = gs.getProperty("instance_name", "unregistered");
		this.error_to_alert = this.pspUtil.getPspPropertyValue("com.perspectium.error_to_alert", "true");
    },
	
	logInfo: function(message, name, sourceGR) {
		if (sourceGR)
			this.log(message, "info", name, null, sourceGR);
		else
			this.log(message, "info", name);
	},
	
	logDebug: function(message, name, sourceGR) {
		if (sourceGR)
			this.log(message, "debug", name, null, sourceGR);
		else
			this.log(message, "debug", name);
	},
	
	logWarn: function(message, name, sourceGR) {
		if (sourceGR)
			this.log(message, "warning", name, null, sourceGR);
		else
			this.log(message, "warning", name);
	},
	
	logError: function(message, name, sourceGR) {
		if (sourceGR)
			this.log(message, "error", name, null, sourceGR);
		else
			this.log(message, "error", name);
		
		// optionally generate alerts
		if (this.error_to_alert == "true") {
			var egr = new GlideRecord("u_psp_alerts");
			egr.u_priority = "critical";
			egr.u_name = name;
			egr.u_value = message;
			egr.u_key = this.instance_name;
			egr.insert();
		}
	},

	// source is tablename.sys_id
    log: function(message, type, name, level, sourceGR) {
		if ((!type || type == "debug") && this.debug != "true") {
			return;
		}
		
		var lgr = new GlideRecord("u_psp_log_message");
		lgr.u_topic = "log";
		
		if (type) {
			lgr.u_type = type;
		} else {
			lgr.u_type = "debug"
		}
		
		if (!name) {
			name = "General";
		}
		
		if (level) {
			lgr.u_level = level;
		}
		
		if (sourceGR) {
			// create the source if available
			lgr.u_source_table = sourceGR.getTableName();;
			lgr.u_source = sourceGR.sys_id;
		}
		
		lgr.u_key = this.instance_name;
		lgr.u_value = message;
		lgr.u_name = name;
		lgr.insertLazy();
    },

    type: 'PerspectiumLogger'
}