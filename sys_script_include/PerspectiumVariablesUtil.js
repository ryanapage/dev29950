var PerspectiumVariablesUtil = Class.create();
PerspectiumVariablesUtil.prototype = {
	initialize: function() {
		/* map: element vs internal field type */
		this.UI_PAGE = 15,
		this.MASKED = 25,
		this.DATE = 9,
		this.NUMERIC_SCALE = 4,
		this.CHECKBOX = 7,
		this.MULTIPLE_CHOICE = 3,
		this.DATE_TIME = 10,
		this.CONTAINER_END = 20,
		this.LABEL = 11,
		this.WIDE_SINGLE_LINE_TEXT = 16,
		this.YES_NO = 1,
		this.HTML = 23,
		this.SELECT_BOX = 5,
		this.MACRO = 14,
		this.REFERENCE = 8,
		this.LOOKUP_MULTIPLE_CHOICE = 22,
		this.LIST_COLLECTOR = 21,
		this.MULTI_LINE_TEXT = 2,
		this.SINGLE_LINE_TEXT = 6,
		this.MACRO_WITH_LABEL = 17,
		this.LOOKUP_SELECT_BOX = 18,
		this.SPLIT = 24,
		/* this is a list of elements that will be ignored */
		this.IGNORE = [this.UI_PAGE, this.CONTAINER_END, this.HTML, this.MACRO,
				this.MACRO_WITH_LABEL, this.SPLIT, this.LABEL];
	},
	
	/*
	 * ritm      GlideRecord object
	 * func      javascript function to customize conditions
	 *           to skip centain variables values
	 * noBlanks  true|false to avoid empty variable's value
	 */
	getDescription: function(ritm, func, noBlanks) {
		var d = '';
		var noBlanks = noBlanks || false;
		var vars = this._getVariablesSorted(ritm);
		
		/* loop the variables */
		for(var i =0; i < vars.length; i++) {
			var o = vars[i];
			var type = o.type;
			var label = o.label;
			/* ignore list of types */
			if(this._isIgnored(o.type))
				continue;
			
			/* if custom function is provided, execute custom conditions */
			if(func && typeof func == 'function') {
				if(func.call(this,o)) continue;
			}
			
			var v = o.value;
			if(gs.nil(v)) {
				if(noBlanks)
					continue;
			} else {
				/* for reference fields get the Display Value */
				if(type == this.REFERENCE || type == this.LIST_COLLECTOR) {
					v = v.getDisplayValue();
				} else if(type == this.MULTI_LINE_TEXT) {
					/* for multi line place an extra line in front 
					and ident every line with one space */
					v = "\n " + o.value.toString().replace(/\n/g,"\n ");
				}  else if(type == this.MULTIPLE_CHOICE || type == this.SELECT_BOX) {
					/* get choices */
					// bug
					//var choices = o.getChoiceList();
					/* get label of value selected */
					//v = choices.getLabelOf(v);
					v = v.getDisplayValue();
				} else {
					v = v.getDisplayValue();
				}
			}
			/* insert a new line for every variable/value pair */
			if(d.length>0)
				d+="\n";
			/* set the label of the variable with it's corresponding value */
			d+=label+": "+v;
		}
		return d;
	},
	
	_getVariablesSorted: function(ritm) {
		var vars = [];
		for(var variableName in ritm.variables) {
			/* GlideObject */
			var go = ritm.variables[variableName].getGlideObject();
			/* Question */
			var question = go.getQuestion();
			/* Label */
			var label = go.getQuestion().getLabel();
			/* Type (numeric value) */
			var type = new Number(go.getType());
			/* Order (numeric value) */
			var order = new Number(go.getQuestion().getOrder());
			/* creates an Object */
			var o = {
				'name': variableName,
				'label': label,
				'value': ritm.variables[variableName],
				'order': order,
				'type': type
			};
			vars.push(o);
		}
		/* return the variables in order as per the form */
		return this._quicksort(vars);
	},
	
	/* quick sort algorithm */
	_quicksort: function(arr) {
		if (arr.length === 0) {
			return [];
		}
		var left = [];
		var right = [];
		var pivot = arr[0];
		for (var i = 1; i < arr.length; i++) {
			/* compare order from object */
			if (arr[i].order < pivot.order) {
				left.push(arr[i]);
			} else {
				right.push(arr[i]);
			}
		}
		return this._quicksort(left).concat(pivot, this._quicksort(right));
	},
	
	/* check if the given field type should be ignored */
	_isIgnored: function(type) {
		for(i=0; i < this.IGNORE.length; i++)
			if( this.IGNORE[i] == type)
			return true;
		return false;
	},
	
    type: 'PerspectiumVariablesUtil'
};