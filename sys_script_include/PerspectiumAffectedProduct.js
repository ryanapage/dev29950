var PerspectiumAffectedProduct = Class.create();
PerspectiumAffectedProduct.prototype = {
	initialize: function() {
		this.logger = new PerspectiumLogger();
	},

	createM2MEntries: function(nodelist) {
		if (nodelist == null)
			return;

		for (var i = 0; i < nodelist.getLength(); i++) {
			var node = nodelist.item(i);
			var kgr = new GlideRecord('kb_knowledge');
			kgr.addQuery('number', this._getValue(node, 'kb_name'));
			kgr.query();

			var cgr = new GlideRecord('cmdb_ci');
			cgr.addQuery('name', this._getValue(node, 'ci_name'));
			cgr.query();

			if (kgr.next() && cgr.next() && !this.m2mEntryExists(kgr, cgr)) {
				var mgr = new GlideRecord('m2m_kb_ci');
				mgr.kb_knowledge = kgr.sys_id;
				mgr.cmdb_ci = cgr.sys_id;
				var is = mgr.insert();
			}
		}
	},
	m2mEntryExists: function(kb, ci) {
		var gr = new GlideRecord('m2m_kb_ci');
		gr.addQuery('cmdb_ci', ci.sys_id);
		gr.addQuery('kb_knowledge', kb.sys_id);
		gr.query();
		if (gr.next()) {
			return true;
		}
		return false;
	},
	getExistingM2M: function(kb, ci) {
		var gr = new GlideRecord('m2m_kb_ci');
		gr.addQuery('cmdb_ci', ci.sys_id);
		gr.addQuery('kb_knowledge', kb.sys_id);
		gr.query();
		if (gr.next()) {
			return gr;
		}
	},
	_getValue: function(nodeItem, nodeName) {        
		var xml_util = (typeof GlideXMLUtil != 'undefined') ? new GlideXMLUtil() : new Packages.com.glide.util.XMLUtil();
		var it = xml_util.childElementIterator(nodeItem);
		while (it.hasNext()) {
			var el = it.next();
			var nn = el.getNodeName();
			var nodeValue = xml_util.getText(el);

			if( nodeValue == null)
				nodeValue = "";

			if(nn == nodeName) {
				return nodeValue;
			}
		}

		return "";
	},

	type: 'PerspectiumAffectedProduct'
};