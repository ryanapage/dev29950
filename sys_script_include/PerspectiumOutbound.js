var PerspectiumOutbound = Class.create();
PerspectiumOutbound.prototype = {
    initialize: function(outboundTable) {
		this.psp = new Perspectium();
		this.logger = new PerspectiumLogger();
		this.outboundTable = outboundTable;
		this.instance_name = gs.getProperty("instance_name", "unregistered");
    },

	processOutbound: function() {		
		var qgr = new GlideAggregate(this.outboundTable);  
		qgr.addAggregate("COUNT");  
		qgr.groupBy("u_target_queue");  
		qgr.query();  
		while (qgr.next()) {  
			if (qgr.u_target_queue.nil()) {
				continue;
			}   
			
			if (!qgr.u_target_queue.u_active) {
				//Target queue is inactive, skip
				continue;
			}
			
			try {
				this.processTargetQueue(qgr.u_target_queue);
			}
			catch(e) {
				this.logger.logError("Error processing PSP queue " + qgr.u_target_queue + " on outbound table " + this.outboundTable + ": " + e, "PerspectiumOutbound.processOutbound");
			}
		}
		
		this.processTargetQueue("default");
	},
	
	processTargetQueue: function(target_queue) {
		if (!this.outboundTable || this.outboundTable == null || this.outboundTable == "") {
			this.logger.logDebug("No outbound table defined to process queue " + target_queue, "PerspectiumOutbound.processTargetQueue");
			return;	
		}
		
		var single_output_processor = this.psp.single_output_processor;		
		if ("true" != single_output_processor) {
			var lock = new GlideMutex.get("processTargetQueue");
			while (lock.toString()+'' == "undefined") {
				this.logger.logDebug("waiting for lock: processTargetQueue (" + target_queue + ")", "PerspectiumOutbound.processTargetQueue");
				gs.sleep(1000); // sleep for 1000 milliseconds
				lock = new GlideMutex.get("processTargetQueue");
			}
			
			this.logger.logDebug("lock acquired: processTargetQueue (" + target_queue + ")", "PerspectiumOutbound.processTargetQueue");
		}
		
        var grPSPOut = new GlideRecord(this.outboundTable);
        grPSPOut.addQuery("u_state", "ready");
        if (target_queue == "default") {
          grPSPOut.addNullQuery("u_target_queue");
        } else if (target_queue) {
          grPSPOut.addQuery("u_target_queue", target_queue);
        }

		var rowLimitStr = this.psp.rowLimitStr;
		
		var sdt = new Date();
        grPSPOut.setLimit(parseInt(rowLimitStr));
        grPSPOut.orderBy("sys_created_on");
		grPSPOut.orderBy("u_sequence");
        grPSPOut.query();
				
        var objJSON = new JSON();
        var arrID = [];
        var arrOut = "";
        var arrJSON = [];
		var tBytes = 0;
					
        while(grPSPOut.next()) {
			// first record we'll check if it's older than our past minutes setting and alert if necessary
			if(tBytes == 0){
				this.psp.checkMessagePastLimit(grPSPOut);
			}
				
			if ("true" != single_output_processor) {
				// dont change to processing if only 1 output processor job
	    		grPSPOut.u_state = "processing";
	    		grPSPOut.update();
			}
			
			var grKey = grPSPOut.u_key;
	    	if (grKey != this.instance_name && grKey.indexOf(this.instance_name + ".") != 0) {
				// may have been cloned, so skip
				grPSPOut.u_state = "skipped";
				grPSPOut.u_state_info = "Message skipped due to mismatch between instance and key of message";
				grPSPOut.update();
				
				this.logger.logDebug("skipping mismatched key: " + grPSPOut.u_key, "PerspectiumOutbound.processTargetQueue");
				continue;
	    	}
			
			try {
				var messageTargetQueue = "";
				if (target_queue != "default") {
					messageTargetQueue = target_queue;
				}
							
				var pm = new PerspectiumMessage(grPSPOut.u_topic, grPSPOut.u_type, grPSPOut.u_key, grPSPOut.u_name, grPSPOut.u_value, messageTargetQueue, grPSPOut.u_extra, grPSPOut.u_attributes, "", "", grPSPOut.u_state, false, this.outboundTable);
				
				var jj = pm.encodeMessageJSON();				
				//var jj = this.psp.encodePSPOutJSON(grPSPOut);				
				var llength = jj.length();

				// if 1 record already is over big, mark it as error and move on
				if (llength > this.psp.maxBytes) {
				    // mark it as error
					var estr = "outbound record (" + grPSPOut.sys_id.toString() + ") of length " + llength + " is more than " + this.psp.maxBytes;
					this.logger.logError(estr, "PerspectiumOutbound.processTargetQueue");
					grPSPOut.u_state_info = estr;
					grPSPOut.update();
					
					// for errors we'll call mark function so we can update messageset
					var eArrID = [];
					eArrID.push(grPSPOut.sys_id.toString());
					this.markMessages(eArrID, "error");					
					continue;
				}

				tBytes += llength;
				if (tBytes > this.psp.maxBytes) {
					this.logger.logDebug("maximum " + this.psp.maxBytes + " bytes reached (" + tBytes + ") at " + arrJSON.length + " records, backing up to " + (tBytes - jj.length()) + ", breaking", "PerspectiumOutbound.processTargetQueue");
					break;
				}
				
				arrJSON.push(jj);
				arrID.push(grPSPOut.sys_id.toString());
			} catch (e) {
				this.logger.logError("unable to process " + grPSPOut.sys_id.toString() + " " + e, "PerspectiumOutbound.processTargetQueue");
				grPSPOut.u_state_info = e + "";
	    		grPSPOut.update();
				
				// for errors we'll call mark function so we can update messageset
				var eArrID = [];
				eArrID.push(grPSPOut.sys_id.toString());
				this.markMessages(eArrID, "error");				
			}
        }
				
		if ("true" != single_output_processor) {
			lock.release();
			this.logger.logDebug("lock released: processTargetQueue (" + target_queue + ")", "PerspectiumOutbound.processTargetQueue");
		}
		
		if (arrJSON.length == 0) {
			return 200;
		}

		var csecs = this.psp._getTimeDiffSecs(sdt);
		var rsecs = arrJSON.length/csecs;
		
        this.logger.logDebug(arrJSON.length + " records (" + target_queue + ") COLLECTED in " + csecs + " secs (" + rsecs.toFixed(2) + " recs/s)", "PerspectiumOutbound.processTargetQueue");
        
		sdt = new Date();		
		var sbuf = new Packages.java.lang.StringBuffer();		
		sbuf.append("[");
		sbuf.append(arrJSON.join(","));
		sbuf.append("]");
		arrOut = sbuf.toString();
		
		csecs = this.psp._getTimeDiffSecs(sdt);
		rsecs = arrJSON.length/csecs;

		this.logger.logDebug(arrJSON.length + " records (" + target_queue + ") ENCODED in " + csecs + " secs (" + rsecs.toFixed(2) + " recs/s)", "PerspectiumOutbound.processTargetQueue");
  
		sdt = new Date();
        var result = this.psp.postMulti(arrOut, target_queue);
		csecs = this.psp._getTimeDiffSecs(sdt);
		rsecs = arrJSON.length/csecs;
		
        this.logger.logDebug(arrJSON.length + " records (" + target_queue + ") POSTED in " + csecs + " secs (" + rsecs.toFixed(2) + " recs/s)", "PerspectiumOutbound.processTargetQueue");
		
        if (result != 200) {
			if ("true" != single_output_processor) {
				// mark only if records were changed to processing
	    		this.markMessages(arrID, "ready");
			}
            throw new Error("HTTP POST failed: " + result);
        } else {
			sdt = new Date();						
	    	this.markMessages(arrID, "sent");
			csecs = this.psp._getTimeDiffSecs(sdt);
			rsecs = arrID.length/csecs;			
        	this.logger.logDebug(arrJSON.length + " records (" + target_queue + ") UPDATED to 'sent' in " + csecs + " secs (" + rsecs.toFixed(2) + " recs/s)", "PerspectiumOutbound.processTargetQueue");
        }
				
		return result;
    },

    markMessages: function (arrID, state) {
        var grPSPOut = new GlideRecord(this.outboundTable);
        grPSPOut.addQuery("sys_id", "IN", arrID.toString());
		
		// Delete the records immediately instead of marking them as sent
		var deleteImmediately;
		if(this.outboundTable == "u_psp_observer_out_message"){
			deleteImmediately = this.psp.pspUtil.getPspPropertyValue("com.perspectium.observer.outbound.delete_immediately", "false");
		}
		else{
			deleteImmediately = this.psp.pspUtil.getPspPropertyValue("com.perspectium.outbound.delete_immediately", "false");
		}
		if((deleteImmediately == true || deleteImmediately == "true") && state == "sent"){
			grPSPOut.deleteMultiple();
			return;
		}
		
        grPSPOut.query();
        while(grPSPOut.next()) {
            grPSPOut.u_state = state;
            grPSPOut.setWorkflow(false);
            grPSPOut.update();			
        }
    },
	
    type: 'PerspectiumOutbound'
};