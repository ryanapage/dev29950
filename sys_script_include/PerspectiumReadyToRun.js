var PerspectiumReadyToRun = Class.create();
PerspectiumReadyToRun.prototype = Object.extendsObject(AbstractAjaxProcessor, {

	QUEUE_CHECK_SUCCESS_MSG: "Connection to queue was successful",

	QUEUE_EXISTS_MSG: "A shared queue with these configurations already exists.",

	TABLE_COMPARE_NAME: "Perspectium Ready To Run Table Compare",

	SERVICENOW_DIRECT: "servicenow_direct",

	PERSPECTIUM_READY_TO_RUN: "perspectium_ready_to_run",

	mbsUrlModified: false,

	//queueStatus: "",

	run: function() {
		try {
			var sysId = {
				shared_queue_sys_id: "",
				dynamic_share_sys_id: "",
				bulk_share_sys_id: "",
				table_compare_sys_id: ""
			};

			var pspPropertiesNames = {
				encryption_key_out: "com.perspectium.replicator.outbound.encryption_key",
				encryption_key_in: "com.perspectium.replicator.inbound.encryption_key",
				mbs_username: "com.perspectium.quser",
				mbs_password: "com.perspectium.qpassword",
				mbs_server: "com.perspectium.snc_input"
			};

			var formType = this.getParameter("sysparm_formType");
			var formValues = JSON.parse(this.getParameter("sysparm_formValues"));
			var schemas;
			var sharedQueueSysId;
			formValues.mbsServerUrl = this.replaceAMQPSWithHTTPS(formValues);

			switch (String(formType)) {
				case this.SERVICENOW_DIRECT:
					sysId["shared_queue_sys_id"] = "";
					sysId["dynamic_share_sys_id"] = this.createDynamicShare(sharedQueueSysId, formValues.tableName[0]);
					sysId["bulk_share_sys_id"] = this.createBulkShare(sharedQueueSysId, formValues.tableName[0], formValues.agentName, formValues.executeBulkShare, formType);
					schemas = this.generateTableSchemas();
					this.saveReplicatorConfiguration(pspPropertiesNames, formValues, formType);
					if (formValues.saveReadyToRunForm == true || formValues.saveReadyToRunForm == "true") {
						if (this.mbsUrlModified) {
							formValues.mbsServerUrl = this.replaceHttpsWithAmqps();
						}
						this.saveConfiguration(sysId, formType, formValues);
					}
					return schemas;

				case this.PERSPECTIUM_READY_TO_RUN:
					if (this.sharedQueueExists(formValues)) {
						return JSON.stringify({
							error: true,
							status: this.QUEUE_EXISTS_MSG
						});
					}
					sharedQueueSysId = this.createSharedQueue(formValues, formValues);

					/*
					if (sharedQueueSysId == false || sharedQueueSysId == "false") {
						return JSON.stringify({error: true, status: this.queueStatus});
					}*/

					sysId["shared_queue_sys_id"] = sharedQueueSysId;
					sysId["dynamic_share_sys_id"] = this.createDynamicShare(sharedQueueSysId, formValues.tableName[0]);
					sysId["bulk_share_sys_id"] = this.createBulkShare(sharedQueueSysId, formValues.tableName[0], formValues.agentName, formValues.executeBulkShare, formType);

					// only make table compare scheduled job if it doesn't already exist
					if (!this.tableCompareExists()) {
						sysId["table_compare_sys_id"] = this.createTableCompare();
					}

					schemas = this.generateTableSchemas();
					this.saveReplicatorConfiguration(pspPropertiesNames, formValues, formType);
					if (formValues.saveReadyToRunForm == true || formValues.saveReadyToRunForm == "true") {
						if (this.mbsUrlModified) {
							formValues.mbsServerUrl = this.replaceHTTPSWithAMQPS(formValues);
						}
						this.saveConfiguration(sysId, formType, formValues);
					}
					return schemas;
			}

		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error: " + e, " at PerspectiumReadyToRun.run");
		}
		return null;
	},

	decryptValue: function() {
		try {
			var value = this.getParameter("sysparm_key");
			var encrypter = (typeof GlideEncrypter != "undefined") ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
			var unencryptedText = encrypter.decrypt(value);
			return unencryptedText;
		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error: " + e, " at PerspectiumReadyToRun.decryptValue");
		}
	},

	replaceAMQPSWithHTTPS: function(formValues) {
		try {
			if (formValues.mbsServerUrl.includes("amqps://")) {
				formValues.mbsServerUrl = formValues.mbsServerUrl.replace("amqps://", "https://");
				this.mbsUrlModified = true;
			} else if (formValues.mbsServerUrl.includes("amqp://")) {
				formValues.mbsServerUrl = formValues.mbsServerUrl.replace("amqp://", "http://");
				this.mbsUrlModified = true;
			}
			return formValues.mbsServerUrl;
		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error: " + e, " at PerspectiumReadyToRun.replaceAMQPSWithHTTPS");
		}
	},

	replaceHTTPSWithAMQPS: function(formValues) {
		try {
			if (formValues.mbsServerUrl.includes("https://")) {
				formValues.mbsServerUrl = formValues.mbsServerUrl.replace("https://", "amqps://");
			} else if (formValues.mbsServerUrl.includes("http://")) {
				formValues.mbsServerUrl = formValues.mbsServerUrl.replace("http://", "amqp://");
			}
			return formValues.mbsServerUrl;
		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error: " + e, " at PerspectiumReadyToRun.replaceHTTPSWithAMQPS");
		}
	},

	// this function will save values to the properties table
	saveReplicatorConfiguration: function(pspPropertiesNames, formValues, formType) {
		try {
			var pspUtil = new PerspectiumUtil();

			if (formType == this.PERSPECTIUM_READY_TO_RUN) {
				pspUtil.setPspPropertyValue(pspPropertiesNames.encryption_key_out, formValues.encryptionKey);
				pspUtil.setPspPropertyValue(pspPropertiesNames.encryption_key_in, formValues.encryptionKey);
				pspUtil.setPspPropertyValue(pspPropertiesNames.mbs_username, formValues.mbsUserName);
				pspUtil.setPspPropertyValue(pspPropertiesNames.mbs_password, formValues.mbsPassword);
				pspUtil.setPspPropertyValue(pspPropertiesNames.mbs_server, formValues.mbsServerUrl);
			} else if (formType == this.SERVICENOW_DIRECT) {
				pspUtil.setPspPropertyValue(pspPropertiesNames.encryption_key_out, formValues.encryptionKey);
				pspUtil.setPspPropertyValue(pspPropertiesNames.encryption_key_in, formValues.encryptionKey);
			}
		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error: " + e, " at PerspectiumReadyToRun.saveReplicatorConfiguration");
		}
	},

	sharedQueueExists: function(formValues) {
		try {
			var exists = false;
			var gr = new GlideRecord("u_psp_queues");
			gr.addQuery("u_name", "psp.out.replicator." + formValues.snInstance + "_agent");
			gr.addQuery("u_endpoint_url", formValues.mbsServerUrl + "/");
			gr.query();
			if (gr.next()) {
				return true;
			}
			return false;

		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error: " + e, " at PerspectiumReadyToRun.sharedQueueExists");
		}
	},

	tableCompareExists: function() {
		try {
			var gr = new GlideRecord("sysauto");
			gr.addQuery("name", this.TABLE_COMPARE_NAME);
			gr.query();
			if (gr.next()) {
				return true;
			}
			return false;

		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error: " + e, " at PerspectiumReadyToRun.tableCompareExists");
		}
	},

	generateTableSchemas: function() {
		try {
			var tables = ["incident", "sys_journal_field"];
			var schemas = [];
			for (var i = 0; i < tables.length; i++) {
				pspSchemaProcessor = new PSPSchemaProcessor(tables[i]);
				schemas.push({
					tableName: tables[i],
					tableSchema: pspSchemaProcessor.process()
				});
			}
			var json = new JSON();
			return json.encode(schemas);

		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error checking scope: " + e, "PerspectiumReadyToRun.generateTableSchemas");
		}

	},

	createSharedQueue: function(formValues) {
		try {
			var gr = new GlideRecord("u_psp_queues");
			gr.initialize();
			gr.u_name = "psp.out.replicator." + formValues.snInstance + "_agent";
			gr.u_endpoint_url = formValues.mbsServerUrl.replace("amqps", "https");
			gr.u_queue_user = formValues.mbsUserName;
			formValues.mbsPassword = formValues.mbsPassword.replace("encrypt:", "");
			gr.u_queue_password.setDisplayValue(formValues.mbsPassword);
			gr.u_direction = "Share";
			gr.u_active = "true";
			return gr.insert();
			/*
			// verify queue connection is good
			this.queueStatus = this.verifyQueueStatus(sharedQueueSysId);
			if (this.queueStatus != "success") {
				return false;
			}*/
		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error checking scope: " + e, "PerspectiumReadyToRun.createSharedQueue");
		}

	},

	createDynamicShare: function(sharedQueueSysId, tableName) {
		try {
			// iterate through for each table name passed
			var gr = new GlideRecord("psp_replicate_conf");
			gr.initialize();
			gr.table_name = tableName;
			gr.active = "true";
			gr.sync_direction = "Share";
			gr.u_interactive_only = "true";
			gr.u_business_rule_when = "after";
			gr.action_create = "true";
			gr.action_update = "true";
			gr.action_delete = "true";
			gr.u_target_queue = sharedQueueSysId ? sharedQueueSysId : "";
			gr.u_share_journal = "true";
			gr.u_share_attachment = "false";
			// sync functionality will need to be added in later versions of ready to run
			//gr.u_sync_active = "true";
			return gr.insert();

		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error checking scope: " + e, "PerspectiumReadyToRun.createDynamicShare");
		}

	},

	createBulkShare: function(sharedQueueSysId, tableName, agentName, executeBulkShare, formType) {
		try {
			var gr = new GlideRecord("psp_bulk_share");

			// name bulk share
			switch (String(formType)) {
				case this.SERVICENOW_DIRECT:
					gr.u_name = "Ready to Run " + agentName;
					break;

				case this.PERSPECTIUM_READY_TO_RUN:
					gr.u_name = "ServiceNow Direct " + agentName;
					break;
			}

			gr.table_name = tableName;
			gr.u_include_attachment = "false";
			gr.u_share_journal = "true";
			gr.u_target_queue = sharedQueueSysId ? sharedQueueSysId : "";
			var bulkShareSysId = gr.insert();

			// execute bulk share and pass in gr
			if (executeBulkShare) {
				var pspR = new PerspectiumReplicator();
				var startTime = new GlideDateTime();
				pspR.scheduleOnceBulkShareJob(gr, startTime);
			}

			return bulkShareSysId;
		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error checking scope: " + e, "PerspectiumReadyToRun.createBulkShare");
		}

	},

	createTableCompare: function() {
		try {
			var gr = new GlideRecord("sysauto_script");
			gr.name = this.TABLE_COMPARE_NAME;
			gr.active = "true";
			gr.run_type = "periodically";
			gr.sys_scope = "global";
			var gdt = new GlideDateTime("1970-01-01 00:30:00");
			gr.run_period = gdt;
			gr.script = "var pspCompare = new PerspectiumReplicatorTableCompare();pspCompare.getStats();";
			return gr.insert();
		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error checking scope: " + e, "PerspectiumReadyToRun.createTableCompare");
		}

	},

	saveConfiguration: function(sysId, formType, formValues) {
		try {
			if (this.isTableEmpty(formType)) {
				this.insertReadyToRunConfiguration(sysId, formType, formValues);
			} else {
				this.updateReadyToRunConfiguration(this.getConfigRecord("u_psp_configuration", this.getConfigurationSysId(formType)), sysId, formType, formValues);
			}
		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error checking scope: " + e, "PerspectiumReadyToRun.saveConfiguration");
		}

	},

	isTableEmpty: function(configurationType) {
		try {
			var gr = new GlideRecord("u_psp_configuration");
			gr.addQuery("u_configuration_type", configurationType);
			gr.query();
			if (gr.next()) {
				return false;
			}
			return true;
		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error checking scope: " + e, "PerspectiumReadyToRun.isTableEmpty");
		}

	},

	getConfigurationSysId: function(configurationType) {
		try {
			var gr = new GlideRecord("u_psp_configuration");
			gr.addQuery("u_configuration_type", configurationType);
			gr.query();
			if (gr.next()) {
				return gr.sys_id;
			}
			return "";
		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error checking scope: " + e, "PerspectiumReadyToRun.getConfigurationSysId");
		}
	},

	getConfigRecord: function(tableName, sysId) {
		try {
			var gr = new GlideRecord(tableName);
			gr.addQuery(sysId);
			gr.query();
			if (gr.next()) {
				return gr;
			}
			return "";
		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error checking scope: " + e, "PerspectiumReadyToRun.getConfigRecord");
		}
	},

	insertReadyToRunConfiguration: function(sysId, configurationType, formValues) {
		try {
			var gr = GlideRecord("u_psp_configuration");
			gr.u_shared_queue_id = sysId.shared_queue_sys_id;
			gr.u_dynamic_share_id = sysId.dynamic_share_sys_id;
			gr.u_bulk_share_id = sysId.bulk_share_sys_id;
			gr.u_configuration_type = configurationType;
			// possible add table compare later
			gr.u_servicenow_username = formValues.snUserName;
			formValues.snPassword = formValues.snPassword.replace("encrypt:", "");
			gr.u_servicenow_password = formValues.snPassword;
			gr.u_encryption_key = formValues.encryptionKey;
			gr_u_os_type = formValues.osType;
			gr.u_database_type = formValues.dbType;
			gr.u_database_server_url = formValues.dbServerUrl;
			gr.u_database_port = formValues.dbPort;
			gr.u_database_username = formValues.dbUserName;
			gr.u_database_sid = formValues.dbSID;
			formValues.dbPassword = formValues.dbPassword.replace("encrypt:", "");
			gr.u_database_password = formValues.dbPassword;
			gr.u_database_name = formValues.dbName;
			if (configurationType == this.PERSPECTIUM_READY_TO_RUN) {
				gr.u_mbs_server_url = formValues.mbsServerUrl;
				gr.u_mbs_username = formValues.mbsUserName;
				formValues.mbsPassword = formValues.mbsPassword.replace("encrypt:", "");
				gr.u_mbs_password = formValues.mbsPassword;
			}
			gr.insert();
		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error checking scope: " + e, "PerspectiumReadyToRun.insertReadyToRunConfiguration");
		}
	},

	updateReadyToRunConfiguration: function(configurationGlideRecord, sysId, configurationType, formValues) {
		try {
			configurationGlideRecord.u_shared_queue_id = sysId.shared_queue_sys_id;
			configurationGlideRecord.u_dynamic_share_id = sysId.dynamic_share_sys_id;
			configurationGlideRecord.u_bulk_share_id = sysId.bulk_share_sys_id;
			configurationGlideRecord.u_configuration_type = configurationType;
			// possible add table compare later
			configurationGlideRecord.u_servicenow_username = formValues.snUserName;
			formValues.snPassword = formValues.snPassword.replace("encrypt:", "");
			configurationGlideRecord.u_servicenow_password = formValues.snPassword;
			configurationGlideRecord.u_encryption_key = formValues.encryptionKey;
			configurationGlideRecord.u_os_type = formValues.osType;
			configurationGlideRecord.u_database_type = formValues.dbType;
			configurationGlideRecord.u_database_server_url = formValues.dbServerUrl;
			configurationGlideRecord.u_database_port = formValues.dbPort;
			configurationGlideRecord.u_database_username = formValues.dbUserName;
			formValues.dbPassword = formValues.dbPassword.replace("encrypt:", "");
			configurationGlideRecord.u_database_password = formValues.dbPassword;
			configurationGlideRecord.u_database_name = formValues.dbName;
			configurationGlideRecord.u_database_sid = formValues.dbSID;
			if (configurationType == this.PERSPECTIUM_READY_TO_RUN) {
				configurationGlideRecord.u_mbs_server_url = formValues.mbsServerUrl;
				configurationGlideRecord.u_mbs_username = formValues.mbsUserName;
				formValues.mbsPassword = formValues.mbsPassword.replace("encrypt:", "");
				configurationGlideRecord.u_mbs_password = formValues.mbsPassword;
			}
			configurationGlideRecord.update();
		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error checking scope: " + e, "PerspectiumReadyToRun.updateReadyToRunConfiguration");
		}
	},

	loadConfiguration: function() {
		try {
			var tableName = this.getParameter("sysParm_tableName");
			var configurationType = this.getParameter("sysParm_formType");
			var loadFormValuesJson = {};

			if (this.isTableEmpty(configurationType)) {
				return JSON.stringify(this.getInstanceName(loadFormValuesJson));
			}

			var gr = this.getConfigRecord(tableName, this.getConfigurationSysId(configurationType));
			loadFormValuesJson = this.populateDefaultJson(loadFormValuesJson, gr, configurationType);

			if (configurationType == this.PERSPECTIUM_READY_TO_RUN) {
				loadFormValuesJson = this.populateReadyToRunJson(loadFormValuesJson, gr);
			}

			return JSON.stringify(loadFormValuesJson);
		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error checking scope: " + e, "PerspectiumReadyToRun.loadConfiguration");
		}
	},

	getInstanceName: function(loadFormValuesJson) {
		try {
			loadFormValuesJson["instanceName"] = gs.getProperty("instance_name");
			loadFormValuesJson["isEmpty"] = true;
			return loadFormValuesJson;
		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error checking scope: " + e, "PerspectiumReadyToRun.getInstanceName");
		}
	},

	populateDefaultJson: function(loadFormValuesJson, gr, configurationType) {
		try {
			loadFormValuesJson["instanceName"] = gs.getProperty("instance_name");
			loadFormValuesJson["snUsername"] = gr.u_servicenow_username.toString();
			loadFormValuesJson["snPassword"] = gr.u_servicenow_password.toString();
			loadFormValuesJson["encryptionKey"] = gr.u_encryption_key.toString();
			loadFormValuesJson["databaseType"] = gr.u_database_type.toString();
			loadFormValuesJson["databaseUrl"] = gr.u_database_server_url.toString();
			loadFormValuesJson["databasePort"] = gr.u_database_port.toString();
			loadFormValuesJson["databaseUsername"] = gr.u_database_username.toString();
			loadFormValuesJson["databasePassword"] = gr.u_database_password.toString();
			loadFormValuesJson["databaseName"] = gr.u_database_name.toString();
			loadFormValuesJson["isEmpty"] = false;
			return loadFormValuesJson;
		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error checking scope: " + e, "PerspectiumReadyToRun.populateDefaultJson");
		}
	},

	populateReadyToRunJson: function(loadFormValuesJson, gr) {
		try {
			loadFormValuesJson["mbsServerUrl"] = gr.u_mbs_server_url.toString();
			loadFormValuesJson["mbsUserName"] = gr.u_mbs_username.toString();
			loadFormValuesJson["mbsPassword"] = gr.u_mbs_password.toString();
			return loadFormValuesJson;
		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error checking scope: " + e, "PerspectiumReadyToRun.populateReadyToRunJson");
		}
	},

	// verify queue connection is good (this is not currently being used)
	verifyQueueStatus: function(sys_id) {
		try {
			var gr = new GlideRecord("u_psp_queues");
			gr.get(sys_id);
			var pspQ = new PerspectiumQueue();
			// need try catch here for if host is bad
			try {
				result = pspQ.updateQueueStatus(gr);
				// check if success message returned
				if (result.indexOf(this.QUEUE_CHECK_SUCCESS_MSG) != -1) {
					return "success";
				}

			} catch (e) {
				return e;
			}
			return result; // this will be an error message
		} catch (e) {
			var logger = new PerspectiumLogger();
			logger.logError("Error checking scope: " + e, "PerspectiumReadyToRun.verifyQueueStatus");
		}
	},

	type: "PerspectiumReadyToRun"
});