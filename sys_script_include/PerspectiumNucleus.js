/**
 * PerspectiumNucleus is a client-callable script include
 * used for handling the AJAX calls for Nucleus. 
 * 
 * The UI generates a standardized JSON object that is used 
 * to transfer information from the client to the server.
 * 
 * {
 * 		commonDoc: 'the common document key',
 * 		provider: '',
 * 		client: '',
 * 		action: '',
 * 		source: '',
 * 		target: '',
 * 		description: '',
 * 		values: {
 * 			'source field name': {
 * 				'mapping' : '',
 * 				'script' : ''
 * 			},
 * 			'unique-parent' : [
 * 				{
 * 					'name' : '',
 * 					'mapping' : '',
 * 					'script' : '',
 * 				}
 * 			]
 * 		}
 * }
 */
var PerspectiumNucleus = Class.create();
PerspectiumNucleus.prototype = {
	initialize: function () {
		this.psp = new Perspectium();
		this.logger = new PerspectiumLogger();
		this.pspU = new PerspectiumUtil();
		// TODO: revisit authentication
		this.passphrase = this.pspU.getPspPropertyValue("com.perspectium.qpassphrase", "");
		this.user = this.pspU.getPspPropertyValue('com.perspectium.quser', '');
		this.pass = this.pspU.getPspPropertyValue('com.perspectium.qpassword', '');
		this.endpoint = this.pspU.getPspPropertyValue('com.perspectium.snc_input', '');
		this.vhost = this.getVhost();
		this.instance = gs.getProperty('instance_name', 'unregistered');
		// statics
		this.SIAM_ENDPOINT = 'siamendpoint';
		this.SIAM_MAPPING = 'siammapping';
		this.AUTH_ERROR_MSG = 'Could not get mapping information from MBS. User is not authenticated. Please ensure customer credentials are setup properly.'
		this.DOMAIN_ERROR_MSG = "ERROR: The number of domains in the domain table is greater than 1. This means that this is either the global or a parent domain. Please check to ensure this customer's domain is defined correctly.";
		this.PSP_QUSER = 'psp_quser';
		this.PSP_QPASSWORD = 'psp_qpassword';
		this.PASSPHRASE = 'passphrase';
		this.PSP_INSTANCE = 'psp_instance';
		this.PSP_ACTIONTYPE = 'psp_actiontype';
		this.PSP_PROVIDER = 'psp_provider';
		this.PSP_CLIENT = 'psp_client';
		this.PSP_ACTION = 'psp_action';
		this.PSP_SOURCE = 'psp_source';
		this.PSP_TARGET = 'psp_target';
		this.PSP_DESCRIPTION = 'psp_description';
		this.PSP_INPUT_QUEUE = 'psp_input_queue';
	},

	/**
	 * Save SIAM configuration profile to MBS 
	 */
	saveSIAMConfig: function (config) {
		var encrypter = new GlideEncrypter();
		var endpoint = this.validateEndpointUrl(this.endpoint) + this.SIAM_ENDPOINT;
		var postMethod = this.createPostHttpMethod(endpoint, this.vhost, this.user, this.pass, this.passphrase, this.instance);
		var httpClient = this.psp.getHttpClient();
		var payload = this.createConfigJSON(config);
		var result;

		postMethod.addRequestHeader("Content-Type", "text/json");
		postMethod.setRequestEntity(new Packages.org.apache.commons.httpclient.methods.StringRequestEntity(JSON.stringify(payload)));
		result = httpClient.executeMethod(postMethod);
		postMethod.releaseConnection();

		if (result != 200 || result == undefined) {
			this.logger.logError('There was an error saving SIAM configuration to MBS. Reason: ' + this.generateStatusMessage(result), 'PerspectiumNucleus.saveSiamConfig');
			throw new Error();
		}
	},

	getSIAMConfig: function (provider, client) {
		var endpoint = this.validateEndpointUrl(this.endpoint) + this.SIAM_ENDPOINT;
		var getMethod = this.createGetHttpMethod(endpoint, this.vhost, this.user, this.pass, this.passphrase, this.instance);
		var httpClient = this.psp.getHttpClient();
		var json = new JSON();
		var result, payload;

		getMethod.setRequestHeader(this.PSP_ACTIONTYPE, 'get_config'); 
		getMethod.setRequestHeader('psp_provider', provider); // query param
		getMethod.setRequestHeader('psp_client', client); // query param 
		result = httpClient.executeMethod(getMethod);
		payload = getMethod.getResponseBodyAsString();
		getMethod.releaseConnection();

		if (result != 200 || result == undefined) {
			var err = "Error getting SIAM configuration profile from MBS. Reason: " + this.generateStatusMessage(result);
			this.logger.logError(err, 'PerspectiumNucleus.getSIAMConfig()');
			throw new Error();
		}

		
		var config = json.decode(payload);
		this.upsertEndpointConfigValue(config);
	},

	/**
	 * Get SIAM configuration profiles for the given client and upsert
	 */
	getSIAMConfigs: function () {
		var endpoint = this.validateEndpointUrl(this.endpoint) + this.SIAM_ENDPOINT;
		var getMethod = this.createGetHttpMethod(endpoint, this.vhost, this.user, this.pass, this.passphrase, this.instance);
		var httpClient = this.psp.getHttpClient();
		var json = new JSON();
		var result, payload;

		getMethod.setRequestHeader(this.PSP_ACTIONTYPE, 'get_configs');
		result = httpClient.executeMethod(getMethod);
		payload = getMethod.getResponseBodyAsString();
		getMethod.releaseConnection();

		if (result != 200 || result == undefined) {
			var err = "Error getting SIAM configuration profiles from MBS. Reason: " + this.generateStatusMessage(result);
			this.logger.logError(err, "PerspectiumNucleus.getSIAMConfigs()");
			throw new Error();
		}

		
		var endpoints = json.decode(payload);
		this.upsertEndpointConfigValues(endpoints);
	},

	upsertEndpointConfigValues: function (endpoints) {
		for (i = 0; i < endpoints.length; i++) {
			this.upsertEndpointConfigValue(endpoints[i]);
		}
	},

	upsertEndpointConfigValue: function (endpoint) {
		var gr = new GlideRecord('u_psp_siam_endpoint');
		gr.addQuery('u_provider', endpoint.u_provider);
		gr.addQuery('u_client', endpoint.u_client);
		gr.query();
		gr.setWorkflow(false);

		if (!gr.next()) {
			gr.initialize();
			gr.insert();
		}

		gr.u_provider = endpoint.u_provider;
		gr.u_client = endpoint.u_client;
		gr.u_provider_endpoint = endpoint.u_provider_endpoint;
		gr.u_provider_endpoint_user = endpoint.u_provider_endpoint_user;
		gr.u_provider_endpoint_password = endpoint.u_provider_endpoint_password;
		gr.u_attributes = endpoint.u_attributes;
		gr.u_connection_timeout = endpoint.u_connection_timeout;
		gr.u_max_retries = endpoint.u_max_retries;
		gr.u_retry_wait_time = endpoint.u_retry_wait_time;
		gr.u_queue_request = endpoint.u_queue_request;
		gr.u_client_dn = endpoint.u_client_dn;
		gr.update();

		this.logger.logDebug('SIAM Endpoint Configuration with provider: ' + gr.u_provider + ' and client: ' + gr.u_client + ' has been updated with the most recent MBS version.');
	},

	/**
	 * create a SIAM configuration profile JSON
	 * TODO
	 * - if this is migrated out of service now context, improve error handling for undefined properties
	 * - change Json name to JSON
	 */
	createConfigJSON: function (config) {
		var encrypter = new GlideEncrypter();
		var json = {};

		json.provider = config.u_provider.toString();
		json.client = config.u_client.toString();
		json.provider_endpoint = config.u_provider_endpoint.toString();
		json.provider_endpoint_user = config.u_provider_endpoint_user.toString();
		json.provider_endpoint_password = encrypter.decrypt(config.u_provider_endpoint_password.toString());
		json.attributes = config.u_attributes.toString();
		json.connection_timeout = config.u_connection_timeout.toString();
		json.max_retries = config.u_max_retries.toString();
		json.retry_wait_time = config.u_retry_wait_time.toString();
		json.queue_request = config.u_queue_request.toString();
		json.client_dn = config.u_client_dn.toString();

		return json;
	},

	/**
	 * save mapping information to MBS
	 * @param payload: json posted from the client script containing configuration profile values
	 */
	saveMapping: function (payload) {
		var pspA = new PerspectiumAuth();
		var encrypter = new GlideEncrypter();
		var mapping = typeof payload.mapping == 'object' ? JSON.stringify(payload.mapping) : payload.mapping;
		var endpoint = this.validateEndpointUrl(this.endpoint) + this.SIAM_MAPPING;
		var postMethod = this.createPostHttpMethod(endpoint, this.vhost, this.user, this.pass, this.passphrase, this.instance);
		var httpClient = this.psp.getHttpClient();
		var result;

		postMethod = this.setCreateMappingHeaderVals(postMethod, payload);
		postMethod.setRequestHeader(this.PSP_INPUT_QUEUE, 'psp.in.servicenow');
		postMethod.setRequestEntity(new Packages.org.apache.commons.httpclient.methods.StringRequestEntity(mapping));
		result = httpClient.executeMethod(postMethod);
		postMethod.releaseConnection();

		if (result != 200 || result == undefined) {
			this.logger.logError('There was an error saving SIAM mapping to MBS. Reason: ' + this.generateStatusMessage(result), 'PerspectiumNucleus.saveSiamMapping');
			throw new Error();
		}
	},

	/**
	 * get mapping information
	 * @param payload: json posted from client script containing configuration profile values.
	 */
	getMapping: function (payload) {
		var encrypter = new GlideEncrypter();
		var pspA = new PerspectiumAuth();
		var json = new JSON();
		var endpoint = this.validateEndpointUrl(this.endpoint) + this.SIAM_MAPPING;
		var getMethod = this.createGetHttpMethod(endpoint, this.vhost, this.user, this.pass, this.passphrase, this.instance);
		var httpClient = this.psp.getHttpClient();
		var result, body;

		getMethod = this.setGetMappingHeaderVals(getMethod, payload);
		getMethod.setRequestHeader(this.PSP_INPUT_QUEUE, 'psp.in.servicenow');
		result = httpClient.executeMethod(getMethod);
		body = getMethod.getResponseBodyAsString();
		getMethod.releaseConnection();

		if (result != 200 || result == undefined) {
			this.logger.logError('Error getting SIAM mapping information from MBS. Reason: ' + this.generateStatusMessage(result));
			throw new Error();
		}

		return json.decode(body)[0]; // GET returns array with flag, ignore
	},

	/**
	 * create a POST http method
	 */
	createPostHttpMethod: function (endpoint, vhost, user, pass, passphrase, instance) {
		var postMethod = new Packages.org.apache.commons.httpclient.methods.PostMethod(endpoint);
		return this.setHttpHeaderVals(postMethod, endpoint, vhost, user, pass, passphrase, instance);
	},

	/**
	 * create a GET http method
	 */
	createGetHttpMethod: function (endpoint, vhost, user, pass, passphrase, instance) {
		var getMethod = new Packages.org.apache.commons.httpclient.methods.GetMethod(endpoint);
		return this.setHttpHeaderVals(getMethod, endpoint, vhost, user, pass, passphrase, instance);
	},

	/**
	 * set header vals for authentication
	 */
	setHttpHeaderVals: function (httpMethod, endpoint, vhost, user, pass, passphrase, instance) {
		httpMethod.setRequestHeader(this.PSP_QUSER, vhost + '/' + user);
		httpMethod.setRequestHeader(this.PSP_QPASSWORD, pass);
		httpMethod.setRequestHeader(this.PASSPHRASE, passphrase);
		httpMethod.setRequestHeader(this.PSP_INSTANCE, instance);
		return httpMethod;
	},

	/**
	 * set header vals for GET mapping
	 */
	setGetMappingHeaderVals: function (httpMethod, payload) {
		httpMethod.setRequestHeader(this.PSP_ACTIONTYPE, 'getmapping');
		httpMethod.setRequestHeader(this.PSP_PROVIDER, payload.provider);
		httpMethod.setRequestHeader(this.PSP_CLIENT, payload.client);
		httpMethod.setRequestHeader(this.PSP_ACTION, payload.action);
		httpMethod.setRequestHeader(this.PSP_SOURCE, payload.source);
		httpMethod.setRequestHeader(this.PSP_TARGET, payload.target);
		return httpMethod;
	},

	/**
	 * set header vals for CREATE mapping
	 */
	setCreateMappingHeaderVals: function (httpMethod, payload) {
		httpMethod.setRequestHeader(this.PSP_ACTIONTYPE, 'createmapping');
		httpMethod.setRequestHeader(this.PSP_PROVIDER, payload.provider);
		httpMethod.setRequestHeader(this.PSP_CLIENT, payload.client);
		httpMethod.setRequestHeader(this.PSP_ACTION, payload.action);
		httpMethod.setRequestHeader(this.PSP_SOURCE, payload.source);
		httpMethod.setRequestHeader(this.PSP_TARGET, payload.target);
		httpMethod.setRequestHeader(this.PSP_DESCRIPTION, payload.description);
		return httpMethod;
	},

	validateEndpointUrl: function (endpoint) {
		if (!endpoint.endsWith('/')) {
			endpoint += '/';
		}

		return endpoint;
	},

	getVhost: function () {
		var gr = new GlideRecord('domain');
		gr.query();

		if (gr.next()) {
			return gr.name.toLowerCase();
		}

		this.logger.logError(this.DOMAIN_ERROR_MSG, "PerspectiumNucleus.getVhost()");
		return "";
	},

	getDomainSysId: function () {
		var gr = new GlideRecord('domain');
		gr.query();
		var sysId = "";
		if (gr.getRowCount() > 1) {
			this.logger.logError(this.DOMAIN_ERROR_MSG, "PerspectiumNucleus.getDomain()");
			return "";
		}

		if (gr.next()) {
			sysId = gr.sys_id;
		}

		return sysId;
	},

	generateStatusMessage: function (statusCode) {
		var msg = "";

		switch (statusCode) {
			case 401:
				msg = "Unauthorized access to MBS endpoint. Please ensure that the customer's vhost and credentaials are setup accurately in the domain and Perspectium properties.";
				break;
			case 404:
				msg = "The requested entity was not found on MBS. Please ensure your query parameters are correct such as provider and client.";
				break;
			case 500:
				msg = "Internal server error. Please check MBS logs for more information.";
				break;
			default:
				msg = "A generic error has been thrown. Please check MBS logs for more information.";
				break;
		}

		return msg;
	},

	type: 'PerspectiumNucleus'
};