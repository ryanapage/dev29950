var PerspectiumNucleusMapping = Class.create();
PerspectiumNucleusMapping.prototype = {
    initialize: function () {
        this.logger = new PerspectiumLogger();
        this.pspUtil = new PerspectiumUtil();
    },

    /** 
     * TODO
     * - error handling?
     */
    generateCommonDoc: function (docType, docJson) {
        var commonDoc = '<' + docType + '>';
        for (var key in docJson) {
            if (docJson[key] != "" && docJson[key] != undefined) {
                commonDoc += this.generateCommonTag(key, docJson[key]);
            } else {
                commonDoc += this.generateCommonTag(key);
            }
        }
        commonDoc += '</' + docType + '>';
        return new XMLDocument(commonDoc);
    },

    generateCommonTag: function (fieldMap, mapping) {
        var commonTag = '<' + fieldMap + '>';
        if (mapping != undefined) {
            commonTag += mapping;
        }
        commonTag += '</' + fieldMap + '>';
        return commonTag;
    },

    /**
     * create normalized json for common xml document generation
     * TODO
     * - error handling?
     */
    getCommonJson: function (type, commonDoc, values) {
        var commonDocFields = this.getCommonDocFields(commonDoc);
        var json = {};

        // pull common document fields
        for (var i = 0; i < commonDocFields.length; i++) {
            json[commonDocFields[i]] = "";
        }

        for (var key in values) { // create template mappings
            json[key] = this.generateJsonPathByType(type, key, values[key]);
        }

        return json;
    },

    generateJsonPathByType: function (type, key, value) {
        // TODO: add the rest
        var JIRA = 'jira';
        switch (type) {
            case JIRA:
                return this.generateJiraJsonPath(key, value);
        }
    },

    generateJsonCdataPath: function (jsonPath) {
        return '<![CDATA[' + this.generateJsonPath(jsonPath) + ']]>';
    },

    generateJiraJsonPath: function (key, value) {
        var path = 'issue.fields.' + value;
        if (key == 'issuetype' || key == 'priority') {
            path += '.name';
        }
        return this.generateJsonPath(path);
    },

    generateJsonPath: function (jsonPath) {
        return '${JSONPATH:$.' + jsonPath + '}';
    },

    generateXPath: function (commonName, field) {
        return "${XPATH:/" + commonName + '/' + field + '}';
    },

    getCommonDocFields: function (commonDoc) {
        // TODO: add the rest
        var COMMON_INCIDENT = "common_incident";

        switch (commonDoc) {
            case COMMON_INCIDENT:
                return this.getFields('u_psp_common_incident');
        }
    },

    /**
     * TODO
     * - abstract to util
     */
    getFields: function (table) {
        var i = 0;
        var fields = [];
        var gr = new GlideRecord('sys_dictionary');
        gr.addQuery('name', table)
        gr.query();
        while (gr.next()) {
            var element = gr.element.toString();

            if (element.startsWith('u_')) {
                element = element.slice(2);
            }

            if (element != "")
                fields.push(element);
        }
        return fields;
    },

    type: 'PerspectiumNucleusMapping'
};