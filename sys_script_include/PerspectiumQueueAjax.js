var PerspectiumQueueAjax = Class.create();
PerspectiumQueueAjax.prototype = Object.extendsObject(AbstractAjaxProcessor, {
	getAllQueuesStatus: function() {
		var pspQ = new PerspectiumQueue();
		var qgr = new GlideRecord("u_psp_queues");
		qgr.orderBy("u_direction");
		qgr.query();
		
		while (qgr.next()) {
			var result = pspQ.updateQueueStatus(qgr);
			var queueEl = this.newItem("queue");
			queueEl.setAttribute("name", qgr.u_name);
			queueEl.setAttribute("direction", qgr.u_direction);
			queueEl.setAttribute("url", qgr.u_endpoint_url);
			queueEl.setAttribute("status", result);
			queueEl.setAttribute("monitor", qgr.u_monitor);
			queueEl.setAttribute("recThreshold", qgr.u_record_alert_threshold);
			queueEl.setAttribute("monitorStatus", qgr.u_monitor_status);
			queueEl.setAttribute("connThreshold", qgr.u_connection_alert_threshold.getDisplayValue());
		}
	},
		
    type: 'PerspectiumQueueAjax'
});