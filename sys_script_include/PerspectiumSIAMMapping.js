var PerspectiumSIAMMapping = Class.create();
PerspectiumSIAMMapping.prototype = Object.extendsObject(AbstractAjaxProcessor, {
	
	setRequestHeaders: function(httpMethod, user, password, client, provider, action, source, target, description, actiontype, inputqueue) {
		httpMethod.setRequestHeader("psp_input_queue", inputqueue);
		httpMethod.setRequestHeader("psp_quser", user);
        httpMethod.setRequestHeader("psp_qpassword", password);
        httpMethod.setRequestHeader("psp_provider", provider);
        httpMethod.setRequestHeader("psp_client", client);
        httpMethod.setRequestHeader("psp_action", action);
		httpMethod.setRequestHeader("psp_source", source);
		httpMethod.setRequestHeader("psp_target", target);
		httpMethod.setRequestHeader("psp_description", description);
		httpMethod.setRequestHeader("psp_actiontype", actiontype);
	},
	
	getUserPassword: function(fieldToQuery, url, provider) {
		var pgr = new GlideRecord("u_psp_queues");
		var inputqueue = "psp.in.siam.client." + provider;
        pgr.addQuery("u_name", inputqueue);
		pgr.addQuery("u_endpoint_url", url);
        pgr.addQuery("u_active", true);
        pgr.addQuery("u_direction", "Share");
        pgr.query();		
		
		if (pgr.next()) {
			if (fieldToQuery === "password") { 
				var encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
				var password = encrypter.decrypt(pgr.u_queue_password);
				return password;
			} else {
				var user = pgr.u_queue_user.toString();
				return user;
			}
		}
	},
	
	getInstance: function() {
		var instance = this.newItem("instance");
		instance.setAttribute("name", gs.getProperty("instance_name", "unregistered"));
	},
	
	// this function will be called when we want to display the table of available record options
	getRecords: function() {
		var encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
		var snc_input = this.getParameter('sysparm_url');
        var instance = gs.getProperty("instance_name", "unregistered");

		if (!snc_input.endsWith('/')) {
			snc_input += "/";
		}

        var connectionUrl = snc_input + "siammapping"; 
        var getMethod = new Packages.org.apache.commons.httpclient.methods.GetMethod(connectionUrl);
		var client = this.getParameter('sysparm_client');
		var provider = this.getParameter('sysparm_provider');
		var action = this.getParameter('sysparm_action');
		var source = this.getParameter('sysparm_source');
		var target = this.getParameter('sysparm_target');
		var inputqueue = "psp.in.siam.client." + provider;
		var user = this.getUserPassword("user", snc_input, provider);
		var password = this.getUserPassword("password", snc_input, provider);
		this.setRequestHeaders(getMethod, user, password, client, provider, "", "", "","", "getrecords", inputqueue);
		
		var psp = new Perspectium();
        // sending the request
        var httpClient = psp.getHttpClient();  
        var result = httpClient.executeMethod(getMethod);
		
		if (result != 200) {
			getMethod.releaseConnection();
            throw new Error("HTTP GET failed: " + result + " for URL: " + snc_input);
		}
		
        var rstr = getMethod.getResponseBodyAsString();
        getMethod.releaseConnection();
        var parser = new JSONParser();
        var parsedData = parser.parse(rstr);
        var length = parsedData.length;
		
		for (var i = 0; i < length; i++) {
			var record = this.newItem("record");
			record.setAttribute("source", parsedData[i].source);
			record.setAttribute("target", parsedData[i].target);
			record.setAttribute("action", parsedData[i].action);
		}
	},
	
	getVersionedMapping: function () {
		var encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
		var snc_input = this.getParameter('sysparm_url');
        var instance = gs.getProperty("instance_name", "unregistered");
		
		if (!snc_input.endsWith('/')) {
			snc_input += "/";
		}
		var connectionUrl = snc_input + "siammapping";

        var getMethod = new Packages.org.apache.commons.httpclient.methods.GetMethod(connectionUrl);
		var provider = this.getParameter('sysparm_provider');
		
		var inputqueue = "psp.in.siam.client." + provider;
		var psp_id = this.getParameter('sysparm_pspId');  // retrieves psp_id that references a record from psp_update_version
		var user = this.getUserPassword("user", snc_input, provider);
		var password = this.getUserPassword("password", snc_input, provider);

		this.setRequestHeaders(getMethod, user, password, "", "", "", "", "","", "getversionedmapping", inputqueue);
		getMethod.setRequestHeader("psp_id", psp_id);
		
		var psp = new Perspectium();
        var httpClient = psp.getHttpClient();  
		// check if result != 200 --> return null (this.newitem .. error message)

        var result = httpClient.executeMethod(getMethod);
		
		if (result != 200) {
			getMethod.releaseConnection();
            throw new Error("HTTP GET failed: " + result + " for URL: " + connectionUrl);
		}		
		
        var rstr = getMethod.getResponseBodyAsString();
        getMethod.releaseConnection();

        var parser = new JSONParser();
        var parsedData = parser.parse(rstr);

        var versionedMapping = this.newItem("versionedMapping");
		var parsedMappingRecord = parser.parse(parsedData[0].record);
	
		versionedMapping.setAttribute("versionedMapping", parsedMappingRecord.mapping);	
	},
	
	getVersionedRecords: function() {
		var encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
		var snc_input = this.getParameter('sysparm_url');
        var instance = gs.getProperty("instance_name", "unregistered");

		if (!snc_input.endsWith('/')) {
			snc_input += "/";
		}

        var connectionUrl = snc_input + "siammapping"; 
        var getMethod = new Packages.org.apache.commons.httpclient.methods.GetMethod(connectionUrl);
		var client = this.getParameter('sysparm_client');
		var provider = this.getParameter('sysparm_provider');
		var action = this.getParameter('sysparm_action');
		var source = this.getParameter('sysparm_source');
		var target = this.getParameter('sysparm_target');
		var inputqueue = "psp.in.siam.client." + provider;
		var user = this.getUserPassword("user", snc_input, provider);
		var password = this.getUserPassword("password", snc_input, provider);
		this.setRequestHeaders(getMethod, user, password, client, provider, action, source, target,"", "getversionedrecords", inputqueue);
		
		var psp = new Perspectium();
        // sending the request
        var httpClient = psp.getHttpClient();  
        var result = httpClient.executeMethod(getMethod);
		
		if (result != 200) {
			getMethod.releaseConnection();
            throw new Error("HTTP GET failed: " + result + " for URL: " + snc_input);
		}
		
        var rstr = getMethod.getResponseBodyAsString();

        getMethod.releaseConnection();
        var parser = new JSONParser();
        var parsedData = parser.parse(rstr);
        var length = parsedData.length;
		
		for (var i = 0; i < length; i++) {
			var versionedRecord = this.newItem("versionedRecord");
			versionedRecord.setAttribute("psp_id", parsedData[i].psp_id);
			versionedRecord.setAttribute("created", parsedData[i].psp_timestamp);
			versionedRecord.setAttribute("version", parsedData[i].version);
			versionedRecord.setAttribute("sourceTable", parsedData[i].source_table);
			versionedRecord.setAttribute("versionedMapping", parsedData[i].versioned_mapping);
		}
	},
	
	getMappingAndVersionedRecords: function() {
		var encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
		var snc_input = this.getParameter('sysparm_url');
        var instance = gs.getProperty("instance_name", "unregistered");
		
		if (!snc_input.endsWith('/')) {
			snc_input += "/";
		}
		var connectionUrl = snc_input + "siammapping";

        var getMethod = new Packages.org.apache.commons.httpclient.methods.GetMethod(connectionUrl);
		var client = this.getParameter('sysparm_client');
		var provider = this.getParameter('sysparm_provider');
		var action = this.getParameter('sysparm_action');
		var source = this.getParameter('sysparm_source');
		var target = this.getParameter('sysparm_target');
		var inputqueue = "psp.in.siam.client." + provider;
		var user = this.getUserPassword("user", snc_input, provider);
		var password = this.getUserPassword("password", snc_input, provider);
		
		this.setRequestHeaders(getMethod, user, password, client, provider, action, source, target,"", "getmapping", inputqueue);
		var psp = new Perspectium();
        var httpClient = psp.getHttpClient();  
		// check if result != 200 --> return null (this.newitem .. error message)
		
        var result = httpClient.executeMethod(getMethod);
		
		if (result != 200) {
			getMethod.releaseConnection();
            throw new Error("HTTP GET failed: " + result + " for URL: " + connectionUrl);
		}		
		
        var rstr = getMethod.getResponseBodyAsString();
        getMethod.releaseConnection();

        var parser = new JSONParser();
        var parsedData = parser.parse(rstr);

        var length = parsedData.length;
        var mapping = this.newItem("mapping");
		var psp_id = this.newItem("psp_id");
		psp_id.setAttribute("psp_id", parsedData[0].psp_id);

		mapping.setAttribute("mapping", parsedData[0].mapping);  // index 0 willl always have the mapping
		
		/*if (parsedData[2]) {
			mapping.setAttribute("mapping", parsedData[2]);
		}*/
		
		// parsedData [1] has "true"/"false" flag.. not sure if stil needed
		var parsedVersionedMapping;
		for (var i = 2; i < length; i++) {
			var versionedRecord = this.newItem("versionedRecord");
			versionedRecord.setAttribute("psp_id", parsedData[i].psp_id);
			versionedRecord.setAttribute("created", parsedData[i].psp_timestamp);
			versionedRecord.setAttribute("version", parsedData[i].version);
			versionedRecord.setAttribute("sourceTable", parsedData[i].source_table);
			
			parsedVersionedMapping =  parser.parse(parsedData[i].record);
			versionedRecord.setAttribute("versionedMapping", parsedVersionedMapping.mapping);
		}
		
		var defaultFlag = this.newItem("defaultFlag");
		defaultFlag.setAttribute("flag", parsedData[1]);

		var source = this.newItem("source");
		var target = this.newItem("target");
		var description = this.newItem("description");

		source.setAttribute("sourceValue", parsedData[0].source);
		target.setAttribute("targetValue", parsedData[0].target);
		description.setAttribute("descriptionValue", parsedData[0].description);
    },
   
	getClients: function() {
		var psp = new Perspectium();
        var encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
		var snc_input = this.getParameter('sysparm_url');
		
		if (!snc_input.endsWith('/')) {
			snc_input += "/";
		}

		var provider = this.getParameter('sysparm_provider');	
        var pgr = new GlideRecord("u_psp_queues");
		var inputqueue = "psp.in.siam.client." + provider;
		var user = this.getUserPassword("user", snc_input, provider);
		var password = this.getUserPassword("password", snc_input, provider);

        var connectionUrl = snc_input + "siammapping";
        var getMethod = new Packages.org.apache.commons.httpclient.methods.GetMethod(connectionUrl);
		this.setRequestHeaders(getMethod,user, password, "", provider, "", "", "","", "getclients", inputqueue);
		
        var httpClient = psp.getHttpClient();  
        var results = httpClient.executeMethod(getMethod);

		if (results != 200) {
			getMethod.releaseConnection();
            throw new Error("HTTP GET failed: " + results + " for URL: " + url);
		}
		
        var rstr = getMethod.getResponseBodyAsString();
		var p = new JSONParser();
		var prstr = p.parse(rstr);

        getMethod.releaseConnection();
		var clients = this.newItem("clients");

		if (prstr.equals("failed")) {
			clients.setAttribute("access", "denied");
		} else {
			clients.setAttribute("access", "granted");
			var parser = new JSONParser();
			var parsedData = parser.parse(rstr);
			var length = parsedData.length;

			clients.setAttribute("client0", gs.getProperty("instance_name", "unregistered"));
		}
	},

    getProviders: function() {
        var result = this.newItem("result");
		var i = 0;
        var pgr = new GlideRecord("u_psp_queues");
        pgr.addQuery("u_name", "CONTAINS", "psp.in.siam.client.");
		var url = this.getParameter('sysparm_url');
		if (!url.endsWith('/')) {
			url += "/";
		}
		pgr.addQuery("u_endpoint_url", url);
        pgr.addQuery("u_active", true);
        pgr.addQuery("u_direction", "Share");
        pgr.query();
		var providersArray = [];

        while (pgr.next()) {
            var prov = pgr.u_name.toString();
			prov = prov.substring(19);
			
			if (providersArray.indexOf(prov) == -1 ) {
				providersArray.push(prov);
				result.setAttribute("provider"+i, prov);
			}

			i++;
        }
		result.setAttribute("length", Number(i));
    },

	getEndpointURLs: function() {
        var result = this.newItem("result");
		var i = 0;
        var pgr = new GlideRecord("u_psp_queues");
        pgr.addQuery("u_name", "CONTAINS", "psp.in.siam.client.");
        pgr.addQuery("u_active", true);
        pgr.addQuery("u_direction", "Share");
        pgr.query();
		var mbsArray = [];
		
        while (pgr.next()) {
            var url = pgr.u_endpoint_url.toString();
			
			if (mbsArray.indexOf(url) == -1) {
				mbsArray.push(url);
				result.setAttribute("url"+i, url);
			}
			i++;
        }
		result.setAttribute("length", Number(i));
    },
	
	updateMapping: function() { 
		var psp = new Perspectium();
		var encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
        var snc_input = this.getParameter('sysparm_url');
		
		if (!snc_input.endsWith('/')) {
			snc_input += "/";
		}
		
		var result;
		var client = this.getParameter('sysparm_client');
		var provider = this.getParameter('sysparm_provider');
		var inputqueue = "psp.in.siam.client." + provider;
	    var action = this.getParameter('sysparm_action');
		var mapping = this.getParameter('sysparm_mapping');
		var source = this.getParameter('sysparm_source');
		var target = this.getParameter('sysparm_target');
		var connectionUrl = snc_input + "siammapping"; 
		var psp_id = this.getParameter('sysparm_pspId');
        var postMethod = new Packages.org.apache.commons.httpclient.methods.PostMethod(connectionUrl);
		var user = this.getUserPassword("user", snc_input, provider);
		var password = this.getUserPassword("password", snc_input, provider);
		
		if (client.equals("default")) {
			client = gs.getProperty("instance_name", "unregistered");
			postMethod.setRequestHeader("psp_qupdate", "false");
			postMethod.setRequestHeader("psp_qcreate", "true");
			postMethod.setRequestHeader("psp_qdescription", "");			
		} else{
			postMethod.setRequestHeader("psp_qupdate", "true");
			postMethod.setRequestHeader("psp_qcreate", "false");
		}

		this.setRequestHeaders(postMethod, user, password, client, provider, action, source, target,"", "updatemapping", inputqueue);
	    postMethod.setRequestEntity(new Packages.org.apache.commons.httpclient.methods.StringRequestEntity(mapping));
		postMethod.setRequestHeader("psp_id", psp_id);

        var httpClient = psp.getHttpClient();  
		result = httpClient.executeMethod(postMethod);
		
		if (result != 200) {
			postMethod.releaseConnection();
            throw new Error("HTTP POST failed: " + result + " for URL: " + connectionUrl);
		}

        postMethod.releaseConnection();
	},
	
	createMapping: function() {
	
		var psp = new Perspectium();
		var encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
        var snc_input = this.getParameter('sysparm_url');
		
		if (!snc_input.endsWith('/')) {
			snc_input += "/";
		}

		var client = this.getParameter('sysparm_client');
		var provider = this.getParameter('sysparm_provider');
		var inputqueue = "psp.in.siam.client." + provider;
	    var action = this.getParameter('sysparm_action');
		var mapping = this.getParameter('sysparm_mapping');
		var target = this.getParameter('sysparm_target');
		var source = this.getParameter('sysparm_source');
		var description = this.getParameter('sysparm_description');
		var connectionUrl = snc_input + "siammapping"; 
		var user = this.getUserPassword("user", snc_input, provider);
		var password = this.getUserPassword("password", snc_input, provider);	
		
		if (client == "default") {
			client = gs.getProperty("instance_name", "unregistered");
		}
		
        var postMethod = new Packages.org.apache.commons.httpclient.methods.PostMethod(connectionUrl);
		this.setRequestHeaders(postMethod, user, password, client, provider, action, source, target, description, "createmapping", inputqueue);
	    postMethod.setRequestEntity(new Packages.org.apache.commons.httpclient.methods.StringRequestEntity(mapping));

        var httpClient = psp.getHttpClient();  
		var result;
		result = httpClient.executeMethod(postMethod);
		
		if (result != 200) {
			postMethod.releaseConnection();
            throw new Error("HTTP POST failed: " + result + " for URL: " + connectionUrl);
		}		
		
		var rstr = postMethod.getResponseBodyAsString();
		var p = new JSONParser();
		var prstr = p.parse(rstr);
		var createFlag = this.newItem("createFlag");
		
		if (prstr.toString() == "true") {
			createFlag.setAttribute("success", "true");
		} else {
			createFlag.setAttribute("success", "false");
		}

        postMethod.releaseConnection();			
	},
 
    type: 'PerspectiumSIAMMapping'
});