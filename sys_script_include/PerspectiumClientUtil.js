var PerspectiumClientUtil = Class.create();
PerspectiumClientUtil.prototype = Object.extendsObject(AbstractAjaxProcessor, {

	getInstanceName: function() {
		return gs.getProperty("instance_name", "");		
	},
	
	getPspPropertyValue: function () {
		var pspUtil = new PerspectiumUtil();
		return pspUtil.getPspPropertyValue(this.getParameter("sysparm_psp_property"));
	},
	
    type: 'PerspectiumClientUtil'
});