var PerspectiumDynScheduledSync = Class.create();
PerspectiumDynScheduledSync.prototype = {
    initialize: function() {
		this.logger = new PerspectiumLogger();
    },
	
	execute: function() {
		var rgr = new GlideRecord("psp_replicate_conf");
		rgr.addQuery("sync_direction", "share");
		rgr.addQuery("active", "true");
		rgr.addQuery("u_sync_active", "true");
		rgr.addQuery("u_syncing", "false");
		rgr.query();
		while (rgr.next()) {
			this.scheduleSyncUp(rgr);
		}
	},
	
	scheduleSyncUp: function(cgr) {
		
		var now = gs.nowDateTime();
		var time = gs.dateDiff(cgr.u_last_sync_time.getDisplayValue(),now,true);
		var seconds = parseFloat(time);
		var intval = cgr.u_sync_interval * 60;
		
		//this.logger.logDebug(seconds + " < " + intval, "PerspectiumDynScheduledSync.scheduleSyncUp");
		
		if (seconds < intval) {
			return; // not time yet
		}
		
		this.logger.logDebug("scheduling " + cgr.table_name + " to run" , "PerspectiumDynScheduledSync.scheduleSyncUp", cgr);
		
		// schedule a sync in the future
		var pspSO = new ScheduleOnce();
		pspSO.setLabel("Perspectium DSync " + cgr.table_name);		
		pspSO.script = "var pspSS = new PerspectiumDynScheduledSync();";
		pspSO.script += "pspSS.syncUp('" + cgr.sys_id + "');";				
		pspSO.schedule();
		
		// set to syncing to prevent duplicates
		cgr.u_syncing = "true";
		cgr.autoSysFields(false);
		cgr.update();
	},
	
	syncUp: function(repl_sysid) {
		var pspR = new PerspectiumReplicator();
		var psp = new Perspectium();
		var cgr = new GlideRecord("psp_replicate_conf");
		if (!cgr.get(repl_sysid)) {
			this.logger.logError("Unable to load psp_replicate_conf " + repl_sysid);
			return;
		}
		
		//check for run_as user
		var curUser = "";
		if (!cgr.u_run_as.isNil()) {
			curUser = gs.getSession().impersonate(cgr.u_run_as);
		}
		
		var now = gs.nowDateTime();
		
		var tgr = new GlideRecord(cgr.table_name);
		tgr.addQuery("sys_updated_on", ">=", cgr.u_last_sync_time);
		tgr.query();
		var rkount = 0;
		while (tgr.next()) {
			var sumCounts = true;
			//Share the base table
			if (cgr.u_share_base_table_only == "true" || cgr.u_share_base_table_only == true){
				pspR.shareRecord(tgr, cgr.table_name, "bulk", cgr.sys_id, null, sumCounts);
			}
			//Share the child table
			else{
				var childTableName = tgr.getTableName();
				if (tgr.isValidField("sys_class_name") && tgr.sys_class_name && !tgr.sys_class_name.isNil()){
					childTableName = tgr.sys_class_name;
				}

				var ngr = new GlideRecord(childTableName);
				if (ngr.get(tgr.sys_id)){
					pspR.shareRecord(ngr, cgr.table_name, "bulk", cgr.sys_id, null, sumCounts);
				}
			}
			rkount++;
		}		
		
		for (var a in pspR.tableCounter) {
			var tableCount = pspR.tableCounter[a];
			psp.sendCounter(a, tableCount.count, "counter");
			psp.sendCounter(a + ".bytes", tableCount.bytes, "counter");
		}
		
		if (!cgr.u_run_as.isNil() && curUser != "") { 
			gs.getSession().impersonate(curUser);
		}
		
		this.exitSyncUp(cgr, now, rkount);
	},
	
	exitSyncUp: function(cgr, now, rkount){
		var retries = 3;
		var released = false;
		while (!released && retries > 0){
			cgr.u_last_sync_time = now;
			cgr.u_last_sync_recs = rkount;
			cgr.u_syncing = "false";
			cgr.u_auto_scheduled_sync_reset = "false";
			cgr.update();
			
			retries--;
			released = this.releasedLock(cgr);
		}
			
		if (!released){
			this.logger.logError("Failed to release Scheduled Sync Up lock, aborting", "PerspectiumDynScheduledSync.syncUp", cgr);
		}
		
	},
	
	releasedLock: function(cgr) {
		// Return true if u_syncing is false
		var gr = new GlideRecord("psp_replicate_conf");
		gr.addQuery("sys_id", cgr.sys_id);
		gr.query();
		return (gr.next() && !gr.u_syncing);
	},
	

    type: 'PerspectiumDynScheduledSync'
};