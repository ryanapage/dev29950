var PerspectiumBulkShare = Class.create();
PerspectiumBulkShare.prototype = {
    initialize: function(bsysid, condition, name) {		
		var bgr;
		this.resetSysIdsOption = true;
		if (typeof bsysid === 'string') {				
			bgr = new GlideRecord('psp_bulk_share');
			bgr.addQuery('sys_id', bsysid);
			bgr.queryNoDomain();
			if (!bgr.next()) {
				return;
			}
		}
		// for backwards compatibility where we pass in bulk share GlideRecord itself
		else {
			bgr = bsysid;
			// set option to reset sys_ids only checkbox as previous version passing GlideRecord didn't
			this.resetSysIdsOption = false;
		}
		
		this.bgr = bgr;
		this.bgr_sys_id = bgr.sys_id;
		this.logger = new PerspectiumLogger();
		this.psp = new Perspectium();
		this.pspR = new PerspectiumReplicator();
		this.encryption = new PerspectiumEncryption();
		
		this.pspUtil = new PerspectiumUtil();
		this.addDisplayValues = this.pspUtil.getPspPropertyValue("com.perspectium.replicator.add_display_values", "false");
		this.currentFieldsOnly = this.pspUtil.getPspPropertyValue("com.perspectium.replicator.share_current_fields", "false");
		this.bulkShareQueryLimit = parseInt(this.pspUtil.getPspPropertyValue("com.perspectium.bulk_share_query_limit", "1000"));
				
		this.recordSerializer = (typeof GlideRecordXMLSerializer != 'undefined') ? new GlideRecordXMLSerializer() : new Packages.com.glide.script.GlideRecordXMLSerializer();
		
		if (name && name != null && name != "") {
			this.name = name;
		}
		else {
			this.name = "Perspectium Replicator Bulk Share Job";	
		}
		
		if (condition && condition != null && condition != "") {
			this.condition = condition;
		}
		else {
			this.condition = "";
		}
		
		var jgr = new GlideRecord("u_psp_bulk_share_job");
		jgr.u_name = this.name;
		jgr.u_condition = this.condition;
		jgr.u_bulk_share = bgr.sys_id;
		jgr.u_status = "Scheduled";
		this.jobSysId = jgr.insert();
    },

	getBulkShareConfig: function() {
		// get the record again to ensure it exists and we don't create another one
		var bgr = new GlideRecord('psp_bulk_share');
		bgr.addQuery('sys_id', this.bgr_sys_id);
		bgr.queryNoDomain();
		if (bgr.next()) {
			return bgr;
		}
		else {
			return null;
		}
	},
	
	getBulkShareJob: function() {
		var jgr = new GlideRecord("u_psp_bulk_share_job");
		jgr.addQuery("sys_id", this.jobSysId);
		jgr.queryNoDomain();
		if (jgr.next()) {
			return jgr;
		}
		else {
			return null;
		}
	},
	
	execute: function(startTime) {		
		// set bulk share status to running
		var bgr2 = this.getBulkShareJob();
		if (bgr2 != null) {
			bgr2.u_status = 'Running';
			bgr2.u_started = gs.nowDateTime();
			bgr2.update();			
		}

		this.action = "bulk";
		this.pspShareMessage = new PerspectiumShareMessage(this.bgr, this.action);
				
		var more = this.shareRecords(null, startTime);
	    while(more != null) {
			more = this.shareRecords(more, startTime);
	    }
		
		// if we're reach the end and we've processed all records, update status to completed
		var bgr3 = this.getBulkShareJob();
		if (bgr3 != null) {			
			bgr3.u_completed = gs.nowDateTime();
			bgr3.u_status = 'Completed';			
			bgr3.update();
			
			this.psp.createPSPOut("monitor", "label", this.pspR.getKey(this.bgr), "Perspectium^PSP", "Bulk share completed : '" + this.bgr.table_name + "' with condition " + this.condition + " ('" + bgr3.u_records_processed + "')", null, null, null, this.bgr);
		}			
	},
	
	shareRecords: function(starting_sysid, startTime) {
		var kount = 0;
		var last_sys_id = null;
		var gr = new GlideRecord(this.bgr.table_name);
		gr.orderBy('sys_id');
		
		if (starting_sysid != null) {
			gr.addQuery('sys_id', '>', starting_sysid);
		}
		
		if (this.condition && this.condition != null && this.condition != "") {
			gr.addEncodedQuery(this.condition);
			
			this.logger.logDebug("shareRecords: " + this.bgr.table_name + " with condition: " + this.condition, "PerspectiumBulkShare.shareRecords", this.bgr);		
		}
		
		gr.setLimit(this.bulkShareQueryLimit);		
		gr.queryNoDomain(); 
			
		// save total row count for attributes
		this.bulk_query_count = gr.getRowCount();

		//var action = "bulk";
		//var pspShareMessage = new PerspectiumShareMessage(this.bgr, action);		
		// first create schema
		//this.pspShareMessage.createSchema();
		// reset share message to start over
		this.pspShareMessage.reset();
		
		// use _next to account for tables such as sys_template that have "next" column
		while(gr._next()) {
			last_sys_id = gr.sys_id;
			this.shareOneRecord(this.bgr, this.bgr.table_name, gr, this.action);
			kount++;
		}
		
		var result = this.pspShareMessage.postRecords();
		
		// update that processed these records
		var bgr2 = this.getBulkShareJob();
		if (bgr2 != null) {
			bgr2.u_records_processed = parseInt(bgr2.u_records_processed) + kount;
			bgr2.update();			
		}
				
		// reach query limit so we return last sys_id for next set of records
		// if we queried all at once count will be greater so we won't return last sys_id
		if (kount == this.bulkShareQueryLimit) {
			return last_sys_id;
		}
		
		return null;			
	},

    shareOneRecord : function(bgr, table_name, gr, op) {
        try {
			var sys_id = gr.sys_id;
            if(gr.sys_id.isNil() || sys_id == null || sys_id == "") {
				// sys_id not found for this table
				this.logger.logDebug("shareOneRecord not found for " + table_name +  "," + sys_id, "PerspectiumBulkShare.shareOneRecord", bgr);
				return false;
			}
				
			var recordXML = this.serializeRecord(gr);
			if (recordXML && recordXML != null && recordXML != "") {
				//this.recordsStr += recordXML;
				this.pspShareMessage.addRecordStr(recordXML);
			}
			
			this.bulk_count++;			
        } catch(e) {
            this.logger.logWarn("shareOneRecord " + table_name + ", error = " + e.message, "PerspectiumBulkShare.shareOneRecord", bgr);
        }
		
		return true;
    },
	
	serializeRecord: function(grParam) {
		var xmlstr = this.recordSerializer.serialize(grParam);
		var xmlDoc = new XMLDocument(xmlstr);
		/*if (xmlstr.indexOf("<sys_translated_") > 0 && operator && operator != "") {
			this.detectAndEmitEmbededRecords(xmlDoc, grParam.getTableName(), operator, targetQueue);
		}*/

		if (this.currentFieldsOnly != "true" && this.addDisplayValues != "true") {
			return xmlstr;
		}

		var tableName = grParam.getTableName();
		var arrFields = grParam.getFields();
		for (var i = 0; i < arrFields.size(); i++) {
			var glideElement = arrFields.get(i);
			var ed = glideElement.getED();
			var elName = glideElement.getName();
			
			// use getFields() so we can get all fields including inactive fields, GlideFieldList does not return inactive fields
			if (this.currentFieldsOnly == "true" && tableName != ed.getTableName()) {
				//this.logger.logDebug("removing element " + elName,"PerspectiumEncryption.encrypt");
				var nn = xmlDoc.getElementByTagName(elName);
				if(nn && nn.parentNode) {
					nn.parentNode.removeChild(nn);
					this.logger.logDebug("removed element " + elName,"PerspectiumBulkShare.serializeRecord");
				}

				continue;
			}

			if (this.addDisplayValues == "true" && (ed.isReference() || ed.isChoiceTable() || ed.getInternalType() == "glide_list")) {
				// for choice fields we'll check if it's a dependent to get the right display value
				var dependentField = glideElement.getDependent();
				if (ed.isChoiceTable() && dependentField != null) {
					var dependentValue = grParam.getValue(dependentField);
					
					// if we don't have a dependent value for this we'll just use regular display value
					if (dependentValue == null || dependentValue == "") {
						xmlDoc.createElement("dv_" + elName, glideElement.getDisplayValue());
						continue;	
					}
					
					// find the sys_choice with this dependent value so we can use the label which will be the proper display value
					var scGr = new GlideRecord("sys_choice");
					scGr.addQuery("dependent_value", dependentValue);
					scGr.addQuery("element", elName);
					scGr.addQuery("value", grParam.getValue(elName));
					scGr.query();
					if (scGr.next()) {
						xmlDoc.createElement("dv_" + elName, scGr.label);
						continue;						
					}								  
				}
				
				xmlDoc.createElement("dv_" + elName, glideElement.getDisplayValue());				
			}

		}
		
		return xmlDoc.toString();
		
	},
	
	addSysId: function(record_sys_id){
		if (!this.bgr_sys_id || this.bgr_sys_id == null || this.bgr_sys_id == ""){
			this.logger.logError("Unable to add sys id " + record_sys_id + " as PerspectiumBulkShare does not have a valid bulk share configuration sys id", "PerspectiumBulkShare.addSysId");
			return;
		}
					
		// enable bulk share's share only sys_ids option if not selected
		if(this.bgr.u_share_only_sys_ids_listed != "true" && this.bgr.u_share_only_sys_ids_listed != true){
			this.logger.logDebug("Updating share only sys ids condition", "PerspectiumBulkShare.addSysId");
			this.bgr.u_share_only_sys_ids_listed = true;
			this.bgr.update();
		}
		
		// save this record's sys_id and bulk share's sys_id into bulk share's sys_id related list 
		var sgr = new GlideRecord('u_psp_bulk_share_sys_id');
		sgr.initialize();
		sgr.setValue("u_bulk_share", this.bgr_sys_id);
		sgr.setValue("u_record_sys_id", record_sys_id);
		sgr.insert();
	},
	
	clearSysIds: function(){
		if (!this.bgr_sys_id || this.bgr_sys_id == null || this.bgr_sys_id == ""){
			this.logger.logError("Unable to clear sys ids as PerspectiumBulkShare does not have a valid bulk share configuration sys id", "PerspectiumBulkShare.clearSysIds");
			return;
		}
		
		// delete all sys_ids in the bulk share's sys_id related list
		var sgr = new GlideRecord('u_psp_bulk_share_sys_id');	
		sgr.addQuery("u_bulk_share", this.bgr_sys_id);
		sgr.deleteMultiple(); 
		
		if (!this.resetSysIdsOption) {
			return;
		}

		// uncheck the bulk share's share only sys_ids option as we don't want to query this list since there are no records
		// enable bulk share's share only sys_ids option if not selected
		if(this.bgr.u_share_only_sys_ids_listed == "true" || this.bgr.u_share_only_sys_ids_listed == true){
			this.logger.logDebug("Updating share only sys ids condition", "PerspectiumBulkShare.clearSysIds");
			this.bgr.u_share_only_sys_ids_listed = false;
			this.bgr.update();
		}
	},
	
    type: 'PerspectiumBulkShare'
};