var PerspectiumQueue = Class.create();
PerspectiumQueue.prototype = {
	
    initialize: function() {
		this.psp = new Perspectium();
		this.logger = new PerspectiumLogger();
		this.pspUtil = new PerspectiumUtil();
		this.qpassphrase = this.pspUtil.getPspPropertyValue("com.perspectium.qpassphrase", "");
		
		this.inputServlet = "customerstatus";
    },
	
	updateQueueStatus: function (queueConfig){
		var encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
		var snc_input = queueConfig.u_endpoint_url.toString();
		var quser = queueConfig.u_queue_user;		
		var qpassword = encrypter.decrypt(queueConfig.u_queue_password);
		var input_queue = queueConfig.u_name;		
		var instance = gs.getProperty("instance_name", "unregistered");
		
		if (!this.psp.endsWith(snc_input, "/")) {
			snc_input += "/";
		}
		
		var connectionUrl = snc_input + this.inputServlet;
		var getMethod = new Packages.org.apache.commons.httpclient.methods.GetMethod(connectionUrl);
		getMethod.setRequestHeader("psp_quser", quser);
        getMethod.setRequestHeader("psp_qpassword", qpassword);
        getMethod.setRequestHeader("passphrase", this.qpassphrase);
        getMethod.setRequestHeader("psp_input_queue", input_queue);
        getMethod.setRequestHeader("psp_instance", instance);
		
		var httpClient = this.psp.getHttpClient();  
		
		try{
			httpClient.setHttpTimeout(30000);
			var result = httpClient.executeMethod(getMethod);
			var rstr = getMethod.getResponseBodyAsString();
			getMethod.releaseConnection();

			var gdt = new GlideDateTime();
			var resultStr = gdt.getDisplayValue() + ": ";		
			if (result != 200) {
				switch (result) {
					case 401:
					case 403:
						resultStr += "Unauthorized access to queue on " + snc_input + ". Please check your queue, credentials and the URL.";
						break;
					case 404:
						resultStr += "Queue does not exist on " + snc_input;					
						break;
					default:
						resultStr += "Error getting status for queue on " + snc_input;
				}

				return resultStr;
			}        

			var json = new JSON();
			var obj = json.decode(rstr);
			// no properties returned meaning queue doesn't exist
			if(!obj){
				return resultStr + "Queue does not exist on " + snc_input;	
			}

			resultStr += "Connection to queue was successful";		
			if(obj["Queue Message Count"]){			
				resultStr += " and there are " + obj["Queue Message Count"] + " messages";	
			}
		} catch (e) {
			resultStr = "Error: " + e;
		}
		return resultStr;
	},

	getQueueRecordCountAndStatus: function(queueConfig) {
		var encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
		var snc_input = queueConfig.u_endpoint_url.toString();
		var quser = queueConfig.u_queue_user;		
		var qpassword = encrypter.decrypt(queueConfig.u_queue_password);
		var input_queue = queueConfig.u_name;		
		var instance = gs.getProperty("instance_name", "unregistered");
		
		if (!this.psp.endsWith(snc_input, "/")) {
			snc_input += "/";
		}
		
		var connectionUrl = snc_input + this.inputServlet;
		var getMethod = new Packages.org.apache.commons.httpclient.methods.GetMethod(connectionUrl);
		getMethod.setRequestHeader("psp_quser", quser);
        getMethod.setRequestHeader("psp_qpassword", qpassword);
        getMethod.setRequestHeader("passphrase", this.qpassphrase);
        getMethod.setRequestHeader("psp_input_queue", input_queue);
        getMethod.setRequestHeader("psp_instance", instance);
		
		var httpClient = this.psp.getHttpClient();  
		
        var result = httpClient.executeMethod(getMethod);
		var rstr = getMethod.getResponseBodyAsString();
		getMethod.releaseConnection();
		var objStr = "{";
		if (result != 200)
			objStr += "\"connected\": \"false\", ";
		else
			objStr += "\"connected\": \"true\", ";
		
		var json = new JSON();
        var obj = json.decode(rstr);
		if (obj != null && obj["Queue Message Count"])
			objStr += "\"record_count\": \"" + obj["Queue Message Count"] + "\"}";
		else
			objStr += "\"record_count\": \"0\"}";

		return objStr;
	},
	
	purgeQueue: function (queueConfig){
		var encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
		var snc_input = queueConfig.u_endpoint_url.toString();
		var quser = queueConfig.u_queue_user;		
		var qpassword = encrypter.decrypt(queueConfig.u_queue_password);
		var input_queue = queueConfig.u_name;		
		var instance = gs.getProperty("instance_name", "unregistered");
		
		if (!this.psp.endsWith(snc_input, "/")) {
			snc_input += "/";
		}
		
		if (quser.indexOf("/") == -1) {			
			return "Purging is not supported on this queue";	
		}
		
		var connectionUrl = snc_input + this.inputServlet + "?action=purge";
		var postMethod = new Packages.org.apache.commons.httpclient.methods.PostMethod(connectionUrl);		
		postMethod.setRequestHeader("psp_quser", quser);
        postMethod.setRequestHeader("psp_qpassword", qpassword);
        postMethod.setRequestHeader("passphrase", this.qpassphrase);
        postMethod.setRequestHeader("psp_input_queue", input_queue);
        postMethod.setRequestHeader("psp_instance", instance);
				
		var httpClient = this.psp.getHttpClient();  		
		var result = httpClient.executeMethod(postMethod);
		var rstr = postMethod.getResponseBodyAsString();
		postMethod.releaseConnection();
		
		var gdt = new GlideDateTime();
		var resultStr = gdt.getDisplayValue() + ": Purge Queue Status - ";		
        if (result != 200) {
			switch (result) {
    			case 401:
				case 403:
					resultStr += "Unauthorized access to queue on " + snc_input + ". Please check your queue, credentials and the URL.";
					break;
				case 404:
					resultStr += "Queue does not exist on " + snc_input;					
					break;
				case 406:
					resultStr += "Cannot purge queue on " + snc_input + ". Please contact support@perspectium.com to purge this queue.";
					break;
				default:
					resultStr += "Error purging queue on " + snc_input;
			}
			
			return resultStr;
		}        
				
		resultStr += "Queue has been purged";		
		return resultStr;
	},
	
	monitorQueues: function () {
		var gr = new GlideRecord("u_psp_queues");
		gr.query();
		while (gr.next()) {
			if (gr.u_monitor == false || gr.u_monitor == "false")
				continue;
	
			if (gr.u_last_monitor_time.nil()) {
				gr.u_last_monitor_time = gs.nowDateTime();
				gr.update();
				continue;
			}
	
			var intervalgdt = new GlideDateTime();
			intervalgdt.setValue(gr.u_last_monitor_time);
			intervalgdt.add(gr.u_monitor_polling_interval);
			var nowgdt = new GlideDateTime();
			// check polling interval to see if we need to do a monitor check
			if (intervalgdt.compareTo(nowgdt) != -1)
				continue;
	
			var recWarn = gr.u_record_warning_threshold;
			var recAlert = gr.u_record_alert_threshold;
			var connWarn = gr.u_connection_warning_threshold;
			var connAlert = gr.u_connection_alert_threshold;
			var lastConnectionIssue = gr.u_last_connection_issue;
			var statusAndCount = this.getQueueRecordCountAndStatus(gr);
			var json = new JSON();
			var jObj = json.decode(statusAndCount);
			var connStatus = jObj["connected"];
			var recordCount = jObj["record_count"];
			if (((connAlert != 0 && connAlert != "") || (connWarn != 0 && connWarn != "")) && connStatus == "false") {
				if (lastConnectionIssue.nil()) {
					lastConnectionIssue = gs.nowDateTime();
					gr.u_last_connection_issue = lastConnectionIssue;
				}
				
				var connAlertGdt = new GlideDateTime();
				connAlertGdt.setValue(lastConnectionIssue);
				connAlertGdt.add(connAlert.dateNumericValue());
				var connWarnGdt = new GlideDateTime();
				connWarnGdt.setValue(lastConnectionIssue);
				connWarnGdt.add(connWarn.dateNumericValue());
				if ((connAlert != 0 && connAlert != "") && connAlertGdt.compareTo(nowgdt) == -1) {
					this.logger.logError("ALERT: unable to connect to " + gr.u_name + " for the past " + connAlert.getDisplayValue() + ".", "Perspectium Queue Monitor");
					gr.u_monitor_status = "ALERT: unable to connect to " + gr.u_name + " for the past " + connAlert.getDisplayValue() + ".";	
				}
				else if ((connWarn != 0 && connWarn != "") && connWarnGdt.compareTo(nowgdt) == -1) {
					this.logger.logWarn("WARNING: unable to connect to " + gr.u_name + " for the past " + connWarn.getDisplayValue() + ".", "Perspectium Queue Monitor");
					gr.u_monitor_status = "WARNING: unable to connect to " + gr.u_name + " for the past " + connWarn.getDisplayValue() + ".";	
				}
			}
		
			if (recordCount != null && recAlert != 0) {
				if (recordCount >= recAlert) {
					this.logger.logError("ALERT: " + recordCount + " records in " + gr.u_name + " is greater than the alert level of " + recAlert + ".", "Perspectium Queue Monitor");
					gr.u_monitor_status = "ALERT: " + recordCount + " records in " + gr.u_name + " is greater than the alert level of " + recAlert + ".";
				}
				else if (recordCount >= recWarn) {
					this.logger.logWarn("WARNING: " + recordCount + " records in " + gr.u_name + " is greater than the warning level of " + recWarn + ".", "Perspectium Queue Monitor");
					gr.u_monitor_status = "WARNING: " + recordCount + " records in " + gr.u_name + " is greater than the warning level of " + recWarn + ".";
				}
			}
			else if (connStatus == "true") {
				gr.u_last_connection_issue = "";
				gr.u_monitor_status = "";
			}
	
			gr.u_last_monitor_time = gs.nowDateTime();
			gr.update();
			
			if (gr.u_track_history){
				this.updateHistory(gr, recordCount);
			}
		}	
	},
	
	updateHistory: function(qGR, count){
		var hGR = new GlideRecord("u_psp_queue_history");
		hGR.initialize();
		hGR.u_queue = qGR.sys_id;
		hGR.u_count = count;
		hGR.u_timestamp = gs.nowDateTime();
		hGR.insert();
	},

	
    type: 'PerspectiumQueue'
};