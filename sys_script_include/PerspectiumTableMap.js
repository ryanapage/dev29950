var PerspectiumTableMap = Class.create();
PerspectiumTableMap.prototype = {
    initialize: function() {
    },

	addAllSourceTableFields: function(parent) {
		var pgr = new GlideRecord(parent.u_source_table);
		pgr.initialize();
		var arrFields = pgr.getFields();
		for (var i = 0; i < arrFields.size(); i++) {
			var glideElement = arrFields.get(i);
			var elemName = glideElement.getName();
			var fgr = new GlideRecord("u_psp_table_field_map");

			// check if this a field map already exists for this sourc efield
			fgr.addQuery('u_parentid', parent.sys_id);
			fgr.addQuery('u_source_field', elemName);
			fgr.query();
			if (fgr.next()) {
				continue;
			}

			fgr.u_parentid = parent.sys_id;
			fgr.u_source_field = elemName;
			// default target field name to also be the same as source
			fgr.u_target_field = elemName;
			fgr.u_use_script = false;
			// default to string type
			fgr.u_field_type = '747127c1bf3320001875647fcf0739e0';
			fgr.u_field_length = 40;

			fgr.insert();
		}			
	},
	
    type: 'PerspectiumTableMap'
};