var PerspectiumToggleShares = Class.create();
var STATUS_SUCCESS = 200;
var STATUS_FAILURE = 500;
var status = { // payload
    code: -1,
    errors: []
};

PerspectiumToggleShares.prototype = Object.extendsObject(AbstractAjaxProcessor, {
	
	start: function() {
		return this.setActivity(true);
	},
	
	stop: function() {
		return this.setActivity(false);
	},
	
	setActivity: function(active) {
		// enable all dynamic shares
        var gr = new GlideRecord("psp_replicate_conf");
        gr.query();
        while(gr.next()) {
            gr.active = active;
            gr.update();
        }

        // enable all scheduled bulk shares
        gr = new GlideRecord("u_psp_scheduled_bulk_share");
        gr.query();
        while(gr.next()) {
            gr.active = active;
            gr.update();
        }

        status.code = STATUS_SUCCESS;
        return JSON.stringify(status);
	},
	
    type: 'PerspectiumToggleShares'
});