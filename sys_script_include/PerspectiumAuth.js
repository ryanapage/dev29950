var PerspectiumAuth = Class.create();
PerspectiumAuth.prototype = {
    initialize: function() {
		this.logger = new PerspectiumLogger();
		this.psp = new Perspectium();
		this.pspUtil = new PerspectiumUtil();
		this.passphrase = this.pspUtil.getPspPropertyValue('com.perspectium.qpassphrase', '');
    },
	
	/**
	 * returns boolean based on authentication status
	 **/
	getAuthStatus: function(user, pass, endpoint) {
		return this.getAuthQueueStatus(user, pass, 'psp.in.servicenow', endpoint);
	},
	
	/**
	 * returns boolean based on authentication status for the given queue
	 **/ 
	getAuthQueueStatus: function(user, pass, queue, endpoint) {
		var CUSTOMER_STATUS = "customerstatus";
		var REQUEST_TIMEOUT_MILLIS = 30000;
		
		var encrypter = new GlideEncrypter();
		var username = user.toString();
		var password = encrypter.decrypt(pass.toString());
		var endpointUrl = this.validateEndpointURL(endpoint) + CUSTOMER_STATUS;
		var input_queue = queue.toString();
		var instance = gs.getProperty('instance_name', 'unregistered');
		var getMethod = this.createAuthHttpMethod(endpointUrl, username, password, this.passphrase, input_queue, instance);
		
		try {		
			var httpClient = this.psp.getHttpClient();
			httpClient.setHttpTimeout(REQUEST_TIMEOUT_MILLIS);
			var statusCode = httpClient.executeMethod(getMethod);
			
			this.logger.logDebug("PerspectiumAuth.getAuthQueueStatus - status code: " + statusCode);
			
			return statusCode == 200;
		} catch (e) {
			this.logger.logError("Unable to authenticate with MBS. Reason: " + e, "PerspectiumAuth.getAuthQueueStatus");
		}
		return false;
	},
	
	/**
	 * create http auth headers
	 **/
	createAuthHttpMethod: function(endpoint, user, pass, passphrase, queue, instance) {
		var method = new Packages.org.apache.commons.httpclient.methods.GetMethod(endpoint);
		method.setRequestHeader("psp_quser", user);
		method.setRequestHeader("psp_qpassword", pass);
		method.setRequestHeader("passphrase", passphrase);
		method.setRequestHeader("psp_input_queue", queue);
		method.setRequestHeader("psp_instance", instance);
		return method;
	},
	
	/**
	 * validate endpoint url for edge cases
	 **/
	validateEndpointURL: function(endpoint) {
		if (!endpoint.endsWith('/')) {
			endpoint += '/';
		}
		return endpoint;
	},

    type: 'PerspectiumAuth'
};