var PerspectiumCatalogTask = Class.create();
PerspectiumCatalogTask.prototype = {
 	initialize: function() {
        this.logger = new PerspectiumLogger();
    },

    addCatalogTasks : function(source, target, pspTag /* optional */) {
        var targetId = target.sys_id;
		var targetTable = target.getTableName();
        this.logger.logDebug('Adding catalog task for: ' + targetTable + "," + targetId, "PerspectiumCatalogTask.addCatalogTasks");

        if (source == null || source.u_catalog_tasks.isNil()) {
            this.logger.logDebug('No catalog tasks found', "PerspectiumCatalogTask.addCatalogTasks");
            return;
        }

        var xmldoc = null;
        try {
             xmldoc = new XMLDocument(source.u_catalog_tasks);
        } catch(err){
            this.logger.logDebug('Error creating XMLDocument for catalog tasks: ' + err, "PerspectiumCatalogTask.addCatalogTasks");
            return;
        }

        if (xmldoc == null) {
           this.logger.logDebug('XMLDocument is null', "PerspectiumCatalogTask.addCatalogTasks");
           return;
        }

        var nodelist = xmldoc.getNodes("//catalog_task");
        if (nodelist == null) {
            this.logger.logDebug('No catalog task nodes found', "PerspectiumCatalogTask.addCatalogTasks");
            return;
        }

        this.logger.logDebug('Found ' + nodelist.getLength() + ' catalog tasks', "PerspectiumCatalogTask.addCatalogTasks");
        for (var i=0; i < nodelist.getLength(); i++) {
            var nodeItem = nodelist.item(i);
            this.addCatalogTask(nodeItem, targetTable, targetId, pspTag);
        }
    },
	
	_getValue : function(nodeItem, nodeName) {        
		var xml_util = (typeof GlideXMLUtil != 'undefined') ? new GlideXMLUtil() : new Packages.com.glide.util.XMLUtil();
		var it = xml_util.childElementIterator(nodeItem);
        while (it.hasNext()) {
            var el = it.next();
            var nn = el.getNodeName();
            var nodeValue = xml_util.getText(el);
			
			if( nodeValue == null)
				nodeValue = "";
			
			if(nn == nodeName) {
				return nodeValue;
			}
		}
											  
		return "";
	},
	
	_setValues : function(ct, ctNumber, targetId, nodeItem) {
		ct.correlation_id = ctNumber;
		ct.request_item = targetId;		
		ct.work_end = this._getValue(nodeItem, "work_end");
		ct.work_start = this._getValue(nodeItem, "work_start");		
		ct.order = this._getValue(nodeItem, "order");		
		ct.short_description = this._getValue(nodeItem, "short_description");
		ct.assignment_group = this._getValue(nodeItem, "assignment_group");
		ct.assigned_to = this._getValue(nodeItem, "assigned_to");		
	},

    addCatalogTask : function(nodeItem, targetTable, targetId, pspTag /* optional */) {
        var StringUtil = (typeof GlideStringUtil != 'undefined') ? new GlideStringUtil() : new Packages.com.glide.util.StringUtil();
		
		var ctNumber = this._getValue(nodeItem, "number");
		
		if (ctNumber == null)
			return; // no sys_id, cannot search
		
		// first see if we can find problem task by sys_id
		var ct = new GlideRecord("sc_task");
		ct.addQuery("correlation_id", ctNumber);
		ct.query();
		if (!ct.next()) {
			ct.initialize();
			this._setValues(ct, ctNumber, targetId, nodeItem);
			ct.insert();
		} else {
			this._setValues(ct, ctNumber, targetId, nodeItem);
			ct.update();
		}
		
		// if pspTag exists, go and tag it
		if (pspTag) {
			var pspUtil = new PerspectiumUtil();
			pspUtil.addTag(ct, pspTag);
		}
    },

    type: 'PerspectiumCatalogTask'
};