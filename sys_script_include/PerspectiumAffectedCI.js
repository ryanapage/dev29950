var PerspectiumAffectedCI = Class.create();
PerspectiumAffectedCI.prototype = {
 	initialize: function() {
        this.logger = new PerspectiumLogger();
    },

    addTaskCIs : function(source, target, pspTag /* optional */) {
        var targetId = target.sys_id;
		var targetTable = target.getTableName();
        this.logger.logDebug('Adding task_ci for: ' + targetTable + "," + targetId, "PerspectiumAffectedCI.addTaskCIs");

        if (source == null || source.u_affected_cis.isNil()) {
            this.logger.logDebug('No affected_cis found', "PerspectiumAffectedCI.addTaskCIs");
            return;
        }

        var xmldoc = null;
        try {
             xmldoc = new XMLDocument(source.u_affected_cis);
        } catch(err){
            this.logger.logDebug('Error creating XMLDocument for affected_ci: ' + err, "PerspectiumAffectedCI.addTaskCIs");
            return;
        }

        if (xmldoc == null) {
           this.logger.logDebug('XMLDocument is null', "PerspectiumAffectedCI.addTaskCIs");
           return;
        }

        var nodelist = xmldoc.getNodes("//affected_ci");
        if (nodelist == null) {
            this.logger.logDebug('No affected_ci nodes found', "PerspectiumAffectedCI.addTaskCIs");
            return;
        }

        this.logger.logDebug('Found ' + nodelist.getLength() + ' task_ci', "PerspectiumAffectedCI.addTaskCIs");
        for (var i=0; i < nodelist.getLength(); i++) {
            var nodeItem = nodelist.item(i);
            this.addTaskCI(nodeItem, targetTable, targetId, pspTag);
        }
    },

    addTaskCI : function(nodeItem, targetTable, targetId, pspTag /* optional */) {
        var StringUtil = (typeof GlideStringUtil != 'undefined') ? new GlideStringUtil() : new Packages.com.glide.util.StringUtil();
		var xml_util = (typeof GlideXMLUtil != 'undefined') ? new GlideXMLUtil() : new Packages.com.glide.util.XMLUtil();
		
		var ciClass = null;
		var ciName = null;
		var ciID = null;
		
        var it = xml_util.childElementIterator(nodeItem);
        while (it.hasNext()) {
            var el = it.next();
            var nodeName = el.getNodeName();
            var nodeValue = xml_util.getText(el);
			
			if( nodeValue == null)
				nodeValue = "";
			
			if(nodeName == "ci_class") {
				ciClass = nodeValue;
			} else if (nodeName == "ci_name")
				ciName = nodeValue;
			else if (nodeName == "ci_id")
				ciID = nodeValue;
		}
		
		if (ciID == null)
			return; // no sys_id, cannot search
		
		// first see if we can find CI by sys_id
		var ci = new GlideRecord("cmdb_ci");
		ci.addQuery("sys_id", ciID);
		ci.query();
		if (!ci.next()) {
			if (ciClass == null || ciName == null) // cannot try if these are null
				return;
			
			// try locating by class and name		
			ci = new GlideRecord(ciClass);
			ci.addQuery("name", ciName);
			// query for sys_id ?
			ci.query();
			if (!ci.next()) {
				// create ?
				return;
			}
		}
		
		var taskCI = new GlideRecord("task_ci");
		taskCI.addQuery("ci_item", ci.sys_id);
		taskCI.addQuery("task", targetId);
		taskCI.query();
		if (taskCI.next()) {
			return; // already exists
		}
		
		taskCI.initialize();
		taskCI.ci_item = ci.sys_id;
		taskCI.task = targetId;
		taskCI.insert();
		
		// if pspTag exists, go and tag it
		if (pspTag) {
			var pspUtil = new PerspectiumUtil();
			pspUtil.addTag(taskCI, pspTag);
		}
    },

    type: 'PerspectiumAffectedCI'
};