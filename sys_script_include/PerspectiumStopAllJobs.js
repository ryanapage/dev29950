var PerspectiumStopAllJobs = Class.create();
var STATUS_SUCCESS = 200;
var STATUS_FAILURE = 500;
var status = { // JSON object to be returned in AJAX call
	code: -1,
	errors: []
};

PerspectiumStopAllJobs.prototype = Object.extendsObject(AbstractAjaxProcessor,{
	
	stopAllJobs: function() {
		var pspGR = new GlideRecord("sysauto");
        pspGR.addEncodedQuery("nameSTARTSWITHPerspectium");
        pspGR.query();
        while (pspGR.next()) {
            if (pspGR.getValue("active") == false) {
                gs.print("Observer already stopped !");
            } else {
                pspGR.active = "false";
                pspGR.update();
                gs.print(pspGR.name + " stopped");
            }
        }

        // inactivate Observer script actions if installed
        try {
            var saGR = new GlideRecord("sysevent_script_action");
            saGR.addEncodedQuery("nameSTARTSWITHpsp event:");
            saGR.query();
            while (saGR.next()) {
                var pE = new GlideRecord("psp_event_subscription");
                pE.addQuery("script_action", saGR.sys_id);
                pE.query();
                if (pE.hasNext()) {
                    saGR.active = "false";
                    saGR.update();
                } else {
                    saGR.deleteRecord(); // remove orpahned script actions
                }
            }
        } catch (e) {
			status.errors.push("Error deactivating observer action: " + e);
        }


        var psp = new Perspectium();
        psp.updateStatus("status", "stopped");
        var key = gs.getProperty("instance_name", "unregistered");
        var str = '{"topic":"monitor", "type":"label", "key":"' + key + '", "name":"Perspectium^PSP", "value":"Scheduled Jobs Stopped"}';

        psp.postStats("input", str);
		
		status.code = STATUS_SUCCESS;
		return JSON.stringify(status);
	},

    type: 'PerspectiumStopAllJobs'
});