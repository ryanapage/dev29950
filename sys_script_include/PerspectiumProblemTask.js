var PerspectiumProblemTask = Class.create();
PerspectiumProblemTask.prototype = {
 	initialize: function() {
        this.logger = new PerspectiumLogger();
    },

    addProblemTasks : function(source, target, pspTag /* optional */) {
        var targetId = target.sys_id;
		var targetTable = target.getTableName();
        this.logger.logDebug('Adding problem task for: ' + targetTable + "," + targetId, "PerspectiumProblemTask.addProblemTasks");

        if (source == null || source.u_problem_tasks.isNil()) {
            this.logger.logDebug('No problem tasks found', "PerspectiumProblemTask.addProblemTasks");
            return;
        }

        var xmldoc = null;
        try {
             xmldoc = new XMLDocument(source.u_problem_tasks);
        } catch(err){
            this.logger.logDebug('Error creating XMLDocument for problem tasks: ' + err, "PerspectiumProblemTask.addProblemTasks");
            return;
        }

        if (xmldoc == null) {
           this.logger.logDebug('XMLDocument is null', "PerspectiumProblemTask.addProblemTasks");
           return;
        }

        var nodelist = xmldoc.getNodes("//problem_task");
        if (nodelist == null) {
            this.logger.logDebug('No problem task nodes found', "PerspectiumProblemTask.addProblemTasks");
            return;
        }

        this.logger.logDebug('Found ' + nodelist.getLength() + ' problem tasks', "PerspectiumProblemTask.addProblemTasks");
        for (var i=0; i < nodelist.getLength(); i++) {
            var nodeItem = nodelist.item(i);
            this.addProblemTask(nodeItem, targetTable, targetId, pspTag);
        }
    },
	
	_getValue : function(nodeItem, nodeName) {        
		var xml_util = (typeof GlideXMLUtil != 'undefined') ? new GlideXMLUtil() : new Packages.com.glide.util.XMLUtil();
		var it = xml_util.childElementIterator(nodeItem);
        while (it.hasNext()) {
            var el = it.next();
            var nn = el.getNodeName();
            var nodeValue = xml_util.getText(el);
			
			if( nodeValue == null)
				nodeValue = "";
			
			if(nn == nodeName) {
				return nodeValue;
			}
		}
											  
		return "";
	},
	
	_setValues : function(ct, ctNumber, targetId, nodeItem) {
		ct.correlation_id = ctNumber;
		ct.problem = targetId;		
		ct.cmdb_ci = this._getValue(nodeItem, "cmdb_ci");		
		ct.priority = this._getValue(nodeItem, "priority");
		ct.state = this._getValue(nodeItem, "state");		
		ct.short_description = this._getValue(nodeItem, "short_description");
		ct.assignment_group = this._getValue(nodeItem, "assignment_group");
		ct.assigned_to = this._getValue(nodeItem, "assigned_to");		
	},

    addProblemTask : function(nodeItem, targetTable, targetId, pspTag /* optional */) {
        var StringUtil = (typeof GlideStringUtil != 'undefined') ? new GlideStringUtil() : new Packages.com.glide.util.StringUtil();
		
		var ctNumber = this._getValue(nodeItem, "number");
		
		if (ctNumber == null)
			return; // no sys_id, cannot search
		
		// first see if we can find problem task by sys_id
		var ct = new GlideRecord("problem_task");
		ct.addQuery("correlation_id", ctNumber);
		ct.query();
		if (!ct.next()) {
			ct.initialize();
			this._setValues(ct, ctNumber, targetId, nodeItem);
			ct.insert();
		} else {
			this._setValues(ct, ctNumber, targetId, nodeItem);
			ct.update();
		}
		
		// if pspTag exists, go and tag it
		if (pspTag) {
			var pspUtil = new PerspectiumUtil();
			pspUtil.addTag(ct, pspTag);
		}
    },

    type: 'PerspectiumProblemTask'
};