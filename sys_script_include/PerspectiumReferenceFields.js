var PerspectiumReferenceFields = Class.create();
PerspectiumReferenceFields.prototype = {
    initialize: function() {
		
    },
	
	addAllTableFields: function(current){
		var cgr = new GlideRecord(current.table_name);
		cgr.initialize();
		var arrFields = cgr.getFields();
		for (var i = 0; i < arrFields.size(); i++) {
			var glideElement = arrFields.get(i);
			var descriptorType = glideElement.getED().getInternalType();
			if(descriptorType != "reference"){
				continue;
			}
			
			var tUtils = new TableUtils(current.table_name);
			var tables = tUtils.getTables().toString();
			var tableStr = tables.substring(1, tables.length() - 1);
			
			
			var pgr = new GlideRecord("sys_dictionary");
			pgr.addQuery('name','IN', tableStr);
			pgr.addQuery('element', glideElement.getName());
			pgr.addQuery('internal_type', "reference");
			pgr.addQuery("active", "true");
			pgr.query();
			
			if(!pgr.next())
				continue;
			
			var ngr = new GlideRecord("u_psp_dynamic_share_referenced");
			ngr.u_table_name = current.table_name;
			ngr.u_dynamic_share = current.getElement("sys_id");
			ngr.u_reference_field = glideElement.getName();
			ngr.insert();
		}		
	},

    type: 'PerspectiumReferenceFields'
};