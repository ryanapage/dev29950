var PerspectiumInstall = Class.create();
PerspectiumInstall.prototype = {
	
	initialize: function() { 
		this.UPDATE_SET_VERSION = "Bismuth";
		this.pspUtil = new PerspectiumUtil();
		this.logger = new PerspectiumLogger();
		this.pspProperties = [
		{name: "com.perspectium.quser", type: "string", description: "The user name that is used for connecting to the Perspectium message bus", category: "Perspectium", order: 10},
		{name: "com.perspectium.qpassword", type: "password", description: "The user password that is used for connecting to the Perspectium message bus", category: "Perspectium", order: 20},
		{name: "com.perspectium.input_queue", value: "psp.in.servicenow", type: "readonly", description: "The message bus input queue name for receiving ServiceNow data", category: "Perspectium", order: 30},
		{name: "com.perspectium.snc_input", type: "url", description: "The target Perspectium server or message bus endpoint", category: "Perspectium", order: 40},
		{name: "com.perspectium.output_bytes_limit", value: "5000000", type: "integer", description: "The maximum number of bytes to share during each post (see Perspectium MultiOutput Processing scheduled job). This maximum value takes precedence over the maximum number of records shared", category: "Perspectium", order: 50},
		{name: "com.perspectium.output_row_limit", value: "8000", type: "integer", description: "The maximum number of records to share during each post (see Perspectium MultiOutput Processing scheduled job)", category: "Perspectium", order: 60},
		{name: "com.perspectium.debug", value: "false", type: "boolean", description: "Enable debugging to generate more log", category: "Perspectium", order: 70},
		{name: "com.perspectium.performance_logging", value: "false", type: "boolean", description: "Enable performance stats logging", category: "Perspectium", order: 75},
		{name: "com.perspectium.outbound.enable_max", value: "false", type: "boolean", description: "Enable limiting the maximum number of messages in the Outbound Messages queue", category: "Perspectium", order: 80},
		{name: "com.perspectium.outbound.max_messages", value: "1000000", type: "integer", description: "The maximum number of messages in the Outbound Messages queue (once enabled limiting option above selected)", category: "Perspectium", order: 90},
		{name: "com.perspectium.outbound.delete_error_messages", value: "false", type: "boolean", description: "Enable deleting error messages older than 1 day in the Outbound Messages queue", category: "Perspectium", order: 100},
		{name: "com.perspectium.enable_inbound", value: "true", type: "boolean", description: "Enable messages to arrive at this instance", category: "Perspectium Replicator", order: 10},
		{name: "com.perspectium.replicator.add_display_values", value: "false", type: "boolean", description: "Enable replicator to add display value fields (prefixed with 'dv_') for reference and choice fields", category: "Perspectium Replicator", order: 20},
		{name: "com.perspectium.replicator.share_current_fields", value: "false", type: "boolean", description: "Share fields that belong only to the selected table. Note that this is unusual and will result in missing fields from the base (parent) table", category: "Perspectium Replicator", order: 30},
		{name: "com.perspectium.replicator.outbound.encryption_key", type: "password", description: "Encryption key for encrypting replicator content shared (must be at least 24 characters long)", category: "Perspectium Replicator", order: 40},
		{name: "com.perspectium.replicator.inbound.encryption_key", type: "password", description: "Decryption key used to decrypt replicator content subscribed (must be at least 24 characters long)", category: "Perspectium Replicator", order: 50},
		{name: "com.perspectium.replicator.encryption_mode", value: "encrypted", type: "choicelist", options: "encrypted,encrypted_multibyte", description: "Specify how data is encrypted before being shared. 'encrypted' uses the same encryption as ServiceNow's password encryption. 'encrypted_multibyte' base 64 encodes the data before encrypting.", category: "Perspectium Replicator", order: 60},
		{name: "com.perspectium.replicator.skip_sys_domain_path", value: "false", type: "boolean", description: "Skip updating the sys_domain_path field when subscribing to records (this option only applies to domain-separated instances)", category: "Perspectium Replicator", order: 70},
		{name: "com.perspectium.outbound.minutes.past", value: "10", type: "integer"},
		{name: "com.perspectium.output.process.count", value: "10", type: "integer"},
		{name: "com.perspectium.qpassphrase", type: "string"},
		{name: "com.perspectium.single_output_processor", value: "true", type: "string"},
		{name: "com.perspectium.update_set.version", value: this.UPDATE_SET_VERSION, type: "readonly", description: "Perspectium Update Set Version", category: "Perspectium", order: 99999},
		{name: "com.perspectium.replicator.verify_large_share", type: "boolean", description: "Enable a prompt to verify a bulk share with a large number of records", value: "true", category: "Perspectium Replicator", order: 80},
		{name: "com.perspectium.replicator.large_share_value", type: "integer", description: "The minimum number of records to prompt before a large bulk share(once option above selected)", value: 1000000, category: "Perspectium Replicator", order: 90},
		{name: "com.perspectium.replicator.scheduled_job.priority", type: "integer", value: 100, category: "Perspectium Replicator"},
		{name: "com.perspectium.replicator.last_system_id", type: "string", value: "", category: "Perspectium Replicator"},
	    {name: "com.perspectium.replicator.preview_sample_size", value: "50", type: "integer"},
		{name: "com.perspectium.data_cleaner.delete.individual", value: "false", type: "boolean", description: "Enable low impact data cleaner that will query records and delete them individually"},
		{name: "com.perspectium.observer.outbound.delete_immediately", value: "false", type: "boolean", description: "Enable deletion of records as they are being shared instead of letting a separate job handle them"},
		{name: "com.perspectium.outbound.delete_immediately", value: "false", type: "boolean", description: "Enable deletion of records as they are being shared instead of letting a separate job handle them"},
		{name: "com.perspectium.queue_history.days_retained", value: "35", type: "integer", description: "How many days of queue history to retain before it is deleted"},
		{name: "com.perspectium.outbound.default_disabled", type: "boolean", value: "false"},
		{name:"com.perspectium.replicator.set_last_sync_time_current", value: "false", type: "boolean"},
		{name:"com.perspectium.outbound.process_message_set", value: "false", type: "boolean"},
		{name:"com.perspectium.replicator.check_long_running_bulk_share", type: "integer", value: 12},
		{name: "com.perspectium.receipt.status_auto_delete", value: "true", type: "boolean", description: "Enable deletion of receipt status records after being processed.", order: 101},
		{name: "com.perspectium.receipt.error_alert_threshold", value: "10", type: "integer", description: "Amount of time (in minutes) to wait before creating receipt status alert notification.", order: 102},
		{name: "com.perspectium.agent.heartbeat_alert_threshold", value: "10", type:"integer", description: "Amount of time (in minutes) to wait before creating agent heartbeat alert notifications.", order: 110},
		{name: "com.perspectium.agent.heartbeat_alert_enabled", value: "true", type:"boolean", description: "Enable to be notified when replicator heartbeats are not received within a configurable amount of time.", order: 109},
		{name: "com.perspectium.replicator.servicenow_direct", value: "false", type: "boolean", description: "Enable to combine all outbound messages in the psp_out_message table to be processed by the Service Now Direct agent."}
		];
	},
	
	install: function() {
		// update the new encryption options to remove unencrypted option
		this.updateEncryptedModeProperty();
		
		this.installProperties();
		
		// after each install we'll update the update set version number to the latest in this update set
		this.updateVersionNumber();
		this.createDefaultSubscribeQueue();
		// backwards compat. check to see that all queues have a trailing slash
		this.checkQueueUrlFormat();
		// enable large bulkshare prompt on install
		this.updateLargeBulkPrompt();
		//update the cipher of dynamic shares with table maps
		this.updateDynamicShareCipher();
		this.removeRedundantScheduledJobs();
	},
	
	createDefaultSubscribeQueue: function() {
		var encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
		var checkGr = new GlideRecord("u_psp_queues");
		checkGr.addQuery("u_name", "psp.out.servicenow." + gs.getProperty("instance_name", "unregistered"));
		checkGr.query();
		if (checkGr.next()) {
			return;
		}
		
		var dsqGr = new GlideRecord("u_psp_queues");
		dsqGr.u_name = "psp.out.servicenow." + gs.getProperty("instance_name", "unregistered");
		dsqGr.u_active = true;
		dsqGr.u_direction = "Subscribe";
		dsqGr.u_endpoint_url = this.pspUtil.getPspPropertyValue("com.perspectium.snc_input", "");
		dsqGr.u_queue_user = this.pspUtil.getPspPropertyValue("com.perspectium.quser", "");
		dsqGr.u_queue_password = encrypter.encrypt(this.pspUtil.getPspPropertyValue("com.perspectium.qpassword", ""));
		dsqGr.insert();
		
	},
	
	checkQueueUrlFormat: function() {
		var gr = new GlideRecord("u_psp_queues");
		gr.query();
		
		while (gr.next()) {
			if (gr.u_endpoint_url.endsWith("/"))
				continue;
			
			gr.u_endpoint_url += "/";
			gr.update();
		}
	},
	
	updateEncryptedModeProperty: function() {
		var gr = new GlideRecord("u_psp_properties");
		gr.addQuery("u_name", "com.perspectium.replicator.encryption_mode");
		gr.query();
		if(gr.next()){
			gr.u_options = "encrypted,encrypted_multibyte";
			gr.update();
		}
	},
	
	updateLargeBulkPrompt: function() {
		this.pspUtil.setPspPropertyValue("com.perspectium.replicator.verify_large_share", "true");
	},
	
	/* find all redundant processes and delete them */
    removeRedundantScheduledJobs: function() {
		// get scheduled jobs table
		var gr = new GlideRecord("sysauto_script"); 

		// query for XPerspectium or XXPerspectium Observer scripts
		gr.addQuery("name", "CONTAINS", "Observer");
		var grOR = gr.addQuery("name", "STARTSWITH", "XPerspectium");
		grOR.addOrCondition("name", "STARTSWITH", "XXPerspectium");
		gr.query();
		gr.deleteMultiple(); // delete results in record set
    },
	
	installProperties: function() {
		// install properties into instance only if they don't exist already
		for (var p in this.pspProperties) {
			var pspProperty = this.pspProperties[p];
			this.installProperty(pspProperty);
		}
	},
	
		updateDynamicShareCipher: function() {
		//update the cipher of dynamic shares that have table maps 
		var gr = new GlideRecord('psp_replicate_conf');
		gr.addQuery("u_direction", "share");
		gr.query();	
		while (gr.next()) {
			if (gr.u_table_map.nil()){
				continue;
			} 
			
			gr.setValue("u_cipher", 3);
			gr.update();
		}
		
	},
	
	updateVersionNumber: function() {
		this.pspUtil.setPspPropertyValue("com.perspectium.update_set.version", this.UPDATE_SET_VERSION);
	},
	
	// function to call when you want to install a property into the PSP properties table (u_psp_properties)
	// pass in an object that represents the property {name: "name", value: "10", type: "string"}
	// see u_psp_properties table for fields that can exist in property
	installProperty: function(pspProperty) {
		if (!pspProperty.name) {
			return;
		}
		
		var pspPropertyName = pspProperty.name;
		// property already exists so we don't need to create
		if (this.pspUtil.getPspPropertyValue(pspPropertyName) != null) {
			return;
		}
		
		// add all remaining attributes to save into properties table
		var pspAttributes = {};
		for (var a in pspProperty) {
			// ignore name and value as we already account for and do special handling on them
			if (a == "name" || a == "value") {
				continue;
			}
			
			pspAttributes[a] = pspProperty[a];
		}
		
		// check if there's a sys_properties record from prior update sets and we"ll use that value
		// to transition over to using our properties table
		var pspPropertyValue = this.pspUtil.getSysPropertyValue(pspPropertyName);
		if (pspPropertyValue == null) {
			// if not, check if property definition has a value we can use
			if (pspProperty.value) {
				pspPropertyValue = pspProperty.value;
			}
			// otherwise default to blank value so user fills in
			else {
				pspPropertyValue = "";
			}
		}
		// password fields we'll need to decrypt to save properly in our table when we encrypt on call in createPspProperty()
		else if (pspProperty.type == "password") {
			var Encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
			var encryptedPw = gs.getProperty(pspPropertyName, "");
			pspPropertyValue = Encrypter.decrypt(encryptedPw);
			//pspPropertyValue = Encrypter.encrypt(pspPropertyValue);
		}
		
		this.logger.logDebug("Creating new Perspectium property " + pspPropertyName, "PerspectiumInstall.installProperties");
		this.pspUtil.createPspProperty(pspPropertyName, pspPropertyValue, pspAttributes);
	},
	
	type: 'PerspectiumInstall'
};