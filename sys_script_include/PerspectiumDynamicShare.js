var PerspectiumDynamicShare = Class.create();
PerspectiumDynamicShare.prototype = {
    initialize: function(qc) {
		this.qc = qc;
		this.logger = new PerspectiumLogger();
    },
	
	runBulkShare: function() {
		if (!this.qc || typeof this.qc == 'undefined' || this.qc == null) {
			this.logger.logDebug("No dynamic share configuration exists to run bulk share", "PerspectiumDynamicShare.runBulkShare");
			return;
		}
		
		// create a bulk share with same options as dynamic
		var bgr = new GlideRecord('psp_bulk_share');		
		bgr.condition = this.qc.condition;
		bgr.table_name = this.qc.table_name;
		bgr.u_before_share_script = this.qc.u_before_share_script;
		bgr.u_cipher = this.qc.u_cipher;
		bgr.u_include_attachment = this.qc.u_share_attachment;
		bgr.u_name = this.qc.table_name + " Dynamic Share Bulk";
		
		bgr.u_include_embedded_images_vide = this.qc.u_include_embedded_images_vide;
		bgr.u_include_sys_audit = this.qc.u_include_sys_audit;
		bgr.u_share_journal = this.qc.u_share_journal;
		bgr.u_table_map = this.qc.u_table_map;
		bgr.u_target_queue = this.qc.u_target_queue;
		bgr.u_view_name = this.qc.u_view_name;		
		bgr.u_after_bulk_share_script = this.qc.u_after_share_script;
		bgr.u_share_only_selected_fields = this.qc.u_share_only_selected_fields;
		
		if (this.qc.u_share_base_table_only == "true" || this.qc.u_share_base_table_only == true) {
			bgr.u_include_child_only = false;
			//bgr.u_include_child_tables = true;
		}
			
		// relate this new bulk share to the dynamic share
		bgr.u_dynamic_share = this.qc.sys_id;
		var newBSysId = bgr.insert();
		
		// if user selected to only share selected fields we'll save the related list to the bulk share
		if (this.qc.u_share_only_selected_fields == "true" || this.qc.u_share_only_selected_fields == true) {
			var sgr = new GlideRecord('u_psp_share_field');
			sgr.addQuery('u_source', this.qc.sys_id);
			sgr.query();
			while(sgr.next()){
				sgr.u_source = newBSysId;
				sgr.insert();
			}
		}
		
		// now schedule bulk share to run
		var pspR = new PerspectiumReplicator();
		var startTime = bgr.getValue("u_last_share_time");
		pspR.scheduleOnceBulkShareJob(bgr, startTime);
	},

    type: 'PerspectiumDynamicShare'
};