var PerspectiumMessageSetAjax = Class.create();
PerspectiumMessageSetAjax.prototype = Object.extendsObject(AbstractAjaxProcessor, {
	getMessageSetServletPath: function(){
		return "messageset";
	},
	
	updateMessageSetActivity: function(){
		this.psp = new Perspectium();
		this.logger = new PerspectiumLogger();
		// first clear out the related list table before we get the updated results
		var configType = this.getParameter('sysparm_share_type');
		var grShareConfig;
		if(configType == "dynamic")
			grShareConfig = new GlideRecord("psp_replicate_conf");
		else
			grShareConfig = new GlideRecord("psp_bulk_share");
		
		if(!grShareConfig.get(this.getParameter('sysparm_sys_id'))) {
			this.logger.logError("no " + configType + " share config exists for " + this.getParameter('sysparm_sys_id'), "PerspectiumMessageSet.updateMessageSetActivity");
			return;
		}
		
		var dgr = new GlideRecord('u_psp_message_set_activity');
		// check based on if dynamic share or bulk
		if(configType == "dynamic"){
			dgr.addQuery('u_dynamic_share', grShareConfig.sys_id);
		}
		else{
			dgr.addQuery('u_bulk_share', grShareConfig.sys_id);
		}
		
		dgr.deleteMultiple();
		
		// make web service call to get activity
		var msUrl = this.psp.getQInputURL();
		// use the target queue URL if there is one
		if(!grShareConfig.u_target_queue.nil() && grShareConfig.u_target_queue != null && grShareConfig.u_target_queue != ""){
			var qgr = new GlideRecord("u_psp_queues");
			if (qgr.get(grShareConfig.u_target_queue)) {
				msUrl = qgr.u_endpoint_url.toString();
			}
		}
		
		if (!this.psp.endsWith(msUrl, "/")) {
			msUrl += "/";
		}
		
		msUrl += this.getMessageSetServletPath() + "?set_id=" + grShareConfig.sys_id;
		
		// get start date for when we should query for message set
		// dynamic share use when dynamic share was created
		if(configType == "dynamic"){
			var startgdt = new GlideDateTime(grShareConfig.sys_created_on);
			msUrl += "&start=" + startgdt.getMonthUTC() + "/" + startgdt.getDayOfMonthUTC() + "/" + startgdt.getYearUTC();
		}
		// bulk share used started date
		else if(!grShareConfig.started.nil()){
			var startgdt = new GlideDateTime(grShareConfig.started);
			msUrl += "&start=" + startgdt.getMonthUTC() + "/" + startgdt.getDayOfMonthUTC() + "/" + startgdt.getYearUTC();
		}
		
		this.logger.logDebug("getting message set activity from: " + msUrl, "PerspectiumMessageSet.updateMessageSetActivity");
		
		var getMethod = new Packages.org.apache.commons.httpclient.methods.GetMethod(msUrl);
		var timeout = 1;
		var httpClient = this.psp.getHttpClient();
		//var httpParams = httpClient.getParams();
		//httpParams.setConnectionTimeout(httpParams, 500); // http.connection.timeout
		
		var result = httpClient.executeMethod(getMethod);
		if (result != 200) {
			getMethod.releaseConnection();
			throw new Error("HTTP GET failed: " + result + " for URL: " + msUrl);
		}
		var rstr = getMethod.getResponseBodyAsString();
		getMethod.releaseConnection();
		
		var json = new JSON();
		var obj = json.decode(rstr);
		
		this.logger.logDebug(obj.length + ' message set records being processed ', "PerspectiumMessageSet.updateMessageSetActivity");
		
		// for each record we'll insert it into the message set activity table
		for(i = 0; i < obj.length; i++) {
			var igr = new GlideRecord("u_psp_message_set_activity");
			igr.initialize();
			
			// save dynamic or bulk share config id as reference for related list
			if(configType == "dynamic"){
				igr.u_dynamic_share = grShareConfig.sys_id;
			}
			else{
				igr.u_bulk_share = grShareConfig.sys_id;
			}
			
			igr.u_component_name = obj[i].component_name;
			igr.u_component_type = obj[i].component_type;
			igr.u_successes = obj[i].successes;
			igr.u_failures = obj[i].failures;
			
			if(obj[i].skipped)
				igr.u_skipped = obj[i].skipped;
			// default skipped to 0 if MBS doesn't return field (MBS version may not be reporting it)
			else
				igr.u_skipped = 0;
			
			// datetimes we convert from epoch to datetime
			var gdt = new GlideDateTime();
			if(obj[i].started && obj[i].started != 0){
				gdt.setNumericValue(obj[i].started);
				igr.u_started = gdt.getDisplayValue();
			}
			
			if(obj[i].updated && obj[i].updated != 0){
				gdt.setNumericValue(obj[i].updated);
				igr.u_updated = gdt.getDisplayValue();
			}
			
			if(obj[i].finished && obj[i].finished != 0){
				gdt.setNumericValue(obj[i].finished);
				igr.u_finished = gdt.getDisplayValue();
			}
			
			igr.insert();
		}
		
	},
    type: 'PerspectiumMessageSetAjax'
});