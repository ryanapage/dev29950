var PerspectiumAttachment = Class.create();
PerspectiumAttachment.prototype = {
 	initialize: function() {
        this.logger = new PerspectiumLogger();
    },

    addAttachments : function(source, target, pspTag /* optional */) {
        var targetId = target.sys_id;
		var targetTable = target.getTableName();
        this.logger.logDebug('Adding attachment for: ' + targetTable + "," + targetId, "PerspectiumAttachment.addAttachments");

        if (source == null || source.u_attachments.isNil()) {
            this.logger.logDebug('No attachments found', "PerspectiumAttachment.addAttachments");
            return;
        }

        var xmldoc = null;
        try {
             xmldoc = new XMLDocument(source.u_attachments);
        } catch(err){
            this.logger.logDebug('Error creating XMLDocument for attachments: ' + err, "PerspectiumAttachment.addAttachments");
            return;
        }

        if (xmldoc == null) {
           this.logger.logDebug('XMLDocument is null', "PerspectiumAttachment.addAttachments");
           return;
        }

        var nodelist = xmldoc.getNodes("//attachment");
        if (nodelist == null) {
            this.logger.logDebug('No attachment nodes found', "PerspectiumAttachment.addAttachments");
            return;
        }

        this.logger.logDebug('Found ' + nodelist.getLength() + ' attachments', "PerspectiumAttachment.addAttachments");
        for (var i=0; i < nodelist.getLength(); i++) {
            var nodeItem = nodelist.item(i);
            this.addAttachment(nodeItem, targetTable, targetId, pspTag);
        }
    },

    addAttachment : function(nodeItem, targetTable, targetId, pspTag /* optional */) {
        var StringUtil = (typeof GlideStringUtil != 'undefined') ? new GlideStringUtil() : new Packages.com.glide.util.StringUtil();
		var xml_util = (typeof GlideXMLUtil != 'undefined') ? new GlideXMLUtil() : new Packages.com.glide.util.XMLUtil();
		
        var attachment = new Attachment();
		attachment.setTargetTable(targetTable);
		attachment.setTargetID(targetId);
		
		var fileName = "";

        var it = xml_util.childElementIterator(nodeItem);
        while (it.hasNext()) {
            var el = it.next();
            var nodeName = el.getNodeName();
            var nodeValue = xml_util.getText(el);
			
			if( nodeValue == null)
				nodeValue = "";
			
			//this.logger.logDebug('Attachment node name:' + nodeName, "PerspectiumAttachment.addAttachment");
			//this.logger.logDebug('Attachment node nodeValue:' + nodeValue, "PerspectiumAttachment.addAttachment");
			
			if(nodeName == "file_name") {
				attachment.setFilename(nodeValue);
				fileName = nodeValue;
			} else if (nodeName == "data")
				attachment.setValue(StringUtil.base64DecodeAsBytes(nodeValue));
			else if (nodeName == "content_type")
				attachment.setContentType(nodeValue);
		}
		
		// don't add attachment if it already exists for this table record
		var agr = new GlideRecord("sys_attachment");
		agr.addQuery("table_sys_id", targetId);
		agr.addQuery("table_name", targetTable);
		agr.addQuery("file_name", fileName);
		agr.query();
		if (agr.next()) {
			this.logger.logDebug("Skipping attachment " + fileName + " as it already exists on record " + targetId + " for table " + targetTable, "PerspectiumAttachment.addAttachment");
			return;	
		}
		
		var msg = attachment.attach();
		this.logger.logDebug('attach msg: ' + msg, "PerspectiumAttachment.addAttachment");
		
		// if pspTag exists, go and tag it
		if (pspTag) {
			var pspUtil = new PerspectiumUtil();
			
			var tgr = new GlideRecord("sys_attachment");
			tgr.addQuery("table_sys_id", targetId);
			tgr.addQuery("file_name", fileName);
			tgr.query();
			
			if (tgr.next())
				pspUtil.addTag(tgr, pspTag);
		}
    },

    type: 'PerspectiumAttachment'
};