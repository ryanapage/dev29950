var PerspectiumCount = Class.create();
PerspectiumCount.prototype = {
    initialize: function() {
    },
	
	updateDynamicSharedRecordCount: function() {
		var pspCountGR = new GlideRecord('u_psp_count');
		pspCountGR.addQuery('u_source_table', 'psp_replicate_conf');
		pspCountGR.query();

		while (pspCountGR.next()) {
			var toUpdate = new GlideRecord('psp_replicate_conf');
			toUpdate.addQuery('sys_id', pspCountGR.u_source);
			toUpdate.query();

			if (toUpdate.next()) { // for every record in dynamic share
				toUpdate.u_shared++;
				toUpdate.autoSysFields(false);
				toUpdate.setWorkflow(false);
				toUpdate.update();
				pspCountGR.deleteRecord();
			}
		}
	},

    type: 'PerspectiumCount'
};