var PerspectiumRunConfirmationPage = Class.create();
var logger = new PerspectiumLogger();
var STATUS_SUCCESS = 200;
var STATUS_FAILURE = 500;
var status = {
	code: -1,
	errors: []
};
// This script include is intended to be used for different confirmation page functions. Add the next generic confirmation page case to the 
// switch statment. Then define a method in which the case calls.
PerspectiumRunConfirmationPage.prototype = Object.extendsObject(AbstractAjaxProcessor, {	

	run: function () {

		var action = String(this.getParameter('sysparm_action'));
		try {
			switch (action) {
				case 'resetDynamicShareRules':
					this.resetDynamicShareRules();
					break;	
			}

			status.code = STATUS_SUCCESS;
			return JSON.stringify(status);	
		}
		catch(e) {
			gs.print("error checking scope: " + e);
			logger.logError("error checking scope: " + e, "Perspectium.PerspectiumRunConfirmationPage");
			status.errors.push("error checking scope: " + e);
		}
		finally {
			return JSON.stringify(status);
		}
	},

	resetDynamicShareRules: function () {
		var psp = new PerspectiumReplicator();
		psp.synchDynShareRules();
	},
	type: 'PerspectiumRunConfirmationPage'
});