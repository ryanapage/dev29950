var PerspectiumShareFields = Class.create();

PerspectiumShareFields.prototype = {
    initialize: function() {
		this.pspUtil = new PerspectiumUtil();
		this.addDisplayValues = this.pspUtil.getPspPropertyValue("com.perspectium.replicator.add_display_values", "false");
    },

	getParentTableName: function(parent){
		if (parent.isValidField("table_name")) {
			return parent.table_name;
		}
		else if (parent.isValidField("u_referenced_table_name")) {
			return parent.u_referenced_table_name;
		} 
		
		return "";
	},
		
	addAllTableFields: function(parent){
		var tableName; 
		
		if (parent.isValidField("table_name")) {
			tableName = parent.table_name;
		}
		// record from u_psp_dynamic_share_referenced
		else if (parent.isValidField("u_referenced_table_name")) {
			tableName = parent.u_referenced_table_name;
		}
		else
			return;

		//var pgr = new GlideRecord(parent.table_name);
		var pgr = new GlideRecord(tableName);
		pgr.initialize();
		var arrFields = pgr.getFields();
		for (var i = 0; i < arrFields.size(); i++) {
			var glideElement = arrFields.get(i);				
			var fgr = new GlideRecord("u_psp_share_field");
			//fgr.u_table_name = parent.table_name;
			fgr.u_table_name = tableName;
			
			fgr.u_field = glideElement.getName();
			fgr.u_source = parent.sys_id;
			fgr.insert();
		}
		
		var egr = new GlideRecord("u_psp_share_field");
		egr.addQuery("u_field", "");	
		egr.addQuery("u_table_name", parent.table_name);
		egr.addQuery("u_source", parent.sys_id);
		egr.deleteMultiple();		
	},
	
	createShareRecordXML: function(grShareConfig, recordgr, tableName, fieldsToReplicate){
		tableName = tableName.toString();
		var xmlDoc = new XMLDocument('<' + tableName + '></' + tableName + '>');
		var i, glideElement, ed, elName;
		
		// for each field listed in fields related list we'll create an element in the XML doc with the field's value
		for(i = 0; i < fieldsToReplicate.length; i++){
			var field = fieldsToReplicate[i];
			xmlDoc.createElement(field, recordgr.getValue(field));					
			glideElement = recordgr.getElement(field);
			ed = glideElement.getED();
			elName = glideElement.getName();
			
			// check if we need to also add display values as we iterate through the list of fields
			if (this.addDisplayValues == "true" && (ed.isReference() || ed.isChoiceTable() 
					|| ed.getInternalType() == "glide_list" || ed.getInternalType() == "glide_duration")) {
				xmlDoc.createElement("dv_" + elName, glideElement.getDisplayValue());
			}
		}
		// add guid fields to the record so record will always have an identifier
		var arrFields = recordgr.getFields();
		for (i = 0; i < arrFields.size(); i++) {
			glideElement = arrFields.get(i);				
			ed = glideElement.getED();
			if(ed.getInternalType() != 'GUID'){
				continue;
			}

			elName = glideElement.getName();			
			// check if we already have this field in the XML
			if(xmlDoc.getElementByTagName(elName))
				continue;
			xmlDoc.createElement(elName, recordgr.getValue(elName));	
		}
		
		// last check for sys_id if record has it to add
		// since sys_id isn't returned in getFields();
		var sysIdEl = recordgr.getElement("sys_id");
		if(sysIdEl != null && !xmlDoc.getElementByTagName("sys_id")){
			xmlDoc.createElement("sys_id", recordgr.getValue("sys_id"));
		}
						
		return xmlDoc.toString();
	},
	
	deleteShareFields: function(parent){
		var egr = new GlideRecord("u_psp_share_field");
		egr.addQuery("u_source", parent.sys_id);
		//egr.addQuery("u_source_table", parent.getTableName());
		egr.deleteMultiple();
	},
	
	getFieldsToReplicate: function(grShareConfig){
		var fgr = new GlideRecord("u_psp_share_field");
		fgr.addQuery("u_table_name", grShareConfig.table_name);
		fgr.addQuery("u_source", grShareConfig.sys_id);
		fgr.query();
		var fieldsToReplicate = [];
		while(fgr.next()){
			fieldsToReplicate.push(fgr.u_field.toString());
		}
		return fieldsToReplicate;
	},
	
    type: 'PerspectiumShareFields'
};