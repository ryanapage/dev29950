var PerspectiumDBCommand = Class.create();
PerspectiumDBCommand.prototype = {
	
	initialize:function(){
		this.stripTablePrefix = true;
		this.stripFieldPrefix = true;
		this.keySuffix;
		this.queue = "default";
	},
	
	insertRecord:function(gr){
		var perspectium = new Perspectium();
		var fieldsjson = perspectium.encodeGlideRecordJSON(gr);
		fieldsjson = fieldsjson.replace(/"/g, "'");
		if(this.stripFieldPrefix)
			fieldsjson = fieldsjson.replace(/'u_/g, "'");
		var valuejson = new JSON();
		var val = {"action":"", "table":"", "fields":""};
		var tablename = gr.getTableName();
		val.action = "insert";
		if(this.stripTablePrefix)
			val.table = tablename.replace(/^u_/g, "");
		else
			val.table = tablename;
		val.fields = fieldsjson;

		return this.createDBCommandOut("command", "db", this.getKey(), val.table, valuejson.encode(val), null, null);
	},
	
	updateRecord:function(gr){
		var perspectium = new Perspectium();
		var fieldsjson = perspectium.encodeGlideRecordJSON(gr, true);
		fieldsjson = fieldsjson.replace(/"/g, "'");
		if(this.stripFieldPrefix)
			fieldsjson = fieldsjson.replace(/'u_/g, "'");

		var valuejson = new JSON();
		var val = {"action":"", "table":"", "fields":"", "where":""};
		var tablename = gr.getTableName();

		val.action = "update";
		if(this.stripTablePrefix)
			val.table = tablename.replace(/^u_/g, "");
		else
			val.table = tablename;
		val.fields = fieldsjson;
		val.where = "psp_id='" + gr.getValue("u_psp_id") + "'";
		
		return this.createDBCommandOut("command", "db", this.getKey(), val.table, valuejson.encode(val), null, null);
	},
	
	deleteRecord:function(gr){
		var fieldsjson = {'fields':''};
		var valuejson = new JSON();
		var val = {"action":"", "table":"", "fields":"{}", "where":""};
		var tablename = gr.getTableName();
		val.action = "delete";
		if(this.stripTablePrefix)
			val.table = tablename.replace(/^u_/g, "");
		else
			val.table = tablename;
		val.fields = fieldsjson;
		val.where = "psp_id='" + gr.getValue("u_psp_id") + "'";

		return this.createDBCommandOut("command", "db", this.getKey(), val.table, valuejson.encode(val), null, null);
	},
	
	deleteWhere:function(tableName, whereClause){
		var fieldsjson = {'fields':''};
		var valuejson = new JSON();
		var val = {"action":"", "table":"", "fields":"{}", "where":""};
		var tablename = tableName;
		val.action = "delete";
		if(this.stripTablePrefix)
			val.table = tablename.replace(/^u_/g, "");
		else
			val.table = tablename;
		val.fields = fieldsjson;
		val.where = whereClause;
		return this.createDBCommandOut("command", "db", this.getKey(), val.table, valuejson.encode(val), null, null);
	},
	
	dropTable:function(tableName){
		var fieldsjson = {};
		var valuejson = new JSON();
		var val = {}; //{'action':'drop_table', 'table':'', 'fields':''};
		var tablename = tableName;
		if(this.stripTablePrefix)
			val.table = tablename.replace(/^u_/g, "");
		else
			val.table = tablename;
		val.action = "drop_table";
		val.fields = fieldsjson;
		return this.createDBCommandOutEscapedQuotes("command", "db", this.getKey(), val.table, valuejson.encode(val), null, null);	
	},
	
	updateWhere:function(gr, whereClause){
		var perspectium = new Perspectium();
		var fieldsjson = perspectium.encodeGlideRecordJSON(gr, true);
		fieldsjson = fieldsjson.replace(/"/g, "'");
		if(this.stripFieldPrefix)
			fieldsjson = fieldsjson.replace(/'u_/g, "'");

		var valuejson = new JSON();
		var val = {"action":"", "table":"", "fields":"{}", "where":""};
		var tablename = gr.getTableName();
		val.action = "update";
		if(this.stripTablePrefix)
			val.table = tablename.replace(/^u_/g, "");
		else
			val.table = tablename;
		val.fields = fieldsjson;
		val.where = whereClause;
		return this.createDBCommandOut("command", "db", this.getKey(), val.table, valuejson.encode(val), null, null);
	},
	
	getKey : function(){
		var key = gs.getProperty("instance_name", "unregistered");
		if(typeof this.keySuffix != "undefined" && !this.keySuffix.isNil())
			key = key + "." + this.keySuffix;
		return key;
	}, 
	
	touchAll : function(tableName){
		var gr=new GlideRecord(tableName);
	    gr.query();
		gs.log("TouchAll will touch: " + gr.getRowCount(), "Perspectium");
		while(gr.next()) {
		    gr.setForceUpdate(true);
		    gr.update();
		}
	},
	
	clearAll : function(tableName){
		var gr=new GlideRecord(tableName);
	    gr.query();
		gs.log("ClearAll will attempt to delete: " + gr.getRowCount() + "remote rows", "Perspectium");
		while(gr.next()) {
			this.deleteRecord(gr);
		}
	},
	
	createDBCommandOut: function(topic, type, key, name, value, target_queue, extra) {
		var perspectium = new Perspectium();
		var strBody = "{\"topic\":\"" + topic + "\", \"type\":\"" + type + "\", \"key\":\"" + key + "\", \"name\":\"" + name + "\", \"value\":\"" + value.replace(/\"/g, "\\\"") + "\", \"attributes\":\"\"}";
		gs.log("createDBCommandOut - body: " + strBody, "Perspectium");
		var rc = perspectium.postOne(strBody, this.queue, false);

		if(rc && rc == 500){
			gs.logError("createDBCommandOut - received status 500 from MBS", "Perspectium");
			gs.addErrorMessage("An error occurred performing this action in the Perspectium Message Bus.");
		}
		else if( rc && rc == 403){
			gs.logError("createDBCommandOut - received status 403 from MBS", "Perspectium");
			gs.addErrorMessage("You do not have permission to perfom this action in the Perspectium Message Bus.");
		}
		return rc;
    },
	
	createDBCommandOutEscapedQuotes: function(topic, type, key, name, value, target_queue, extra) {
		var perspectium = new Perspectium();
		var strBody = "{\"topic\":\"" + topic + "\", \"type\":\"" + type + "\", \"key\":\"" + key + "\", \"name\":\"" + name + "\", \"value\":\"" + value.replace(/\"/g, "\\\"") + "\", \"attributes\":\"\"}";
		gs.log("createDBCommandOut - body: " + strBody, "Perspectium");
		var rc = perspectium.postOne(strBody, this.queue, false);

		if(rc && rc == 500){
			gs.logError("createDBCommandOut - received status 500 from MBS", "Perspectium");
			gs.addErrorMessage("An error occurred performing this action in the Perspectium Message Bus.");
		}
		else if( rc && rc == 403){
			gs.logError("createDBCommandOut - received status 403 from MBS", "Perspectium");
			gs.addErrorMessage("You do not have permission to perfom this action in the Perspectium Message Bus.");
		}
		return rc;
    },	
	
	setStripTablePrefix:function(bStrip){
		this.stripTablePrefix = bStrip;
	},
	
	setStripFieldPrefix:function(bStrip){
		this.stripFieldPrefix = bStrip;
	},
	
	setKeySuffix:function(suffix){
		this.keySuffix = suffix;
	},
	
	setQueue:function(queue){
		this.queue = queue;
	},

    type: 'PerspectiumDBCommand'
	
}
	