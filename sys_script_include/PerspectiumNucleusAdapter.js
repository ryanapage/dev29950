/**
 * PerspectiumNucleusAdapter contains methods used for generating adapter templates based
 * on a common JSON structure. 
 * Outbound Map: template used for creating payload objects for 3rd party services.
 * Inbound Map: template used for creating common document payloads with 3rd party data.
 * 
 * Payload
 * {
 *          'commonDoc' : '',
 *          'provider' : '',
 *          'client' : '',
 *          'action' : '',
 *          'source' : '',
 *          'target' : '',
 *          'description' : '',
 *          'body' : {
 *              'node_name' : {
 *                  'mapping' : '',
 *                  'script' : ''
 *              },
 *              'unique_parent' : [
 *                  {
 *                      'name' : 'non-unique name',
 *                      'mapping' : '',
 *                      'script' : ''
 *                  }
 *              ]
 *          }
 *       }
 */

var PerspectiumNucleusAdapter = Class.create();
PerspectiumNucleusAdapter.prototype = {
    initialize: function () {
        this.JIRA = 'jira';
        this.ISSUETYPE = 'issuetype';
        this.PRIORITY = 'priority';
        this.mapUtil = new PerspectiumNucleusMapping();
    }, 
    /********** ATOS **********/
    /********** ATT **********/

    generateATTOutboundMap: function(payload) {

        var p = {
            'commonDoc' : '',
            'provider' : '',
            'client' : '',
            'action' : '',
            'source' : '',
            'target' : '',
            'description' : '',
            'body' : {
                'node_name' : {
                    'mapping' : '',
                    'script' : ''
                },
                'unique_parent' : [
                    {
                        'name' : 'non-unique name',
                        'mapping' : '',
                        'script' : ''
                    }
                ]
            }
        }

    },

    generateATTInboundMap: function(payload) {

    },
 
    /********** BT **********/

    generateBTOutboundMap: function(payload) {

    },

    generateBTInboundMap: function(payload) {

    },

    /********** CHERWELL **********/

    generateCherwellOutboundMap: function(payload) {

    }, 

    generateCherwellInboundMap: function(payload) {

    },

    /********** GETRONICS **********/

    /********** JIRA **********/
    /**
     * generate map for common -> jira format
     * @param: payload: json
     * ex. 
     * {
     *  values: {
     *          'attributes':'mapped_attributes'
     *      },
     *  commonDoc: 'common_change'
     * }
     * TODO:
     * - error handling
     */
    generateJiraOutboundMap: function (payload) {
        var values = payload.values;
        var commonDoc = payload.commonDoc;
        var map = {
            fields: {}
        };

        for (var key in values) { // clean up to switch if cases added
            if (key == this.ISSUETYPE) {
                map.fields[key] = {
                    name: values[key]
                }
            } else if (key == this.PRIORITY) {
                map.fields[key] = {
                    name: this.mapUtil.generateXPath(commonDoc, values[key])
                }
            } else {
                map.fields[key] = this.mapUtil.generateXPath(commonDoc, values[key]);
            }
        }
        return map;
    },

    /**
     * generate map for jira -> common format
     * TODO:
     * - error handling
     */
    generateJiraInboundMap: function (payload) {
        // normalize json to common doc format
        var values = this.mapUtil.getCommonJson(this.JIRA, payload.commonDoc, payload.values);
        return this.mapUtil.generateCommonDoc(payload.commonDoc, values);
    },

    /********** GETRONICS **********/

    generateGetronicsOutboundMap: function(payload) {

    },

    generateGetronicsInboundMap: function(payload) {

    },

    /********** REMEDY **********/

    generateRemedyInboundMap: function () {
        // todo
    },

    generateRemedyOutboundMap: function () {
        // todo
    },

    /********** SERVICEMANAGER **********/

    generateServiceManagerInboundMap: function(payload) {

    }, 

    generateServiceManagerOutboundMap: function(payload) {

    },

    /********** SERVICENOW **********/
    
    // TODO?
    
    /********** SHAREPOINT **********/
    
    generateSharepointInboundMap: function(payload) {

    },

    generateSharepointOutboundMap: function(payload) {

    },

    /********** TIVIT **********/

    generateTivitInboundMap: function(payload) {

    },

    generateTivitOutboundMap: function(payload) {

    },

    type: 'PerspectiumNucleusAdapter'
};