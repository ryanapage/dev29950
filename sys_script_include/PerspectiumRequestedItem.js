var PerspectiumRequestedItem = Class.create();
PerspectiumRequestedItem.prototype = {
 	initialize: function() {
		
        this.logger = new PerspectiumLogger();
    },

    addRequestedItems : function(source, target, pspTag /* optional */) {
        var targetId = target.sys_id;
		var targetTable = target.getTableName();
        this.logger.logDebug('Adding requested item for: ' + targetTable + "," + targetId, "PerspectiumRequestedItem.addRequestedItems");

        if (source == null || source.u_requested_items.isNil()) {
            this.logger.logDebug('No requested items found', "PerspectiumRequestedItem.addRequestedItems");
            return;
        }

        var xmldoc = null;
        try {
             xmldoc = new XMLDocument(source.u_requested_items);
        } catch(err){
            this.logger.logDebug('Error creating XMLDocument for requested items: ' + err, "PerspectiumRequestedItem.addRequestedItems");
            return;
        }

        if (xmldoc == null) {
           this.logger.logDebug('XMLDocument is null', "PerspectiumRequestedItem.addRequestedItems");
           return;
        }

        var nodelist = xmldoc.getNodes("//requested_item");
        if (nodelist == null) {
            this.logger.logDebug('No requested item nodes found', "PerspectiumRequestedItem.addRequestedItems");
            return;
        }

        this.logger.logDebug('Found ' + nodelist.getLength() + ' requested item', "PerspectiumRequestedItem.addRequestedItems");
        for (var i=0; i < nodelist.getLength(); i++) {
            var nodeItem = nodelist.item(i);
            this.addRequestedItem(nodeItem, targetTable, targetId, pspTag);
        }
    },
	
	_getValue : function(nodeItem, nodeName) {        
		var xml_util = (typeof GlideXMLUtil != 'undefined') ? new GlideXMLUtil() : new Packages.com.glide.util.XMLUtil();
		var it = xml_util.childElementIterator(nodeItem);
        while (it.hasNext()) {
            var el = it.next();
            var nn = el.getNodeName();
            var nodeValue = xml_util.getText(el);
			
			if( nodeValue == null)
				nodeValue = "";
			
			if(nn == nodeName) {
				return String(nodeValue);
			}
		}
											  
		return "";
	},
	
	_setValues : function(ct, ctNumber, targetId, nodeItem) {
		ct.correlation_id = ctNumber;
		ct.request = targetId;	
		
		ct.quantity = this._getValue(nodeItem, "quantity");
		ct.cat_item = this._getValue(nodeItem, "cat_item");		
		ct.due_date = this._getValue(nodeItem, "due_date");		
		ct.price = this._getValue(nodeItem, "price");
		ct.stage = this._getValue(nodeItem, "stage");
		ct.assigned_to = this._getValue(nodeItem, "assigned_to");
		ct.sc_catalog = this._getValue(nodeItem, "sc_catalog");
	},

    addRequestedItem : function(nodeItem, targetTable, targetId, pspTag /* optional */) {
        var StringUtil = (typeof GlideStringUtil != 'undefined') ? new GlideStringUtil() : new Packages.com.glide.util.StringUtil();
		
		var ctNumber = this._getValue(nodeItem, "number");
		
		if (ctNumber == null)
			return; // no sys_id, cannot search
		
		// first see if we can find request item by sys_id
		var ct = new GlideRecord("sc_req_item");
		ct.addQuery("correlation_id", ctNumber);
		ct.query();
		if (!ct.next()) {
			ct.initialize();
			this._setValues(ct, ctNumber, targetId, nodeItem);
			ct.insert();
		} else {
			this._setValues(ct, ctNumber, targetId, nodeItem);
			ct.update();
		}
		
		// if pspTag exists, go and tag it
		if (pspTag) {
			var pspUtil = new PerspectiumUtil();
			pspUtil.addTag(ct, pspTag);
		}
    },

    type: 'PerspectiumRequestedItem'
};