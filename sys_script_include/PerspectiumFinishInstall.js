var PerspectiumFinishInstall = Class.create();
var STATUS_SUCCESS = 200;
var STATUS_FAILURE = 500;
var status = {
    code: -1,
    errors: []
};

PerspectiumFinishInstall.prototype = Object.extendsObject(AbstractAjaxProcessor, {

    finishInstall: function () {
        gs.print("*************************");
        gs.print("Starting Finish Install...");

        var logger = new PerspectiumLogger();

        try {
            if (gs.getCurrentScopeName().indexOf("global") < 0) {
                var scopeErrorMsg = "Perspectium application was not installed in the global scope and will not function properly. Please re-install it in the global scope.";
                gs.print(scopeErrorMsg);
                status.errors.push(scopeErrorMsg);
                var session = gs.getSession();
                session.setInteractive(true);
                var notification = new UINotification();
                notification.setText(scopeErrorMsg);
                notification.send();
                session.setInteractive(false);

                status.code = STATUS_FAILURE;
                status.errors.push(scopeErrorMsg);
                return JSON.stringify(status);
            }
        } catch (e) {
            gs.print("error checking scope: " + e);
            logger.logError("error checking scope: " + e, "Perspectium.FinishInstall");
            status.errors.push("error checking scope: " + e);
        }

        try {

            var gr = new GlideRecord("sysauto_script");
            gr.addEncodedQuery("nameSTARTSWITHPerspectium^nameENDSWITHInstall");
            gr.query();
            while (gr.next()) {
                gs.print("Executing " + gr.name + " job...");
                SncTriggerSynchronizer.executeNow(gr);
                /*var pspSO = new ScheduleOnce();		
                var gdt = new GlideDateTime();
                gdt.addSeconds(1); 		
                pspSO.setTime(gdt.getValue()); 		
                pspSO.script = gr.script;				
                pspSO.schedule();*/
            }
			
			var pspRefresh = new PerspectiumRefreshHistorySet();
			pspRefresh.validateExistingSubscribeConfigurations();

            gs.sleep(3000);
            gs.print("");
            gs.print("Starting interval scheduled jobs...");
            var pspGR = new GlideRecord("sysauto");
            pspGR.addEncodedQuery("nameSTARTSWITHPerspectium^nameNOT LIKEInstall");
            pspGR.query();
            while (pspGR.next()) {
                if (pspGR.getValue("active") == true) {
                    continue;
                }

                pspGR.active = "true";
                pspGR.update();
                gs.print(pspGR.name + " started");
            }


            gs.print("");
            gs.print("Resetting dynamic share business rules...");
            var psp = new PerspectiumReplicator();
            psp.synchDynShareRules();

            this.checkInstanceCreatedOn();

            var sgr = new GlideRecord("sys_db_object");
            sgr.addQuery("name", "psp_out_message");
            sgr.query();
            if (sgr.next()) {
                var igr = new GlideRecord("v_index_creator");
                //igr.addEncodedQuery("table=fa9c36fa20fe19408121459a3546860e");
                igr.addQuery("table", sgr.sys_id);
                igr.query();
                // display if it only contains the default indexes as we want to have our optimized indexes
                if (igr.getRowCount() <= 3) {
                    gs.print("");
                    gs.print("*************************");
                    gs.print("For optimal performance, please index the table " + sgr.name + " as noted at");
                    gs.print("http://wiki.perspectium.com/doku.php?id=replicator_optimization");
                    gs.print("*************************");
                }
            }
        } catch (e) {
            gs.print("error = " + e);
            logger.logError("error = " + e, "Perspectium.FinishInstall");
            status.errors.push("error = " + e);
        }

        gs.print("");
        gs.print("*************************");
        gs.print('Finished installing Perspectium update set. Please review your Perspectium properties and update accordingly');
		
		this.validatePropertyFields();

        status.code = STATUS_SUCCESS;
        return JSON.stringify(status);
    },

    checkInstanceCreatedOn: function () {
        var gr = new GlideRecord('u_psp_queues');
        gr.addQuery('u_direction', 'subscribe');
        gr.addNullQuery('u_instance_created_on');
        gr.query();
        while (gr.next()) {
            gr.u_instance_created_on = gs.getProperty('instance_name');
            gr.update();
        }
    },

    /**
     * Validate current property field values and ensure consistency with master list.
     */
    validatePropertyFields: function() {
        var pspUtil = new PerspectiumUtil();
        var properties = new PerspectiumInstall().pspProperties;
        var field = "u_type";

        // iterate and enforce type
        for (var i = 0; i < properties.length; i++) {
            var property = properties[i];
            if (property.type != pspUtil.getPspProperty(property.name, field)) {
                pspUtil.setPspProperty(property.name, field, property.type);
            }
        }
    },

    type: 'PerspectiumFinishInstall'
});