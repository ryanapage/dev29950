var PerspectiumDataCleaner = Class.create();
PerspectiumDataCleaner.prototype = {
    initialize: function() {
    },
	
	cleanTable: function(table, encodedQuery) {
		var pspUtil = new PerspectiumUtil();
		var deleteIndividually = pspUtil.getPspPropertyValue("com.perspectium.data_cleaner.delete.individual", "false");
		var grPOut = new GlideRecord(table);
		grPOut.addEncodedQuery(encodedQuery);
		
		// so deletes aren't audited as suggested by servicenow team
		grPOut.setWorkflow(false);
		
		if (deleteIndividually == true || deleteIndividually == "true") {
			grPOut.query();
			while (grPOut.next())
				grPOut.deleteRecord();
		}
		else
			grPOut.deleteMultiple(); 
	},

    type: 'PerspectiumDataCleaner'
};