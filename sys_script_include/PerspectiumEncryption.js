var PerspectiumEncryption = Class.create();
PerspectiumEncryption.prototype = {
    initialize: function(customEKey) {
    	this.psp = new Perspectium();
		this.logger = new PerspectiumLogger();
		this.pspUtil = new PerspectiumUtil();
		this.addDisplayValues = this.pspUtil.getPspPropertyValue("com.perspectium.replicator.add_display_values", "false");
		this.currentFieldsOnly = this.pspUtil.getPspPropertyValue("com.perspectium.replicator.share_current_fields", "false");
		this.inboundKey = this.pspUtil.getPspPropertyValue("com.perspectium.replicator.inbound.encryption_key", "nothingmustbemorethan24bytes");
		this.outboundKey = this.pspUtil.getPspPropertyValue("com.perspectium.replicator.outbound.encryption_key", "nothingmustbemorethan24bytes");
		this.encryptionMode = this.pspUtil.getPspPropertyValue("com.perspectium.replicator.encryption_mode", "encrypted");
		
		this.maxBytes = parseInt(this.pspUtil.getPspPropertyValue("com.perspectium.output_bytes_limit", "5000000")); // max bytes to cap the send string limit of 16MB for Java String

		if (customEKey) {
			this.customKey = customEKey;
		}
    },

    getEncrypter: function(propName) {
		var eKey = "nothingmustbemorethan24bytes";
		if (this.customKey && this.customKey != "") {
			eKey = this.customKey;
		} 
		else if (propName == "com.perspectium.replicator.inbound.encryption_key") {
			eKey = this.inboundKey;
		}
		else if (propName == "com.perspectium.replicator.outbound.encryption_key") {
			eKey = this.outboundKey;
		}
		else {
        	eKey = this.pspUtil.getPspPropertyValue(propName, "nothingmustbemorethan24bytes");
		}
		
        var Encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter(eKey) : new Packages.com.glide.util.Encrypter(eKey);

        return Encrypter;
    },

	encryptStringWithCipher: function( source, cipher ){
		if( typeof GlideEncrypter != 'undefined' && cipher == "0")
			return source;
		else if( typeof GlideEncrypter != 'undefined' && cipher == "2")
			return this.encryptAESString(source);
		else if( typeof GlideEncrypter != 'undefined' && cipher == "3"){
			var StringUtil = (typeof GlideStringUtil != 'undefined') ? new GlideStringUtil() : new Packages.com.glide.util.StringUtil();
			return StringUtil.base64Encode(source);
		}
		else
			return this.encryptString(source);
	},

	decryptStringWithCipher: function( source, cipher ){
		if( typeof GlideEncrypter != 'undefined' && cipher == "0")
			return source;
		else if( typeof GlideEncrypter != 'undefined' && cipher == "2")
			return this.decryptAESString(source);
		else if( typeof GlideEncrypter != 'undefined' && cipher == "3"){
			var StringUtil = (typeof GlideStringUtil != 'undefined') ? new GlideStringUtil() : new Packages.com.glide.util.StringUtil();
			return StringUtil.base64Decode(source);
		}
		else
			return this.decryptString(source);
	},

	getOutboundTableMap: function(tableMapId){
		var tmap = new GlideRecord('u_psp_table_map');
		tmap.addQuery('sys_id', tableMapId);
		tmap.addQuery('u_direction', 'outbound');
		tmap.query();
		if(tmap.next()){
			return tmap;
		}
		else
			return;
	},

	encryptTableMap: function(grParam, grTableMap, cipher, pspTag) {
		this.logger.logDebug("encryptTableMap: " + grTableMap.u_name,"PerspectiumEncryption.encrypt");

		if(grTableMap.u_use_script){
			this.logger.logDebug("Using tablemap script");
			var gc = (typeof GlideController != 'undefined') ? GlideController : Packages.com.glide.script.GlideController;
			var oldCurrent = current;
			gc.putGlobal('current', grParam);
			var answer = gc.evaluateString(grTableMap.u_script);
			gc.removeGlobal('current');
			current = oldCurrent;
			this.logger.logDebug("Table map script returning: " + answer);
			return this.encryptStringWithCipher(answer, cipher);
		}
		var fmap = new GlideRecord('u_psp_table_field_map');
		fmap.addQuery('u_parentid' , grTableMap.sys_id);
		// so output XML shows fields in alphabetical order
		fmap.orderBy('u_target_field');
		fmap.query();
		var mappedvalue;
		var useJSON = false;
		if( grTableMap.u_format == "JSON" )
			useJSON = true;
		if(useJSON)
			mappedvalue = {};
		else
			mappedvalue = new XMLDocument('<'+grTableMap.u_target_tablename+'></'+grTableMap.u_target_tablename+'>');
		while(fmap.next()) {
			if(fmap.u_target_field.startsWith("@"))
				continue; //skip mapped attributes

			if(fmap.u_source_field.startsWith("${")){
				var targetValue = this.pspUtil.resolveVariables(grParam, fmap.u_source_field, pspTag);

				// if the value is too large then we'll error
				// account for both if the value is bigger than our max sending size
				// as well as the largest size allowed for a String
				if (targetValue.length > this.maxBytes || (targetValue.length * 2) > 16000000) {
					this.logger.logError("value length " + targetValue.length + " is too large for field <" + fmap.u_target_field + "> and will not be created in outbound message for " + grParam.getTableName() + " record " + grParam.sys_id, "PerspectiumEncryption.encryptTableMap");
					continue;
				}
				
				// create first element that will hold all the
				var targetEl = mappedvalue.createElement(fmap.u_target_field);

				// blank value so don't need to create any child elements
				if(targetValue == ""){
					continue;
				}

								
				this.logger.logDebug("resolved value: " + targetValue, "PerspectiumEncryption.encryptTableMap");

				// set current element so we can create elements under it
				mappedvalue.setCurrent(targetEl);

				// resolveVariables() should return an XML doc that we'll go through
				// and create as elements in the mapped XML
				var resolvedXmlDoc = new XMLDocument(targetValue);
				var nodelist = resolvedXmlDoc.getDocumentElement().getChildNodes();
				for (j = 0; j < nodelist.getLength(); j++) {
					var n = nodelist.item(j);

					var nodelist2 = n.getChildNodes();
					// has child elements so we'll go through and create elements for each child element
					if(nodelist2 && nodelist2.getLength() > 0){
						// first create this element in the mapped xml so we can add the child elements to it
						var n2 = mappedvalue.createElement(n.getNodeName());
						mappedvalue.setCurrent(n2);
						for (jj = 0; jj < nodelist2.getLength(); jj++) {
							var nn = nodelist2.item(jj);
							mappedvalue.createElement(nn.getNodeName(), nn.getTextContent());
						}
						// after adding child elements set current back
						mappedvalue.setCurrent(targetEl);
					}
					// otherwise we just add this element's contents to the mapped XML
					else{
						mappedvalue.createElement(n.getNodeName(), n.getTextContent());
					}

				}

				// after we finish set current back to root element of this mapped xml
				mappedvalue.setCurrent(mappedvalue.getDocumentElement());
			}
			else if(!fmap.u_use_script){
				if(useJSON){
					mappedvalue[fmap.u_target_field] = grParam.getValue(fmap.u_source_field);
				}
				else {
					//mappedvalue.createElement(fmap.u_target_field, grParam.getValue(fmap.u_source_field));
					var targetFieldValue = grParam.getValue(fmap.u_source_field);
					//checks if the user wants CDATA tags to be added to the field. 
					if (fmap.u_add_cdata_tags) {
						targetFieldValue = "<![CDATA[" + grParam.getValue(fmap.u_source_field) + "]]>";
					}
					
					mappedvalue.createElement(fmap.u_target_field, targetFieldValue);				
				}				
			} else {
				var gc = (typeof GlideController != 'undefined') ? GlideController : Packages.com.glide.script.GlideController;
				var oldCurrent = current;
				gc.putGlobal('current', grParam);
				var answer = gc.evaluateString(fmap.u_source_script);
				gc.removeGlobal('current');		
				if(useJSON){
					if( answer == null){
						//skip element
					}	
					else{
						mappedvalue[fmap.u_target_field] = answer;			
					}				
				}
				else {
					var targetFieldValue = answer;
					//checks if the user wants CDATA tags added to the field. 
					if (fmap.u_add_cdata_tags) {
						targetFieldValue = "<![CDATA[" + answer + "]]>";
					}

					mappedvalue.createElement(fmap.u_target_field, targetFieldValue);
				}
				current = oldCurrent;
			}
		}
		
		var strMappedValue;
		if(useJSON)
			strMappedValue = JSON.stringify(mappedvalue);
		else
			strMappedValue = mappedvalue.toString();
		this.logger.logDebug("Table Map used to build output " + strMappedValue, "PerspectiumEncryption.encrypt");
		return this.encryptStringWithCipher(strMappedValue, cipher);

	},

    encrypt: function(grParam, viewName, operator, targetQueue, cipher) {
        var recordSerializer = (typeof GlideRecordXMLSerializer != 'undefined') ? new GlideRecordXMLSerializer() : new Packages.com.glide.script.GlideRecordXMLSerializer();

		var xmlstr = recordSerializer.serialize(grParam);

		var xmlDoc = new XMLDocument(xmlstr);
		if (xmlstr.indexOf("<sys_translated_") > 0 && operator && operator != "") {
			this.detectAndEmitEmbededRecords(xmlDoc, grParam.getTableName(), operator, targetQueue);
		}

		if (this.currentFieldsOnly != "true" && this.addDisplayValues != "true" && !viewName) {
			return this.encryptByCipher(xmlstr, cipher);
		}

		var vName = "";
		if (viewName && viewName != null) {
			vName = viewName;
		}

        //var fl = (typeof GlideFieldList != 'undefined') ? new GlideFieldList() : new Packages.com.glide.processors.FieldList();
        //var fieldNames = fl.get(grParam.getTableName(), vName);
		var tableName = grParam.getTableName();

		var arrFields = grParam.getFields();
		for (var i = 0; i < arrFields.size(); i++) {
			var glideElement = arrFields.get(i);
			var ed = glideElement.getED();
			var elName = glideElement.getName();
			//this.logger.logDebug(elName + "  " + tableName + ":" + ed.getTableName(),"PerspectiumEncryption.encrypt");

			// use getFields() so we can get all fields including inactive fields, GlideFieldList does not return inactive fields
			if (this.currentFieldsOnly == "true" && tableName != ed.getTableName()) {
				//this.logger.logDebug("removing element " + elName,"PerspectiumEncryption.encrypt");
				var nn = xmlDoc.getElementByTagName(elName);
				if(nn && nn.parentNode) {
					nn.parentNode.removeChild(nn);
					this.logger.logDebug("removed element " + elName,"PerspectiumEncryption.encrypt");
				}

				continue;
			}

			if (this.addDisplayValues == "true" && (ed.isReference() || ed.isChoiceTable() 
					|| ed.getInternalType() == "glide_list" || ed.getInternalType() == "glide_duration")) {
				// for choice fields we'll check if it's a dependent to get the right display value
				var dependentField = glideElement.getDependent();
				if (ed.isChoiceTable() && dependentField != null) {
					this.processDependentChoiceValue(xmlDoc, grParam, glideElement);
				}
				else {
					xmlDoc.createElement("dv_" + elName, glideElement.getDisplayValue());
				}				
			}
			
		}

		return this.encryptByCipher(xmlDoc.toString(), cipher);
    },
	
	processDependentChoiceValue : function(xmlDoc, grParam, glideElement) {
		// Handling for platform bug where display values for choices with dependent values are not always honored correctly
		var dependentValue = grParam.getValue(glideElement.getDependent());
		var tableName = grParam.getTableName();
		var ed = glideElement.getED();
		var elName = glideElement.getName();
		
		// if we don't have a dependent value for this we'll just use regular display value
		if (dependentValue == null || dependentValue == "") {
			xmlDoc.createElement("dv_" + elName, glideElement.getDisplayValue());
			return;	
		}

		var lang = gs.getSession().getLanguage();
		var choiceTableName = tableName;
		if (ed.getChoiceTable()) {
			choiceTableName = ed.getChoiceTable().toString();
		}

		// find the sys_choice with this dependent value so we can use the label which will be the proper display value
		var scGr = new GlideRecord("sys_choice");
		scGr.addQuery("dependent_value", dependentValue);
		scGr.addQuery("element", elName);
		scGr.addQuery("value", grParam.getValue(elName));
		scGr.addQuery("name", choiceTableName);

		var qCondition = scGr.addQuery("language", lang);
		
		//Remove check for domain
		/*
		if (grParam.sys_domain) {
			scGr.addQuery("sys_domain", grParam.sys_domain);
		}
		*/

		scGr.query();
		if (scGr.next()) {
			xmlDoc.createElement("dv_" + elName, scGr.label);
		} else { // default to english
			qCondition.addOrCondition("language", "en");
			scGr.query();

			if (scGr.next()) {
				xmlDoc.createElement("dv_" + elName, scGr.label);
			}
		}
	},
	
	encryptByCipher: function(source, cipher) {
		var Encrypter = this.getEncrypter("com.perspectium.replicator.outbound.encryption_key");
		var StringUtil = (typeof GlideStringUtil != 'undefined') ? new GlideStringUtil() : new Packages.com.glide.util.StringUtil();
		
		if (this.encryptionMode == "encrypted_multibyte" && cipher != "0" && cipher != "3"){
			source = StringUtil.base64Encode(source);
		}
		
		if (cipher == "1"){
			return Encrypter.encrypt(source);
		}
		else if (cipher == "2"){
			return this.encryptAESString(source);
		}
		else if (cipher == "3"){
			return StringUtil.base64Encode(source);
		}
		else //unecrypted
			return source;
	
	}, 

	detectAndEmitEmbededRecords : function(xmlDoc, tableName, op, targetQueue) {
		var nodelist = xmlDoc.getNodes("//" + tableName + "/*");
		this.logger.logDebug(tableName + " " + op + " " + nodelist.getLength(),"PerspectiumEncryption.detectAndEmitEmbededRecords");
		var iterator = n.getChildNodeIterator();
		while (iterator.hasNext()) {
			var n = iterator.next();
			var lchild = n.getLastChild();
			if (lchild == null) {
				if (n.getNodeName() == "sys_translated_text") {
					//  hack alert, need to improve this code soon
					// remove action="delete" sys_translated_text
					n.parentNode.removeChild(n);
				}
				
				continue;
			}

			this.logger.logDebug(n.getNodeName() + ": " + n.getNodeType() + ", " + lchild.getNodeType() ,"PerspectiumEncryption.detectAndEmitEmbededRecords");

			if (n.getNodeType() != Packages.org.w3c.dom.Node.ELEMENT_NODE) {
				continue;
			}

			if (lchild.getNodeType() != Packages.org.w3c.dom.Node.ELEMENT_NODE) {
				continue;
			}

			this.logger.logDebug("emitting " + tableName + "." + n.getNodeName(), "PerspectiumEncryption.detectAndEmitEmbededRecords");

			var nDoc = new XMLDocument("<" + n.getNodeName() + "></" + n.getNodeName() + ">");
			var nodelist2 = n.getChildNodes();
			var iterator2 = n.getChildNodeIterator();
			while(iterator2.hasNext()) {
				var nn = iterator2.next();
				nDoc.createElement(nn.getNodeName(), nn.getTextContent());
			}
			this.logger.logDebug(nDoc.toString(), "PerspectiumEncryption.detectAndEmitEmbededRecords");
			var enc = this.encryptString(nDoc.toString());
            var name = n.getNodeName() + "." + op;
            var key = gs.getProperty("instance_name", "unregistered");
            this.psp.createPSPOut("replicator", "servicenow", key, name, enc, targetQueue);

			// remove myself
			n.parentNode.removeChild(n);
		}
	},

	encryptString : function(source) {
		// 3 modes of encryption: "encrypted", "encrypted_multibyte", "unencrypted"
		var Encrypter = this.getEncrypter("com.perspectium.replicator.outbound.encryption_key");
		var StringUtil = (typeof GlideStringUtil != 'undefined') ? new GlideStringUtil() : new Packages.com.glide.util.StringUtil();

		if (this.encryptionMode == "unencrypted") {
			return source;
		}

		if (this.encryptionMode == "encrypted_multibyte") { // to be multi-byte, multi-language safe, base64 encode before encryption
			return Encrypter.encrypt(StringUtil.base64Encode(source));
		}

		if (this.encryptionMode != "encrypted") {
			this.logger.logWarning("Unsupported encryption mode '" + this.encryptionMode + "', defaulting to 'encrypted'", "PerspectiumEncryption.encryptString");
		}

		return Encrypter.encrypt(source);
	},

	decryptString : function(source) {
		if (source == null) {
		    return null;
		}

		// 3 modes of encryption: "encrypted", "encrypted_multibyte", "unencrypted"
        var Encrypter = this.getEncrypter("com.perspectium.replicator.inbound.encryption_key");
		var StringUtil = (typeof GlideStringUtil != 'undefined') ? new GlideStringUtil() : new Packages.com.glide.util.StringUtil();

		// test source and decrypt intelligently

		// 1. if source is XML or JSON, just return
		if (this.isXMLString(source)) {
			return source;
		}

		// 2. regular decryption
		var d = Encrypter.decrypt(source);
		if (this.isXMLString(d)) {
			return d;
		}
		
		if (this.isJSONString(d)) {
			return d;
		}

		// 3. it was base64 encoded because it was encrypted_multibyte at the source
		var e = StringUtil.base64Decode(d);
		return e;
	},

	encryptAESString : function(source){
		// typecast to javascript string so CryptoJS can work properly
		this.outboundKey = String(this.outboundKey);
		
		this.logger.logDebug("encryptAESString - key: " + this.outboundKey);

		var iv_val = this.outboundKey.substring(0,16);
		// Encrypt...
		var plaintextArray;
		try{
			plaintextArray = PerspectiumCryptoJS.AES.encrypt(
			  PerspectiumCryptoJS.enc.Latin1.parse("16 bytes padding" + source),
			  PerspectiumCryptoJS.enc.Latin1.parse(this.outboundKey.substring(0,16)),
			  { iv:PerspectiumCryptoJS.enc.Latin1.parse(iv_val) }
			);
		}
		catch(e){
			this.logger.logDebug("encryptAESString - Exception thrown");
		}
		this.logger.logDebug("encrypted content: " + plaintextArray);
		return new Packages.java.lang.String(plaintextArray.ciphertext.toString(PerspectiumCryptoJS.enc.Base64));
	},

	decryptAESString : function(source){
		// typecast to javascript string so CryptoJS can work properly
		this.inboundKey = String(this.inboundKey);
		
		this.logger.logDebug("decryptAESString - key: " + this.inboundKey);
		var rawData = PerspectiumCryptoJS.enc.Base64.parse(source).toString(PerspectiumCryptoJS.enc.Latin1);

		var iv_val = rawData.substring(0,16);
		var crypttext = rawData.substring(16);
		// Decrypt...
		var plaintextArray;
		try{
			plaintextArray = PerspectiumCryptoJS.AES.decrypt(
			  { ciphertext:PerspectiumCryptoJS.enc.Latin1.parse(crypttext) },
			  PerspectiumCryptoJS.enc.Latin1.parse(this.inboundKey.substring(0,16)),
			  { iv:PerspectiumCryptoJS.enc.Latin1.parse(iv_val) }
			);
		}
		catch(e){
			this.logger.logDebug("decryptAESString - Exception thrown");
		}
		
		var decryptedValue = new Packages.java.lang.String(PerspectiumCryptoJS.enc.Latin1.stringify(plaintextArray));
		
		//check for multibyte encryption
		if(this.isJSONString(decryptedValue) || this.isXMLString(decryptedValue))
			return decryptedValue;
		
		var StringUtil = (typeof GlideStringUtil != 'undefined') ? new GlideStringUtil() : new Packages.com.glide.util.StringUtil();
		
		return StringUtil.base64Decode(decryptedValue);
	},

	isXMLString : function(source){
		try{
			var d = new XMLDocument(source);
			return true;
		}
		catch(err){
			return false;
		}
	},
	
	isJSONString : function(source){
		try{
			var j = new JSON();
			j = JSON.parse(source);
			//if (j.indexOf("{") == 0)
			if (source.indexOf("{") == 0)
				return true;
			else
				return false;
		}
		catch(err){
			return false;
		}
	},

    type: 'PerspectiumEncryption'
};