var PerspectiumKnowledgeFeedback = Class.create();
PerspectiumKnowledgeFeedback.prototype = {
    initialize: function() {
    },
	addKnowledgeFeedbacks: function(nodelist, source) {		
		if (nodelist == null)
			return;
		
		for (var i = 0; i < nodelist.getLength(); i++) {
			var node = nodelist.item(i);
			var gr = new GlideRecord('kb_knowledge');
			gr.addQuery('number', source.u_number);
			gr.query();
			
			if (!gr.next())
				return;
			
			var kgr = new GlideRecord('kb_feedback');
			kgr.addQuery('comments', this._getValue(node, 'comments'));
			kgr.addQuery('sys_created_on', this._getValue(node, 'sys_created_on'));
			kgr.query();
			
			if (kgr.next()) {
				kgr.article = gr.sys_id;
				kgr.update();
			} else {
				kgr.article = gr.sys_id;
				kgr.comments = this._getValue(node, 'comments');
				kgr.flagged = this._getValue(node, 'flagged');
				kgr.live_message = this._getValue(node, 'live_message');
				kgr.parent_feedback = this._getValue(node, 'parent_feedback');
				kgr.query = this._getValue(node, 'query');
				kgr.rating = this._getValue(node, 'rating');
				kgr.resolved = this._getValue(node, 'resolved');
				kgr.root_feedback = this._getValue(node, 'root_feedback');
				kgr.search_id = this._getValue(node, 'search_id');
				kgr.session_id = this._getValue(node, 'session_id');
				kgr.insert();
			}
		}
	},
	_getValue: function(nodeItem, nodeName) {        
		var xml_util = (typeof GlideXMLUtil != 'undefined') ? new GlideXMLUtil() : new Packages.com.glide.util.XMLUtil();
		var it = xml_util.childElementIterator(nodeItem);
		while (it.hasNext()) {
			var el = it.next();
			var nn = el.getNodeName();
			var nodeValue = xml_util.getText(el);

			if( nodeValue == null)
				nodeValue = "";

			if(nn == nodeName) {
				return nodeValue;
			}
		}

		return "";
	},
    type: 'PerspectiumKnowledgeFeedback'
};