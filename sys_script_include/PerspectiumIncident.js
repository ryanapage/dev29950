var PerspectiumIncident = Class.create();
PerspectiumIncident.prototype = {
 	initialize: function() {
        this.logger = new PerspectiumLogger();
    },

	_getValue : function(nodeItem, nodeName) {        
		var xml_util = (typeof GlideXMLUtil != 'undefined') ? new GlideXMLUtil() : new Packages.com.glide.util.XMLUtil();
		var it = xml_util.childElementIterator(nodeItem);
        while (it.hasNext()) {
            var el = it.next();
            var nn = el.getNodeName();
            var nodeValue = xml_util.getText(el);
			
			if( nodeValue == null)
				nodeValue = "";
			
			if(nn == nodeName) {
				return nodeValue;
			}
		}
											  
		return "";
	},
	
//       <incident>
//          <number>INC0010001</number>
//          <opened_at>2017-09-17 06:19:39</opened_at>
//          <category>inquiry</category>
//          <caller_id_name>Adela Cervantsz</caller_id_name>
//          <assigned_to_name />
//          <short_description>test</short_description>
//          <priority>5</priority>
//          <id>5e972f704f510300d8f453418110c749</id>
//          <state>1</state>
//          <assignment_group_name />
//       </incident>
	
	addIncidentFixed : function(nodeItem, targetTable, targetId, pspTag /* optional */) {
		this.addIncident("rfc", nodeItem, targetTable, targetId, pspTag);
	},
	
	addIncidentCaused : function(nodeItem, targetTable, targetId, pspTag /* optional */) {
		this.addIncident("caused_by", nodeItem, targetTable, targetId, pspTag);
	},
	
    addIncident : function(targetField, nodeItem, targetTable, targetId, pspTag /* optional */) {
        var StringUtil = (typeof GlideStringUtil != 'undefined') ? new GlideStringUtil() : new Packages.com.glide.util.StringUtil();
		
		var pID = this._getValue(nodeItem, "id");
		var number = this._getValue(nodeItem, "number");
		
		if (pID == "")
			return; // no sys_id, cannot search
		
		// first see if we can find CI by sys_id
		var p = new GlideRecord("incident");
		p.addQuery("sys_id", pID);
		p.query();
		if (!p.next()) {
			
			// try to locate by correlation_id
			p.initialize();
			p.addQuery("correlation_id", number);
			p.query();
			if (!p.next()) {
				// create the incident
				p.initialize();
				p.correlation_id = number;
				p.short_description = this._getValue(nodeItem, "short_description");
				p.opened_at = this._getValue(nodeItem, "opened_at");
				p.category = this._getValue(nodeItem, "category");
				p.priority = this._getValue(nodeItem, "priority");
				p.state = this._getValue(nodeItem, "state");
				p.insert();
			}
		}
		
		p[targetField] = targetId;
		p.update();
		
		// if pspTag exists, go and tag it
		if (pspTag) {
			var pspUtil = new PerspectiumUtil();
			pspUtil.addTag(p, pspTag);
		}
    },

    type: 'PerspectiumIncident'
};