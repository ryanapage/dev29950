var PerspectiumCountRecords = Class.create();
PerspectiumCountRecords.prototype = Object.extendsObject(AbstractAjaxProcessor, {
	
	getPromptBoolean: function() {
		var pspUtil = new PerspectiumUtil();
		return pspUtil.getPspPropertyValue("com.perspectium.replicator.verify_large_share", "false");
	},
	
	getPromptMinValue: function() {
		var pspUtil = new PerspectiumUtil();
		return pspUtil.getPspPropertyValue("com.perspectium.replicator.large_share_value", 1000000);
	},
	
	getCounts : function() {
	    var tableName = this.getParameter("sysparm_table_name");
		var sys_id = this.getParameter("sysparm_sys_id");
		var recordCount = 0;
	    var bgr = new GlideRecord("psp_bulk_share");
		bgr.get(sys_id);
		bgr.u_share_updates_since_then = this.getParameter("sysparm_updates_since");
		bgr.condition = this.getParameter("sysparm_condition");
		var limitNumberOfRecords = "u_limit_number_of_records_shared";
		if(!bgr.isValidField("u_limit_number_of_records_shared")){
			limitNumberOfRecords = "u_limit_number_of_records_shar";
		}
		bgr.setValue(limitNumberOfRecords, this.getParameter("sysparm_limit"));
		bgr.u_share_only_sys_ids_listed = this.getParameter("sysparm_sys_ids_listed");
		bgr.u_include_attachment = this.getParameter("sysparm_attachment");
		bgr.u_share_journal = this.getParameter("sysparm_share_journal");
		bgr.u_include_sys_audit = this.getParameter("sysparm_include_audit");
	    var rec = new GlideAggregate(tableName);	
		rec.addAggregate('COUNT');

	    // add condition
	    if (!bgr.condition.nil()) {
			rec.addEncodedQuery(bgr.condition);
	    }   
	   
	    if(bgr.u_share_updates_since_then == "true" || bgr.u_share_updates_since_then) {
			if (bgr.u_last_share_time == '')
				rec.addQuery('sys_updated_on', '>=', bgr.started);
			else 
				rec.addQuery('sys_updated_on','>=', bgr.u_last_share_time);
		}
		
		var sharedSysIDs;
	    if (bgr.getValue("u_share_only_sys_ids_listed") == true) {
	 	   var sgr = new GlideAggregate("u_psp_bulk_share_sys_id");
	 
	 		sgr.addQuery("u_bulk_share", bgr.sys_id);
	 		sgr.addAggregate('COUNT');
	 		sgr.query();
		 		while(sgr.next()){
	 				sharedSysIDs = sgr.getAggregate("COUNT");
	 			}	
	 	}

		rec.query();
		while(rec.next()){
	 	   recordCount = rec.getAggregate("COUNT");
		}

		var limitNumber = bgr.getValue(limitNumberOfRecords);

	    if (limitNumber != null  && parseInt(recordCount) > parseInt(limitNumber)) {	
	 		recordCount = limitNumber;
	    }
	    if (sharedSysIDs != null  && parseInt(recordCount) > parseInt(sharedSysIDs)) {	
	 		recordCount = sharedSysIDs;
	    }
	 
	    // sample journal fields, audits and attachments if selected
	    if (bgr.u_include_attachment == "true" || bgr.u_include_attachment == true) {
	    	recordCount = getAdditionalCounts("sys_attachment", "table_sys_id", bgr, recordCount);
	    }
	 
		if (bgr.u_share_journal == "true" || bgr.u_share_journal == true) {
			recordCount = getAdditionalCounts("sys_journal_field", "element_id", bgr, recordCount);	 
	    }
	 
	    if (bgr.u_include_sys_audit == "true" || bgr.u_include_sys_audit == true) {
	 	   recordCount = getAdditionalCounts("sys_audit", "documentkey", bgr, recordCount);
	    }
	  

	    return recordCount;
	},
	
	getAdditionalCounts : function(tableName, referenceFieldName, bgr, recordCount){
		var sampleTotal = 0;
	    var estimatedRecords = 0;
	    var sampleSize = parseInt(this.pspUtil.getPspPropertyValue("com.perspectium.replicator.preview_sample_size", "50"));
	    var displayTableName = tableName;

		// get sample of latest created to get better estimate to reflect latest usage
	    var pgr = new GlideRecord(bgr.table_name);
	    // add condition
	    if (!bgr.condition.nil()) {
			pgr.addEncodedQuery(bgr.condition);
	    } 
	 
		pgr.orderByDesc('sys_created_on');
		pgr.setLimit(sampleSize);	 
	    pgr.query();
		sampleSize = pgr.getRowCount();
	    while (pgr.next()) {
	 		// query other table for its records	 
	 		var agr = new GlideRecord(tableName);
	 		agr.addQuery(referenceFieldName, pgr.sys_id);
	 		agr.query();
	 		sampleTotal += agr.getRowCount();
	 
		 	// for attachments we'll also want to check for sys_attachment_doc
	 		if(tableName == 'sys_attachment'){
	 			while(agr.next()){
	 				var dgr = new GlideRecord('sys_attachment_doc');
	 				dgr.addQuery('sys_attachment', agr.sys_id);
	 				dgr.query();
	 				sampleTotal += dgr.getRowCount();
	 			}
	 
	 			displayTableName = 'attachment (sys_attachment and sys_attachment_doc)';
	 		}
	    }
	 
	    // we estimate total by using the total records / sample size * recount count
	    estimatedRecords = Math.round((sampleTotal / sampleSize) * recordCount);
	 
	    return estimatedRecords;
	},
	
	getCountGeneral: function() {
		var tableName = this.getParameter('sysparm_tablename');
		var uniqueValue = this.getParameter('sysparm_uniquevalue');
		var gr = new GlideAggregate(tableName);
		gr.addQuery('u_source', uniqueValue);
		gr.addAggregate('COUNT');
		gr.query();
		
		var rowCount = 0;
		var result = this.newItem("result");
		if (gr.next())
			rowCount = gr.getAggregate('COUNT');
		
		result.setAttribute("count", rowCount);	
	},	

    type: 'PerspectiumCountRecords'
});