var PerspectiumConfig = Class.create();
PerspectiumConfig.prototype = {
    initialize: function() {
		this.logger = new PerspectiumLogger();
		this.instanceKey = gs.getProperty("instance_name", "unregistered");
		this.configValue = "";
		this.configAttributes = "";
		this.messageValue = "";
		this.messageAttributes = "";
		
		this.pspUtil = new PerspectiumUtil();
		// encoding will make value bigger so we limit less to be below limit
		this.maxBytes = Math.round(parseInt(this.pspUtil.getPspPropertyValue("com.perspectium.output_bytes_limit", "5000000")) * .5);
	},
	
	addGlideRecordXML: function(grParam){		
		var recordSerializer = (typeof GlideRecordXMLSerializer != 'undefined') ? new GlideRecordXMLSerializer() : new Packages.com.glide.script.GlideRecordXMLSerializer();
		var xmlstr = recordSerializer.serialize(grParam);	

		// remove <?xml ?> tag created for each gliderecord xml as we don't need it
		// have to cast to JavaScript string or else it tries using Java replace method
		xmlstr = String(xmlstr).replace(/<\?xml .*\?>/i, '')

		// if we haven't added any records yet we'll create a root element for it
		if(this.configValue == ""){
			this.configValue = '<?xml version="1.0" encoding="UTF-8"?><config>';
		}
						
		// add to XML holding all the configs
		// we'll make it an unload record so it can be imported easier
		xmlstr = xmlstr.replace("<" + grParam.getTableName() + ">", "<" + grParam.getTableName() + " action=\"INSERT_OR_UPDATE\">");
		var gdt = new GlideDateTime();
		
		// don't add any more records if we're at limit for sending
		var recordXML = "<unload unload_date=\"" + gdt.toString() + "\">" + xmlstr + "</unload>";
		if(this.configValue.length + recordXML.length > this.maxBytes){
			if(this.configAttributes.indexOf("max_messsage_length=true") < 0){
				this.configAttributes += ", max_messsage_length=true";
			}
			
			return;
		}
		
		//this.configValue += "<unload unload_date=\"" + gdt.toString() + "\">" + xmlstr + "</unload>";	
		this.configValue += recordXML;
	},
		
	createOutboundMessage: function(topic, type, name, value, attributes){	
		// first encode XML in value
		var StringUtil = (typeof GlideStringUtil != 'undefined') ? new GlideStringUtil() : new Packages.com.glide.util.StringUtil();
		var encodedValue = StringUtil.base64Encode(value);	
		
		//attributes += ", component_type=servicenow";
		
		if(value.length > this.maxBytes){
			this.logger.logWarn("Reached maximum bytes size for " + topic + " message so not all " + topic + "s were sent", "PerspectiumConfig.createOutboundMessage");
			attributes += ", max_messsage_length=true";				
		}
		
		if(attributes.indexOf(",") == 0){
			attributes = attributes.substr(1);	
		}
		
		var pspM = new PerspectiumMessage(topic, type, this.instanceKey, name, encodedValue, "", "", attributes);
		pspM.enqueue();	
	},
		
	postAllConfigs: function(){
		this.postMessages();
		
		this.postReplicateConfigs();
		this.postDynamicShareBusinessRules();
		this.postBulkShareConfigs();
		this.postQueueDefinitions();
		this.postProperties();
		
		// create outbound with all configs we found
		if(this.configValue != ""){
			// add closing tag for root element before we send out
			this.configValue += "</config>";			
			this.createOutboundMessage("config", "servicenow", "config", this.configValue, this.configAttributes);
		}
		
		this.postLogs();
	},
	
	postReplicateConfigs: function(){
		var dgr = new GlideRecord("psp_replicate_conf");
		//dgr.addQuery("sync_direction", "share");
		dgr.query();
		while (dgr.next()) {
			this.addGlideRecordXML(dgr);
		}
	},
	
	postDynamicShareBusinessRules: function(){
		var dgr = new GlideRecord("sys_script");
		dgr.addQuery('name', 'STARTSWITH', 'Perspectium Replicate');
		dgr.query();
		while (dgr.next()) {
			// for each table we get the # of business rules for this table to send as attribute
			// so we can see if there are many business rules on this table that might cause issues
			var tgr = new GlideRecord("sys_script");
			tgr.addQuery("collection", dgr.collection);
			tgr.query();
			this.addGlideRecordXML(dgr);
			
			// don't need to add business rule count if we already have it for this table
			if(this.configAttributes.indexOf(dgr.collection + "_business_rules_count=") >= 0)
				continue;
			
			this.configAttributes += ", " + dgr.collection + "_business_rules_count=" + tgr.getRowCount();
		}		
	},
	
	postBulkShareConfigs: function(){
		var bgr = new GlideRecord("psp_bulk_share");
		bgr.query();
		while (bgr.next()) {
			this.addGlideRecordXML(bgr);
		}
	},
	
	postQueueDefinitions: function(){
		var qgr = new GlideRecord("u_psp_queues");
		qgr.query();
		while (qgr.next()) {
			this.addGlideRecordXML(qgr);
		}
	},
	
	postProperties: function(){
		var mgr = new GlideRecord('sys_properties_category_m2m');  
		var mgrOR = mgr.addQuery('category.name', 'Perspectium');
		mgrOR.addOrCondition('category.name', 'Perspectium Replicator');  
		mgr.query();  
		while (mgr.next()) {
			var sgr = new GlideRecord('sys_properties');
			sgr.addQuery('name', mgr.property.name);
			sgr.query();
			if(sgr.next()){
				this.addGlideRecordXML(sgr);
			}			  
		}  	
	},
		
	postMessages: function(){
		var outStates = ['ready', 'sent', 'error'];
		for (var o in outStates) {
			this.postMessage("psp_out_message", outStates[o]);	
		}
		
		var inStates = ['ready', 'received', 'skipped', 'error'];
		for (var i in inStates) {
			this.postMessage("psp_in_message", inStates[i]);	
		}
		
		if(this.messageValue != ""){
			// add closing tag for root element before we send out
			this.messageValue += "</message>";
			this.createOutboundMessage("config", "servicenow", "message", this.messageValue, this.messageAttributes);
		}		
	},
	
	postMessage: function(table, state){
		var recordSerializer = (typeof GlideRecordXMLSerializer != 'undefined') ? new GlideRecordXMLSerializer() : new Packages.com.glide.script.GlideRecordXMLSerializer();
		
		var mgr = new GlideRecord(table);
		mgr.addQuery('state', state);
		mgr.addQuery('topic', '!=', 'monitor');
		mgr.addQuery('topic', '!=', 'counter');
		mgr.addQuery('topic', '!=', 'config');
		mgr.addQuery('topic', '!=', 'log');
		mgr.query();
		var msgCount = mgr.getRowCount();
		
		// get first message and send as a sample with the total messages for this state
		if (mgr.next()) {				
			var xmlstr = recordSerializer.serialize(mgr);	
			
			// remove <?xml ?> tag created for each gliderecord xml as we don't need it
			// have to cast to JavaScript string or else it tries using Java replace method			
			xmlstr = String(xmlstr).replace(/<\?xml .*\?>/i, '');
			
			// if we haven't added any records yet we'll create a root element for it
			if(this.messageValue == ""){
				this.messageValue = '<?xml version="1.0" encoding="UTF-8"?><message>';
			}
			
			// add to XML holding all the logs
			xmlstr = xmlstr.replace("<" + mgr.getTableName() + ">", "<" + mgr.getTableName() + " action=\"INSERT_OR_UPDATE\">");
			var gdt = new GlideDateTime();
			this.messageValue += "<unload unload_date=\"" + gdt.toString() + "\">" + xmlstr + "</unload>";			
			this.messageAttributes += table + "_" + state + "_messages_count=" + msgCount;	
		}				
	},
	
	postLogs: function(){
		var states = ['warning', 'error'];
		for (var s in states) {
			this.postLog(states[s]);	
		}
	},
	
	postLog: function(state){
		var logValue = "";
		var logAttributes = "";
		var recordSerializer = (typeof GlideRecordXMLSerializer != 'undefined') ? new GlideRecordXMLSerializer() : new Packages.com.glide.script.GlideRecordXMLSerializer();
		var lgr = new GlideRecord('u_psp_log_message');
		var lgrOR = lgr.addQuery('u_type', state);
		lgr.query();
		
		while (lgr.next()) {				
			var xmlstr = recordSerializer.serialize(lgr);	
			
			// remove <?xml ?> tag created for each gliderecord xml as we don't need it
			// have to cast to JavaScript string or else it tries using Java replace method			
			xmlstr = String(xmlstr).replace(/<\?xml .*\?>/i, '');
			
			// if we haven't added any records yet we'll create a root element for it
			if(logValue == ""){
				logValue = '<?xml version="1.0" encoding="UTF-8"?><log>';
			}
			
			// add to XML holding all the logs
			xmlstr = xmlstr.replace("<" + lgr.getTableName() + ">", "<" + lgr.getTableName() + " action=\"INSERT_OR_UPDATE\">");
			
			// don't add any more records if we're at limit for sending
			var logXML = "<unload unload_date=\"" + gdt.toString() + "\">" + xmlstr + "</unload>";
			if(logValue.length + logXML.length > this.maxBytes){
				this.logger.logWarn("Reached maximum bytes size for log message so not all " + state + "s were sent", "PerspectiumConfig.postLog");
				logAttributes = "max_messsage_length=true";
				break;
			}
			
			var gdt = new GlideDateTime();			
			logValue += logXML;				

		}
		
		if(logValue != ""){
			// add closing tag for root element before we send out
			logValue += "</log>";
			this.createOutboundMessage("log", "servicenow", state, logValue, logAttributes);
		}
	},

    type: 'PerspectiumConfig'
};