var PerspectiumSIAMAudit = Class.create();
PerspectiumSIAMAudit.prototype = {
    initialize: function() {
		this.psp = new Perspectium();
		this.logger = new PerspectiumLogger();
		//this.pspUtil = new PerspectiumUtil();
		this.qpassphrase = this.pspUtil.getPspPropertyValue("com.perspectium.qpassphrase", "");
		
		this.inputServlet = "siamaudit";
    },
	
	getAudits: function() {
		var pgr = new GlideRecord("u_psp_queues");
		pgr.addQuery("u_name", "CONTAINS", "siam");
		pgr.addQuery("u_active", true);
		pgr.addQuery("u_direction", "Share");
		pgr.query();
		while (pgr.next()) {
			this.getAudit(pgr);
		}
	},
		
	getAudit: function(queueConfig) {
		var encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
		var snc_input = queueConfig.u_endpoint_url.toString();
		var quser = queueConfig.u_queue_user;		
		var qpassword = encrypter.decrypt(queueConfig.u_queue_password);
		var input_queue = queueConfig.u_name;		
		var instance = gs.getProperty("instance_name", "unregistered");
		
		if (!this.psp.endsWith(snc_input, "/")) {
			snc_input += "/";
		}

		var connectionUrl = snc_input + this.inputServlet + "?action=update";
		var getMethod = new Packages.org.apache.commons.httpclient.methods.GetMethod(connectionUrl);
		getMethod.setRequestHeader("psp_quser", quser);
        getMethod.setRequestHeader("psp_qpassword", qpassword);
        getMethod.setRequestHeader("passphrase", this.qpassphrase);
        getMethod.setRequestHeader("psp_input_queue", input_queue);
        getMethod.setRequestHeader("psp_instance", instance);
		
		var httpClient = this.psp.getHttpClient();  
		var result = httpClient.executeMethod(getMethod);
		var rstr = getMethod.getResponseBodyAsString();
		getMethod.releaseConnection();
		
		if (result != 200) {
			var provider = input_queue.substring(input_queue.lastIndexOf('.') + 1);
			var errorMsg = "Error updating audits: " + result + " " + rstr + " returned for provider " + provider;
			this.logger.logError(errorMsg, "PerspectiumSIAMAudit.updateAudit");
			gs.addErrorMessage(errorMsg);
			return;
		}
		
		var json = new JSON();
        var obj = json.decode(rstr);
		// no properties returned meaning queue doesn't exist
		if (!obj) {
			var provider = input_queue.substring(input_queue.lastIndexOf('.') + 1);
			var errorMsg = "No audits available for provider " + provider;
			this.logger.logError(errorMsg + ": " + result + " " + rstr, "PerspectiumSIAMAudit.updateAudit");
			gs.addErrorMessage(errorMsg);
			return;
		}
		
		for (i = 0; i < obj.length; i++) {					
			var auditRecord = obj[i]; 
            var agr = new GlideRecord("u_psp_siam_audit");	
						
			// check if we already have this record so we don't need to add again
			if (auditRecord["psp_id"] && auditRecord["psp_id"] != "null" && auditRecord["psp_id"] != "") {
				agr.addQuery("u_psp_id", auditRecord["psp_id"]);
				agr.query();
				if (agr.next()) {
					continue;
				}
			}
			
            agr.initialize();						
			for (var o in auditRecord) {
				if (!agr.isValidField("u_" + o) || auditRecord[o] == "null") {
					continue;
				}
				
				var fieldValue = auditRecord[o];
				if (o == "psp_timestamp") {
					// make as a glidedatetime so it can be inserted properly as GMT
					var gdt = new GlideDateTime(fieldValue);
					fieldValue = gdt.getValue();					
				}
				else if (o == "client_entity_id") {
					// see if we can get the sys_id of the task record to save in this field to reference
					var tgr = new GlideRecord("task");
					if (tgr.get("number", fieldValue)) {
						fieldValue = tgr.sys_id;
					}
				}
				
				agr.setValue("u_" + o, fieldValue);	
			}
			
			agr.insert();
		}
		
 	},

    type: 'PerspectiumSIAMAudit'
};