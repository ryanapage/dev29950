var PSPSchemaProcessor = Class.create();

PSPSchemaProcessor.prototype = {
  initialize : function(tableName) {
	  
	if (tableName && tableName != null && tableName != '') {
		this.output = new Packages.java.io.StringWriter();	
		this.tableName = tableName;
	}
	else {  
		this.glideOutputWriter = GlideOutputWriterFactory.get(g_request, g_response);
		g_response.setContentType("text/xml");
		this.output = new Packages.java.io.PrintWriter(this.glideOutputWriter);	  
	}
	  
	this.pspUtil = new PerspectiumUtil();
	this.addDisplayValues = this.pspUtil.getPspPropertyValue("com.perspectium.replicator.add_display_values", "false");
	this.currentFieldsOnly = this.pspUtil.getPspPropertyValue("com.perspectium.replicator.share_current_fields", "false");   
	var cgr = new GlideRecord('sys_choice');
	var cge = cgr.getElement("label");
	var ced = cge.getED();
	// for display value on choice list use the length of the label field as field's length can be shorter than label
	this.choiceLabelLength = ced.getLength();    
  },
	
  getTableList : function(){
    this.output.write('<tables>');
    this.writeTablesFromSysDictionary();
    this.writeTablesFromDBView();
    this.output.write('</tables>');
  },

  writeTablesFromSysDictionary: function() {
    var gr = new GlideRecord('sys_dictionary');
    var encQ = 'elementISEMPTY';
    gr.addEncodedQuery(encQ);
    this.writeTables(gr);
  },

  writeTablesFromDBView: function() {
    var gr = new GlideRecord('sys_db_view');
    if (!gr.isValid())
      return;

    this.writeTables(gr);
  },
 
  isRotationExtension: function(tableName) {
    var td = new GlideTableDescriptor.get(tableName);
    return td.isRotationExtension();
  },

  isTextIndexTable: function(tableName) {
    return tableName.startsWith('ts_index_');
  },

  writeTables: function(gr) {
    gr.query();
    while (gr.next()){
      var tableName = gr.getValue('name');
      if ( JSUtil.nil(tableName))
          continue;

      var grtable = new GlideRecord(tableName);
      if (!grtable.isValid())
        continue;

      //if (!this.isTextIndexTable(tableName) && !this.isRotationExtension(tableName) && grtable.canRead()){
	  // can read data != can read schema
	  if (!this.isTextIndexTable(tableName) && !this.isRotationExtension(tableName)){
        this.output.write('<element name="');
        this.output.write(String(tableName));
        this.output.write('" />');
      }
    }     
  },

  finish : function() {
	// don't need to flush or close for StringWriter  
	if (this.tableName && this.tableName != null && this.tableName != '') {
		return;
	}  
	  
    this.output.flush();
    this.output.close();

    if (this.glideOutputWriterHasWriteToMethod())
      this.glideOutputWriter.writeTo(g_response.getOutputStream());
  },
	
  isTableMap: function(name) {
	 var tableMapGR = new GlideRecord("u_psp_table_map");
	 tableMapGR.addQuery("u_type", name);
	 tableMapGR.addQuery("u_generate_schema", "true");
	 tableMapGR.addQuery("u_direction", "outbound");
	 tableMapGR.query();
	 return tableMapGR.hasNext();
  },
	
  process : function(){
	 // see if we were initialized with a table name for case where this isn't being called from a processor
	 if (this.tableName && this.tableName != null && this.tableName != '') {
		 g_target = this.tableName;
	 } 
	  
     this.output.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
     // first , get all of the tables names
     //check to see if the list of tables is being requested, the g_target will be navpage
     if ( g_target == 'navpage'){
        this.getTableList();
        this.finish();
        return this.output.toString();
     }

     var gr = new GlideRecord(g_target);     
     if (!gr.isValid()){
		// check to see if its a Table Map
		if (this.isTableMap(g_target)) {
			this.processTableMap();
		} else {
			this.output.write('<error>Invalid table: ' + g_target + '</error>');
			this.finish();
			//return this.output.toString();
		}
		 
        return this.output.toString();		 
     }
	  	  
     this.output.write("<" + g_target + ">");
     var schemaHM = new GlideTableDescriptor.get(g_target);
     var target = new GlideRecord(g_target);
     var map = schemaHM.getSchema();
     var keys = map.keySet();
     
     var iter = keys.iterator();
     while( iter.hasNext()){
       var o = iter.next();
       var ge = target.getElement(o);
		 
	   // can read data != can read schema
       //if (!ge.canRead())
       //  continue;

       var ed = map.get(o);
	   var refDF = this.getReferenceDisplayField(ed);
	   var refAI = this.getReferenceAdditionalInfo(ed);
		 
	   if (this.currentFieldsOnly == "true" && ed.getTableName() != g_target && o.toString() != "sys_id") {
	   	continue;
	   }
		 
       this.output.write('<element name="'  + o.toString()
                         + '" internal_type="' + ed.getInternalType() 
                         + '" max_length="'    + ed.getLength() 
                         + '" choice_list="' + this.isChoiceList(ed) + '"'
                         + refDF
                         + refAI 
                         +' />');
		 
		 // write display value field
		 if (this.addDisplayValues == "true" && (this.isChoiceList(ed) == "true" || ed.getInternalType() == "glide_duration")) {
       	 this.output.write('<element name="dv_'  + o.toString()
                         + '" internal_type="string'
                         + '" max_length="'    + this.choiceLabelLength
                         + '" choice_list="' + this.isChoiceList(ed) + '"'
                         +' />');		 
		 } else if(this.addDisplayValues == "true" && refDF != "") {
			var referenceTable = ed.getReference();
			var ref = new GlideTableDescriptor.get(referenceTable);

			var refFieldName = ref.getDisplayName();
			var refTableSchemaMap = ref.getSchema();
			var refFieldED = refTableSchemaMap.get(refFieldName);
			var refInternalType = "string";
			var refLength = "100";
			var refIsChoiceList = "false";
			if (refFieldED || refFieldED != null) {
				refInternalType = refFieldED.getInternalType();
				refLength = refFieldED.getLength();
				refIsChoiceList = this.isChoiceList(refFieldED);
			}

       	 this.output.write('<element name="dv_'  + o.toString()
                         + '" internal_type="' + refInternalType 
                         + '" max_length="'    + refLength
                         + '" choice_list="' + refIsChoiceList + '"'
                         +' />');			
		}
     }

     this.output.write('</' + g_target + '>');
     this.finish();
	  
	 // only useful for returning when initialized with outputString 
	 return this.output.toString(); 
  },

  processTableMap: function() {
     this.output.write("<" + g_target + ">");
     var tableMapGR = new GlideRecord("u_psp_table_map");
	 tableMapGR.addQuery("u_type", g_target);
	 tableMapGR.addQuery("u_generate_schema", "true");
	 tableMapGR.addQuery("u_direction", "outbound");
	 tableMapGR.query();
	 tableMapGR.next(); // must work because we call isTableMap before
	  
	 var target = new GlideRecord(tableMapGR.u_source_table);
     var schemaHM = new GlideTableDescriptor.get(tableMapGR.u_source_table);
     var map = schemaHM.getSchema();
	  
	 var tableFieldMapGR = new GlideRecord("u_psp_table_field_map");
	 tableFieldMapGR.addQuery("u_parentid", tableMapGR.sys_id);
	 tableFieldMapGR.query();
	 
     while(tableFieldMapGR.next()) {		 
	   var targetFieldName = tableFieldMapGR.u_target_field;
	   var targetFieldType = "string"; // default to string
	   var sourceFieldName = tableFieldMapGR.u_source_field;
	   var targetFieldLength = "1000";
		 
	   // skip attributes
	   if (targetFieldName.startsWith("@"))
		   continue;
		 
	   var refDF = null;
	   var refAI = null;
		 
	   if ((tableFieldMapGR.u_use_script + "") != "true" && !sourceFieldName.startsWith("$")) {
		   var ge = target.getElement(sourceFieldName);
		   var ed = map.get(sourceFieldName);
		   if (ed == null) { // could be a reference field
			   var f = target.getElement(sourceFieldName);
			   if (f != null)
				   ed = f.getED();
		   }
		   
		   targetFieldType = ed.getInternalType();
		   targetFieldLength = ed.getLength();
	   }
		 
	   if (!tableFieldMapGR.u_field_type.isNil()) { // custom override
		   targetFieldType = tableFieldMapGR.u_field_type.name;
	   }
		 
	   if (!tableFieldMapGR.u_field_length.isNil()) { // custom override
		   targetFieldLength = tableFieldMapGR.u_field_length;
	   }
		 
       this.output.write('<element name="'  + targetFieldName
                         + '" internal_type="' + targetFieldType 
                         + '" max_length="'    + targetFieldLength 
                         + '" choice_list="false"');

	   this.output.write(' />');
     }

     this.output.write('</' + g_target + '>');
     this.finish();
  },
	
  getReferenceDisplayField: function(ed) {
      var referenceTable = ed.getReference();
      if (JSUtil.nil(referenceTable)) 
          return "";

      var ref = new GlideTableDescriptor.get(referenceTable);
      return ' display_field="'  +ref.getDisplayName() + '"';
  },

  getReferenceAdditionalInfo: function(ed) {
      var referenceTable = ed.getReference();
      if (JSUtil.nil(referenceTable)) 
          return "";

      var refInfo = ' reference_table="' + referenceTable + '" ';
      var ref = new GlideTableDescriptor.get(referenceTable);

      if (!JSUtil.nil(ref)) {
          var refFieldName = ref.getDisplayName();
          var refTableSchemaMap = ref.getSchema();
          var refFieldED = refTableSchemaMap.get(refFieldName);

          if (!JSUtil.nil(refFieldED))
             refInfo += ' reference_field_max_length="' + refFieldED.getLength() + '" ';
      }
      return refInfo;
  },

  isChoiceList: function(ed) { 
     if (ed.isChoiceTable())
        return "true";

     return "false";
  },

  glideOutputWriterHasWriteToMethod: function() {
    return GlideJSUtil.javaObjectHasMethod(this.glideOutputWriter, "writeTo");
  }
}