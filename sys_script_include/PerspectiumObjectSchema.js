var PerspectiumObjectSchema = Class.create();
PerspectiumObjectSchema.prototype = {
    initialize: function() {
    },
		
	createTable: function(cgr) {
		
		// get import set table so we can make it as the parent table that this new table extends
		var igr = new GlideRecord('sys_db_object');
		igr.addQuery('name', 'sys_import_set_row');
		igr.query();
		
		if (!igr.next()) {
			// can't find import set table
			return false;
		}
		
		var schemaForTable = JSON.parse(GlideStringUtil.base64Decode(cgr.value.toString()));
		
		var tableName = "apttus_"+schemaForTable.Name.toString();
				
		// create table
		var table = new GlideRecord('sys_db_object');
		table.initialize();
		table.name = tableName;
		table.super_class = igr.sys_id;
		tableSysId = table.insert();
		
		var fieldList = schemaForTable.Fields;
		
		for (var fieldKey in fieldList) {
			var field = new GlideRecord('sys_dictionary');
			field.initialize();
			field.name = tableName;
			field.column_label = fieldList[fieldKey].Name;
			
			if(fieldList[fieldKey].Length) {
				field.max_length = fieldList[fieldKey].Length;
			} else {
				field.max_length = 255;
			}
			
			field.internal_type = 'string';
			field.insert();
		}
		
		gs.log("Created table: "+tableName);
		
		var uigr = new GlideRecord("sys_ui_action");
		uigr.active = true;
		uigr.name = "Import Data"
		uigr.list_banner_button = true;
		uigr.table = tableName;
		uigr.action_name = "sysverb_import_"+String(tableName).toLowerCase();
		
		uigr.script = "var pspA = new PerspectiumApttus(); pspA.getObjectData('u_"+String(schemaForTable.Name.toString()).toLowerCase()+"');";
		uigr.insert();
		
		return true;
	},

    type: 'PerspectiumObjectSchema'
};