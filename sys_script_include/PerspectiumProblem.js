var PerspectiumProblem = Class.create();
PerspectiumProblem.prototype = {
 	initialize: function() {
        this.logger = new PerspectiumLogger();
    },

    addProblems : function(source, target, pspTag /* optional */) {
        var targetId = target.sys_id;
		var targetTable = target.getTableName();
        this.logger.logDebug('Adding impacted services for: ' + targetTable + "," + targetId, "PerspectiumProblem.addProblems");

        if (source == null || source.u_problems.isNil()) {
            this.logger.logDebug('No impacted service found', "PerspectiumProblem.addProblems");
            return;
        }

        var xmldoc = null;
        try {
             xmldoc = new XMLDocument(source.u_problems);
        } catch(err){
            this.logger.logDebug('Error creating XMLDocument for impacted services: ' + err, "PerspectiumProblem.addProblems");
            return;
        }

        if (xmldoc == null) {
           this.logger.logDebug('XMLDocument is null', "PerspectiumProblem.addProblems");
           return;
        }

        var nodelist = xmldoc.getNodes("//problem");
        if (nodelist == null) {
            this.logger.logDebug('No impacted services nodes found', "PerspectiumProblem.addProblems");
            return;
        }

        this.logger.logDebug('Found ' + nodelist.getLength() + ' impacted services', "PerspectiumProblem.addProblems");
        for (var i=0; i < nodelist.getLength(); i++) {
            var nodeItem = nodelist.item(i);
            this.addProblem(nodeItem, targetTable, targetId, pspTag);
        }
    },
	
	_getValue : function(nodeItem, nodeName) {        
		var xml_util = (typeof GlideXMLUtil != 'undefined') ? new GlideXMLUtil() : new Packages.com.glide.util.XMLUtil();
		var it = xml_util.childElementIterator(nodeItem);
        while (it.hasNext()) {
            var el = it.next();
            var nn = el.getNodeName();
            var nodeValue = xml_util.getText(el);
			
			if( nodeValue == null)
				nodeValue = "";
			
			if(nn == nodeName) {
				return nodeValue;
			}
		}
											  
		return "";
	},
	
//       <problem>
//          <short_description>Windows XP SP2 causing errors in Enterprise</short_description>
//          <assignment_group_name />
//          <id>46eaa7c9a9fe198100bbe282da0d4b7d</id>
//          <assigned_to_name>ITIL User</assigned_to_name>
//          <state>3</state>
//          <number>PRB0000001</number>
//       </problem>
	
    addProblem : function(nodeItem, targetTable, targetId, pspTag /* optional */) {
        var StringUtil = (typeof GlideStringUtil != 'undefined') ? new GlideStringUtil() : new Packages.com.glide.util.StringUtil();
		
		var pID = this._getValue(nodeItem, "id");
		var number = this._getValue(nodeItem, "number");
		
		if (pID == "")
			return; // no sys_id, cannot search
		
		// first see if we can find CI by sys_id
		var p = new GlideRecord("problem");
		p.addQuery("sys_id", pID);
		p.query();
		if (!p.next()) {
			
			// try to locate by correlation_id
			p.initialize();
			p.addQuery("correlation_id", number);
			p.query();
			if (!p.next()) {
				// create the problem
				p.initialize();
				p.correlation_id = number;
				p.short_description = this._getValue(nodeItem, "short_description");
				p.state = this._getValue(nodeItem, "state");
				p.insert();
			}
		}
		
		p.rfc = targetId;
		p.update();
		
		// if pspTag exists, go and tag it
		if (pspTag) {
			var pspUtil = new PerspectiumUtil();
			pspUtil.addTag(taskCI, pspTag);
		}
    },

    type: 'PerspectiumProblem'
};