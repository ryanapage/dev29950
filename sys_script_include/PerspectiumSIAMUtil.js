var PerspectiumSIAMUtil = Class.create();
PerspectiumSIAMUtil.prototype = {	
    initialize: function() {
		this.logger = new PerspectiumLogger();
		this.psp = new Perspectium();
		this.pspR = new PerspectiumReplicator();
		this.encryption = new PerspectiumEncryption();
    },

	shareJournalOnlyDelayed: function(sys_id, sys_updated_on, grShareConfig, operator) {
		this.logger.logDebug("sharing sys_journal_field delayed for " + sys_id + "  " + operator + " " + sys_updated_on, "PerspectiumSIAMUtil.shareJournalOnlyDelayed", grShareConfig);
		var pspSO = new ScheduleOnce();		
		var gdt = new GlideDateTime();
		gdt.addSeconds(30); // schedule in 30s so correlation_id is put in
		
		pspSO.setTime(gdt.getValue()); 
		
		pspSO.script += "var pspSU = new PerspectiumSIAMUtil();";	
		pspSO.script += "pspSU.shareJournalOnly('" + sys_id + "','" + sys_updated_on + "','" + grShareConfig.sys_id + "', '" + operator + "');";				
		pspSO.schedule();
	},

	shareJournalOnly: function(sys_id, sys_updated_on, grShareConfigSysId, operator) {
		var grShareConfig = new GlideRecord('psp_replicate_conf');		
		grShareConfig.addQuery('sys_id', grShareConfigSysId);
		grShareConfig.queryNoDomain();
		if (!grShareConfig.next()) {
			this.logger.logDebug("no share configuration found for " + grShareConfigSysId, "PerspectiumSIAMUtil.shareJournalOnly");
			return; // nothing can be done
		}
		
		if (!grShareConfig.isValidField("u_table_map") || grShareConfig.u_table_map.nil()){
			this.logger.logDebug(grShareConfigSysId + " share configuration does not have table map setup, not sharing journal", "PerspectiumSIAMUtil.shareJournalOnly");
			return;
		}
				
		var grTableMap = this.encryption.getOutboundTableMap(grShareConfig.u_table_map);	
		if(grTableMap.u_topic != "siam"){
			this.logger.logDebug(grShareConfigSysId + " share configuration does not have a siam topic, not sharing journal", "PerspectiumSIAMUtil.shareJournalOnly");
			return;			
		}
		
		var cgr = new GlideRecord(grShareConfig.table_name);
		cgr.addQuery('sys_id', sys_id);
		cgr.query();
		if (!cgr.next()) {
			this.logger.logDebug(sys_id + " record not found in " + grShareConfig.table_name + ", not sharing journal", "PerspectiumSIAMUtil.shareJournalOnly");
			return;
		}
		
		var name = grShareConfig.table_name;
		var type = "servicenow";
		var target_queue = grShareConfig.u_target_queue;		
		var recCount = 0;
		var byteCount = 0;		
		var topic = grTableMap.u_topic;
		if (!grTableMap.u_type.nil()) {
			type = grTableMap.u_type;
		}
		
		if (!grTableMap.u_target_tablename.nil()) {
			name = grTableMap.u_target_tablename;
		}
		
		name = name + ".update";
		var attributes = "";		
		attributes = this.pspR.appendTableMapAttributes(cgr, grTableMap, attributes);
		// add attributes with share conf id to use for message set id
		attributes = this.pspR.appendAttribute("set_id", grShareConfig.sys_id, attributes);
		attributes = this.pspR.appendAttribute("cipher", grShareConfig.u_cipher, attributes);
		attributes = this.pspR.appendAttribute("psp_flag", "last", attributes); // don't need to set psp_first as it'll be set since first message sent of group

		// create the common_incident record that we'll add the journal fields to
		var enc = this.encryption.encryptTableMap(cgr, grTableMap, "3");
		var xmlData = this.encryption.decryptString(enc);
		var xmlDoc = new XMLDocument(xmlData);		
		var xml_util = (typeof GlideXMLUtil != 'undefined') ? new GlideXMLUtil() : new Packages.com.glide.util.XMLUtil();
		var root = xmlDoc.getDocumentElement();
        
        // clear all fields out
		var it = xml_util.childElementIterator(root);
        while (it.hasNext()) {
			var el = it.next();
			var n = el.getNodeName();			
			var v = xml_util.getText(el);
			
			// save correlation_id as we'll need it to update the provider with journal
			if (v == null || v == "" || n.indexOf('correlation_id') == 0) {
				continue;
			}
			
			try {
				el.setTextContent("");
			}
			catch (e) {
				this.logger.logWarn("Error updating XML element " + n + ": " + e, "PerspectiumSIAMUtil.shareJournalOnly");
			}			
		}
		
		// only send journal entries for this session, not all of them
		var jgr = new GlideRecord("sys_journal_field");
		jgr.addQuery("element_id", sys_id);
		jgr.orderBy('sys_created_on');
		
		if (sys_updated_on != null && sys_updated_on != "" && !sys_updated_on.isNil()) {
			this.logger.logDebug("sharing journal for " + sys_id + " from " + sys_updated_on, "PerspectiumSIAMUtil.shareJournalOnly", grShareConfig);
			
			var queryOperator = ">=";
			if (operator && operator != null && operator != "") {
				queryOperator = operator;
			}
			
			jgr.addQuery("sys_created_on", queryOperator, sys_updated_on);
		}
		
		jgr.query();
		while (jgr.next()) {
			var jel = xmlDoc.getElementByTagName(jgr.element);
			if (jel == null) {
				continue;
			}
			
			jel.setTextContent(jgr.value);			
			var StringUtil = (typeof GlideStringUtil != 'undefined') ? new GlideStringUtil() : new Packages.com.glide.util.StringUtil();
			var jEnc = StringUtil.base64Encode(xmlDoc.toString());			
			this.pspR.createPSPOut(topic, type, this.pspR.getKey(grShareConfig), name, jEnc, target_queue, "", attributes, grShareConfig);
			
			// increment counter for each message sent
			this.pspR.incrementMessageSetCounter(name);
			
			// increment counters
			recCount++;
			byteCount += enc.length();	
			
			// sleep so comments are sent out slowly to keep in order
			gs.sleep(30000);
		}
		
		// send share counter data
		if(recCount > 0){
			this.psp.sendCounter(name, recCount, "counter");
			this.psp.sendCounter(name + ".bytes", byteCount, "counter");
		}
	},
	
    type: 'PerspectiumSIAMUtil'
};