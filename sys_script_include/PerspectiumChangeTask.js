var PerspectiumChangeTask = Class.create();
PerspectiumChangeTask.prototype = {
 	initialize: function() {
        this.logger = new PerspectiumLogger();
    },

    addChangeTasks : function(source, target, pspTag /* optional */) {
        var targetId = target.sys_id;
		var targetTable = target.getTableName();
        this.logger.logDebug('Adding change task for: ' + targetTable + "," + targetId, "PerspectiumChangeTask.addChangeTasks");

        if (source == null || source.u_change_tasks.isNil()) {
            this.logger.logDebug('No change tasks found', "PerspectiumChangeTask.addChangeTasks");
            return;
        }

        var xmldoc = null;
        try {
             xmldoc = new XMLDocument(source.u_change_tasks);
        } catch(err){
            this.logger.logDebug('Error creating XMLDocument for change tasks: ' + err, "PerspectiumChangeTask.addChangeTasks");
            return;
        }

        if (xmldoc == null) {
           this.logger.logDebug('XMLDocument is null', "PerspectiumChangeTask.addChangeTasks");
           return;
        }

        var nodelist = xmldoc.getNodes("//change_task");
        if (nodelist == null) {
            this.logger.logDebug('No change task nodes found', "PerspectiumChangeTask.addChangeTasks");
            return;
        }

        this.logger.logDebug('Found ' + nodelist.getLength() + ' chnage tasks', "PerspectiumChangeTask.addChangeTasks");
        for (var i=0; i < nodelist.getLength(); i++) {
            var nodeItem = nodelist.item(i);
            this.addChangeTask(nodeItem, targetTable, targetId, pspTag);
        }
    },
	
	_getValue : function(nodeItem, nodeName) {        
		var xml_util = (typeof GlideXMLUtil != 'undefined') ? new GlideXMLUtil() : new Packages.com.glide.util.XMLUtil();
		var it = xml_util.childElementIterator(nodeItem);
        while (it.hasNext()) {
            var el = it.next();
            var nn = el.getNodeName();
            var nodeValue = xml_util.getText(el);
			
			if( nodeValue == null)
				nodeValue = "";
			
			if(nn == nodeName) {
				return nodeValue;
			}
		}
											  
		return "";
	},
	
	_setValues : function(ct, ctNumber, targetId, nodeItem) {
		ct.correlation_id = ctNumber;
		ct.change_request = targetId;
		ct.short_description = this._getValue(nodeItem, "short_description");
		ct.assignment_group = this._getValue(nodeItem, "assignment_group");
		ct.work_notes = this._getValue(nodeItem, "work_notes");
		ct.comments = this._getValue(nodeItem, "comments");
		ct.change_task_type = this._getValue(nodeItem, "change_task_type");
		ct.assigned_to = this._getValue(nodeItem, "assigned_to");
		ct.state = this._getValue(nodeItem, "state");
		ct.description = this._getValue(nodeItem, "description");
		ct.cmdb_ci = this._getValue(nodeItem, "cmdb_ci");		
	},

    addChangeTask : function(nodeItem, targetTable, targetId, pspTag /* optional */) {
        var StringUtil = (typeof GlideStringUtil != 'undefined') ? new GlideStringUtil() : new Packages.com.glide.util.StringUtil();
		
		var ctNumber = this._getValue(nodeItem, "number");
		
		if (ctNumber == null)
			return; // no sys_id, cannot search
		
		// first see if we can find change task by sys_id
		var ct = new GlideRecord("change_task");
		ct.addQuery("correlation_id", ctNumber);
		ct.query();
		if (!ct.next()) {
			ct.initialize();
			this._setValues(ct, ctNumber, targetId, nodeItem);
			ct.insert();
		} else {
			this._setValues(ct, ctNumber, targetId, nodeItem);
			ct.update();
		}
		
		// if pspTag exists, go and tag it
		if (pspTag) {
			var pspUtil = new PerspectiumUtil();
			pspUtil.addTag(taskCI, pspTag);
		}
    },

    type: 'PerspectiumChangeTask'
};