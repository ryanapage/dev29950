var PerspectiumMessageSetMessage = Class.create();
PerspectiumMessageSetMessage.prototype = {
    initialize: function(set_id) {		
		this.set_id = set_id;	
		
		this.successes = 0;
		this.failures = 0;
		this.skipped = 0;
		this.first = false;
		this.last = false;
		this.started = "";
		this.finished = "";
		
		this.logger = new PerspectiumLogger();				
    },
		
	incSuccess: function(){
		this.successes++;
	},
	
	incFailure: function(){
		this.failures++;
	},
	
	incSkipped: function(){
		this.skipped++;
	},
	
	setFirst: function(first){
		this.first = first;	
	},
	
	setLast: function(last){
		this.last = last;
	},
	
	setStarted: function(started){
		this.started = started;
	},
	
	setFinished: function(finished){
		this.finished = finished;
	},
	
	encodeMessageJSON: function (instanceKey, componentType) {
		// create JSON string that contains all message set info
		var str = new Packages.java.lang.StringBuffer();
		
		str.append('{"component_name":"' + instanceKey + '",');
		str.append('"component_type":"' + componentType + '",');
						
		if(this.started && this.started != null && this.started != ""){
			var sgdt = new GlideDateTime(this.started);
			str.append('"started":"' + sgdt.getNumericValue() + '",');			
		}
				
		if(this.finished && this.finished != null && this.finished != ""){
			var cgdt = new GlideDateTime(this.finished);		
			str.append('"finished":"' + cgdt.getNumericValue() + '",');		
		}
				
		str.append('"failures":"' + this.failures + '",');		
		str.append('"skipped":"' + this.skipped + '",');		
		str.append('"successes":"' + this.successes + '"}');    						
	
		return str.toString();
	},
			
    type: 'PerspectiumMessageSetMessage'
};