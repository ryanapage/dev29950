var PerspectiumShareMessage = Class.create();
PerspectiumShareMessage.prototype = {
    initialize: function(bgr, action) {
		this.pspUtil = new PerspectiumUtil();
		
		this.version = this.pspUtil.getPspPropertyValue("com.perspectium.share_message.version", "1.0");	// version of message we're sharing to determine how to handle schema
		this.inputServlet = "bulkin";

		this.tableName = bgr.table_name;
		this.tableSchema = "";
		this.recordsStr = "";
		this.bgr = bgr;
		this.action = action;
		this.recordCount = 0;
		this.queueFull = false;
		
		this.setID = bgr.u_set_id;
		
		this.psp = new Perspectium();		
		this.pspR = new PerspectiumReplicator();
		this.logger = new PerspectiumLogger();
		this.encryption = new PerspectiumEncryption();
		this.maxBytes = parseInt(this.pspUtil.getPspPropertyValue("com.perspectium.output_bytes_limit", "5000000")); 
		// max bytes to cap the send string limit of 16MB for Java String but we'll cap at 15MB for storing in value field
		if (this.maxBytes > 15000000) {
			this.maxBytes = 15000000;
		}
		
		this.encryptionMode = this.pspUtil.getPspPropertyValue("com.perspectium.replicator.encryption_mode", "encrypted");
		//this.pspS = new PerspectiumSchema();
		this.pspS = new PSPSchemaProcessor(this.tableName);
		
		this.snc_input = this.psp.getQInputURL();
        this.quser = this.psp.getQUser();
        this.qpassword = this.psp.getQPassword();
		this.qpassphrase = this.psp.qPassphrase;
        //this.input_queue = this.psp.defaultInputQueue;
        this.input_queue = "ets";
        this.instance = gs.getProperty("instance_name", "unregistered");

        if (this.bgr.u_target_queue && this.bgr.u_target_queue != "default" && this.bgr.u_target_queue != "") {
			var qgr = new GlideRecord("u_psp_queues");
        	if (qgr.get(this.bgr.u_target_queue)) {
               var encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
               this.snc_input = qgr.u_endpoint_url.toString();
               this.quser = qgr.u_queue_user;
               this.qpassword = encrypter.decrypt(qgr.u_queue_password);
               this.input_queue = qgr.u_name;
               // this.instance = qgr.u_instance;
           }
        }
		
		// for input queue we'll use instance name at front
		this.input_queue = this.instance + "." + this.input_queue;
		
		if (!this.psp.endsWith(this.snc_input, "/")) {
			this.snc_input += "/";
		}
    },
	
	reset: function() {		
		// reset and start over with schema first
		this.recordsStr = "";
		this.createSchema();
		//this.recordsStr = this.tableSchema;
		this.recordCount = 0;
	},
	
	createSchema: function() {
		//var tableSchema = this.pspS.process(this.tableName);
		var tableSchema = this.pspS.process();
		if (tableSchema && tableSchema != null && tableSchema != "") {
			// remove <xml> tag as we'll have one at the top to make entire string xml well formed
			tableSchema = String(tableSchema).replace(/\<\?xml.+\?\>|\<\!DOCTYPE.+]\>/g, '');
			this.recordsStr += tableSchema;
			this.tableSchema = tableSchema;
		}		
	},
	
	addRecordStr: function(recordStr) {	
		// remove any leading <xml> or <doctype> so we can make entire xml well formed
		this.recordsStr += String(recordStr).replace(/\<\?xml.+\?\>|\<\!DOCTYPE.+]\>/g, '');
		this.recordCount++;
		if (String(this.recordsStr.length) > this.maxBytes) {
			this.logger.logDebug("maximum " + this.maxBytes + " bytes reached: " + String(this.recordsStr.length) + ", posting message", "PerspectiumShareMessage.addRecordStr");
			
			// post records
			var result = this.postRecords();
						
			// reset and start over with schema first
			/*this.recordsStr = this.tableSchema;
			this.recordCount = 0;*/
			this.reset();
		}
	},
	
    postRecords: function() {
		if (this.psp.getQUser() == "" || this.psp.getQPassword() == "" || this.psp.getQInputURL() == "") {
			this.logger.logError("Unable to run as the target server or credentials are not configured properly. Please check your Perspectium properties", "PerspectiumShareMessage.postMessage");
			return;	
		}
		
		if(this.bgr.status == "Cancelling") {
			this.pspR.cancelledBulkShare(this.bgr);
		}
		
		// add starting and closing to make well-formed XML		
		var recordsMessage = '<?xml version="1.0" encoding="UTF-8" ?><message>' + this.recordsStr + '</message>';		
		var cipher = this.bgr.u_cipher;		
		var postData;
		if (this.encryptionMode == "unencrypted") {
			postData = recordsMessage;
		}
		else {
			postData = this.encryption.encryptString(recordsMessage);
		}
				
		var topic = "replicator";
		var type = "servicenow";	
		var key = this.pspR.getKey(this.bgr);
		var name = this.bgr.table_name + "." + this.action;
		
		// store records and bytes into extra for customer to view in outbound after post
		var byteCount = String(postData).length;
		var extra = "records=" + this.recordCount + ",bytes=" + byteCount;
		var bAttributes = "";
		bAttributes = this.pspR.appendAttribute("cipher", cipher, bAttributes);
		//bAttributes = this.pspR.appendAttribute("set_id", this.setID, bAttributes);
	
		var pspM = new PerspectiumMessage(topic, type, key, name, postData, this.bgr.u_target_queue, extra, bAttributes, this.bgr, this.setID, "", false, "u_psp_ets_out_message");		
		var messageState = "";
		
		// previously received a 600 from mbs so we'll just put into outbound from now on
		if (this.queueFull) {
			messageState = "ready";
			pspM.setValue(postData);
			pspM.setState(messageState);
			pspM.setExtra(extra += ",response=600");
			pspM.enqueue();
			return;	
		}
		
        this.logger.logDebug("posting to: " + this.snc_input + this.inputServlet + ", " + this.input_queue + " (" + byteCount + " bytes)", "PerspectiumShareMessage.postMessage");
        
        /*var postMethod = new Packages.org.apache.commons.httpclient.methods.PostMethod(this.snc_input + this.inputServlet);		
						
        postMethod.setRequestHeader("psp_quser", this.quser);
        postMethod.setRequestHeader("psp_qpassword", this.qpassword);
        postMethod.setRequestHeader("passphrase", this.qpassphrase);
        postMethod.setRequestHeader("psp_input_queue", this.input_queue);
        postMethod.setRequestHeader("psp_instance", this.instance);
	
		postMethod.setRequestHeader("turbo", "true");
	
		// set headers for message properties
		postMethod.setRequestHeader("topic", topic);
		postMethod.setRequestHeader("type", type);
		postMethod.setRequestHeader("key", key);
		postMethod.setRequestHeader("name", name);
		postMethod.setRequestHeader("attributes", bAttributes);
		postMethod.setRequestHeader("extra", extra);
		postMethod.setRequestHeader("version", this.version);
		
		var httpClient = this.psp.getHttpClient();				
        //postMethod.addRequestHeader("Content-Type", "text/json");
        postMethod.setRequestEntity(new Packages.org.apache.commons.httpclient.methods.StringRequestEntity(postData));
        var result = httpClient.executeMethod(postMethod);
		postMethod.releaseConnection();
		*/
		var result = this.postMessage(pspM);
		
		pspM.setExtra(extra += ",response=" + result);
		// success write to outbound
		if (result == 200) {			
			messageState = "sent";
			// clear out value since we succeeded and don't need it in outbound table to save space
			pspM.setValue("");
		}
		// queue full so we'll post to outbound queue to post later by scheduled job
		else if (result == 600) {
			this.queueFull = true;
			messageState = "ready";
			pspM.setValue(postData);
		}
		else {
			messageState = "ready";
			pspM.setValue(postData);
		}

		pspM.setState(messageState);
		pspM.enqueue();
		
		// send share counters regardless since bulk share completed
	    this.psp.sendCounter(name, this.recordCount, "counter");
	    this.psp.sendCounter(name + ".bytes", byteCount, "counter");

        return result;
    },	
	
	postMessage: function(pspMessage) {
        var postMethod = new Packages.org.apache.commons.httpclient.methods.PostMethod(this.snc_input + this.inputServlet);		
						
        postMethod.setRequestHeader("psp_quser", this.quser);
        postMethod.setRequestHeader("psp_qpassword", this.qpassword);
        postMethod.setRequestHeader("passphrase", this.qpassphrase);
        postMethod.setRequestHeader("psp_input_queue", this.input_queue);
        postMethod.setRequestHeader("psp_instance", this.instance);
	
		postMethod.setRequestHeader("turbo", "true");
	
		// set headers for message properties
		postMethod.setRequestHeader("topic", pspMessage.topic);
		postMethod.setRequestHeader("type", pspMessage.type);
		postMethod.setRequestHeader("key", pspMessage.key);
		postMethod.setRequestHeader("name", pspMessage.name);
		postMethod.setRequestHeader("attributes", pspMessage.attributes);
		postMethod.setRequestHeader("extra", pspMessage.extra);
		postMethod.setRequestHeader("version", this.version);
		
		var now = new GlideDateTime().getNumericValue();        
		postMethod.setRequestHeader("dt", now);
		
		var httpClient = this.psp.getHttpClient();				
        //postMethod.addRequestHeader("Content-Type", "text/json");
        postMethod.setRequestEntity(new Packages.org.apache.commons.httpclient.methods.StringRequestEntity(pspMessage.value));
        var result = httpClient.executeMethod(postMethod);
		postMethod.releaseConnection();

		return result;
	},
		
    type: 'PerspectiumShareMessage'
};