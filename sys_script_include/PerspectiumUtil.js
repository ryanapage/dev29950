var PerspectiumUtil = Class.create();
PerspectiumUtil.prototype = {
	// PerspectiumLogger uses PerspectiumUtil so don't use here or else cyclical reference created
    initialize: function() {
    },
	
	getSysPropertyValue: function(propertyName) {
		var propGr = new GlideRecord("sys_properties");
		propGr.addQuery("name", propertyName);
		propGr.query();
		if(propGr.next()) {		
			return propGr.value;
		}
		
		// not found return null
		return null;
	},
	
	setSysPropertyValue: function(propertyName, value) {		
		var propGr = new GlideRecord("sys_properties");
		propGr.addQuery("name", propertyName);
		propGr.query();
		if(propGr.next()) {		
			propGr.value = value;
			propGr.update();
			return;
		}
		
		// not found, create property
		this.createSysProperty(propertyName, value);		
	},
	
	createSysProperty: function(propertyName, value){
		var propGr = new GlideRecord("sys_properties");
		propGr.initialize();
		propGr.name = propertyName;
		propGr.ignore_cache = true;
		propGr.is_private = true;
		propGr.value = value;
		propGr.insert();
		
		return propGr;
	},

	getPspPropertyValue: function(propertyName, defaultValue) {		
		var propGr = new GlideRecord("u_psp_properties");
		propGr.addQuery("u_name", propertyName);
		propGr.query();
		if (propGr.next()) {
			if (propGr.u_type == 'password') {
				var Encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
				return Encrypter.decrypt(propGr.u_value);  	
			}
			
			return propGr.u_value;			
		}

		// not found return default value if specified (check for undefined in case passed in "" as default value)
		if(typeof defaultValue != 'undefined')
			return defaultValue;
		// otherwise return null
		else
			return null;
	},
	
	setPspPropertyValue: function(propertyName, value) {		
		var propGr = new GlideRecord("u_psp_properties");
		propGr.addQuery("u_name", propertyName);
		propGr.query();
		if(propGr.next()) {		
			if (propGr.u_type == 'password') {
				var Encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
				value = Encrypter.encrypt(value);  	
			}
			
			propGr.u_value = value;
			propGr.update();
			return;
		}
		
		// not found, create property
		this.createPspProperty(propertyName, value);		
	},
	
	createPspProperty: function(propertyName, value, attributes){
		var propGr = new GlideRecord("u_psp_properties");
		propGr.initialize();
		propGr.u_name = propertyName;
		propGr.u_value = value;
		
		// update any attributes passed in
		if(attributes && attributes != null){
			for(var a in attributes){
				// fields in property table will be named "u_" + name of field as passed in
				propGr.setValue("u_" + a, attributes[a]);	
				
				// if we were passed in a type that's password we'll want to set value to be encrypted
				if (a == 'type' && attributes[a] == 'password') {
					var Encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
					propGr.u_value = Encrypter.encrypt(value);  	
				}
			}
		}
	
		propGr.insert();		
		return propGr;
    },

    /**
     * Get a property page field value
     * @param propertyName unique property identifier
     * @returns GlideRecord
     */
    getPspProperty: function(propertyName, field) {
        var gr = new GlideRecord("u_psp_properties");
        gr.addQuery("u_name", propertyName);
        gr.query();
        while (gr.next()) {
            return gr.getValue(field);
        }
    },

    /**
     * Set a property page field value
     * @param propertyName unique property identifier
     * @param field
     * @param value
     */
    setPspProperty: function(propertyName, field, value) {
        var gr = new GlideRecord("u_psp_properties");
        gr.addQuery("u_name", propertyName);
        gr.query();
        while(gr.next()) {
            gr.setValue(field, value);
			gr.update();
        }
    },

	resolveVariables: function(grParam, sourceField, pspTag){		
		var reg = new RegExp("\\$\\[(.+?)\\]", "gi");   
		var result;
		while((result = reg.exec(sourceField)) !== null) {
			// resolve the variable and then replace it in the source string passed in
			var resolvedValue = this.resolveVariable(grParam, result[1], pspTag);
			sourceField = sourceField.replace(result[0], resolvedValue);
		}
		
		var reg2 = new RegExp("\\$\\{(.+?)\\}", "gi");
		var result2;		
		while((result2 = reg2.exec(sourceField)) !== null) {
			// resolve the variable and then replace it in the source string passed in
			var resolvedValue = this.resolveVariable(grParam, result2[1], pspTag);
			sourceField = sourceField.replace(result2[0], resolvedValue);
		}
		
		return sourceField;
	},
	
	resolveVariable: function(grParam, sourceField, pspTag){		
		if(sourceField.indexOf("TM:") == 0){
			return this.resolveTableMap(grParam, sourceField, pspTag);
		}
		else if(sourceField.indexOf("GR:") == 0){
			return this.resolveGlideRecordValue(grParam, sourceField);
		}
		return sourceField;
	},
		
	resolveGlideRecordValue: function(grParam, sourceField){
		// get rid of GR:
		var sourceValue = sourceField.substring(sourceField.indexOf(":") +1); 		
		
		return grParam.getValue(sourceValue);		
	},
	
	resolveTableMap: function(grParam, sourceField, pspTag){
		// get rid of TM:
		var sourceValue = sourceField.substring(sourceField.indexOf(":") +1); 		
		var sourceValues = sourceValue.split(";");
		if(sourceValues.length < 1){
			return "";
		}

		var tableMapName = sourceValues[0];

		// get table map indicated in source field
		var tmap = new GlideRecord('u_psp_table_map');
		tmap.addQuery('u_name', tableMapName);
		tmap.addQuery('u_direction', 'outbound');
		tmap.query();
		if(!tmap.next()){
			return "";
		}
		
		// check if we should not resolve this table map (such as an attachment) on a record insert
		// since we'll be handling table map (attachment) elsewhere such as a script action
		if (grParam.operation() == "insert" && sourceValues.length >= 4 && sourceValues[3] == 'skip_insert') {
			return "";
		}
		
		var addTagSysIds = [];
		// create XML where for root element we'll use the target name as well
		// since whatever is resolving it will just want the root element to iterate through all the returned values
		// and then for each record we'll put it under the target table name as well since each record represents a table mapped record
		// <attachment>
		//    <attachment>
		//    </attachment>
		//    <attachment>
		//    </attachment>
		// </attachment>
		var mappedxml = new XMLDocument('<'+tmap.u_target_tablename+'></'+tmap.u_target_tablename+'>');
		
		// get the records from table map's table to use		
		var tgr = new GlideRecord(tmap.u_source_table);		
		
		// check if we have an encoded query to add for getting the records
		if(sourceValues.length > 1){			
			tgr.addEncodedQuery(sourceValues[1]);		
		}
		
		// check if we were passed the pspTag in the resolve variable string
		if(!pspTag && sourceValues.length >= 3 && sourceValues[2] != ''){
			pspTag = sourceValues[2];	
		}
		
		tgr.query();								
		while(tgr.next()){
			// check if this record already has this tag and we should skip
			if(this.recordHasTag(tgr, pspTag)){
				continue;	
			}
			else{
				addTagSysIds.push(tgr.getValue("sys_id"));
			}
			   
			var recEl = mappedxml.createElement(tmap.u_target_tablename);
			// set this new element created as one we want to add records to
			mappedxml.setCurrent(recEl);
			
			// go through each field of table map to construct XML for this record
			var fmap = new GlideRecord('u_psp_table_field_map');
			fmap.addQuery('u_parentid' , tmap.sys_id);
			fmap.query();
			
			while(fmap.next()) {
				if(fmap.u_target_field.startsWith("@"))
					continue; //skip mapped attributes
				
				if(fmap.u_source_field.startsWith("${")){
					var targetValue = this.resolveVariable(fmap.u_source_field, fmap.u_target_field);	
					mappedxml.createElement(fmap.u_target_field, targetValue);
				}						
				else if(!fmap.u_use_script){
					mappedxml.createElement(fmap.u_target_field, tgr.getValue(fmap.u_source_field));
				} else {
					var gc = (typeof GlideController != 'undefined') ? GlideController : Packages.com.glide.script.GlideController;
					var oldCurrent = current;
					gc.putGlobal('current', tgr);
					var answer = gc.evaluateString(fmap.u_source_script);
					gc.removeGlobal('current');		
					mappedxml.createElement(fmap.u_target_field, answer);
					current = oldCurrent;
				}					
			}
			
			// after we finish set current back to root element
			mappedxml.setCurrent(mappedxml.getDocumentElement());
			
		}
		
		// now go through and add tags to the records we just processed
		// we do it afterwards to avoid any reference conflicts as we iterate through
		// getting the data
		if(typeof pspTag != 'undefined' && pspTag != null && pspTag != "" && addTagSysIds.length > 0){
			for(var a = 0; a < addTagSysIds.length; a++){
				var tgr2 = new GlideRecord(tmap.u_source_table);
				if(!tgr2.get(addTagSysIds[a])){
					continue;
				}
				
				this.addTag(tgr2, pspTag);
			}
		}
		
		return mappedxml.toString();
	},
	
	recordHasTag: function(tgr, pspTag){				
		// no tag actually passed in
		if(typeof pspTag == 'undefined' || pspTag == null || pspTag == ""){
			return false;	
		}
		
		// query for tag in label table to get its sys_id
		var labelgr = new GlideRecord("label");
		labelgr.addQuery("name", pspTag);
		labelgr.query();
		var tagSysId = "";
		if(!labelgr.next()){
   		return false;
		}	
		
		tagSysId = labelgr.sys_id;
		
		var lgr = new GlideRecord("label_entry");
		lgr.addQuery("table", tgr.getTableName());
		lgr.addQuery("table_key", tgr.sys_id);
		lgr.addQuery("label", tagSysId);  // sys_id of the tag
		lgr.query();
		if(lgr.next()){
			return true;	// tag exists for this record
		}
		else{
			/*var lgr2 = new GlideRecord("label_entry");
			lgr2.table = tgr.getTableName();
			lgr2.table_key = tgr.sys_id;
			lgr2.label = tagSysId;  // sys_id of tag
			lgr2.read = "yes";
			//lgr2.title = "Attachment - " + jgr.file_name;
			lgr2.title = tgr.getClassDisplayValue() + " - " + tgr.getDisplayValue();
			lgr2.url = tgr.getTableName() + ".do?sys_id=" + tgr.sys_id + "&sysparm_view=";
			lgr2.insert();*/			
			return false;
		}		
	},
	
	addTag: function(tgr, pspTag){
		// no tag actually passed in
		if(typeof pspTag == 'undefined' || pspTag == null || pspTag == ""){
			return;	
		}
		
		// query for tag in label table to get its sys_id
		var labelgr = new GlideRecord("label");
		labelgr.addQuery("name", pspTag);
		labelgr.query();
		var tagSysId = "";
		if(!labelgr.next()){
   		return;
		}			
		tagSysId = labelgr.sys_id;
		
		var lgr = new GlideRecord("label_entry");
		lgr.addQuery("table", tgr.getTableName());
		lgr.addQuery("table_key", tgr.sys_id);
		lgr.addQuery("label", tagSysId);  // sys_id of the tag
		lgr.query();
		if(lgr.next()){
			return;	// tag exists for this record
		}
		
		var lgr2 = new GlideRecord("label_entry");
		lgr2.table = tgr.getTableName();
		lgr2.table_key = tgr.sys_id;
		lgr2.label = tagSysId;  // sys_id of tag
		lgr2.read = "yes";
		//lgr2.title = "Attachment - " + jgr.file_name;
		lgr2.title = tgr.getClassDisplayValue() + " - " + tgr.getDisplayValue();
		lgr2.url = tgr.getTableName() + ".do?sys_id=" + tgr.sys_id + "&sysparm_view=";
		lgr2.insert();	
	},
	
	isChildTable: function(childTableName, baseTableName){
		// Creating TableUtils on child table will get all tables from it up to base
		var tUtils = new TableUtils(childTableName);
		var tables = tUtils.getTables();
		return tables.contains(baseTableName);
	},

	/**
	 * get array of field names for table
	 */
	getTableFields: function(tableName) {
		var pgr = new GlideRecord(tableName);
		pgr.initialize();
		return pgr.getFields();
	},
		
    type: 'PerspectiumUtil'
};