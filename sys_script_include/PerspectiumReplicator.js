var PerspectiumReplicator = Class.create();

// Function that returns whether a table is replicatable, called by condition of
// Perspectium Replicate business rules
PerspectiumReplicator.isReplicatedTable = function(strTable, strDirection, strOperation) {
    if (strOperation == "insert") {
        strOperation = "create"; 
    }

    var grPSPReplConf = new GlideRecord("psp_replicate_conf");
    grPSPReplConf.addQuery("action_" + strOperation, true);
    grPSPReplConf.addQuery("sync_direction", strDirection);
    grPSPReplConf.addQuery("active", true);
    grPSPReplConf.addQuery("table_name", strTable);
    grPSPReplConf.query();
    if(grPSPReplConf.next()) {
        return true;
    }

    return false;
};

PerspectiumReplicator.prototype = {  
	
    initialize: function () {
		this.tableCounter = {};
		this.bulk_count = 0;
		this.bulk_byte_count = 0;
		this.bulk_query_count = 0;
    	this.psp = new Perspectium();
		this.logger = new PerspectiumLogger();
		this.encryption = new PerspectiumEncryption();
		this.key = gs.getProperty("instance_name", "unregistered");
						
		this.pspMS = new PerspectiumMessageSet();
		this.messageSetCounter = {};
		this.sentFirstMessage = false;
		this.flagLastMessage = false;
		this.bulk_share_query_limit = 1000;
		this.bulk_share_all_records = false;
		this.encryptionType = {"NONE":"0","TRIPLE_DES":"1","AES_128":"2", "BASE64_ONLY":"3"};

		this.pspUtil = new PerspectiumUtil();
		this.isServiceNowDirect = this.pspUtil.getPspPropertyValue('com.perspectium.replicator.servicenow_direct', 'false');
		this.dynSysJournalLimit = this.pspUtil.getPspPropertyValue('com.perspectium.dynamic.sys_journal_field.limit', '100');
		this.dynSysAuditLimit = this.pspUtil.getPspPropertyValue('com.perspectium.dynamic.sys_audit.limit', '200');		
		this.domainSeparated = gs.getProperty('glide.sys.restrict_global_domain_processes', 'false');		
		this.messageStatsMod = "Subscribing";
		this.fieldsToReplicate = [];

		this.maxBytes = parseInt(this.pspUtil.getPspPropertyValue("com.perspectium.output_bytes_limit", "5000000")); // max bytes to cap the send string limit of 16MB for Java String
    },
	
	getKey:function(grShareConfig){
		if(!grShareConfig.u_target_queue_group.isNil())
			return this.key + "." + grShareConfig.u_target_queue_group;
		else
			return this.key;
	},
	
	// function called by Replicator functions so we can keep track of first message sent
	createPSPOut: function(topic, type, key, name, value, target_queue, extra, attributes, shareGR, record_sys_id) {
		// if value blank we don't send out
		if(value == ""){
			this.logger.logError("Not sending message with topic " + topic + ", type " + type + ", key " + key + ", name " + name + " as value is empty", "PerspectiumReplicator.createPSPOut", shareGR);
			return;	
		}
		
		// if first message hasn't been sent out yet we'll set flag in attributes.
		if(!this.sentFirstMessage){					
			attributes = this.appendAttribute("psp_flag", "first", attributes);			
			this.sentFirstMessage = true;
		}

		// siam add attribute for tracking in audit
		if (topic == 'siam') {
			var siamMessageId;
			if (record_sys_id && record_sys_id != null && record_sys_id != "") {
				siamMessageId = record_sys_id;				
			}
			else {
				siamMessageId = this.generateUUID();
			}
			
			var currentMS = (new Date).getTime();
			siamMessageId += '-' + String(currentMS);
			
			// create a SIAM summary record so we know this message went outbound from servicenow
			/*var sasgr = new GlideRecord("u_psp_siam_audit");
			sasgr.u_message_id = siamMessageId;
			sasgr.u_message_timestamp = String(currentMS);
			sasgr.u_psp_timestamp = gs.nowDateTime();
			sasgr.u_direction = 'Outbound';
			sasgr.u_component_type = 'ServiceNow';
			var sasSysId = sasgr.insert();*/
			
			attributes = this.appendAttribute("SIAM_message_id", siamMessageId, attributes);	
		}
		
		// check if we have deferred messages to put in deferred state
		var state = "ready";
		if (name.indexOf(".deferred") > 0) {
			// set name back to proper name of update
			name = name.replace("deferred", "update");
			if (topic != 'counter') {
				state = "deferred";			
			}
		}
		
		if ((this.isServiceNowDirect == false || this.isServiceNowDirect == 'false')
				&& (name.contains("sys_attachment_doc.") || name.contains("sys_attachment."))) {
			var pspM = new PerspectiumMessage("replicator", "servicenow", key, name, value, target_queue, "", attributes, shareGR, "",state, false, "u_psp_attachment_out_message", record_sys_id);					
			//places the message in the attachments outbound table
			return pspM.enqueue();
		}
		else if ((this.isServiceNowDirect == false || this.isServiceNowDirect == 'false')
					&& name.contains("sys_audit.")) {
			var pspM = new PerspectiumMessage("replicator", "servicenow", key, name, value, target_queue, extra, attributes, shareGR, "", state, false, "u_psp_audit_out_message");
				
			//places the message in the sys audit outbound table
			return pspM.enqueue();
		 }		
		else {	
			// call actual Perspectium function to create outbound message
			this.psp.createPSPOut(topic, type, key, name, value, target_queue, extra, attributes, shareGR, state, record_sys_id);
		}
	},

	scheduleBulkShare : function(schd_sys_id) {
		this.logger.logDebug("Starting scheduled bulk share: " + schd_sys_id, "PerspectiumReplicator.scheduleBulkShare");
		var bgr = new GlideRecord("psp_bulk_share");
		bgr.addQuery("u_run_schedule", schd_sys_id);
		bgr.query();

		while(bgr.next()) {
			var sgr = new GlideRecord("u_psp_scheduled_bulk_share");
			sgr.get(schd_sys_id);
			var concurrentJobsEnabled = sgr.u_allow_concurrent_jobs == true || sgr.u_allow_concurrent_jobs == 'true';
			var jobInProgress = bgr.status == "Scheduled" || bgr.status == "Running" || bgr.status == "Cancelling";

			if ((concurrentJobsEnabled == 'false' || concurrentJobsEnabled == false) && (jobInProgress == 'true' || jobInProgress == true)) {
				this.logger.logWarn("Scheduled bulk share [" + sgr.name + "] has NOT scheduled bulk share [" + bgr.u_name + "] this interval, as it's already processing with state [" + bgr.status + "]", "PerspectiumReplicator.scheduleBulkShare");
				continue;
			} else if (jobInProgress == true || jobInProgress == 'true') {
				this.logger.logWarn("Scheduled Bulk Share: [" + sgr.name + "] has scheduled Bulk Share: [" + bgr.u_name + "] while it's already running with state " + bgr.status, "PerspectiumReplicator.scheduleBulkShare");
			}

			this.logger.logDebug("scheduled bulk sharing: " + bgr.sys_id, "PerspectiumReplicator.scheduleBulkShare");
			var startTime = "";
			if (bgr.u_share_updates_since_then) {
				startTime = bgr.getValue("started");
			}

			this.scheduleOneBulkShare(bgr, startTime);
		}
		this.logger.logDebug("Finished scheduled bulk share: " + schd_sys_id, "PerspectiumReplicator.scheduleBulkShare");
	},
	
	scheduleOneBulkShare : function(bgr, startTime) {
		// first check if we can find this bulk share so we don't create duplicates
		var bgr2 = new GlideRecord('psp_bulk_share');
		bgr2.addQuery('sys_id', bgr.sys_id);
		bgr2.queryNoDomain();
		if (!bgr2.next()) {	
	    	return;
		}
		
		// copy fields to save to a history of the bulk share and display as a related list
		var nr = new GlideRecord("u_psp_previous_bulk_shares");
		nr.name = bgr.u_name;
		nr.u_table_name = bgr.table_name;
		nr.u_started = bgr.started;
		nr.u_completed = bgr.completed;
		nr.u_duration = bgr.u_duration;
		nr.u_condition_script = "sys_updated_on>javascript:gs.dateGenerate('" + nr.u_started + "')";
		nr.u_records_processed = bgr.u_records_processed;
		nr.u_status = bgr.status;
		nr.u_bulk_share = bgr.sys_id;
		if (!nr.u_started.isNil())
			nr.insert();
		
		this.scheduleOnceBulkShareJob(bgr, startTime);	
	},
	
	scheduleOnceBulkShareJob: function(bgr, startTime) {		
		bgr.status = "Scheduled";
		bgr.u_cancel = false;
		bgr.started = gs.nowDateTime();		
		bgr.completed = "";		
		bgr.u_duration = "";
		bgr.u_records_processed = "";
		bgr.u_records_per_second = "";		
		bgr.update();
		
		// clear out previous history of records processed if any so we don't have old data in this table
		var ogr = new GlideRecord("u_psp_records_processed");
		ogr.addQuery("u_source_table", bgr.getTableName());
		ogr.addQuery("u_source", bgr.sys_id);
		ogr.deleteMultiple();
		
		var pspSO;
		// if advanced is selected or user selected to distribute bulk share, we use the custom script include to schedule job
		if ((bgr.u_advanced == true || bgr.u_advanced == "true") || 
			   (bgr.u_distribute_bulk_share_workload == true || bgr.u_distribute_bulk_share_workload == "true")) {
			var jobPriority;
			if (!bgr.u_scheduled_job_priority.nil() && bgr.u_scheduled_job_priority > 0) {
				jobPriority = parseInt(bgr.u_scheduled_job_priority);
			}
			else {
				jobPriority = parseInt(this.pspUtil.getPspPropertyValue("com.perspectium.replicator.scheduled_job.priority", "100"));
			}
			
			var systemId = "";
			var scgr = new GlideRecord("sys_cluster_state");
			if (!bgr.u_node_to_run_on.nil()) {
				// Assign systemId to specific node only if it exists
				scgr.addQuery("sys_id", bgr.u_node_to_run_on);
				scgr.query();
				if (scgr.next()) {
					this.logger.logDebug("Assigning Bulk Share: [ " + bgr.u_name + " ] to the node: [ " + scgr.system_id + " ]", "PerspectiumReplicator.scheduleOnceBulkShareJob", bgr);
					systemId = scgr.system_id;
				}
				else {
					this.logger.logWarn("Bulk Share: [ " + bgr.u_name + " ] scheduled without node assignment forced since specified node is invalid: [ " + bgr.u_node_to_run_on + " ]", "PerspectiumReplicator.scheduleOnceBulkShareJob", bgr);
				}
			}
			else if (bgr.u_distribute_bulk_share_workload == true || bgr.u_distribute_bulk_share_workload == "true") {
				// get the last node we ran with so we query for one that isn't that one
				var lastSystemId = this.pspUtil.getPspPropertyValue('com.perspectium.replicator.last_system_id', '');
				scgr.addQuery("system_id", "!=", lastSystemId);
				scgr.query();
				if (scgr.next()) {
					systemId = scgr.system_id;
					this.pspUtil.setPspPropertyValue('com.perspectium.replicator.last_system_id', systemId);
				}							
			}	
			pspSO = new PerspectiumScheduleOnce(jobPriority, systemId);
		}
		else {
			pspSO = new ScheduleOnce();
		}		
				
		pspSO.setLabel("Perspectium Replicator Bulk Share " + bgr.table_name);
		var gdt = new GlideDateTime();
		gdt.addSeconds(10); // schedule in 10 seconds
		pspSO.setTime(gdt.getValue()); 
		pspSO.script =  "var pspR = new PerspectiumReplicator();";
		pspSO.script += "pspR.bulkShareRecords('" + bgr.sys_id + "', '" + startTime + "');";		
		pspSO.schedule();	
	},
	
	executeBeforeBulkShareScript: function(script, bgr){
		var gc = (typeof GlideController != 'undefined') ? GlideController : Packages.com.glide.script.GlideController;
		var cancel = false;

		// put bulk share configuration global for reference in script
        gc.putGlobal('bulkshare_gr', bgr);   
		// put a cancel var global so user can cancel running bulk share
        gc.putGlobal('cancel', cancel);
		
		var rc = gc.evaluateString(script);        		
		
		cancel = gc.getGlobal('cancel');
        gc.removeGlobal('cancel');
        gc.removeGlobal('bulkshare_gr');		
				
		return cancel;			
	},
	
	executeAfterBulkShareScript: function(script, bgr){
		var gc = (typeof GlideController != 'undefined') ? GlideController : Packages.com.glide.script.GlideController;
		
		// put bulk share configuration global for reference in script
        gc.putGlobal('bulkshare_gr', bgr); 
		
		var rc = gc.evaluateString(script);        		
		gc.removeGlobal('bulkshare_gr');		
	},

	generateUUID: function () { // Public Domain/MIT
		var d = new Date().getTime();
		if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
			d += performance.now(); //use high-precision timer if available
		}
		return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
			var r = (d + Math.random() * 16) % 16 | 0;
			d = Math.floor(d / 16);
			return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
		});
	},
	
	etsBulkShareRecords: function(bgr) {
		if (bgr.status == 'Cancelling')
			this.cancelledBulkShare(bgr);
		
		var conditions = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"];
		var jobs = parseInt(this.pspUtil.getPspPropertyValue("com.perspectium.bulk_share_scheduled_jobs", "4"));
		
		bgr.u_set_id = this.generateUUID();
		bgr.update();
		
		for (var j = 0; j < jobs; j++){
			var pspSO = new ScheduleOnce();
			var jobName = "Perspectium Replicator Bulk Share " + (j + 1) + " " + bgr.table_name;
			pspSO.setLabel(jobName);

			var condition = "";
			// create conditions based on number of jobs so we can evenly spread out records between the jobs
			// for ex. job #1 'sys_idSTARTSWITH0^ORsys_idSTARTSWITH4^ORsys_idSTARTSWITH8^ORsys_idSTARTSWITHc^EQ'
			if (jobs > 1) {
				condition = "sys_idSTARTSWITH" + conditions[j];
				var conditionCount = j + jobs;
				while (conditionCount < 16) {
					condition += "^ORsys_idSTARTSWITH" + conditions[conditionCount];
					conditionCount += jobs;
				}				
			}
			
			var gdt = new GlideDateTime();
			gdt.addSeconds(5); // schedule in 5 seconds
			
			pspSO.setTime(gdt.getValue()); 			
			pspSO.script = "var pspB = new PerspectiumBulkShare('" + bgr.sys_id + "', '" + condition + "^EQ', '" + jobName + "');";
			pspSO.script += "pspB.execute();";			
			pspSO.schedule();			
		}
	},
	
    bulkShareRecords : function(bsysid, startTime) { 
		var bgr = new GlideRecord('psp_bulk_share');
		bgr.addQuery('sys_id', bsysid);
		bgr.queryNoDomain();
		if(!bgr.next()) {	
	    	return;
		}
		
		this.logger.logDebug("bulkShareRecords("+ bsysid +")", "PerspectiumReplicator.bulkShareRecords", bgr);
		
		if (bgr.status == "Cancelling") {
			this.cancelledBulkShare(bgr);
			return;
		}
		
		if (bgr.status == "Cancelled") {
			return;
		}
		
		this.logger.logDebug("bulkShareRecords: " + bgr.table_name, "PerspectiumReplicator.bulkShareRecords", bgr);

		// check if a "run as" user chosen to run bulk share as
		var curUser = "";
		if (!bgr.u_run_as.isNil()) {
			curUser = gs.getSession().impersonate(bgr.u_run_as);	
		}

		bgr.status = 'Running';
		bgr.update();
		this.psp.createPSPOut("monitor", "label", this.getKey(bgr), "Perspectium^PSP", "Bulk share started : '" + bgr.table_name + "'", null, null, null, bgr);
		// before bulk sharing any records we'll run before script if specified
		if (!bgr.u_before_bulk_share_script.nil()) {
			var cancel = this.executeBeforeBulkShareScript(bgr.u_before_bulk_share_script, bgr);
			if (cancel == true || cancel == "true") {
				// user cancels bulk share so we'll update to cancelled status
				this.cancelledBulkShare(bgr);
				this.logger.logDebug(bgr.u_name + " bulk share (" + bgr.sys_id + ") cancelled by Before Bulk Share Script", "PerspectiumReplicator.bulkShareRecords");
				
				// before returning set back to original user
				if (!bgr.u_run_as.isNil() && curUser != "") {
					gs.getSession().impersonate(curUser);	
				}

				return;
			}			
		}
				
		this.logger.logDebug("bulkShareRecords: after post label", "PerspectiumReplicator.bulkShareRecords", bgr);

		var action = "bulk";
        if (bgr.u_insert_only == "true" || bgr.u_insert_only == true) {
        	action = "insert";
        }

		if (bgr.u_share_only_selected_fields == "true" || bgr.u_share_only_selected_fields == true){
			var pspSF = new PerspectiumShareFields();
			this.fieldsToReplicate = pspSF.getFieldsToReplicate(bgr);
		}
		
		// check for ETS mode
		if (bgr.u_ets_active == "true" || bgr.u_ets_active == true) {
			this.etsBulkShareRecords(bgr);
			
			// finished bulk share so we'll execute any after script if specified
			if (!bgr.u_after_bulk_share_script.nil()) {
				this.executeAfterBulkShareScript(bgr.u_after_bulk_share_script, bgr);
			}
			
			return;
		}
		else if (bgr.u_share_only_sys_ids_listed == "true" || bgr.u_share_only_sys_ids_listed == true) {
			this.logger.logDebug("bulkShareRecords: " + bgr.table_name + " sharing only sys ids listed", "PerspectiumReplicator.bulkShareRecords", bgr);
			
			// go through related list of sys ids and share each sys id
			var tgr = new GlideRecord('u_psp_bulk_share_sys_id');
			tgr.addQuery('u_bulk_share', bgr.sys_id);
			tgr.query();
						
			// save total row count for attributes
			this.bulk_query_count = tgr.getRowCount();

			while(tgr.next()){
	    		var gr=new GlideRecord(bgr.table_name);
			
				gr.addQuery('sys_id', tgr.u_record_sys_id);
				
				// if a run as user selected we'll want to do a regular query()
				// to honor domain separation since people will only choose a run as user to get the results that user only sees
				if (!bgr.u_run_as.isNil()) {
					gr.query();
				}
				else {
					gr.queryNoDomain();
				}
				
				if(!gr.next()) {	
				//if(!gr.get(tgr.u_record_sys_id)) {
					this.logger.logDebug("No record with sys id " + tgr.u_record_sys_id + " found in table " + bgr.table_name, "PerspectiumReplicator.bulkShareRecords", bgr);
					continue;
				}
				
				this.logger.logDebug("Bulk sharing record: " + tgr.u_record_sys_id + " from table " + bgr.table_name, "PerspectiumReplicator.bulkShareRecords", bgr);
				
				this.bulkShareRecord(bgr, gr, action);
				
				if (this.bulk_count % 1000 == 0) {					
					bgr.addQuery('sys_id', bsysid);
					bgr.queryNoDomain();
					if(bgr.next()) {
						// only update count if we found bulk share
						// so we don't insert a new record
						bgr.u_records_processed = this.bulk_count;
		    			bgr.update();
						
						// check if cancelled
						if (bgr.status == "Cancelling") {
							this.cancelledBulkShare(bgr);
							
							// before returning set back to original user
							if (!bgr.u_run_as.isNil() && curUser != "") {
								gs.getSession().impersonate(curUser);	
							}

							return; // bail
						}
						
						if (bgr.status == "Cancelled") {							
							// before returning set back to original user
							if (!bgr.u_run_as.isNil() && curUser != "") {
								gs.getSession().impersonate(curUser);	
							}

							return;
						}
					}
				}				
			}	    	        	
        }		
		else if (bgr.condition.nil()) {
	    	var more = this.bulkShareAllRecords(bgr, null, startTime);
	    	while(more != null) {
				more = this.bulkShareAllRecords(bgr, more, startTime);
	    	}
		} else {	
			this.logger.logDebug("bulkShareRecords: " + bgr.table_name + " with condition: " + bgr.condition, "PerspectiumReplicator.bulkShareRecords", bgr);
			
	    	var gr=new GlideRecord(bgr.table_name);
	    	gr.addEncodedQuery(bgr.condition);
	    	if(bgr.u_share_updates_since_then && startTime && startTime != 'undefined' && startTime != "") {
				gr.addQuery('sys_updated_on', '>=', new GlideDateTime(startTime));
			}
			
			if (bgr.isValidField("u_limit_number_of_records_shared") && !bgr.u_limit_number_of_records_shared.nil()) {
				gr.setLimit(bgr.getValue("u_limit_number_of_records_shared"));
			}
			else if(bgr.isValidField("u_limit_number_of_records_shar") && !bgr.u_limit_number_of_records_shar.nil()) {
				gr.setLimit(bgr.getValue("u_limit_number_of_records_shar"));
			}
						
			// if a run as user selected we'll want to do a regular query()
			// to honor domain separation since people will only choose a run as user to get the results that user only sees
			if (!bgr.u_run_as.isNil()) {
				gr.query();
			}
			else {
                gr.queryNoDomain(); 
			}	
				
			// save total row count for attributes
			this.bulk_query_count = gr.getRowCount();

			// use _next to account for tables such as sys_template that have "next" column
	    	while(gr._next()) {
				this.bulkShareRecord(bgr, gr, action);
				
				if (this.bulk_count % this.bulk_share_query_limit == 0) {					
					bgr.addQuery('sys_id', bsysid);
					bgr.queryNoDomain();
					if(bgr.next()) {
						// only update count if we found bulk share
						// so we don't insert a new record
			    		bgr.u_records_processed = this.bulk_count;
			    		bgr.update();

						// check if cancelled
						if (bgr.status == "Cancelling") {
							this.cancelledBulkShare(bgr);				
							// before returning set back to original user
							if (!bgr.u_run_as.isNil() && curUser != "") {
								gs.getSession().impersonate(curUser);	
							}
							
							return; // bail
						}
						
						if (bgr.status == "Cancelled") {
							// before returning set back to original user
							if (!bgr.u_run_as.isNil() && curUser != "") {
								gs.getSession().impersonate(curUser);	
							}
							
							return;
						}
					}
				}
	    	}
		}
		
		// finished bulk share so we'll execute any after script if specified
		if (!bgr.u_after_bulk_share_script.nil()) {
			this.executeAfterBulkShareScript(bgr.u_after_bulk_share_script, bgr);
		}
		
		// check if we can find this bulk share so we don't create duplicates
		var bgr2 = new GlideRecord('psp_bulk_share');
		bgr2.addQuery('sys_id', bgr.sys_id);
		bgr2.queryNoDomain();
		if (bgr2.next()) {	
			bgr2.completed = gs.nowDateTime();
			if (bgr2.u_cancel == true) {
				bgr2.status = 'Cancelled';
			}
			else {
				bgr2.status = 'Completed';
			}
			bgr2.u_records_processed = this.bulk_count;
			bgr2.update();
		}

		// send out # processed message set message
		this.pspMS.createMessageSetProcessed(this.messageSetCounter, bgr, this.getKey(bgr));
									
		// get total breakdown of records processed and update the related list
		this.updateTotalRecordsProcessed(bgr, this.messageSetCounter);
		
		// send share counters
		var counterName = bgr.table_name + "." + action;
	    this.psp.sendCounter(counterName, this.bulk_count, "counter");
	    this.psp.sendCounter(counterName + ".bytes", this.bulk_byte_count, "counter");
		
		this.psp.createPSPOut("monitor", "label", this.getKey(bgr), "Perspectium^PSP", "Bulk share completed : '" + bgr.table_name + "' ('" + this.bulk_count + "')", null, null, null, bgr);
		
		// set back to original user after finishing
		if (!bgr.u_run_as.isNil() && curUser != "") {
			gs.getSession().impersonate(curUser);	
		}
    },
	
	updateTotalRecordsProcessed: function (bgr, messageSetCounter) {
		// clear out previous history of records processed if any so we don't have old data in this table
		var ogr = new GlideRecord("u_psp_records_processed");
		ogr.addQuery("u_source_table", bgr.getTableName());
		ogr.addQuery("u_source", bgr.sys_id);
		ogr.deleteMultiple();
		
		var totalCount = 0;
		for (var m in messageSetCounter) {
			var value = messageSetCounter[m];
			if (value == 0) {
				continue;
			}
			
			var rpgr = new GlideRecord("u_psp_records_processed");
			rpgr.u_source_table = bgr.getTableName();
			rpgr.u_source = bgr.sys_id;
			
			var name = m;
			// remove action such as .bulk as we don't need it
			if (name.indexOf(".") > 0) {
				name = name.substr(0, name.indexOf("."));
			}
			
			rpgr.u_name = name;
			rpgr.u_value = value;
			rpgr.insert();	
			
			totalCount += value;
		}
		
		if (totalCount == 0) {
			return;
		}
		
		// check if we can find this bulk share so we don't create duplicates
		var bgr2 = new GlideRecord('psp_bulk_share');
		bgr2.addQuery('sys_id', bgr.sys_id);
		bgr2.queryNoDomain();
		if (bgr2.next()) {	
			bgr2.u_records_processed = totalCount;
			bgr2.update();
		}
		
	},
	
	cancelledBulkShare : function(bgr) {
		// first check if we can find this bulk share so we don't create duplicates
		var bgr2 = new GlideRecord('psp_bulk_share');
		bgr2.addQuery('sys_id', bgr.sys_id);
		bgr2.queryNoDomain();
		if (!bgr2.next()) {	
	    	return;
		}
		
		bgr.status = "Cancelled";
		bgr.u_records_processed = this.bulk_count;
		bgr.update();
		
		// only send counter if we actually shared records
		if(this.bulk_count > 0){		
			var counterName = bgr.table_name + ".bulk";
			this.psp.sendCounter(counterName, this.bulk_count, "counter");
	    	this.psp.sendCounter(counterName + ".bytes", this.bulk_byte_count, "counter");
			
			// send out # processed message set message
			this.pspMS.createMessageSetProcessed(this.messageSetCounter, bgr, this.getKey(bgr));
		}
	
		this.psp.createPSPOut("monitor", "label", this.getKey(bgr), "Perspectium^PSP", "Bulk share cancelled : '" + bgr.table_name + "' ('" + this.bulk_count + "')", null, null, null, bgr);
	},
	
    bulkShareAllRecords : function(bgr, starting_sysid, startTime) {	

		var gr=new GlideRecord(bgr.table_name);
		gr.orderBy('sys_id');
		
		// check if this is a database view
		var sdv = new GlideRecord("sys_db_view");
		sdv.addQuery("name", bgr.table_name);
		sdv.queryNoDomain();		
		// if not then we limit the query to 1000 records to optimize performance
		if (!sdv.next()) {
			this.logger.logDebug("Limiting query to  " + this.bulk_share_query_limit, "PerspectiumReplicator.bulkShareAllRecords", bgr);
			if (bgr.isValidField("u_limit_number_of_records_shared") && !bgr.u_limit_number_of_records_shared.nil()) {
				gr.setLimit(bgr.getValue("u_limit_number_of_records_shared"));
			}
			else if(bgr.isValidField("u_limit_number_of_records_shar") && !bgr.u_limit_number_of_records_shar.nil()) {
				gr.setLimit(bgr.getValue("u_limit_number_of_records_shar"));
			}
			else{
				gr.setLimit(this.bulk_share_query_limit);		
			}		
		}
			 					
		if (starting_sysid != null) {
			gr.addQuery('sys_id', '>', starting_sysid);
		}
		
		if(bgr.u_share_updates_since_then && startTime && startTime != 'undefined' && startTime != "") {
			gr.addQuery('sys_updated_on', '>=', new GlideDateTime(startTime));
		}
		
		// if a run as user selected we'll want to do a regular query()
		// to honor domain separation since people will only choose a run as user to get the results that user only sees
		if (!bgr.u_run_as.isNil()) {
			gr.query();
		}
		else {
            gr.queryNoDomain();
		}				

		this.bulk_share_all_records = true;
		// save total row count for attributes when we have complete total
		if(gr.getRowCount() != this.bulk_share_query_limit)
			this.bulk_query_count = this.bulk_count + gr.getRowCount();

		var last_sys_id = null;
		var kount = 0;
	
		var action = "bulk";
		if (bgr.u_insert_only == "true" || bgr.u_insert_only == true) {
			action = "insert";
		}
		
		this.logger.logDebug("bulkShareAllRecords: " + bgr.table_name + " action=" + action, "PerspectiumReplicator.bulkShareAllRecords", bgr);
	
		// use _next to account for tables such as sys_template that have "next" column
		while(gr._next()) {
			last_sys_id = gr.sys_id;
			this.bulkShareRecord(bgr, gr, action);
			kount ++;
			
			// check every 1000 records if we want to cancel
			if (kount % this.bulk_share_query_limit == 0) {								
				bgr.addQuery('sys_id', bgr.sys_id);
				bgr.queryNoDomain();
				if(bgr.next()) {
					// only update count if we found bulk share
					// so we don't insert a new record
					bgr.u_records_processed = this.bulk_count;
					bgr.update();

					// check if cancelled
					if (bgr.status == "Cancelling") {
						this.cancelledBulkShare(bgr);
						return null; // bail
					}
					
					var limitRecordsShared;
					if(bgr.isValidField("u_limit_number_of_records_shared")){
						limitRecordsShared = bgr.u_limit_number_of_records_shared;
					}
					else{
						limitRecordsShared = bgr.u_limit_number_of_records_shar;
					}
					
					if(limitRecordsShared != null && limitRecordsShared != "" && limitRecordsShared <= kount) {
						return null;
					}
					
					
					if (bgr.status == "Cancelled") {
						return null;
					}
				}				
			}
		}
		
		this.logger.logDebug("bulkShareAllRecords: " + bgr.table_name + " shared: " + kount + " records", "PerspectiumReplicator.bulkShareAllRecords", bgr);
			
		// reach query limit so we return last sys_id for next set of records
		// if we queried all at once count will be greater so we won't return last sys_id
		if (kount == this.bulk_share_query_limit) {
			return last_sys_id;
		}
		
		var bgr2 = new GlideRecord('psp_bulk_share');
		bgr2.addQuery('sys_id', bgr.sys_id);
		bgr2.queryNoDomain();
		if (bgr2.next()) {			
			bgr.u_records_processed = this.bulk_count;
			bgr.update();
		}
		
		return null;	
    },

	bulkShareRecord : function(bgr, gr, op) {				
		if (bgr.u_include_child_only == "true" ||  bgr.u_include_child_only == true) {
			var tname = bgr.table_name;
			if (gr.sys_class_name && !gr.sys_class_name.isNil()) {
				tname = gr.sys_class_name;
				
				// get the record from the new table name to get the right record and ensure we get any fields new table name only has
				var tngr = new GlideRecord(tname);
				
				tngr.addQuery('sys_id', gr.sys_id);
				
				// if a run as user selected we'll want to do a regular query()
				// to honor domain separation since people will only choose a run as user to get the results that user only sees
				if (!bgr.u_run_as.isNil()) {
					tngr.query();
				}
				else {
					tngr.queryNoDomain();
				}
				
				if(!tngr.next()){
				//if(!tngr.get(gr.sys_id)){
					this.logger.logDebug("Cannot find record " + gr.sys_id + " in " + tname, "PerspectiumReplicator.bulkShareRecord", bgr);
					return;
				}				
				
				this.bulkShareOneRecord(bgr, tname, tngr, op);				
			}
			else
				this.bulkShareOneRecord(bgr, tname, gr, op);
			
			return;
		} else if (bgr.u_include_child_tables == "true" ||  bgr.u_include_child_tables == true) {
			var found = false;
			var currentTable = "";
			var parentTable = gr.sys_class_name;
			
			while(parentTable != -1 && currentTable != bgr.table_name){
				currentTable = parentTable;
				
				// check we can get child table record and then share it instead of base table record
				var ctgr = new GlideRecord(currentTable);	
				
				ctgr.addQuery('sys_id', gr.sys_id);
				
				// if a run as user selected we'll want to do a regular query()
				// to honor domain separation since people will only choose a run as user to get the results that user only sees
				if (!bgr.u_run_as.isNil()) {
					ctgr.query();
				}
				else {
					ctgr.queryNoDomain();
				}
				
				if(!ctgr.next()){
					continue;	
				}
				
				this.logger.logDebug("bulkShareRecord " + gr.sys_id + " for child table " + currentTable, "PerspectiumReplicator.bulkShareRecord", bgr);
				
				
				if(this.bulkShareOneRecord(bgr, currentTable, ctgr, op)) {				
					found = true;
				}
				
				parentTable = this.getParentTable(currentTable);
			}
			
			if (found) {
				this.logger.logDebug("bulkShareRecord there were child tables, no need to sync base","PerspectiumReplicator.bulkShareRecord", bgr);
				return;
			}
		}
		
		this.bulkShareOneRecord(bgr, bgr.table_name, gr, op);
	},
	
	getParentTable: function(table_name) {
		var cgr = new GlideRecord('sys_db_object');
		cgr.addQuery('name', table_name);
		cgr.query();
		if(cgr.next()){
			if(cgr.super_class.nil()){
				return -1;
			}
			else{
				return cgr.super_class.name;
			}
		}
		return -1;
	},
	
	// override encryption key if target queue contains a custom key
	setQueueKey : function (tqgr) {
		if (tqgr.u_target_queue.isNil())
			return;
		
		if (tqgr.u_target_queue.u_record_encryption_key.isNil())
			return;
		
		var encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
		var qKey = encrypter.decrypt(tqgr.u_target_queue.u_record_encryption_key);
				
		this.logger.logDebug("setQueueKey using target queue key: " + qKey);
		this.encryption = new PerspectiumEncryption(qKey);
	},
	
    bulkShareOneRecord : function(bgr, table_name, gr, op) {
        try {
			var sys_id = gr.sys_id;
            if(gr.sys_id.isNil() || sys_id == null || sys_id == "") {
				// sys_id not found for this table
				this.logger.logDebug("bulkShareOneRecord not found for " + table_name +  "," + sys_id, "PerspectiumReplicator.bulkShareOneRecord", bgr);
				return false;
			}
			
			if (!bgr.u_before_share_script.nil()) {
				var jsonParams = {};
				jsonParams.psp_action = op;
				var ignore = this.psp.executeBeforeShareScript(bgr.u_before_share_script, gr,jsonParams);
				if (ignore == true || ignore == "true") {
					this.logger.logDebug("bulkShareOneRecord ignored by beforeShareScript", "PerspectiumReplicator.bulkShareOneRecord");
					return false;
				}
				op = jsonParams.psp_action;
			}
						
			// share history set first
			if (bgr.u_include_history_set == "true" || bgr.u_include_history_set == true) {
				this.shareHistorySet(table_name, sys_id, null, bgr);	
			}
			
			// override encryption key if target queue contains a custom key
			if (!bgr.u_target_queue.isNil() && !bgr.u_target_queue.u_record_encryption_key.isNil()) {
				var encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
				var qKey = encrypter.decrypt(bgr.u_target_queue.u_record_encryption_key);
				
				this.logger.logDebug("bulkShareOneRecord using target queue key: " + qKey);
				this.encryption = new PerspectiumEncryption(qKey);
			}

			this.setQueueKey(bgr);

			var enc = "";
			//var cipher = this.encryptionType.TRIPLE_DES;
			var cipher = bgr.u_cipher;
			var grTableMap;
			if(bgr.isValidField("u_table_map") && !bgr.u_table_map.nil()){
				grTableMap = this.encryption.getOutboundTableMap(bgr.u_table_map);
				if(grTableMap.u_topic == "siam"){
					cipher = this.encryptionType.BASE64_ONLY;
					enc = this.encryption.encryptTableMap(gr, grTableMap, cipher); 
				}
				else{
					cipher = bgr.u_cipher;
			    	enc = this.encryption.encryptTableMap(gr, grTableMap, bgr.u_cipher);
				}
			} else if (!bgr.u_view_name.nil()) {
				enc = this.encryption.encrypt(gr, bgr.u_view_name, op, bgr.u_target_queue, cipher);
			} else if (bgr.u_share_only_selected_fields == "true" || bgr.u_share_only_selected_fields == true){
				var pspSF = new PerspectiumShareFields();
				var recordXmlStr = pspSF.createShareRecordXML(bgr, gr, table_name, this.fieldsToReplicate);
				enc = this.encryption.encryptString(recordXmlStr);
			} else {
            	enc = this.encryption.encrypt(gr, null, op, bgr.u_target_queue, cipher);
			}
			
			// if the value is too large then we'll error
			// account for both if the value is bigger than our max sending size
			// as well as the largest size allowed for a String
			if (enc.length() > this.maxBytes || (enc.length() * 2) > 16000000) {					
				this.logger.logError(op + " message for " + table_name + " <" + gr.sys_id + "> cannot be created in outbound queue as it is larger than message limit", "PerspectiumReplicator.bulkShareOneRecord");
				return;
			}

            //topic, type, key, name, value
			var topic = "replicator";
			var type = "servicenow";
			var bAttributes = "";

			bAttributes = this.appendAttribute("cipher", cipher, bAttributes);
			if(typeof(grTableMap) != "undefined" && !grTableMap.isNil()){
				if(!grTableMap.u_topic.isNil())
					topic = grTableMap.u_topic;
				if(!grTableMap.u_type.isNil())
					type = grTableMap.u_type;
				if(!grTableMap.u_target_tablename.isNil())
					table_name = grTableMap.u_target_tablename;
				bAttributes = this.appendTableMapAttributes(gr, grTableMap, bAttributes);
			}
            //var name = table_name + "." + op;
            //this.psp.createPSPOut("replicator", "servicenow", this.getKey(bgr), name, enc, bgr.u_target_queue);
			//this.bulk_count++;
			
			// add count for bytes but getting length of value
			// to ensure it's a string add ""
			this.bulk_byte_count += enc.length();
			
			if (bgr.u_include_attachment == "true" || bgr.u_include_attachment == true) {
				this.shareAttachments(sys_id, bgr, op);	
			}
			
			if (bgr.u_share_journal == "true" || bgr.u_share_journal == true) {
				this.shareJournal(sys_id, null, bgr.sys_id, true);				
			}
		
			if (bgr.u_include_sys_audit == "true" || bgr.u_include_sys_audit == true) {
				this.shareSysAudit(table_name, sys_id, null, bgr);
			}
						
			if (bgr.u_include_embedded_images_vide == "true" || bgr.u_include_embedded_images_vide == true) {
				this.shareEmbedded(gr, sys_id, bgr, op);	
			}

			// send table record last so we can set psp_flag last for last message in set
			//topic, type, key, name, value, queue, extra, attributes
            var name = table_name + "." + op;

			// add attributes with bulk share conf id to use for message set id
			bAttributes = this.appendAttribute("set_id", bgr.sys_id, bAttributes);
			
			// for case of bulk sharing all records and we're at our per query limit
			// we'll check if this is the last record so we can set psp_flag last
			// otherwise flag won't be set since we can' tell if we're at the 1000 record in the set or 1000 is the last record 
			if(this.bulk_share_all_records && (this.bulk_count + 1) % this.bulk_share_query_limit == 0){
				var trgr = new GlideRecord(bgr.table_name);
				trgr.orderBy('sys_id');
				trgr.addQuery('sys_id', '>', gr.sys_id);
				trgr.query();
				
				if (!trgr.next()){
					bAttributes = this.appendAttribute("psp_flag", "last", bAttributes);
				}				
			}			   
			// set flag in attributes for last message in bulk share
			else if((this.bulk_count + 1) == this.bulk_query_count){				
				bAttributes = this.appendAttribute("psp_flag", "last", bAttributes);				
			}
			
			this.createPSPOut(topic, type, this.getKey(bgr), name, enc, bgr.u_target_queue, "", bAttributes, bgr, gr.sys_id);
			this.bulk_count++;
			
			// increment counter for each message sent
			this.incrementMessageSetCounter(name);			
			
        } catch(e) {
            if (String(e) == "java.lang.NullPointerException") {
				this.logger.logWarn("bulkShareOneRecord failed due to invalid null characters, sys_id: " + gr.sys_id + ", error = " + e, "PerspectiumReplicator.bulkShareOneRecord", bgr);
			} 
			else {
				this.logger.logWarn("bulkShareOneRecord " + table_name + ", record = " + gr.sys_id + ", error = " + e.message, "PerspectiumReplicator.bulkShareOneRecord", bgr);
			}
        }
		
		return true;
    },
	
	// operation is passed if cgr was reconstituted from XML e.g. from sys_audit_delete business rule
	shareRecord : function(cgr, table_name, operation, grShareConfigSysId, pspAttributes, sumCounts){
		var sys_class_name = cgr.getTableName();
		var sys_id = cgr.sys_id;
		var op = "insert";
		
		if (!table_name) {
			table_name = sys_class_name;
		}
			
		if (cgr.operation() != null) {
	    	op = cgr.operation().toString();
		}
		
		// override operation if passed one
		if (operation && operation != "") {
			op = operation;
		}
		
		// each time dynamic share called we'll re-set flag that we sent first message since each dynamic share
		// is the first message
		this.sentFirstMessage = false;
		
		var qc = new GlideRecord('psp_replicate_conf');
    	try {
            qc.addQuery('table_name', table_name);
	    	qc.addQuery("sync_direction", "share");
	    	qc.addQuery("active", "true");
			
			// if we were passed in a dynamic share config we'll query only for it
			if(grShareConfigSysId && grShareConfigSysId != null && grShareConfigSysId != ""){
	    		qc.addQuery("sys_id", grShareConfigSysId);				
			}

            qc.query();
			
			if (!qc.hasNext()) {
				this.logger.logDebug("shareRecord no share definition found for " + sys_class_name + " " + op, "PerspectiumReplicator .shareRecord", qc);
				return; // nothing can be done
			}

            while (qc.next()) {
		        this.logger.logDebug("sharing " + sys_class_name + " " + sys_id + " " + op + " using share config " + qc.sys_id, "PerspectiumReplicator.shareRecord", qc);
				
				if (!operation && op == "delete" && qc.u_use_listener == true) {
					this.logger.logDebug("this came in from business rule, but we are using audit listener so dont process share config " + qc.sys_id, "PerspectiumReplicator.shareRecord", qc);
					break;
				}
				
				// check to see if column updates should be ignored or shared on
				if (qc.u_select_column_updates_to_ignore == "true" ||
					qc.u_select_column_updates_to_ignore == true) {
					var isShared = this.checkUpdatedField(qc, cgr, true);
					if (isShared == false || isShared == "false") {
						this.logger.logDebug("shareRecord skipped for " + table_name + " - [" + cgr.sys_id + "] due to " + 
											 "Ignore Updated Fields", "PerspectiumReplicator.shareRecord", qc);
						return;
					} 
				} else if (qc.u_select_column_updates_to_share_on == "true" ||
						   qc.u_select_column_updates_to_share_on == true) {
					var isShared = this.checkUpdatedField(qc, cgr, false);
					if (isShared == false || isShared == "false") {
						this.logger.logDebug("shareRecord skipped for " + table_name + " - [" + cgr.sys_id + "] due to " + 
											 "Share On Updated Fields", "PerspectiumReplicator.shareRecord", qc);
						return;
					}
				}
				
				if (qc.u_share_only_selected_fields == "true" || qc.u_share_only_selected_fields == true && this.fieldsToReplicate.length == 0){
					var pspSF = new PerspectiumShareFields();
					this.fieldsToReplicate = pspSF.getFieldsToReplicate(qc);
				}
				
				// if not delete and we weren't pass share config sys id, then we'll ignore share configs that do have a reference to a business rule
				// since those were created in the new way and we only want those to run when share config sys id passed				
				if(!grShareConfigSysId && 
				   (qc.u_business_rule != null && qc.u_business_rule != "" && !qc.u_business_rule.isNil())){
					continue;
				}
				
    		    if (qc.u_share_base_table_only == true) {
			        sys_class_name = table_name;
		        }
				else if ((qc.u_include_child_tables == "true" ||  qc.u_include_child_tables == true) && sys_class_name != table_name) {	
					var parentTable = this.getParentTable(sys_class_name);
					while(parentTable != -1){
				
						// check if we can get child table record and then share it instead of base table record
						var ctgr = new GlideRecord(parentTable);	
						ctgr.addQuery('sys_id', sys_id);
						ctgr.queryNoDomain();
						
						this.logger.logDebug("shareRecord " + sys_id + " for child table " + parentTable, "PerspectiumReplicator.shareRecord", qc);
										
						if (ctgr._next()) {
							this.shareOneRecord(ctgr, qc, parentTable, op, null, pspAttributes, sumCounts);
						}
						
						if (parentTable == table_name) {
							break;
						}
						parentTable = this.getParentTable(parentTable);
					}
				}				
				
				// get start and finished datetime as we share record to send for messageset
				var startedDateTime = gs.nowNoTZ();
				
				this.shareOneRecord(cgr, qc, sys_class_name, op, null, pspAttributes, sumCounts);

				var finishedDateTime = gs.nowNoTZ();

				// for each share configuration, send message set 
				this.pspMS.createMessageSetProcessed(this.messageSetCounter, qc, this.getKey(qc), startedDateTime, finishedDateTime);
				
			}
    	} catch(e) {
            this.logger.logWarn("shareRecord " + sys_class_name + ", error = " + e, "PerspectiumReplicator.shareRecord", qc);
    	}
    },
	
	// function will return true if a share should occur, else false
	checkUpdatedField: function (qc, cgr, ignoreFlag) {
		var gru = GlideScriptRecordUtil.get(cgr);
		var changedFields = gru.getChangedFieldNames();
		gs.include("j2js");
		changedFields = j2js(changedFields);
		
		// get array of columns to ignore
		var exclusiveColumns = [];
		var excludeGR = new GlideRecord("u_updated_columns_to_ignore");
		excludeGR.addQuery("u_dyname_share", qc.sys_id);
		excludeGR.query();
		while (excludeGR.next()) {
			exclusiveColumns.push(excludeGR.u_column_name.toString());
		}
		
		// select column updates to ignore is selected
		if (ignoreFlag) {
			for (var i = 0; i < changedFields.length; i++) {
				var logIndex = "Updated Column (" + (i + 1) + " / " + changedFields.length + "): " + changedFields[i];
				if (exclusiveColumns.indexOf(changedFields[i]) == -1) {
					// updated column not in the ignore list - replicate
					this.logger.logDebug(logIndex + " is NOT in the exclusion list: " + exclusiveColumns.toString(),
										 "PerspectiumReplicator.checkUpdatedField", qc);
					return true;
				}
				// updated column in the ignore list - keep checking
				this.logger.logDebug(logIndex + " is in the exclusion list: " + exclusiveColumns.toString(),
									 "PerspectiumReplicator.checkUpdatedField", qc);
			}
			return false;
		} else { // select column updates to share on is selected
			for (var i = 0; i < changedFields.length; i++) {
				var logIndex = "Updated Column (" + (i + 1) + " / " + changedFields.length + "): " + changedFields[i];
				if (exclusiveColumns.indexOf(changedFields[i]) != -1) {
					// updated column in the share on list - replicate
					this.logger.logDebug(logIndex + " is in the share on list: " + exclusiveColumns.toString(),
										 "PerspectiumReplicator.checkUpdatedField", qc);
					return true;
				}
				// updated column not in the share on list - keep checking
					this.logger.logDebug(logIndex + " is NOT in the share on list: " + exclusiveColumns.toString(),
									 "PerspectiumReplicator.checkUpdatedField", qc);
			}
		}
		return false;
	},
	
	shareOneRecord : function(cgr, qc, sys_class_name, op, pspTag, pspAttributes, sumCounts) {
		try {
			this.logger.logDebug("shareOneRecord " + sys_class_name + " : " + qc.table_name + " " + cgr.sys_id + " " + op, "PerspectiumReplicator.shareOneRecord", qc);

			var filter = (typeof GlideFilter != 'undefined') ? GlideFilter : Packages.com.glide.script.Filter;
			if (!qc.condition.isNil() && !filter.checkRecord(cgr, qc.condition.toString())) {
				this.logger.logDebug(qc.condition.toString() + " not matched for sharing " + sys_class_name, "PerspectiumReplicator.shareOneRecord", qc);
				return;
			}

			if (!qc.u_before_share_script.nil()) {
				var jsonParams = {};
				jsonParams.psp_action = op;
				var ignore = this.psp.executeBeforeShareScript(qc.u_before_share_script, cgr, jsonParams);
				if (ignore == true || ignore == "true") {
					this.logger.logDebug("shareOneRecord ignored by beforeShareScript", "PerspectiumReplicator.shareOneRecord");
					return;
				}
				op = jsonParams.psp_action;
			}

			this.addShareCount(qc);

			var hasAudit = false;
			this.setQueueKey(qc);

			// if insert we'll want to do share attachments since we we won't share when called by script action
			// since we can't get record to check conditions when we do attachment before first inserting the record
			if (op == 'insert' && (qc.u_share_attachment == "true" || qc.u_share_attachment == true)) {
				this.shareAttachments(cgr.sys_id, qc, "bulk");
			}

			if (qc.u_share_journal == true && op != "delete") {
				this.shareJournalDelayed(cgr.sys_id, cgr.sys_updated_on, qc);
			}

			if ((qc.u_include_sys_audit == "true" || qc.u_include_sys_audit == true) && op != "delete") {
				// check if it has any audit messages
				// if it does we'll share them and set flag so message set psp_flag=last 
				// will be sent on last sys_audit message when audit messages sent in background job 1 second later
				var agr = new GlideRecord("sys_audit");
				agr.addQuery("tablename", sys_class_name);
				agr.addQuery("documentkey", cgr.sys_id);

				if (cgr.sys_updated_on != null && cgr.sys_updated_on != "" && !cgr.sys_updated_on.isNil()) {
					agr.addQuery("sys_created_on", ">=", cgr.sys_updated_on);
				}

				agr.orderByDesc("sys_created_on");
				agr.query();

				if(agr.getRowCount() > 0){	
					hasAudit = true;
					this.shareSysAuditDelayed(sys_class_name, cgr.sys_id, cgr.sys_updated_on, qc);
				}			
			}

			if (qc.u_share_referenced_field_recor == true && op != "delete") {
				this.shareReferencedFieldRecords(cgr, qc);
			}

			if ((qc.u_include_embedded_images_vide == "true" || qc.u_include_embedded_images_vide == true) && op != "delete") {
				this.shareEmbedded(cgr, cgr.sys_id, qc, "bulk");	
				// for embedded to work correctly 
				// (i.e. such as images referenced <img src="/sys_attachment.do?sys_id&#61;8e400b140f0b0a00a7f0c3ace1050eb8"/> 
				// we'll also want to share attachments for this record
				this.shareAttachments(cgr.sys_id, qc, "bulk");	

			}

			var enc = "";
			//var cipher=this.encryptionType.TRIPLE_DES; 
			var cipher = qc.u_cipher;
			var grTableMap;
			if (qc.u_share_base_table_only == true && op != "delete") {
				var ngr = new GlideRecord(qc.table_name);
				ngr.setWorkflow(false);

				ngr.addQuery('sys_id', cgr.sys_id);
				ngr.queryNoDomain();
				if (ngr.next()) {
					// Match any changes occured within the script
					if(!qc.u_before_share_script.nil()){
						for(var k in cgr){ 
							ngr[k] = cgr[k]; 
						}
					}
					if (qc.isValidField("u_table_map") && !qc.u_table_map.nil() && qc.u_conditional_share != true) {
						grTableMap = this.encryption.getOutboundTableMap(qc.u_table_map);	
						if(grTableMap.u_topic == "siam"){
							cipher = this.encryptionType.BASE64_ONLY;
							enc = this.encryption.encryptTableMap(ngr, grTableMap, cipher, pspTag);
						}
						else{
							cipher = qc.u_cipher;					
							enc = this.encryption.encryptTableMap(ngr, grTableMap, qc.u_cipher, pspTag);						
						}
					} else if (!qc.u_view_name.nil()) {
						enc = this.encryption.encrypt(ngr, qc.u_view_name, op, qc.u_target_queue, cipher);
					} else if (qc.u_share_only_selected_fields == "true" || qc.u_share_only_selected_fields == true){
						var pspSF = new PerspectiumShareFields();
						var recordXmlStr = pspSF.createShareRecordXML(qc, ngr, qc.table_name, this.fieldsToReplicate);
						enc = this.encryption.encryptString(recordXmlStr);
					} else {
						enc = this.encryption.encrypt(ngr, null, op, qc.u_target_queue, cipher);
					}
					sys_class_name = qc.table_name;
				}
			} else {
				if (qc.u_share_base_table_only == true && op == "delete") {
					sys_class_name = qc.table_name;
					var ngr = new GlideRecord(qc.table_name);
					for(var k in cgr) {
						ngr[k] = cgr[k];
					}

					cgr = ngr;
				}

				if (qc.isValidField("u_table_map") && !qc.u_table_map.nil() && qc.u_conditional_share != true){
					grTableMap = this.encryption.getOutboundTableMap(qc.u_table_map);	
					if(grTableMap.u_topic == "siam"){
						cipher = this.encryptionType.BASE64_ONLY;
						enc = this.encryption.encryptTableMap(cgr, grTableMap, "3", pspTag);
					}
					else{
						cipher = qc.u_cipher;
						enc = this.encryption.encryptTableMap(cgr, grTableMap, qc.u_cipher, pspTag);
					}
				} else if (!qc.u_view_name.nil()) {
					enc = this.encryption.encrypt(cgr, qc.u_view_name, op, qc.u_target_queue, cipher);
				} else if (qc.u_share_only_selected_fields == "true" || qc.u_share_only_selected_fields == true){
						var pspSF = new PerspectiumShareFields();
						var recordXmlStr = pspSF.createShareRecordXML(qc, cgr, sys_class_name, this.fieldsToReplicate);
						enc = this.encryption.encryptString(recordXmlStr);
				} else {
					enc = this.encryption.encrypt(cgr, null, op, qc.u_target_queue, cipher);
				}
			}

			// if the value is too large then we'll error
			// account for both if the value is bigger than our max sending size
			// as well as the largest size allowed for a String
			if (enc.length() > this.maxBytes || (enc.length() * 2) > 16000000) {					
				this.logger.logError(op + " message for " + sys_class_name + " <" + cgr.sys_id + "> cannot be created in outbound queue as it is larger than message limit", "PerspectiumReplicator.shareOneRecord");
				return;
			}

			//topic, type, key, name, value
			var topic = "replicator";
			var type = "servicenow";
			var name = sys_class_name;
			var attributes = "";

			if(pspAttributes != null && pspAttributes.length > 0){
				attributes = pspAttributes;
			}
			attributes = this.appendAttribute("cipher", cipher, attributes);
			if(typeof(grTableMap) != "undefined" && !grTableMap.isNil()){
				if(!grTableMap.u_topic.nil()){
					topic = grTableMap.u_topic;
				}
				if(!grTableMap.u_type.nil()){
					type = grTableMap.u_type;
				}
				if(!grTableMap.u_target_tablename.nil()){
					name = grTableMap.u_target_tablename;
				}
				attributes = this.appendTableMapAttributes(cgr, grTableMap, attributes);
				this.logger.logDebug("Attributes: " + attributes);
			}

			if (qc.u_update_or_insert == true && (op == "insert" || op == "update")) {
				name = name + ".bulk";
			}
			else
				name = name + "." + op;

			// add dynamic share configuration id in attributes for message set
			attributes = this.appendAttribute("set_id", qc.sys_id, attributes);	

			// also add psp_flag that this is last message if not audit messages sent
			if(!hasAudit){
				attributes = this.appendAttribute("psp_flag", "last", attributes);
			}

			if (qc.u_conditional_share == 'true' || qc.u_conditional_share ==  true) {
				var ctmgr = new GlideRecord('u_psp_conditional_share');
				ctmgr.addQuery('u_dynamic_share', qc.sys_id);
				ctmgr.query();
				while (ctmgr.next()) {				
					if (GlideFilter.checkRecord(cgr, ctmgr.u_condition.toString()) == false || GlideFilter.checkRecord(cgr, ctmgr.u_condition.toString()) == 'false') 
						continue;

					if (ctmgr.u_use_table_map == true || ctmgr.u_use_table_map == 'true') {
						grTableMap = this.encryption.getOutboundTableMap(ctmgr.u_table_map);
						if(grTableMap.u_topic == "siam"){
							topic = "siam";
							type = grTableMap.u_type;
							if (!grTableMap.u_target_tablename.nil()) {
								var recAction = name.substr(name.indexOf('.'));
								name = grTableMap.u_target_tablename + recAction;							
							}

							cipher = this.encryptionType.BASE64_ONLY;
							enc = this.encryption.encryptTableMap(cgr, grTableMap, cipher, pspTag);
						}
						else{
							cipher = qc.u_cipher;					
							enc = this.encryption.encryptTableMap(cgr, grTableMap, qc.u_cipher, pspTag);						
						}

						attributes = this.appendTableMapAttributes(cgr, grTableMap, attributes);
					}
					else if (!qc.u_view_name.nil()) 
						enc = this.encryption.encrypt(cgr, qc.u_view_name, op, ctmgr.u_target_queue, cipher);
					else 
						enc = this.encryption.encrypt(cgr, null, op, ctmgr.u_target_queue, cipher);

					this.createAndSendOutbounds(topic, type, this.getKey(qc), name, enc, ctmgr.u_target_queue, null, attributes, qc, sumCounts);
				}

				// bail out after this, we don't want to send duplicates
				return;
			}

			var extra = "";
			if (op == "deferred" && cgr.isValidField("number")) {
				extra = "number=" + cgr.number;
			}

			if (qc.u_receipt_alert_notification == true || qc.u_receipt_alert_notification == 'true') {
				// create delivery receipt
				var deliveryGR = new GlideRecord('u_psp_receipt_status');
				deliveryGR.u_record = cgr.sys_id;
				deliveryGR.u_record_table = cgr.getTableName();
				var recordId = deliveryGR.insert();

				var r = 'ReceiptStatusId=' + recordId;
				// append receipt id
				if (attributes) {
					attributes += ',' + r;
				} else {
					attributes = r;
				}
			}
			
			this.createAndSendOutbounds(topic, type, this.getKey(qc), name, enc, qc.u_target_queue, extra, attributes, qc, cgr.sys_id, sumCounts);

			if (!qc.u_after_share_script.nil()) {
				var jsonParams = {};
				jsonParams.psp_action = op;
				this.executeAfterShareScript(qc.u_after_share_script, cgr, qc, jsonParams);
			}			
		} catch (e) {
			if (String(e) == "java.lang.NullPointerException") {
				this.logger.logWarn("shareOneRecord failed due to invalid null characters, sys_id: " + cgr.sys_id + ", error = " + e, "PerspectiumReplicator.shareOneRecord", qc);
			}
			else {
				this.logger.logWarn("shareOneRecord " + sys_class_name + ", error = " + e, "PerspectiumReplicator.shareOneRecord", qc);
			}
		}
		
	},
	
	addShareCount: function (qc){
		var pspCountGR = new GlideRecord("u_psp_count");
		pspCountGR.u_source_table = qc.getTableName();
		pspCountGR.u_source = qc.sys_id;
		pspCountGR.u_value = "1";
		pspCountGR.insert();
	},
	
	executeAfterShareScript: function(script, gr, qc, jsonParams) {
		var gc = (typeof GlideController != 'undefined') ? GlideController : Packages.com.glide.script.GlideController;
		var oldCurrent = current;
        gc.putGlobal('current', gr);
		gc.putGlobal('dynamicshare_gr', qc);
		
		if(jsonParams != null){
			for(var param in jsonParams){
			   this.logger.logDebug("Adding After Share Script paramter " + param);
			   gc.putGlobal(param, jsonParams[param]);
			}			
		}
		
        var rc = gc.evaluateString(script);
		
		if(jsonParams != null){
			for(var param in jsonParams){
			   jsonParams[param] = gc.getGlobal(param);
			   gc.removeGlobal(param);
			}			
		}
		
        gc.removeGlobal('current');
		gc.removeGlobal('dynamicshare_gr');
		
		current = oldCurrent;		
	},
	
	createAndSendOutbounds : function (topic, type, key, name, enc, targetQueue, extra, attributes, qc, record_sys_id, sumCounts) {
		this.createPSPOut(topic, type, key, name, enc, targetQueue, extra, attributes, qc, record_sys_id);
		// send share counter data for this record
		var aSumCounts = false;
		if (sumCounts && sumCounts != null && sumCounts != "") {
			aSumCounts = sumCounts;
		}
		// increment counter for each message sent
		this.incrementMessageSetCounter(name);
		if (aSumCounts) {
			this.incrementTableCounter(name, enc.length());
		} 
		else {
			this.psp.sendCounter(name, 1, "counter");
			this.psp.sendCounter(name + ".bytes", enc.length(), "counter");
		}
	},
	
	shareReferencedFieldRecords : function(cgr, qc) {
		this.logger.logDebug("sharing referenced records for " + cgr.getTableName() + "." + cgr.sys_id, "PerspectiumReplicator.shareReferencedFieldRecords", qc);
		
		var kounter = {};
		var bytesKounter = {};
        
		var dgr = new GlideRecord("u_psp_dynamic_share_referenced");
		dgr.addQuery("u_dynamic_share", qc.sys_id);
		dgr.query();
		while(dgr.next()) {
		  // if referenced field does not contain a sys_id, do not send the record
		  if (cgr.getElement(dgr.u_reference_field).isNil()) {
				continue;
		  }
			
		  var refRecord = cgr.getElement(dgr.u_reference_field).getRefRecord();
		  var refTableName = refRecord.getTableName();
		  this.logger.logDebug("sharing referenced record " + refTableName + "." + refRecord.sys_id, "PerspectiumReplicator.shareReferencedFieldRecords", qc);
		  if (dgr.u_share_only_selected_fields == "true" || dgr.u_share_only_selected_fields == true){
              var pspSF = new PerspectiumShareFields();
              var ar = new GlideRecord("u_psp_share_field");

			  ar.addQuery("u_source", dgr.sys_id);
              ar.query();
              var referencedFields = [];
              while(ar.next()){
                 referencedFields.push(ar.u_field.toString());
              }
              var recordXmlStr = pspSF.createShareRecordXML(dgr, refRecord, refTableName, referencedFields);
              var enc = this.encryption.encryptString(recordXmlStr);
          }
          else 
		  	  var enc = this.encryption.encrypt(refRecord, "", "", "", qc.u_cipher);
			
		  // add dynamic share configuration id to attributes for message set 		
		  var bAttributes = this.appendAttribute("set_id", qc.sys_id, "");		
		  bAttributes = this.appendAttribute("cipher", qc.u_cipher, bAttributes);
	
		  // always bulk share ref records
		  if ( cgr.u_target_queue != null && cgr.u_target_queue != "" && !cgr.u_target_queue.isNil()) {			  
		    	//this.psp.createPSPOut("replicator", "servicenow", this.getKey(qc), refTableName + ".bulk", enc, null, null, null, qc);
				this.createPSPOut("replicator", "servicenow", this.getKey(qc), refTableName + ".bulk", enc, null, null, bAttributes, qc, refRecord.sys_id);
		  } else {
				//this.psp.createPSPOut("replicator", "servicenow", this.getKey(qc), refTableName + ".bulk", enc, qc.u_target_queue, null, null, qc);
				this.createPSPOut("replicator", "servicenow", this.getKey(qc), refTableName + ".bulk", enc, qc.u_target_queue, null, bAttributes, qc, refRecord.sys_id);
		  }
			
		  // track share by table
		  var valueBytes = enc.length();
		  if (kounter[refTableName]) {
				kounter[refTableName] ++;
				bytesKounter[refTableName] = bytesKounter[refTableName] + valueBytes;
		  } else {
				kounter[refTableName] = 1;
				bytesKounter[refTableName] = valueBytes;
		  }
		}
		
		// send share counter data for each table
		for(n in kounter) {			
			this.psp.sendCounter(n + ".bulk", kounter[n], "counter");
			this.psp.sendCounter(n + ".bulk.bytes", bytesKounter[n], "counter");

			// increment counter for each message sent
			this.incrementMessageSetCounter(n + ".bulk");			
		}
	},	

	shareAttachmentsDelayed: function(sys_id, grShareConfig, op, useTableMap, pspTag) {
		this.logger.logDebug("sharing attachments delayed for " + sys_id, "PerspectiumReplicator.shareAttachmentsDelayed", grShareConfig);
		var pspSO = new ScheduleOnce();		
		var gdt = new GlideDateTime();
		gdt.addSeconds(10); // schedule in 10 seconds to send out after record attachment is added to
		
		pspSO.setTime(gdt.getValue()); 
		
		pspSO.script += "var pspR = new PerspectiumReplicator();";		
		pspSO.script += "pspR.shareAttachments('" + sys_id + "', '', '" + op + "', " + useTableMap + ", '" + pspTag + "', '" + grShareConfig.sys_id + "');";
				
		pspSO.schedule();
	},
	
	// called from script action and shareRecords
	shareAttachments : function(sys_id, grShareConfig, op, useTableMap, pspTag, grShareConfigSysId) {
		if (grShareConfigSysId) {
			grShareConfig = new GlideRecord('psp_replicate_conf');
			grShareConfig.addQuery('sys_id', grShareConfigSysId);
			grShareConfig.queryNoDomain();
			if (!grShareConfig.next()) {
				this.logger.logDebug("no share definition found for " + grShareConfigSysId, "PerspectiumReplicator.shareAttachments");
				return // nothing can be done
			}
		}
				
		// to account for when shareAttachments() called from script action/custom ui actions directly
		// check if record passes filter conditions to continue sharing
		var cgr = new GlideRecord(grShareConfig.table_name);
		
		cgr.addQuery('sys_id', sys_id);
		
		// if a run as user selected we'll want to do a regular query()
		// to honor domain separation since people will only choose a run as user to get the results that user only sees
		if (grShareConfig.isValidField("u_run_as") && !grShareConfig.u_run_as.isNil()) {	   
			cgr.query();
		}
		else {
			cgr.queryNoDomain();
		}
		
		if (!cgr.next()) {	
			this.logger.logDebug("not sharing attachments immediately as " + sys_id + " not found in table " + grShareConfig.table_name, "PerspectiumReplicator.shareAttachments", grShareConfig);
			return;	
		}
						
		var filter = (typeof GlideFilter != 'undefined') ? GlideFilter : Packages.com.glide.script.Filter;
		// if this has a condition we'll need to get the actual record in order to check if it passes the condition
		if (!grShareConfig.condition.isNil() 
			// && cgr.next() 
			&& !filter.checkRecord(cgr, grShareConfig.condition.toString())) {
			this.logger.logDebug("skipping sharing attachments for share config " + grShareConfig.sys_id + " as " + grShareConfig.condition.toString() + " condition not met", "PerspectiumReplicator.shareAttachments", grShareConfig);
			return;
		}
		this.setQueueKey(grShareConfig);
		
		if (!grShareConfig.u_before_share_script.nil()) {
			var jsonParams = {};
			jsonParams.psp_action = op;
			var ignore = this.psp.executeBeforeShareScript(grShareConfig.u_before_share_script, cgr, jsonParams);
			if (ignore == true || ignore == "true") {
				this.logger.logDebug("skipping sharing attachments for " + grShareConfig.table_name + " <"+ sys_id + "> as record ignored by beforeShareScript in share config" + grShareConfig.sys_id, "PerspectiumReplicator.shareAttachments");
				return;
			}
		}
		
		// check if conditional share is enabled to share out to its queue
		if (grShareConfig.isValidField("u_conditional_share") && (grShareConfig.u_conditional_share == 'true' || grShareConfig.u_conditional_share ==  true)) {
			var ctmgr = new GlideRecord('u_psp_conditional_share');
			ctmgr.addQuery('u_dynamic_share', grShareConfig.sys_id);
			ctmgr.query();
			while (ctmgr.next()) {	
				// doesn't match this conditinal share so don't share out record
				if (GlideFilter.checkRecord(cgr, ctmgr.u_condition.toString()) == false || GlideFilter.checkRecord(cgr, ctmgr.u_condition.toString()) == 'false') {
					this.logger.logDebug("skipping sharing attachments for conditional share " + ctmgr.sys_id + ", share config " + grShareConfig.sys_id + " as " + ctmgr.u_condition.toString() + " condition not met", "PerspectiumReplicator.shareAttachments", grShareConfig);
					continue;
				}
				
				// share out attachments to this conditional share's queue
				this.shareRecordAttachments(sys_id, grShareConfig, op, ctmgr.u_target_queue, useTableMap, pspTag);
			}
			
			return;
		}
				
		this.shareRecordAttachments(sys_id, grShareConfig, op, grShareConfig.u_target_queue, useTableMap, pspTag);		
	},
	
	shareRecordAttachments: function(sys_id, grShareConfig, op, targetQueue, useTableMap, pspTag) {	
		// get the matching sys_attachment_doc for this dynamic share
		var dsgr;
		if (useTableMap) {
			dsgr = new GlideRecord('psp_replicate_conf');
			dsgr.addQuery('table_name', 'sys_attachment_doc');
			dsgr.addQuery('sync_direction', 'share');
			dsgr.addQuery('u_target_queue', grShareConfig.u_target_queue);
			dsgr.query();
			if (!dsgr.next()) {
				this.logger.logDebug("skipping sharing attachments for " + grShareConfig.table_name + " <"+ sys_id + "> as no sys_attachment_doc share config found for share " + grShareConfig.sys_id, "PerspectiumReplicator.shareRecordAttachments");
				return;
			}
			this.setQueueKey(grShareConfig);
			
			if (dsgr.u_table_map.nil() 
			   && !(dsgr.u_conditional_share == 'true' || dsgr.u_conditional_share ==  true)
			   ) {
				this.logger.logDebug("skipping sharing attachments for " + grShareConfig.table_name + " <"+ sys_id + "> as no table map set for sys_attachment_doc share config " + dsgr.sys_id, "PerspectiumReplicator.shareRecordAttachments");
				return;
			}
		}

		var saRecCount = 0;
		var saByteCount = 0;
		var sadRecCount = 0;
		var sadByteCount = 0;
		
		// first get the sys_attachment
		var jgr = new GlideRecord("sys_attachment");
		jgr.addQuery("table_sys_id", sys_id);
		jgr.query();
		while (jgr.next()) {
			if (useTableMap) {		
				// sent out already so we don't have to send again
				if (pspTag && this.pspUtil.recordHasTag(jgr, pspTag)){
					continue;	
				}
				
				var agr = new GlideRecord('sys_attachment_doc');
				agr.addQuery('sys_attachment', jgr.sys_id);
				agr.query();
				while (agr.next()) {
					this.shareRecord(agr, 'sys_attachment_doc', op, dsgr.sys_id);
				}
				
				// add tag so we don't send out in the future
				if (pspTag) {
					this.pspUtil.addTag(jgr, pspTag);
				} 
								
				continue;
			}
			
			var enc = this.encryption.encrypt(jgr, "", "", "", grShareConfig.u_cipher);
			// always send as upsert to get all entries, potentially problematic if a lot !!
			var name = "sys_attachment." + op;
			var target_queue = targetQueue;
									
			// add attributes with bulk share conf id to use for message set id
			var bAttributes = "";
			bAttributes = this.appendAttribute("set_id", grShareConfig.sys_id, bAttributes);
			bAttributes = this.appendAttribute("cipher", grShareConfig.u_cipher, bAttributes);

			this.logger.logDebug("sharing sys_attachment " + jgr.sys_id + " to queue " + target_queue, "PerspectiumReplicator.shareRecordAttachments", grShareConfig);			
			this.createPSPOut("replicator", "servicenow", this.getKey(grShareConfig), name, enc, target_queue, "", bAttributes, grShareConfig, jgr.sys_id);
			
			// increment counter for each message sent
			this.incrementMessageSetCounter(name);
					
			// increment counters
			saRecCount++;
			saByteCount += enc.length();
			
			// now for each sys_attachment, get the related sys_attachment_doc entries as well
			var dgr = new GlideRecord("sys_attachment_doc");
			dgr.addQuery("sys_attachment", jgr.sys_id);
			dgr.query();
			while(dgr.next()) {
				var enc = this.encryption.encrypt(dgr, "", "", "", grShareConfig.u_cipher);
				// always send as upsert to get all entries, potentially problematic if a lot !!
				var name = "sys_attachment_doc." + op;	
				
				this.createPSPOut("replicator", "servicenow", this.getKey(grShareConfig), name, enc, target_queue, "", bAttributes, grShareConfig, dgr.sys_id);
				
				// increment counter for each message sent
				this.incrementMessageSetCounter("sys_attachment_doc");
				
				// increment counters
				sadRecCount++;
				sadByteCount += enc.length();
			}
		}
		
		// send share counter data if we have one sys_attachment
		// since then we'll have byte data as well as at least one sys_attachment_doc record for it
		if (saRecCount > 0) {
			var sysAttachmentName = "sys_attachment." + op;
			var sysAttachmentDocName = "sys_attachment_doc." + op;
			
			this.psp.sendCounter(sysAttachmentName, saRecCount, "counter");
			this.psp.sendCounter(sysAttachmentName + ".bytes", saByteCount, "counter");
			this.psp.sendCounter(sysAttachmentDocName, sadRecCount, "counter");
			this.psp.sendCounter(sysAttachmentDocName + ".bytes", sadByteCount, "counter");
		}		
	},
		
	shareJournal : function(sys_id, sys_updated_on, grShareConfigSysId, isBulkShare) {
		var grShareConfig;
		if (isBulkShare) {
			grShareConfig = new GlideRecord('psp_bulk_share');
		}
		else {
			grShareConfig = new GlideRecord('psp_replicate_conf');
		}
		
		grShareConfig.addQuery('sys_id', grShareConfigSysId);
		grShareConfig.queryNoDomain();
		if(!grShareConfig.next()){
			this.logger.logDebug("no share definition found for " + grShareConfigSysId, "PerspectiumReplicator.shareJournal");
			return // nothing can be done
		}
		this.setQueueKey(grShareConfig);
		
		this.logger.logDebug("sharing journal for " + sys_id, "PerspectiumReplicator .shareJournal", grShareConfig);
		
		// add attributes with bulk share conf id to use for message set id
		var bAttributes = "";
		bAttributes = this.appendAttribute("set_id", grShareConfig.sys_id, bAttributes);
		bAttributes = this.appendAttribute("cipher", grShareConfig.u_cipher, bAttributes);
		
		var target_queue = grShareConfig.u_target_queue;
		
		var recCount = 0;
		var byteCount = 0;

		var name = "sys_journal_field.bulk";
		
		// only send journal entries for this session, not all of them
		var jgr = new GlideRecord("sys_journal_field");
		jgr.addQuery("element_id", sys_id);
		
		if (sys_updated_on != null && sys_updated_on != "" && !sys_updated_on.isNil()) {
			jgr.addQuery("sys_created_on", ">=", sys_updated_on);
			jgr.setLimit(this.dynSysJournalLimit);
		}
		
		jgr.orderByDesc("sys_created_on");
		jgr.query();
		while (jgr.next()) {
			var enc = this.encryption.encrypt(jgr, "", "", "", grShareConfig.u_cipher);	
			this.createPSPOut("replicator", "servicenow", this.getKey(grShareConfig), name, enc, target_queue, "", bAttributes, grShareConfig, jgr.sys_id);
			
			// increment counter for each message sent
			this.incrementMessageSetCounter(name);
			
			// increment counters
			recCount++;
			byteCount += enc.length();
			
		}
		
		// send share counter data
		if(recCount > 0){
			this.psp.sendCounter(name, recCount, "counter");
			this.psp.sendCounter(name + ".bytes", byteCount, "counter");
		}
	},
	
	shareJournalDelayed : function(sys_id, sys_updated_on, grShareConfig) {
		this.logger.logDebug("sharing sys_audit delayed for " + sys_id, "PerspectiumReplicator.shareJournalDelayed", grShareConfig);
		//var target_queue = grShareConfig.u_target_queue;
		var pspSO = new ScheduleOnce();		
		var gdt = new GlideDateTime();
		gdt.addSeconds(1); // schedule in 1 second to make sure all audits are in
		
		pspSO.setTime(gdt.getValue()); 
		
		pspSO.script += "var pspR = new PerspectiumReplicator();";
		// set flag false so we don't send first message and to flag last message which will be a sys_audit message
		pspSO.script += "pspR.sentFirstMessage = true;";
		pspSO.script += "pspR.flagLastMessage = true;";
		
		pspSO.script += "pspR.shareJournal('" + sys_id + "','" + sys_updated_on + "','" + grShareConfig.sys_id + "');";
				
		pspSO.schedule();
	},
	
	shareSysAuditDelayed : function(table_name, sys_id, sys_updated_on, grShareConfig) {
		this.logger.logDebug("sharing sys_audit delayed for " + sys_id, "PerspectiumReplicator.shareSysAuditDelayed", grShareConfig);
		//var target_queue = grShareConfig.u_target_queue;
		var pspSO = new ScheduleOnce();		
		var gdt = new GlideDateTime();
		gdt.addSeconds(1); // schedule in 1 second to make sure all audits are in
		
		pspSO.setTime(gdt.getValue()); 
		
		pspSO.script += "var pspR = new PerspectiumReplicator();";
		// set flag false so we don't send first message and to flag last message which will be a sys_audit message
		pspSO.script += "pspR.sentFirstMessage = true;";
		pspSO.script += "pspR.flagLastMessage = true;";
		
		//pspSO.script += "pspR.shareSysAudit('" + table_name + "','" + sys_id + "','" + sys_updated_on + "','" + target_queue + "','" + this.getKey(grShareConfig) + "');";
		pspSO.script += "pspR.shareSysAuditDynamic('" + table_name + "','" + sys_id + "','" + sys_updated_on + "','" + grShareConfig.sys_id + "');";
				
		pspSO.schedule();
	},
	
	// background job created to share sys_audit records for a dynamic share
	shareSysAuditDynamic : function(table_name, sys_id, sys_updated_on, grShareConfigSysId){
		// get dynamic share configuration by sys_id to call shareSysAudit() to actually share sys_audit records
		var grShareConfig = new GlideRecord('psp_replicate_conf');
        	
		grShareConfig.addQuery('sys_id', grShareConfigSysId);
		grShareConfig.queryNoDomain();
		if(!grShareConfig.next()){
		//if (!grShareConfig.get(grShareConfigSysId)) {
			this.logger.logDebug("no share definition found for " + table_name + " " + grShareConfigSysId, "PerspectiumReplicator.shareSysAuditDynamic");
			return // nothing can be done
		}
		this.setQueueKey(grShareConfig);
		
		// since dynamically shared sys_audit records are done separate from dynamic share and its records
		// we'll send message set message for it separately
		var startedDateTime = gs.nowDateTime();
						
		this.shareSysAudit(table_name, sys_id, sys_updated_on, grShareConfig);

		var finishedDateTime = gs.nowDateTime();

		// for each share configuration, send message set 
		this.pspMS.createMessageSetProcessed(this.messageSetCounter, grShareConfig, this.getKey(grShareConfig), startedDateTime, finishedDateTime);		
	},
	
	shareSysAudit : function(table_name, sys_id, sys_updated_on, grShareConfig) {		
		this.logger.logDebug("sharing sys_audit for " + sys_id + ", " + table_name, "PerspectiumReplicator.shareSysAudit", grShareConfig);
		
		var target_queue = grShareConfig.u_target_queue;
		var target_key = this.getKey(grShareConfig);
		
		// add attributes with bulk share conf id to use for message set id
		var bAttributes = "";
		bAttributes = this.appendAttribute("set_id", grShareConfig.sys_id, bAttributes);
		bAttributes = this.appendAttribute("cipher", grShareConfig.u_cipher, bAttributes);
		
		var name = "sys_audit.bulk";
		var recCount = 0;
		var byteCount = 0;
		
		// only send journal entries for this session, not all of them
		var jgr = new GlideRecord("sys_audit");
		jgr.addQuery("tablename", table_name);
		jgr.addQuery("documentkey", sys_id);		
		
		if (sys_updated_on != null && sys_updated_on != "" && !sys_updated_on.isNil()) {
			jgr.addQuery("sys_created_on", ">=", sys_updated_on);
			jgr.setLimit(this.dynSysAuditLimit);
		}
		
		jgr.orderByDesc("sys_created_on");
		jgr.query();
		
		// save # of rows returned to flag last message in message set for dynamic share
		var auditCount = jgr.getRowCount();
		
		while (jgr.next()) {
			var enc = this.encryption.encrypt(jgr, "", "", "", grShareConfig.u_cipher);
			
			// last audit record we'll want set psp_flag=last if flag set
			if(recCount == (auditCount - 1) && this.flagLastMessage){
				bAttributes = this.appendAttribute("psp_flag", "last", bAttributes);
			}
			
			this.createPSPOut("replicator", "servicenow", target_key, name, enc, target_queue, "", bAttributes, grShareConfig, jgr.sys_id);
			
			// increment counter for each message sent
			this.incrementMessageSetCounter(name);
						
			// increment counters
			recCount++;
			byteCount += enc.length();
			
		}
		
		// send share counter data
		if(recCount > 0){
			this.psp.sendCounter(name, recCount, "counter");
			this.psp.sendCounter(name + ".bytes", byteCount, "counter");
		}
	},
	
	//helpder function for pausing the loop  
	pauseLoop : function(ms) {  
		ms += new Date().getTime();  
		while (new Date() < ms) {}  
	},  
	
	// the entire set is always shared
	shareHistorySet : function(table_name, sys_id, sys_updated_on, grShareConfig) {
		this.logger.logDebug("sharing history set for " + sys_id + "," + table_name, "PerspectiumReplicator.shareHistorySet", grShareConfig);

		var target_queue = grShareConfig.u_target_queue;
		
		// add attributes with bulk share conf id to use for message set id
		var bAttributes = "";
		bAttributes = this.appendAttribute("set_id", grShareConfig.sys_id, bAttributes);
		bAttributes = this.appendAttribute("cipher", grShareConfig.u_cipher, bAttributes);

		var jgr = new GlideRecord("sys_history_set");
		jgr.addQuery("table", table_name);
		jgr.addQuery("id", sys_id);
		jgr.query();
		// there is only one
		if (!jgr.next()) {
			this.logger.logDebug("ERROR - no history set for " + sys_id, "PerspectiumReplicator.shareHistorySet", grShareConfig);
			return;
		}
	
		var enc = this.encryption.encrypt(jgr, "", "", "", grShareConfig.u_cipher);
		var name = "sys_history_set.bulk";
		this.createPSPOut("replicator", "servicenow", this.key, name, enc, target_queue, "", bAttributes, grShareConfig, jgr.sys_id);
		
		// increment counter for each message sent
		this.incrementMessageSetCounter(name);
		
		// send share counter data
		this.psp.sendCounter(name, 1, "counter");
		this.psp.sendCounter(name + ".bytes", enc.length(), "counter");

		// now share history line(s)
		var lgr = new GlideRecord("sys_history_line");
		lgr.addQuery("set", jgr.sys_id);
		if (sys_updated_on != null && sys_updated_on != "" && !sys_updated_on.isNil()) {
			lgr.addQuery("sys_created_on", ">=", sys_updated_on);
		}
		lgr.query();
		
		var recCount = 0;
		var byteCount = 0;
		while(lgr.next()) {
			enc = this.encryption.encrypt(lgr, "", "", "", grShareConfig.u_cipher);
			name = "sys_history_line.bulk";
			this.createPSPOut("replicator", "servicenow", this.key, name, enc, target_queue, "", bAttributes, grShareConfig, lgr.sys_id);
			
			
			// increment counter for each message sent
			this.incrementMessageSetCounter(name);			
			
			// increment counters
			recCount++;
			byteCount += enc.length();
		}
		
		// send share counter data
		if(recCount > 0){
			this.psp.sendCounter(name, recCount, "counter");
			this.psp.sendCounter(name + ".bytes", byteCount, "counter");
		}
	},
	
	// to share embedded images and videos stored in db_image and db_video
	shareEmbedded : function(grParam, sys_id, grShareConfig, op) {
		this.logger.logDebug("sharing embedded for " + sys_id, "PerspectiumReplicator.shareEmbedded", grShareConfig);
		
		// go through fields and see if any are html type
		var fields = grParam.getFields();
		for (i=0; i<fields.size(); i++) { 
			var field = fields.get(i);
			var descriptor = field.getED(); 			
			var internalType = descriptor.getInternalType();
			
			// check either field is a html type
			if(internalType && internalType != '' && internalType.toLowerCase().indexOf('html') >= 0){			
		    	this.logger.logDebug("checking embedded images/videos for field: type " + internalType + ", name " + field.getName() + " for table " + grParam.getTableName() + " sys_id " +  sys_id, "PerspectiumReplicator.shareEmbedded", grShareConfig);
				this.findEmbeddedElements(field, grShareConfig, op);								
			}
		}
	},
	
	findEmbeddedElements: function(geParam, grShareConfig, op){
		if(!geParam.hasValue())
			return;

		// get field's value and use XML document to parse through to find <img>				
		var fieldValue = geParam.getHTMLValue();
		// get escaped value if it's encoded
		if(fieldValue.indexOf('&lt;') >= 0){
			fieldValue = geParam.getEscapedValue();
		}
				
		var xml_util = (typeof GlideXMLUtil != 'undefined') ? new GlideXMLUtil() : new Packages.com.glide.util.XMLUtil();
		//var xmlDoc = xml_util.parseHTML(geParam.getHTMLValue());
		var xmlDoc = xml_util.parseHTML(fieldValue);
		
        if (!xmlDoc) {
            this.logger.logWarn("deserializing: XML unparseable " + fieldValue, "PerspectiumReplicator.findEmbeddedElements", grShareConfig);
            return;
        }
		
		var root = xmlDoc.getDocumentElement();
		
		// start with root and iterate through to find img elements
		this.findEmbeddedElement(root, xml_util, grShareConfig, op);			
	},
	
	findEmbeddedElement: function(element, xml_util, grShareConfig, op){
		var it = xml_util.childElementIterator(element);
        while (it.hasNext()) {
            var el = it.next();
            
            var n = el.getNodeName();
            var v = xml_util.getText(el);
			if (v != null)
				v += "";
            
			//this.logger.logDebug(n + ", " + v, "Perspectium.findEmbeddedElement", grShareConfig);
			
			// found img now try to share it if we can get its name from its src attribute
			if(n == "img"){
				var imageName = xml_util.getAttribute(el, "src");
				if(imageName != null){
					// to make sure it's a string returned add + ""
					this.shareDBMedia(imageName + "", grShareConfig, op, "db_image");
				}
			}
			// video can be either in object or embed object depending on version of TinyMCE used to embed
			// object is more common with TinyMCE in Calgary
			else if(n == "object"){
				var videoName = xml_util.getAttribute(el, "data");
				if(videoName != null){
					// to make sure it's a string returned add + ""
					this.shareDBMedia(videoName + "", grShareConfig, op, "db_video");
				}
			}
			// embed and source more common with TinyMCE in Eureka
			else if(n == "embed" || n == "source"){
				var videoName = xml_util.getAttribute(el, "src");
				if(videoName != null){
					// to make sure it's a string returned add + ""
					this.shareDBMedia(videoName + "", grShareConfig, op, "db_video");
				}
			}			
			
			// go through this element's children
			this.findEmbeddedElement(el, xml_util, grShareConfig, op);        
		}		
	},

	shareDBMedia: function(mediaFilename, grShareConfig, op, dbTableName){
		mediaFilename = this.cleanEmbeddedFilename(mediaFilename);
		
		// for ones with sys_attachment the filename is something like sys_attachment.do?sys_id=adb7ee106f93c200dbf856b21c3ee401 
		// so we pass the sys_id listed in the filename to share
		// for when user is sharing an embedded image that is not attached to the record (such as one that already exists in the system)
		if(mediaFilename.indexOf("sys_attachment") >= 0){			
			// convert to = if &#61; i.e. sys_attachment.do?sys_id&#61;a255dd140fc70a00a7f0c3ace1050e6e
			mediaFilename = mediaFilename.replace(/&#61;/g, "=");
			
			// get sys_id part only
			mediaFilename = mediaFilename.substring(mediaFilename.indexOf("sys_id=") + "sys_id=".length);
			
			// disregard any & after for any other parameters i.e. sys_attachment.do?sys_id=59e6a59c49cb420082f2053890c13db2&amp;view=true
			if(mediaFilename.indexOf("&") > 0){
				mediaFilename = mediaFilename.substring(0, mediaFilename.indexOf("&"));	
			}
				
			this.shareOneAttachment(mediaFilename, grShareConfig, op);
			return;
		}
		
		this.logger.logDebug("finding media with filename " + mediaFilename, "PerspectiumReplicator.shareDBMedia", grShareConfig);
		var recCount = 0;
		var byteCount = 0;
		var name = dbTableName + "." + op;

		// query to find record with this name and share
		var dr = new GlideRecord(dbTableName);
		dr.addQuery('name', mediaFilename);
		dr.query();
		while (dr.next()) {
			var drSysId = dr.sys_id;
			this.logger.logDebug("sharing " + dbTableName + " " + drSysId, "PerspectiumReplicator.shareDBMedia", grShareConfig);
			
			var enc = this.encryption.encrypt(dr, "", "", "", grShareConfig.u_cipher);
			
			// add attributes with bulk share conf id to use for message set id
			var bAttributes = "";
			bAttributes = this.appendAttribute("set_id", grShareConfig.sys_id, bAttributes);
			bAttributes = this.appendAttribute("cipher", grShareConfig.u_cipher, bAttributes);
			
			var target_queue = grShareConfig.u_target_queue;
			/*if (target_queue == null || target_queue == "" || target_queue.isNil()) {
				this.psp.createPSPOut("replicator", "servicenow", this.getKey(grShareConfig), name, enc);
			} else {*/
				this.createPSPOut("replicator", "servicenow", this.getKey(grShareConfig), name, enc, target_queue, "", bAttributes, grShareConfig, dr.sys_id);
			//}
			
			// increment counter for each message sent
			this.incrementMessageSetCounter(name);
			
			// increment counters
			recCount++;
			byteCount += enc.length();
			
			// next share sys_attachment and sys_attachment_doc for this db_image
			this.shareAttachments(drSysId, grShareConfig, op);
			
		}
		
		// send share counter data
		if(recCount > 0){
			this.psp.sendCounter(name, recCount, "counter");
			this.psp.sendCounter(name + ".bytes", byteCount, "counter");
		}
	},
	
	cleanEmbeddedFilename: function(embeddedName){
		// if name begins with / or ends with "x" i.e. image.pngx		
		if(embeddedName.indexOf("/") == 0){
			embeddedName = embeddedName.substring(1);
		}
		
		if(this.psp.endsWith(embeddedName, "x")){
			embeddedName = embeddedName.substring(0, embeddedName.length - 1);
		}
					
		// replace with space to match stored in db
		embeddedName = embeddedName.replace(/%20/g, " ");
		
		return embeddedName;
	},
	
	// called from shareDBMedia for sharing embedded elements
	// passing in sys_attachment sys_id
	// for when user is sharing an embedded image that is not attached to the record (such as one that already exists in the system)
	shareOneAttachment : function(sys_id, grShareConfig, op) {
		this.logger.logDebug("sharing sys_attachment " + sys_id, "PerspectiumReplicator.shareOneAttachment", grShareConfig);
		
		var saRecCount = 0;
		var saByteCount = 0;
		var sadRecCount = 0;
		var sadByteCount = 0;
		// first get the sys_attachment
		var jgr = new GlideRecord("sys_attachment");
		
		jgr.addQuery('sys_id', sys_id);
		jgr.query();
		if(!jgr.next()){	
		//if(!jgr.get(sys_id)){
			this.logger.logDebug("no record found in sys_attachment for sys_id " + sys_id, "PerspectiumReplicator.shareOneAttachment", grShareConfig);
			return;			
		}
			
		var enc = this.encryption.encrypt(jgr, "", "", "", grShareConfig.u_cipher);
		// always send as upsert to get all entries, potentially problematic if a lot !!
		var name = "sys_attachment." + op;
						
		// add attributes with bulk share conf id to use for message set id
		var bAttributes = "";
		bAttributes = this.appendAttribute("set_id", grShareConfig.sys_id, bAttributes);
		bAttributes = this.appendAttribute("cipher", grShareConfig.u_cipher, bAttributes);
		
		var target_queue = grShareConfig.u_target_queue;
		this.createPSPOut("replicator", "servicenow", this.getKey(grShareConfig), name, enc, target_queue, "", bAttributes, grShareConfig, jgr.sys_id);
					
		// increment counter for each message sent
		this.incrementMessageSetCounter(name);
		
		// increment counters
		saRecCount++;
		saByteCount += enc.length();
			
		// now get the related sys_attachment_doc entries as well
		var dgr = new GlideRecord("sys_attachment_doc");
		dgr.addQuery("sys_attachment", jgr.sys_id);
		dgr.query();
		while(dgr.next()) {
			var enc = this.encryption.encrypt(dgr, "", "", "", grShareConfig.u_cipher);
			// always send as upsert to get all entries, potentially problematic if a lot !!
			var name = "sys_attachment_doc." + op;
			this.createPSPOut("replicator", "servicenow", this.getKey(grShareConfig), name, enc, target_queue, "", bAttributes, grShareConfig, dgr.sys_id);
						
			// increment counter for each message sent
			this.incrementMessageSetCounter(name);
			
			// increment counters
			sadRecCount++;
			sadByteCount += enc.length();
		}
				
		// send share counter data if we have one sys_attachment
		// since then we'll have byte data as well as at least one sys_attachment_doc record for it
		if(saRecCount > 0){
			var sysAttachmentName = "sys_attachment." + op;
			var sysAttachmentDocName = "sys_attachment_doc." + op;
			
			this.psp.sendCounter(sysAttachmentName, saRecCount, "counter");
			this.psp.sendCounter(sysAttachmentName + ".bytes", saByteCount, "counter");
			this.psp.sendCounter(sysAttachmentDocName, sadRecCount, "counter");
			this.psp.sendCounter(sysAttachmentDocName + ".bytes", sadByteCount, "counter");
		}
	},
	
    fetchFromDefaultQueue: function() {
		var encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
		var queueName = "psp.out.servicenow." + this.key;
		var endpoint = this.psp.getQInputURL();
		var quser = this.psp.getQUser();
		var qpassword = this.psp.getQPassword();
		var instance = this.key;
		
		this.fetchFromOneQueue(endpoint, queueName, quser, qpassword, instance);
	},
	
    fetchFromQueue: function() {
		var encrypter = (typeof GlideEncrypter != 'undefined') ? new GlideEncrypter() : new Packages.com.glide.util.Encrypter();
		var qGR = new GlideRecord("u_psp_queues");
		qGR.addQuery("u_active", "true");
		qGR.addQuery("u_direction", "Subscribe");
		qGR.addQuery("u_ets", "false");
        qGR.orderBy("u_order");
		qGR.query();
		while(qGR.next()) {
	    	try{
	    		var p = encrypter.decrypt(qGR.u_queue_password);
				// pass subscribed queue GlideRecord object itself to use for messageset
	    		this.fetchFromOneQueue(qGR.u_endpoint_url, qGR.u_name, qGR.u_queue_user, p, qGR.u_instance, qGR);
			}
			catch(e) {
				this.logger.logError("Error fetching from queue " + qGR.u_name + " on " + qGR.u_endpoint_url + ": " + e, "PerspectiumReplicator.fetchFromQueue");
			}
		}
    },
    
    fetchFromOneQueue: function(endpoint, queueName, quser, qpassword, instance, subscribedQueue) {
		if (!subscribedQueue.u_instance_created_on.isNil() && subscribedQueue.u_instance_created_on != this.key) {
    		subscribedQueue.u_active = 'false'; 
			subscribedQueue.update();
    		this.logger.logWarn("The queue " + subscribedQueue.u_name + " was deactivated to protect against cloning", "PerspectiumReplicator.fetchFromOneQueue");
			return;
    	}
		
		if (quser == "" || qpassword == "" || endpoint == "") {
			this.logger.logError("Unable to run as the target server or credentials are not configured properly. Please check your Perspectium properties", "PerspectiumReplicator.fetchFromOneQueue");
			return;	
		}
		
		if (!this.endsWith(endpoint, "/")) {
			endpoint += "/";
		}
		
		this.logger.logDebug(endpoint + " " + queueName + " " + quser + " " + instance, "PerspectiumReplicator.fetchFromOneQueue");
		
		var sdt = new Date();
		var pspS = new Perspectium();
        
		var url = endpoint + "output/" + queueName;
        var getMethod = new Packages.org.apache.commons.httpclient.methods.GetMethod(url);
        getMethod.setRequestHeader("psp_quser", quser);
        getMethod.setRequestHeader("psp_qpassword", qpassword);
        getMethod.setRequestHeader("psp_instance", instance);
        var httpClient = this.psp.getHttpClient();
        var result = httpClient.executeMethod(getMethod);
        
        if (result != 200) {
			getMethod.releaseConnection();
            throw new Error("HTTP GET failed: " + result + " for URL: " + url);
        }
        
        var answer = getMethod.getResponseBodyAsString();
		// release connection after fetch
		getMethod.releaseConnection();
        
		var kounter = {};
		var bytesKounter = {};
        var json = new JSON();
        var obj = json.decode(answer);
		
		this.logger.logDebug(obj.length + ' records being processed for ' + queueName, "PerspectiumReplicator.fetchFromOneQueue");
		var csecs = pspS._getTimeDiffSecs(sdt);
		var rsecs = obj.length/csecs;		
        this.logger.logDebug(obj.length + " records (" + queueName + ") COLLECTED in " + csecs + " secs (" + rsecs.toFixed(2) + " recs/s)", "PerspectiumReplicator.fetchFromOneQueue");
		this.psp.addMessageStats(obj.length + " inbound records COLLECTED from " + queueName, csecs, this.messageStatsMod, '', "COLLECTED", obj.length);
		
		sdt = new Date();
		var numRecords = 0;
		
		// create new messageset object to reset and hold all the messageset messages for this group of mesages
		var pspMS2 = new PerspectiumMessageSet();
		var recTypes = [];
        for(var i = 0; i < obj.length; i++) {			
            var topic = obj[i].topic;
            var type = obj[i].type;
            var key = obj[i].key;
            var name = obj[i].name;
            var value = obj[i].value;
			var description = obj[i].description;
			var timestamp = obj[i].psp_timestamp;
			var extra = obj[i].extra;
			var priority = obj[i].priority;
			var attributes = obj[i].attributes;            
            recTypes.push(obj[i].name.toString());
            var igr = new GlideRecord("psp_in_message");
            igr.newRecord();
            igr.topic = topic;
			
			if (topic === "replicator") {
				numRecords += 1;
			}
			
            igr.type = type;
            igr.key = key;
            igr.name = name;
            igr.value = value;
			igr.u_description = description;
			var gmt = new GlideDateTime(timestamp);
			igr.u_timestamp = gmt.getDisplayValue();
			igr.u_extra = extra;
			igr.u_priority = priority;
			igr.u_attributes = attributes;
			if(subscribedQueue && subscribedQueue != null && subscribedQueue != "") {
				igr.u_subscribed_queue = subscribedQueue.sys_id;
			}
			
			var messageState = "ready";
			try {		
				if (pspS.processPSPInMessage(igr)) {
    				messageState = "received";	
					igr.u_target_record = pspS.recordSysId;
                    igr.u_target_table = pspS.recordTargetTable;
	    		} else if(igr.state != "error") {
					messageState = "skipped";
	    		} else if (igr.state == "error") {
					messageState = "error";
				}
			} catch (e) {
				messageState = "error";
				igr.u_state_info = e;
				this.logger.logWarn("error: " + e, "PerspectiumReplicator.fetchFromOneQueue");
			} finally {
				igr.state = messageState;
				igr.insert();
				
				pspS.recordSysId = '';
				pspS.recordTargetTable = '';
				
				// handle counters and messageset for replicator messages being subscribed (not errors coming back to show in outbound)
				if (topic == "replicator" && type != "error") {
					
					// only update counters if message was received and processed
					if(messageState == "received"){
						// get # of bytes for value field
						var valueBytes;
						// try to get from decoded value field first
						if(pspS.decodeData != null && pspS.decodeData != '')
							valueBytes = pspS.decodeData.length(); // dloo
						// otherwise use encrypted value field for # of bytes
						else
							valueBytes = value.length(); // dloo
												
						if (kounter[name]) {
							kounter[name] ++;
							bytesKounter[name] = bytesKounter[name] + valueBytes;
						} else {
							kounter[name] = 1;
							bytesKounter[name] = valueBytes;
						}
					}
					
					// handle messageset for this message if we have a set_id
					var pspMsg = new PerspectiumMessage(topic, type, key, name, value, "", extra, attributes, "", "");
					var messageSetId = pspMsg.getAttribute("set_id");
					this.logger.logDebug("set_id: " + messageSetId, "PerspectiumReplicator.fetchFromOneQueue");
					if(messageSetId == null){
						continue;	
					}
					
					// increment count based on state of message
					pspMS2.incrementMessageSetCount(messageSetId, messageState);
					
					var messagePspFlag = pspMsg.getAttribute("psp_flag");
					if(messagePspFlag == null){
						continue;	
					}
					
					// update if we received first or last message
					if(messagePspFlag.indexOf("first") >= 0){
						pspMS2.setMessageSetFirst(messageSetId);
					}
					
					if(messagePspFlag.indexOf("last") >= 0){
						pspMS2.setMessageSetLast(messageSetId);
					}
					
				}
			}
        }
		
		csecs = pspS._getTimeDiffSecs(sdt);
		rsecs = obj.length/csecs;	
        this.logger.logDebug(obj.length + " records (" + queueName + ") PROCESSED in " + csecs + " secs (" + rsecs.toFixed(2) + " recs/s)", "PerspectiumReplicator.fetchFromOneQueue");
		this.psp.addMessageStats(obj.length + " inbound records PROCESSED from " + queueName, csecs, this.messageStatsMod, recTypes.toString(), "PROCESSED", numRecords);
		
		// insert all history set into table for scheduled job to process
		pspS.pspRHS.insertIntoTable();
		
		for(n in kounter) {
			pspS.sendOutboundCounter(n, kounter[n]);
			// send bytes as well
			pspS.sendOutboundCounter(n + ".bytes", bytesKounter[n]);			
		}
		
		// send out messageset messages for all replicator messages processed in this job
		if(subscribedQueue && subscribedQueue != null && subscribedQueue != ""){
			pspMS2.createMessageSetMessages(this.key, subscribedQueue, "snc-subscribed");
		}
		else{
			pspMS2.createMessageSetMessages(this.key, "", "snc-subscribed");			
		}
    },
	
	createListener : function(table_name, rgr) {
		this.createSysAuditDeleteListener(table_name, rgr);
	},
	
	createSysAuditDeleteListener : function(table_name, rgr) {
		var grBR = new GlideRecord("sys_script");
		grBR.addQuery("collection", "sys_audit_delete");
		grBR.addQuery("name", "Perspectium DL - " + table_name);
		// to make sure we find right one look for the one with the dynamic share's sys_id in its script
		grBR.addQuery("script", "CONTAINS", rgr.sys_id);			
		grBR.query();
		if(!grBR.next()) {
			this.logger.logDebug("Creating Perspectium Delete Listener for " + table_name, "PerspectiumReplicator.createSysAuditDeleteListener");
			grBR.name = "Perspectium DL - " + table_name;
			grBR.collection = "sys_audit_delete";
			grBR.script = "var psp = new Perspectium();var pspR = new PerspectiumReplicator();var egr = psp.deserialize(current.tablename, current.payload.toString());pspR.shareRecord(egr, '" + table_name + "', 'delete', '" + rgr.sys_id + "');";
			grBR.when = "after";
			grBR.order = "50";
			grBR.advanced = "true";
			grBR.execute_function = "false";
			grBR.active = "true";
			grBR.action_insert = "true";
			grBR.sys_domain = "global";
			grBR.condition = "current.tablename == '" + table_name + "' || (new PerspectiumUtil().isChildTable(current.tablename, '" + table_name + "'))";	
			
			grBR.setWorkflow(false);
			grBR.insert();
		} else {
			this.logger.logDebug("Perspectium Delete Listener already exists for " + table_name, "PerspectiumReplicator.createSysAuditDeleteListener");
		}
	},
	
	deleteListener : function(table_name, rgr) {
		this.deleteSysAuditDeleteListener(table_name, rgr);
	},
	
	deleteSysAuditDeleteListener : function(table_name, rgr) {
		this.logger.logDebug("deleting SysAuditDeleteListener for " + table_name, "PerspectiumReplicator.deleteSysAuditDeleteListener");
		var grBR = new GlideRecord("sys_script");
		grBR.addQuery("collection", "sys_audit_delete");
		grBR.addQuery("name", "Perspectium DL - " + table_name);
		// to make sure we find right one look for the one with the dynamic share's sys_id in its script
		grBR.addQuery("script", "CONTAINS", rgr.sys_id);					
		grBR.deleteMultiple();
	},
	
	updateReplicatorBusinessRule : function(rgr, createNew) {
		this.logger.logDebug(rgr.table_name, "PerspectiumReplicator .updateReplicatorBusinessRule", rgr);
		
		if (rgr.u_use_listener == true && rgr.action_delete == true && rgr.active == true) {
			this.logger.logDebug("creating delete listener for " + rgr.table_name, "PerspectiumReplicator.updateReplicatorBusinessRule", rgr);
		    this.createListener(rgr.table_name, rgr);
		} else {
		    this.deleteListener(rgr.table_name, rgr);
		}
		
		// flag if we should not update and always create a new business rule
		var createNewBR = false;
		if(typeof createNew != 'undefined' && createNew != null)
			createNewBR = createNew;
		
		//Flip the active flag on the appropriate Business Rule
		var grBR = new GlideRecord("sys_script");
		grBR.addQuery("collection", rgr.table_name);
		grBR.addQuery("name", "Perspectium Replicate");
		
		// use the business rule referenced in share config
		if (rgr.u_business_rule != null && rgr.u_business_rule != "" && !rgr.u_business_rule.isNil()) {
			grBR.addQuery("sys_id", rgr.u_business_rule);
		}
		grBR.query();
		
		// for async we'll always send as bulk since we can't tell if record is update or insert
		var grOperation = "";
		if (rgr.u_business_rule_when == "async") {
			grOperation = "bulk";
		}
		
		// Build the condition string
		var replicateCondition = "current.operation() != null && PerspectiumReplicator .isReplicatedTable('" + rgr.table_name + "', 'share', current.operation().toString())";
		if (rgr.u_interactive_only){
			replicateCondition += " && gs.isInteractive()";
		}
		if (!rgr.u_run_on_subscribe){
			replicateCondition += " && current.psp_subscribed_record != true";
		}
		
		var replicateScript = "var pspR = new PerspectiumReplicator();";
		 //replicateScript += "var currentCopy = new GlideRecord(current.getTableName());var grUtil = new GlideRecordUtil();grUtil.mergeToGR(current, currentCopy);"
		 //replicateScript += "pspR.shareRecord(currentCopy, '" + rgr.table_name + "', current.operation(), '" + rgr.sys_id + "');";
		replicateScript += "pspR.shareRecord(current, '" + rgr.table_name + "', '" + grOperation + "', '" + rgr.sys_id + "');";
		
		if(grBR.next() && !createNewBR) {
			this.logger.logDebug("updateReplicatorBusinessRule updating", "PerspectiumReplicator.updateReplicatorBusinessRule", rgr);
			grBR.active = rgr.active;
			grBR.action_delete = rgr.action_delete;
			grBR.action_insert = rgr.action_create;
			grBR.action_update = rgr.action_update;
		
			if ( rgr.u_business_rule_order > 0 ) {
				grBR.order = rgr.u_business_rule_order;
			}

            grBR.condition = replicateCondition;
			grBR.script = replicateScript;
			// pass share sys_id so we can reference it when sharing record
			// to support multiple share configurations with different business order rule and when for the same table
				
			// always set when to run business rule based on share config as default value on share config form is "after"
			grBR.when = rgr.u_business_rule_when;
			
			grBR.setWorkflow(false);
			grBR.update();			
		} else {
			this.logger.logDebug("updateReplicatorBusinessRule inserting", "PerspectiumReplicator.updateReplicatorBusinessRule", rgr);
			grBR = new GlideRecord("sys_script");
			grBR.name = "Perspectium Replicate";
			grBR.collection = rgr.table_name;
			
			grBR.order = "50";
			if( rgr.u_business_rule_order > 0 )
				grBR.order = rgr.u_business_rule_order;
			
			grBR.condition = replicateCondition;
			grBR.script = replicateScript;
			// pass share sys_id so we can reference it when sharing record
			// to support multiple share configurations with different business order rule and when for the same table
            
				
			// always set when to run business rule based on share config as default value on share config form is "after"
			grBR.when = rgr.u_business_rule_when;			
			
			grBR.active = rgr.active;
			grBR.advanced = "true";
			grBR.execute_function = "false";
			grBR.action_delete = rgr.action_delete;
			grBR.action_insert = rgr.action_create;
			grBR.action_update = rgr.action_update;
			grBR.sys_domain = "global";
			grBR.setWorkflow(false);
			var ior = grBR.insert();
			
			// save business rule's sys_id into the share config so we can reference for next time we update
			// setworkflow false so it doesn't fire business rule again and do a cyclical update
			rgr.u_business_rule = ior;
			rgr.setWorkflow(false);
			rgr.update();
			
			this.logger.logDebug("updateReplicatorBusinessRule after insert =" + ior, "PerspectiumReplicator.updateReplicatorBusinessRule", rgr);
		}
	},

	resetReplicatorBusinessRule: function(rgr){
		// delete business rule and then recreate it from scratch so it has all the proper settings
		this.deleteReplicatorBusinessRule(rgr);		

		this.deleteListener(rgr.table_name, rgr);
		
		if (rgr.u_business_rule != null && rgr.u_business_rule != "" && !rgr.u_business_rule.isNil()) {
			this.updateReplicatorBusinessRule(rgr);		
		}
		// if this share config doesn't have a business rule reference then we'll want to delete 
		// all share configs for this table and re-create them
		// since there will only be one business rule for the table previously and resetting one will
		// cause the other ones to not have a business rule to use anymore
		else{
			var qc = new GlideRecord('psp_replicate_conf');
            qc.addQuery('table_name', rgr.table_name);
	    	qc.addQuery("sync_direction", "share");
	    	qc.addQuery("active", "true");
            qc.query();
			
			while(qc.next()){
				this.updateReplicatorBusinessRule(qc, true);
			}			
		}
	},
	
	deleteReplicatorBusinessRule : function(rgr) {
		//Flip the active flag on the appropriate Business Rule
		var grBR = new GlideRecord("sys_script");
		grBR.addQuery("collection", rgr.table_name);
		grBR.addQuery("name", "Perspectium Replicate");
		
		// use the business rule referenced in share config
		if (rgr.u_business_rule != null && rgr.u_business_rule != "" && !rgr.u_business_rule.isNil()) {
			grBR.addQuery("sys_id", rgr.u_business_rule);
		}
		
		grBR.query();
		if(grBR.next()) {
			grBR.deleteRecord();
		}
	},
	
	synchDynShareRules : function() {		
		// delete all Perspectium Replicate business rules
		var grBR = new GlideRecord("sys_script");
		grBR.addQuery("name", "Perspectium Replicate");
		grBR.deleteMultiple();
		
		// delete all Perspectium DL - x business rules
		var grDL = new GlideRecord("sys_script");
		grDL.addQuery("name", "STARTSWITH", "Perspectium DL");
		grDL.deleteMultiple();
		
		//gs.print("deleted existing rules and recreating new ones");
		
		// recreate them
		var rgr2 = new GlideRecord("psp_replicate_conf");
		rgr2.addQuery("sync_direction", "share");
		rgr2.addActiveQuery();
		rgr2.query();
		while(rgr2.next()) {
			gs.print("creating rule for " + rgr2.table_name);
			this.updateReplicatorBusinessRule(rgr2, true);
		}
	},
	
	appendAttribute : function(attrKey, attrVal, attrString){
		if(attrString.length > 0)
			attrString += ",";
		attrString += attrKey + "=" + attrVal;
		return attrString;
	},
	
	appendTableMapAttributes : function(grParam, grTableMap, attrString){
		var fmap = new GlideRecord('u_psp_table_field_map');
		fmap.addQuery('u_parentid' , grTableMap.sys_id);
		fmap.query();
		while(fmap.next()) {
			if(fmap.u_target_field.startsWith("@")){
				if(fmap.u_use_script){
					var gc = (typeof GlideController != 'undefined') ? GlideController : Packages.com.glide.script.GlideController;
					var oldCurrent = current;
					gc.putGlobal('current', grParam);
					var answer = gc.evaluateString(fmap.u_source_script);
					gc.removeGlobal('current');		
					current = oldCurrent;				
					this.logger.logDebug("fmap.u_target_field = " + fmap.u_target_field.substring(1));
					attrString = this.appendAttribute(fmap.u_target_field.substring(1), answer, attrString);					
				}
				else{
					this.logger.logDebug("fmap.u_target_field = " + fmap.u_target_field.substring(1));
					attrString = this.appendAttribute(fmap.u_target_field.substring(1), grParam.getValue(fmap.u_source_field), attrString);
				}
			}
		}
		return attrString;
	},
		
	incrementMessageSetCounter : function(name) {
		if (this.messageSetCounter[name]) {
			this.messageSetCounter[name] ++;
		} 
		else {
			this.messageSetCounter[name] = 1;
		}		
	},
		
	endsWith : function(str, suffix) {
    	return str.indexOf(suffix, str.length - suffix.length) !== -1;
	},
	
	incrementTableCounter : function(tableName, recordByte) {
		if(this.tableCounter[tableName]) {
			var byteTotal = 0;
			this.tableCounter[tableName].bytes = recordByte + this.tableCounter[tableName].bytes;
			this.tableCounter[tableName].count++;
		}
		else {
			this.tableCounter[tableName] = {
				count: 1,
				bytes: recordByte
			};
		}
	},
    
    type: 'PerspectiumReplicator '
};