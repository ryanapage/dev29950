var PerspectiumImpactedService = Class.create();
PerspectiumImpactedService.prototype = {
 	initialize: function() {
        this.logger = new PerspectiumLogger();
    },

    addImpactedServices : function(source, target, pspTag /* optional */) {
        var targetId = target.sys_id;
		var targetTable = target.getTableName();
        this.logger.logDebug('Adding impacted services for: ' + targetTable + "," + targetId, "PerspectiumImpactedService.addImpactedServices");

        if (source == null || source.u_impacted_services.isNil()) {
            this.logger.logDebug('No impacted service found', "PerspectiumImpactedService.addImpactedServices");
            return;
        }

        var xmldoc = null;
        try {
             xmldoc = new XMLDocument(source.u_impacted_services);
        } catch(err){
            this.logger.logDebug('Error creating XMLDocument for impacted services: ' + err, "PerspectiumImpactedService.addImpactedServices");
            return;
        }

        if (xmldoc == null) {
           this.logger.logDebug('XMLDocument is null', "PerspectiumImpactedService.addImpactedServices");
           return;
        }

        var nodelist = xmldoc.getNodes("//impacted_service");
        if (nodelist == null) {
            this.logger.logDebug('No impacted services nodes found', "PerspectiumImpactedService.addImpactedServices");
            return;
        }

        this.logger.logDebug('Found ' + nodelist.getLength() + ' impacted services', "PerspectiumImpactedService.addImpactedServices");
        for (var i=0; i < nodelist.getLength(); i++) {
            var nodeItem = nodelist.item(i);
            this.addImpactedService(nodeItem, targetTable, targetId, pspTag);
        }
    },
	
	_getValue : function(nodeItem, nodeName) {        
		var xml_util = (typeof GlideXMLUtil != 'undefined') ? new GlideXMLUtil() : new Packages.com.glide.util.XMLUtil();
		var it = xml_util.childElementIterator(nodeItem);
        while (it.hasNext()) {
            var el = it.next();
            var nn = el.getNodeName();
            var nodeValue = xml_util.getText(el);
			
			if( nodeValue == null)
				nodeValue = "";
			
			if(nn == nodeName) {
				return nodeValue;
			}
		}
											  
		return "";
	},
	
//       <impacted_service>
//          <operational_status />
//          <type>cmdb_ci_computer</type>
//          <location_name>322 West 52nd Street, New York,NY</location_name>
//          <id>46b673c6a9fe1981016c72cc01ac53c2</id>
//          <manually_added>1</manually_added>
//          <name>382735F5AD9E492</name>
//       </impacted_service>
	
    addImpactedService : function(nodeItem, targetTable, targetId, pspTag /* optional */) {
        var StringUtil = (typeof GlideStringUtil != 'undefined') ? new GlideStringUtil() : new Packages.com.glide.util.StringUtil();
		
		var ciID = this._getValue(nodeItem, "id");
		
		if (ciID == "")
			return; // no sys_id, cannot search
		
		var ciClass = this._getValue(nodeItem, "type");
		var ciName = this._getValue(nodeItem, "name");
		
		// first see if we can find CI by sys_id
		var tci = new GlideRecord("task_cmdb_ci_service");
		tci.addQuery("cmdb_ci_service", ciID);
		tci.addQuery("task", targetId);
		tci.query();
		if (!tci.next()) {
			if (ciClass == null || ciName == null) // cannot try if these are null
				return;
			
			// try locating by class and name		
			var ci = new GlideRecord(ciClass);
			ci.addQuery("name", ciName);
			// query for sys_id ?
			ci.query();
			if (!ci.next()) {
				ci.initialize();
				ci.name = ciName;
				ci.insert();
			}
			
			tci.initialize();
			tci.cmdb_ci_service = ci.sys_id;
			tci.task = targetId;
			tci.manually_added = this._getValue(nodeItem, "manually_added");
			tci.insert();
		}
		
		// if pspTag exists, go and tag it
		if (pspTag) {
			var pspUtil = new PerspectiumUtil();
			pspUtil.addTag(taskCI, pspTag);
		}
    },

    type: 'PerspectiumImpactedService'
};