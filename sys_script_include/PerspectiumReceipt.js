var PerspectiumReceipt = Class.create();
PerspectiumReceipt.prototype = {
    initialize: function() {
		this.instance_name = gs.getProperty("instance_name", "unregistered");
		this.logger = new PerspectiumLogger();
    },
	
	add: function(gr, key) {
		var tsource = this.instance_name;
		if (gr.key) {
			tsource = gr.key;
		}
		
		var rgr = new GlideRecord("u_psp_receipt");
		rgr.u_topic = gr.topic;
		rgr.u_type = gr.type;
		rgr.u_key = key;
		rgr.u_name = gr.name;
		rgr.u_value = gr.name + ' ' + gr.state;
		rgr.u_attributes = gr.u_attributes;
		rgr.u_source = tsource;
		rgr.u_source_record = gr.u_target_record;
		rgr.u_source_table = gr.u_target_table;
		
		return rgr.insert();
	},
	
	receiveInbound: function(inboundGR) {
		
		// ignore inbound monitor
		if (inboundGR.state == "monitor") {
			return;
		}
		
		// if its my own inbound, ignore
		if (inboundGR.key == this.instance_name) {
			return; // dont receipt my own
		}
		
		// we need to create the receipt before processing
		if (this.isSiamCommand(inboundGR) || this.isRepeaterMsg(inboundGR)) {
			var receiptSysId = this.add(inboundGR, this.instance_name);
			var rgr = new GlideRecord('u_psp_receipt');
			rgr.addQuery('sys_id', receiptSysId);
			rgr.query();

			if (rgr.next()) {
				rgr.value = ""; // prevent incorrect skipped msg
				rgr.update();
				this.processReceipt(rgr);
				return;
			}

			return; // dont try to insert again
		}
		
		if (inboundGR.name.startsWith("u_psp_receipt")) {
			var rgr = new GlideRecord('u_psp_receipt');
			rgr.addQuery('sys_id', inboundGR.u_target_record);
			rgr.query();

			if (rgr.next()) {
				this.processReceipt(rgr);
			}
			
			return; // dont get into a loop
		}
		
		// the key is me, the source is where this message came from and will
		// be used to send receipt back to originator using Dynamic Share
		// return sys_id of created receipt
		return this.add(inboundGR, this.instance_name); // the source is the key, we trigger a Dynamic Share on this
	},
	
	isSiamCommand: function(inboundGR) {
		return inboundGR.topic == 'siam' && !inboundGR.name.startsWith('common_');
	},
	
	isRepeaterMsg: function(inboundGR) {
		return this.parseAttributes('repeater_message', inboundGR.u_attributes) == 'true';
	},
	
	/**
	 * Process inbound receipts to update status table.
	 * @param receipt - The Receipt GlideRecord object.
	 **/
	processReceipt: function(receipt) {
		
		if (receipt.u_topic == 'siam') {
			receipt.u_source_record = this.parseAttributes('SIAM_message_id', receipt.u_attributes);
			receipt.update();
		}
		
		var gr = new GlideRecord('u_psp_receipt_status');
		gr.addQuery('sys_id', this.parseAttributes('ReceiptStatusId', receipt.u_attributes));
		gr.query();
		
		if (gr.next()) {
			this.updateReceiptStatus(gr, receipt.sys_id);
		}
	},
	
	/**
	 * Update the receipt status record with the appropriate values
	 * @param receipt - the receipt GlideRecord object
	 **/
	updateReceiptStatus: function(receiptStatus, receiptId) {
		var pspUtil = new PerspectiumUtil();
		var autoDel = pspUtil.getPspPropertyValue('com.perspectium.receipt.status_auto_delete', 'true');
		if (autoDel == true || autoDel == 'true') {
			receiptStatus.deleteRecord();
		} else {
			receiptStatus.u_status = 'received';
			receiptStatus.u_receipt = receiptId;
			receiptStatus.update();
		}
	},
	
	/**
	 * Parse for the value of a specific message attribute.
	 * @param key - the attribute to parse for
	 * @param attributes - the attributes string
	 * @returns string
	 **/
	parseAttributes: function(key, attributes) {
				
		var attArr = attributes.split(',');
		
		// iterate through attributes, check if message_id 
		for (var i = 0; i < attArr.length; i++) {
			var attribute = attArr[i];
			var attSplit = attribute.split('=');
			
			if (attSplit[0] == key) {
				var val;
				if (key == 'SIAM_message_id') { 
					val = attSplit[1].split('-')[0]; 
				} else {
					val = attSplit[1];
				}
				return val;
			}
		}
		return "";
	},
	
	createReceiptStatus: function(cgr) {
		var deliveryGR = new GlideRecord('u_psp_receipt_status');
		deliveryGR.u_record = cgr.sys_id;
		deliveryGR.u_record_table = cgr.getTableName();
		return deliveryGR.insert();
	},
	
	appendStatusReceiptId: function(recordId, attributes) {
		var r = 'ReceiptStatusId=' + recordId;
		if (attributes) {
			attributes += ',' + r;
		} else {
			attributes = r;
		}
		return attributes;
	},

	createReceiptStatusApttus: function(commandName, commandValue) {
		var gr = new GlideRecord('u_psp_receipt_status');
		gr.u_name = commandName;
		gr.u_value = commandValue;
		return gr.insert();
	},

    type: 'PerspectiumReceipt'
};