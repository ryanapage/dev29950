var PerspectiumETSOutbound = Class.create();
PerspectiumETSOutbound.prototype = {
    initialize: function() {
		this.psp = new Perspectium();
		this.logger = new PerspectiumLogger();
		this.outboundTable = "u_psp_ets_out_message";
		this.instance_name = gs.getProperty("instance_name", "unregistered");
		this.queueFull = false;
    },
	
	processOutbound: function() {		
		var qgr = new GlideAggregate(this.outboundTable);  
		qgr.addAggregate("COUNT");  
		qgr.groupBy("u_target_queue");  
		qgr.query();  
		while (qgr.next()) {  
			if (qgr.u_target_queue.nil()) {
				continue;
			}
			
			try {
				this.processTargetQueue(qgr.u_target_queue);
			}
			catch(e) {
				this.logger.logError("Error processing PSP queue " + qgr.u_target_queue + " on outbound table " + this.outboundTable + ": " + e, "PerspectiumETSOutbound.processOutbound");
			}
		} 
	},
	
	processTargetQueue: function(target_queue) {
		var single_output_processor = this.psp.single_output_processor;		
		if ("true" != single_output_processor) {
			var lock = new GlideMutex.get("processTargetQueue");
			while (lock.toString()+'' == "undefined") {
				this.logger.logDebug("waiting for lock: processTargetQueue (" + target_queue + ")", "PerspectiumETSOutbound.processTargetQueue");
				gs.sleep(1000); // sleep for 1000 milliseconds
				lock = new GlideMutex.get("processTargetQueue");
			}
			
			this.logger.logDebug("lock acquired: processTargetQueue (" + target_queue + ")", "PerspectiumETSOutbound.processTargetQueue");
		}
		
        var grPSPOut = new GlideRecord(this.outboundTable);
        grPSPOut.addQuery("u_state", "ready");
        if (target_queue == "default") {
			grPSPOut.addNullQuery("u_target_queue");
        } else if (target_queue) {
			grPSPOut.addQuery("u_target_queue", target_queue);
        }

		var rowLimitStr = this.psp.rowLimitStr;		
		var sdt = new Date();
        grPSPOut.setLimit(parseInt(rowLimitStr));
        grPSPOut.orderBy("sys_created_on");
		grPSPOut.orderBy("u_sequence");
        grPSPOut.query();
		
		while (grPSPOut.next()) {	
			// if queue full we'll just stop processing since no records will be able to get to MBS
			if (this.queueFull) {
				break;
			}
			
			if ("true" != single_output_processor) {
				// dont change to processing if only 1 output processor job
	    		grPSPOut.u_state = "processing";
	    		grPSPOut.update();
			}
			
			var grKey = grPSPOut.u_key;
	    	if (grKey != this.instance_name && grKey.indexOf(this.instance_name + ".") != 0) {
				// may have been cloned, so skip
				grPSPOut.u_state = "skipped";
				grPSPOut.u_state_info = "Message skipped due to mismatch between instance and key of message";
				grPSPOut.update();
				
				this.logger.logDebug("skipping mismatched key: " + grPSPOut.u_key, "PerspectiumETSOutbound.processTargetQueue");
				continue;
	    	}
			
			try {				
				// each outbound message is already a large payload so we'll post it to MBS
				var bgr = new GlideRecord(grPSPOut.u_source_table);
				if (!bgr.get(grPSPOut.u_source)) {
					this.logger.logDebug("skipping outbound message: " + grPSPOut.sys_id + " as " + grPSPOut.u_source + " record not found in " + grPSPOut.u_source_table, "PerspectiumETSOutbound.processTargetQueue");
					continue;
				}
				
				var pspMessage = new PerspectiumMessage(grPSPOut.u_topic, grPSPOut.u_type, grPSPOut.u_key, grPSPOut.u_name, grPSPOut.u_value, bgr.u_target_queue, grPSPOut.u_extra, grPSPOut.u_attributes, bgr, "", "", false, "u_psp_ets_out_message");				
				var pspSM = new PerspectiumShareMessage(bgr, "");
				var result = pspSM.postMessage(pspMessage);				
				if (result != 200) {
					// queue full so we just leave it here to try again later instead of throwing error
					if (result == 600) {
						this.queueFull = true;
						this.logger.logDebug("skipping outbound message: " + grPSPOut.sys_id + " as MBS queue is full", "PerspectiumETSOutbound.processTargetQueue");					
						continue;						
					}
					
					throw new Error("HTTP POST failed: " + result);					
				}
				
				grPSPOut.u_state = "sent";
				grPSPOut.setWorkflow(false);
				grPSPOut.update();								
			} catch (e) {
				this.logger.logError("unable to process " + grPSPOut.sys_id.toString() + " " + e, "PerspectiumETSOutbound.processTargetQueue");
				grPSPOut.u_state = "error";
				grPSPOut.u_state_info = e + "";
	    		grPSPOut.update();			
			}
		}
			
	},

    type: 'PerspectiumETSOutbound'
};