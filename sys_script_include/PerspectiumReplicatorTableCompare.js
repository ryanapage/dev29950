var PerspectiumReplicatorTableCompare = Class.create();
PerspectiumReplicatorTableCompare.prototype = {
    initialize: function() {
		this.pspUtil = new PerspectiumUtil();
		this.psp = new Perspectium();
    },
	
	getStats: function() {
		var connectionUrl = this.pspUtil.getPspPropertyValue("com.perspectium.snc_input", "");
		var days_ago = this.pspUtil.getPspPropertyValue("com.perspectium.table.compare.days_ago", "7");
		var key = gs.getProperty("instance_name", "");
		if (!connectionUrl.endsWith("/")) {
			connectionUrl += "/";
		}
		
		connectionUrl += "customerstatus/stats?instance=" + key + "&days_ago=" + days_ago;
		var getMethod = new Packages.org.apache.commons.httpclient.methods.GetMethod(connectionUrl);
		var httpClient = this.psp.getHttpClient();  
		var result = httpClient.executeMethod(getMethod);
		var rstr = getMethod.getResponseBodyAsString();
		getMethod.releaseConnection();
		var parser = new JSONParser();
		var JSONArray = parser.parse(rstr);
		var tsgr = new GlideRecord("u_psp_table_stats");
		tsgr.deleteMultiple();
		// iterate the array
		if (JSONArray.length == 0) {
			gs.addErrorMessage("Unable to find table stats for " + key + " in the past " + days_ago + " days.");
			return;
		}
		
		for (var i=0; i<JSONArray.length; i++) {
			var name = JSONArray[i].name.toString();
			var jsonStr = JSONArray[i].value.toString();
			var jObj = parser.parse(jsonStr);
			var timestamp = JSONArray[i].psp_timestamp.toString();
			// iterate the values in the obj
			for (var j in jObj) {
				var gr = new GlideRecord("u_psp_table_stats");
				gr.u_value = jObj[j];
				var field = j.split('.');
				gr.u_table_name = field[1];
				gr.u_time_of_database_count = timestamp;
				var tgr = new GlideRecord(field[1]);
				if (!tgr.isValid()) {
					continue;
				}
				
				gr.u_instance_value = this.getInstanceCounts(field[1].toLowerCase());
				gr.u_agent_name = name;
				gr.u_database = field[0];
				gr.insert();
			}
		}
	},
	
	getInstanceCounts: function(table) {
		var recordcount = 0;
		var rec = new GlideAggregate(table);	
		if (!rec.isValid())
			return recordcount;
		
		rec.addAggregate("COUNT");
		rec.query();
		if (rec.next()){
	 	   recordcount = rec.getAggregate("COUNT");
		}

		return recordcount;
	},

    type: 'PerspectiumReplicatorTableCompare'
};