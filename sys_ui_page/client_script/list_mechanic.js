var LM = 'gs.include("ListMechanic"); var v = new ListMechanic();';

function actionOK(reset) {
  var keys = ["Saving", "Reloading"];
  var msgs = new GwtMessage().getMessages(keys);
  var array = slush.getValues(slush.getRightSelect());  var f = array.join(',');
  var o = slush.getRightValues();
  var changes = true;
  if (f == o)
     changes = false;
  setStatus(msgs["Saving"] + "...");

  var ga = new GlideAjax('UIPage');
  ga.addParam('sysparm_name','createListMechanic');
  ga.addParam('sysparm_reset',reset ? true : false);
  ga.addParam('sysparm_list_view',g_list_view);
  ga.addParam('sysparm_list_parent',g_list_parent);
  ga.addParam('sysparm_list_parent_id',g_list_parent_id);
  ga.addParam('sysparm_list_relationship',g_list_relationship);
  ga.addParam('sysparm_table',g_table);
  ga.addParam('sysparm_f',f);
  ga.addParam('sysparm_changes',changes);
  ga.addParam('sysparm_compact',gel('ni.compact').checked);
  ga.addParam('sysparm_wrap',gel('ni.wrap').checked);
  ga.addParam('sysparm_highlighting',gel('ni.highlighting').checked);
  ga.addParam('sysparm_field_style_circles', gel('ni.field_style_circles').checked);

  var o = gel('ni.list_edit_enable');
  if (o)
     ga.addParam('sysparm_edit_enable', o.checked);

  o = gel('ni.list_edit_double');
  if (o)
     ga.addParam('sysparm_edit_double',o.checked);

  ga.getXMLWait();
  ga.getAnswer();
  setStatus("<span style='color:green'> " + msgs['Reloading'] + "</span>");
  setTimeout('reloadWindow(self);', 1);
  GlideDialogWindow.get().destroy();
  return false;
}

function setStatus(statusHTML) {
  gel('list_mechanic').getElementsByClassName('status_msg_update')[0].innnerHTML = statusHTML;
}

function getList() {

  if (typeof g_table == "undefined")
    g_table = 'sys_user';

  if (typeof g_list_view == "undefined")
    g_list_view = "";

  if (typeof g_list_parent == "undefined")
    g_list_parent = "";

  if (typeof g_list_relationship == "undefined")
    g_list_relationship = "";
  var ga = new GlideAjax('UIPage');
  ga.addParam('sysparm_name','getList');
  ga.addParam('sysparm_list_view',g_list_view);
  ga.addParam('sysparm_list_parent',g_list_parent);
  ga.addParam('sysparm_list_parent_id',g_list_parent_id);
  ga.addParam('sysparm_list_relationship',g_list_relationship);
  ga.addParam('sysparm_table',g_table);
  ga.getXML(listResponse);
}

function listResponse(request) {
  var xml = request.responseXML.documentElement;

  var z = xml.getElementsByTagName("columns")[0];
  populateSelect(gel('slush_left'), z);

  z = xml.getElementsByTagName("selected")[0];
  populateSelect(gel('slush_right'), z);
  var array = slush.getValues(slush.getRightSelect());
  var r = array.join(",");
  slush.saveRightValues(r);

  // a user list?
  z = xml.getElementsByTagName("choice_list_set")[0];
  var a = z.getAttribute("user_list");
  gel('ni.highlighting').checked = eval(z.getAttribute('table.highlighting'));
  gel('ni.compact').checked = eval(z.getAttribute('table.compact'));
  gel('ni.wrap').checked = eval(z.getAttribute('table.wrap'));
  gel('ni.field_style_circles').checked = eval(z.getAttribute('field_style_circles'));

  var o = gel('ni.list_edit_enable');
  if (o)
    o.checked = eval(z.getAttribute('list_edit_enable'));
  var o = gel('ni.list_edit_double');
  if (o)
    o.checked = eval(z.getAttribute('list_edit_double'));

  if (a == 'true') {
     gel('reset_column_defaults').style.visibility = 'visible';
  } else {
    gel('reset_column_defaults').style.visibility = 'hidden';
  }
}

function populateSelect(select, xml) {
  select.options.length = 0;
  var choices = xml.getElementsByTagName("choice");
  for (var i = 0; i < choices.length; i++ ) {
    var choice = choices[i];
    var o = new Option(choice.getAttribute('label'), choice.getAttribute('value'));
    select.options[select.options.length] = o;
  }
}

getList();