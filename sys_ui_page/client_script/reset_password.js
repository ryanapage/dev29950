var wfCheckDoneFrequency = 500;

var ok_output = {
	msg: getMessage("The system has reset your password and will email it to you."),
	type: 'info'
};
var block_output = {
	msg: getMessage("At this time, you are not allowed to use the Password Reset application because either you recently reset your password or you failed to provide a valid user name and email address. Please contact your service desk."),
	type: 'error'
};
var external_error_output = {
	msg: getMessage("Use your network password to log in. If your network password does not work, contact your service desk."),
	type: 'error'
};
var not_match_output = {
	msg: getMessage("We cannot find either the user name or email address. Please try again."),
	type: 'error'
};
var wf_error_output = {
	msg: getMessage("Your password could not be reset. Please contact your service desk."),
	type: 'error'
};


function fireAJAX() { 
	var sysparm_user_id = gel('user').value;
	var sysparm_email = gel('email').value;
	
	var ga = new GlideAjax('ResetPwdAjaxProcessor');
	ga.addParam('sysparm_name', 'resetPwdAjaxProcess');
	ga.addParam('sysparm_user_id', sysparm_user_id);
	ga.addParam('sysparm_email', sysparm_email);
	
	ga.getXML(ajaxResponse);
	
	return false;
}


function ajaxResponse(response){
	var answer = response.responseXML.documentElement.getAttribute("answer");
	
	// verification succeeds, workflow started
	if(answer == "wf_started"){
		setTimeoutForCheckWFState();
	}
	// error
	else {
		var output = "";
		if(answer == "not_match_error"){
		    output = not_match_output;
	    }
	    else if(answer == "block"){
		    output = block_output;
	    }
	    else if(answer == "external_error"){
		    output = external_error_output;
	    }
	    else if(answer == "wf_error"){
		    output = wf_error_output;
	    }
		outPutPrint(output);
	}
}

function outPutPrint(output){
	GlideUI.get().clearOutputMessages();
    GlideUI.get().addOutputMessage(output);
}

function setTimeoutForCheckWFState(){
	window.setTimeout(function (){
		var ga = new GlideAjax('ResetPwdAjaxProcessor');
		ga.addParam('sysparm_name', 'checkWFProgress');
		ga.getXMLWait();
		
		var state = ga.getAnswer();
		if(state == 'wf_timeout'){
			outPutPrint(wf_error_output);
			return;
		}
		if(!state.match(/finished/i)){
			setTimeoutForCheckWFState();
			return;
		}			
		else if(state.match(/failure/i)){
			outPutPrint(wf_error_output);
			return;
		}
		else {
			outPutPrint(ok_output);
			return;
		}
	}, wfCheckDoneFrequency);
}



      







