jQuery(document).ready(function() {

	angular.module('sn.filter_widget').controller('NgElementsTest', function($scope){

		$scope.fields = [
		{
			"name":"color_display",
			"ref":"sys_test.color_display",
			"inline":true,
			"value":"background-color:#3ED662",
			"readonly":false,
			"type":"color_display",
			"displayValue":"background-color:#3ED662",
			"table":"sys_test",
			"sysId":"0b0064b37f012200348ef0d8adfa91b2",
			"maxLength":40,
			"onBlur" : function(){console.log('color display on blur activated')}
		},
		{
			"name":"integer",
			"ref":"sys_test.integer",
			"inline":true,
			"value":"42",
			"readonly":false,
			"type":"integer",
			"displayValue":"42",
			"table":"sys_test",
			"sysId":"4b0064b37f012200348ef0d8adfa91b1",
			"maxLength":40,
			"onBlur" : function(){console.log('integer on blur activated')}
		}
	];
				 
	});

	angular.bootstrap(document.getElementById('snfilterwidgettest'), [
		'sn.filter_widget',
		'sn.ngform',
		'sn.common.ui'
	]);
});