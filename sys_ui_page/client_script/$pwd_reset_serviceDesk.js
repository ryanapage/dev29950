$j('title').text("${gs.getMessage('Password Reset - Service Desk')}");
addLoadEvent(onLoadPopulateProcessSelect);
addLoadEvent(focusOnCorrectField);
	
function goBack() {
    window.history.back();
}

function focusOnCorrectField() {
	var user = gel('sys_display.ref_sys_user');
	if ((user != undefined) && (user.value == "")) {
		user.focus();
		return;
	}
	gel('processSelectId').focus();
}

function onLoadPopulateProcessSelect() {
	var fromSysUserForm = gel('ref_sys_user').value;
	if (fromSysUserForm.length > 0) {
		populateProcessSelect();
	}
}

function clearErrorMessage() {
	clearFieldError('error_msg_id', 'user_id_form_group');
	clearFieldError('error_msg_select', 'process_select_form_group');
}

function populateProcessSelect() {
	clearErrorMessage();
	clearError();
	var user_id = gel('ref_sys_user').value;
	
	var sel = document.getElementById("processSelectId");
	sel.options.length = 1;
	
	if (user_id != "") {
		
		var ga = new GlideAjax('PwdAjaxVerifyIdentityServiceDesk');
		ga.addParam('sysparm_name', 'getProcessNamesAsync');
		ga.addParam('sysparm_user', user_id);
		ga.getXML(function(response) {
			var processes = response.responseXML.getElementsByTagName("process");
			var numProcesses = processes.length;
			for (var i = 0; i < processes.length; i++) {
				var name = processes[i].getAttribute("name");
				var procSysId = processes[i].getAttribute("procSysId");
				sel[i + 1] = new Option(name, procSysId);
			}
			if (numProcesses == 0) {
				displayError(getMessage('No service-desk processes found for the user selected.'));
			}
		});
	}
}

var wasSubmitted = false;
function verifyIdentity() {
	if (wasSubmitted)
		return false;
	wasSubmitted = true;
	
	clearErrorMessage();
	var user_id = gel('ref_sys_user').value;
	if ((user_id == undefined) || (user_id == "")) {
		displayFieldError("error_msg_id", getMessage("Invalid user"), "user_id_form_group");
		wasSubmitted = false;
		return false;
	}
	
	var sel = document.getElementById("processSelectId");
	var procSysId = sel.options[sel.selectedIndex].value;
	
	if (sel.selectedIndex == 0) {
		displayFieldError("error_msg_select", getMessage("Invalid selection"), "process_select_form_group");
		wasSubmitted = false;
		return false;
	}
	
	var ga = new GlideAjax('PwdAjaxVerifyIdentityServiceDesk');
	ga.addParam('sysparm_name', 'saveAndProceed');
	ga.addParam('sysparm_user_id', user_id);
	ga.addParam('sysparm_procSysId', procSysId);
	
	ga.getXML(function(response) {
		var result = response.responseXML.getElementsByTagName("result");
		var status = result[0].getAttribute("status");
		
		if (!status.match(/success/i)) {
			displayFieldError("error_msg_id", status, "user_id_form_group");
		} else {
			var submitForm = gel('pwd_master_form');
			submitForm.action = '$pwd_verify.do';
			submitForm.submit();
		}
		wasSubmitted = false;
	});
	return false;
}


function isEmailAddress(str) {
	var email_pattern = /^\w+(\.\w+)*@\w+(\.\w+)+$/;
	return str.match(email_pattern);
}

function errorImage() {
	return '<img src="images/outputmsg_error.gifx" alt="Error Message" />';
}

function displayFieldError(field, message, formGroup) {
	fieldElem = gel(field);
	if (fieldElem != undefined) {
		fieldElem.innerHTML = message;
		//fieldElem.style.display = 'inline';
	}
	$j('#' + formGroup).removeClass("is-required");
	$j('#' + formGroup).addClass("has-error");
}

function clearFieldError(field, formGroup) {
	fieldElem = gel(field);
	if (fieldElem != undefined) {
		fieldElem.innerHTML = '';
		//fieldElem.style.display = 'none';
	}
	$j('#' + formGroup).addClass("is-required");
	$j('#' + formGroup).removeClass("has-error");

}