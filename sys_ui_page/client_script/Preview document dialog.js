addLoadEvent(function(){
	
	if (!gel('task_table').value
	|| !gel('task_sys_id').value || !gel('isPdfTemplate').value ) {
		g_form.addErrorMessage(getMessage('The corresponding PDF template is empty'));
		GlideDialogWindow.get().destroy();
		return;
	}
	
	//return if script name is not present
	if (gel('script_name') == null) {
		g_form.addErrorMessage(getMessage('The script name is empty'));
		return;
	}
	
	//proceed with setting document only if pdftemplate is false
	if(gel('isPdfTemplate').value == 'false') {
		//apiName may differ if called from scope application
		var apiName = getApiName(gel('script_name'));
		var ga = new GlideAjax(apiName);
		ga.addParam("sysparm_name", "getDocumentBody");
		ga.addParam("sysparm_tableName", gel('task_table').value);
		ga.addParam("sysparm_tableId", gel('task_sys_id').value);
		ga.addParam("sysparm_category", gel('task_category').value);
		ga.addParam("sysparm_targetTable", gel('target_table').value);
		ga.addParam("sysparm_targetId", gel('target_Id').value);
		ga.addParam("sysparm_canEdit", gel('edit').value);

		ga.getXMLAnswer(function(answer) {
			var body;
			var unEvaluatedVariables = [];
			var inaccessibleVariables = [];
			if (!answer.isJSON())
				body = answer;
			else {
				var result = JSON.parse(answer);
				body = result.body;
				unEvaluatedVariables = result.unEvaluatedVariables;
				inaccessibleVariables = result.inaccessibleVariables;
			}
			gel('content_id').innerHTML = body;
			gel('content_id').focus();
			if (body == '')
				gel('info_message').style.display = "block";
			if (unEvaluatedVariables.length > 0 || inaccessibleVariables.length > 0) {
				var errStr = '';
				if (unEvaluatedVariables.length > 0) {
					errStr += "<div id='inValid_variables'>"+getMessage("Following variables are not valid or defined:")+"<ul>";
					for (var i=0; i<unEvaluatedVariables.length; i++) {
						errStr += "<li>" + unEvaluatedVariables[i] + "</li>";
					}
					errStr += "</ul></div>";
				}
				if (inaccessibleVariables.length > 0) {
					errStr += "<div id='inaccessible_variables'>"+getMessage("Following variables are not accessible based on your permissions:")+"<ul>";
					for (var j=0; j<inaccessibleVariables.length; j++) {
						errStr += "<li>" + inaccessibleVariables[j] + "</li>";
					}
					errStr += "</ul></div>";
				}
				gel('error_message').innerHTML = errStr;
				gel('error_message').style.display = "block";
				gel('error_message').style.color = "#FF402C";
			}
		});
	} else {
		var api = getApiName(gel('script_name'));
		var pdfAjxCall = new GlideAjax(api);
		pdfAjxCall.addParam("sysparm_name", "getPreFilledPDFSysId");
		pdfAjxCall.addParam("sysparm_pdf_sysid", gel('pdfSysId').value);
		pdfAjxCall.addParam("sysparm_case_sysid", gel('task_sys_id').value);
		pdfAjxCall.addParam("sysparm_table_name", gel('task_table').value);
		pdfAjxCall.addParam("sysparm_table_sysid", gel('task_sys_id').value);
		pdfAjxCall.getXMLAnswer(function(answer){
			if(answer != ""){
				var result  = JSON.parse(answer);
				var previewSysId = result.previewSysId + '';
				var url = '/sys_attachment.do?view=true&sys_id=' + previewSysId;
				document.getElementById('iframe_pdf_preview').setAttribute("src", url);
			}
		});
		
	} 
});

function getApiName(scriptNameElement){
	if(scriptNameElement != null) {
		if(scriptNameElement.value)
			return scriptNameElement.value + "";
	}
}

function disableOrEnableButton(){
	if(gel('content_id').innerHTML.trim().length){
		gel('save_button').disabled = false;
		gel('ok_button').disabled = false;
	}else{
		gel('save_button').disabled = true;
		gel('ok_button').disabled = true;
	}
	
}
function submitCancel() {
	gel('info_message').style.display = "none";
	GlideDialogWindow.get().destroy();
	return false;
}

function submitSave() {
	if(gel('isPdfTemplate').value == 'false')
		setDocument();
	gel('info_message').style.color = "#4BD762";
	gel('info_message').innerHTML = getMessage('The document body has been saved');
	gel('info_message').style.display = "block";
	
	return false;
}

function submitOk() {
	if (gel('isPdfTemplate').value) {
		gel('info_message').style.display = "none";
		
		var gDialog = new GlideModal('accept_signature');
		gDialog.setTitle(new GwtMessage().getMessage('Please sign document'));
		gDialog.setWidth("500");
		gDialog.setPreference('sysparm_document_id', gel('task_sys_id').value);
		gDialog.setPreference('sysparm_table_name', gel('task_table').value);
		gDialog.setPreference('sysparm_draw_only', 'false');
		gDialog.render();
		if(gel('isPdfTemplate').value == 'false')
			setDocument();
	} else {
		gel('info_message').style.color = "#FF402C";
		gel('info_message').innerHTML = getMessage('The document body should not be empty');
		gel('info_message').style.display = "block";
	}
	return false;
}

function generateDocument() {
	var apiName = getApiName(gel('script_name'));
	var ga = new GlideAjax(apiName);
	ga.addParam("sysparm_name", "generateDocument");
	ga.addParam("sysparm_table_name", gel('task_table').value);
	ga.addParam("sysparm_table_id", gel('task_sys_id').value);
	ga.getXMLAnswer(function(answer) {
	});
}

function setDocument(){
	var apiName = getApiName(gel('script_name'));
	var ga = new GlideAjax(apiName);
	ga.addParam("sysparm_name", "setDocumentBody");
	ga.addParam("sysparm_documentBody", gel('content_id').innerHTML);
	ga.addParam("sysparm_table_name", gel('task_table').value);
	ga.addParam("sysparm_table_id", gel('task_sys_id').value);
	ga.addParam("sysparm_targetTable", gel('target_table').value);
	ga.addParam("sysparm_targetId", gel('target_Id').value);
	ga.addParam("sysparm_canEdit", gel('edit').value);
	ga.getXMLAnswer(function(answer) {
	});
}