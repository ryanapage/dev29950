angular.module('sn.securityDashboard', ['sn.base']);

angular.module('sn.securityDashboard').controller('securityDashboard', function($scope, $http) {
	$scope.alertBoxes = [
		{
			todayTitle: 'Failed login attempts Today',
			yesterdayTitle: 'Yesterday',
			thisWeekTitle: 'This Week',
			table: 'sysevent',
			query: 'name=login.failed'
		},
		{
			todayTitle: 'Admin logins Today',
			yesterdayTitle: 'Yesterday',
			thisWeekTitle: 'This Week',
			table: 'sysevent',
			query: 'name=login',
			user: 'parm1',
			role: 'admin'
		},
		{
			todayTitle: 'Security-admin logins Today',
			yesterdayTitle: 'Yesterday',
			thisWeekTitle: 'This Week',
			table: 'sysevent',
			query: 'name=login',
			user: 'parm1',
			role: 'security_admin'
		},
		{
			todayTitle: 'Admin users added Today',
			yesterdayTitle: 'Yesterday',
			thisWeekTitle: 'This Week',
			table: 'sys_user_has_role',
			query: 'role=2831a114c611228501d4ea6c309d626d^state=active'
		}
	];
	
	$scope.refreshInterval = 60 * 1000;
	$scope.refreshNow = function() {
		$scope.$broadcast('sn.alertPanel.refresh');
	};
	
	$http.get('/api/now/guided_setup/content/0b018fe311232200964f016bcc1bdd29?max_levels=0&audit_info=true').then(function(response) {
		try {
			$scope.score = Math.floor(response.data.result.audit.progress);
		} catch (e) {}
	});
});

angular.module('sn.securityDashboard').directive('alertPanel', function($interval) {	
	return {
		restrict: 'E',
		scope: {
				todayTitle: '=',
				yesterdayTitle: '=',
				thisWeekTitle: '=',
				table: '=',
				query: '=',
				user: '=',
				role: '=',
				refreshInterval: '='
		},

		template: '<div class="panel-body">' +
'				<h3 class="h4 text-center"><a ng-href="{{table}}_list.do?sysparm_query={{today.query}}" target="_blank">{{ today.count }}</a><br/><span class="h5"><strong>{{ today.msg }}</strong></span></h3>' +
'				<hr/>' +
'				{{ yesterday.msg }}<a ng-href="{{table}}_list.do?sysparm_query={{yesterday.query}}" class="pull-right" ng-bind="yesterday.count" target="_blank"></a>' +
'				<hr/>' +
'				{{ thisWeek.msg }}<a ng-href="{{table}}_list.do?sysparm_query={{thisWeek.query}}" class="pull-right" ng-bind="thisWeek.count" target="_blank"></a>' +
				'<hr/>' +
'			</div>',
		replace: true,
		controller: function($scope) {
			$scope.today = { msg: $scope.todayTitle, query: getQuery('today')};
			$scope.yesterday = { msg: $scope.yesterdayTitle, query: getQuery('yesterday') };
			$scope.thisWeek = { msg: $scope.thisWeekTitle, query: getQuery('thisWeek') };
			
			var intervalPointer;
			
			load();
			
			if ($scope.refreshInterval) {
				intervalPointer = $interval(load, $scope.refreshInterval);
				$scope.$watch('refreshInterval', function(newValue, oldValue) {
					if (newValue == oldValue)
						return;
					
					$interval.stop(intervalPointer);
					$interval(load, $scope.refreshInterval);
				});
			}
			
			$scope.$on('sn.alertPanel.refresh', function() {
				load();
			});
			
			function getQuery(slot, userIDs) {
				var dateQuery = {
					'today': '^sys_created_onONtoday@javascript:gs.daysAgoStart(0)@javascript:gs.daysAgoEnd(0)',
					'yesterday': '^sys_created_onONyesterday@javascript:gs.daysAgoStart(1)@javascript:gs.daysAgoEnd(1)',
					'thisWeek': '^sys_created_onONthisweek@javascript:gs.beginningOfThisWeek()@javascript:gs.endOfThisWeek()'
				};
				
				var query = $scope.query + dateQuery[slot];
				if(userIDs && userIDs.length > 0) {
					query += '^user_idIN' + userIDs.join(',');
				}
				
				return query;
			}
			
			function load() {
				if(!$scope.user) {
					setInitialCount($scope.today.query, 'today');
					setInitialCount($scope.yesterday.query, 'yesterday');
					setInitialCount($scope.thisWeek.query, 'thisWeek');
				} else {
					setUserCounts();
				}
			}
						
			function setInitialCount(query, slot) {
				var gr = new GlideRecord($scope.table);
				gr.encodedQuery = query;
				gr.query(function(gr) {		 
					$scope[slot].count = gr.rows.length;
					$scope.$evalAsync();
				});
			}
			
			function setUserCounts() {
				// new GlideAjax object referencing name of AJAX script include
				var ga = new GlideAjax("NumberOfAdminLogins");
				ga.addParam("sysparm_name", "getUserIDs");
				ga.addParam("sysparm_role", $scope.role);

				ga.getXML(function(serverResponse) {
					getUserCount('today', serverResponse);
					getUserCount('yesterday', serverResponse);
					getUserCount('thisWeek', serverResponse);
					$scope.$evalAsync();
				});
				
			}
			
			function getUserCount(slot, serverResponse) {
				var result = serverResponse.responseXML.getElementsByTagName(slot + ".count");
				$scope[slot].count = result[0].getAttribute("value");

				var users = serverResponse.responseXML.getElementsByTagName(slot + ".user");
				var userIDs = [];
				for(var i = 0; i < users.length; i++) {
					var userID = users[i].getAttribute("userID");
					userIDs.push(userID);
				}				
				$scope[slot].query = getQuery(slot, userIDs);
			}
		}
	}
});