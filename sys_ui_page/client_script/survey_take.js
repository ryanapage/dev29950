addLoadEvent (function(){
   suppressEnterKey($j("input, select"));
});

function onSubmit() {
  this.mandatoryResult = g_form.mandatoryCheck();
  return this.mandatoryResult;
}

/*
 This will prevent the enter key from submitting the form (due to the presence of a button of type "submit").
 jElements : jquery array of elements
 */
function suppressEnterKey(jElements) {
	jElements.keypress(function(evt) {
		return !enterKeyPrevented(evt);
	});
}

function enterKeyPrevented(evt) {
	if (evt.which == 13) {
		evt.preventDefault();
		evt.stopPropagation();
		return true;
	} else
		return false;
}