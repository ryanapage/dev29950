var rows = gel('rows').value;
rows = jQuery.parseJSON(rows);
buildCards(rows);

function buildCards(data) {
	var i = 0;
	if (data[0] != '') {
		while (i < data.length) {
			var info = buildBody(data, i);
			if (info.name != '') {
				var window = new Array();
				window[i] = createGlideWindow(data[i].inst_id, 'id' + data[i].inst_id, true, info.name, info.body);
				i++;
			}
		}
	}
}

function showAll(instance, type) {
	var url = 'assessment_list.do?sysparm_show=all';
	window.location = url;
}
function showMine(instance, type) {
	var url = 'assessment_list.do?sysparm_show=user';
	window.location = url;
}

function takeAssessment(instance, type) {
	var url = 'assessment_take2.do?sysparm_assessable_sysid=' + instance + '&sysparm_assessable_type=' + type;
	window.location = url;
}

function createGlideWindow(div, id, readOnly, title, body) {
	var w = new GlideWindow(id, readOnly);
	w.setPosition('relative');
	w.setTitle(title);
	w.setSize("100%", "");
	w.setBody(body);
	w.setFontSize(12);
	w.setFontWeight("normal");
	w.setHeaderColor("");
	w.setClassName("drag_section");
	w.insert(div);
	if(w.getWindowDOM()){
		w.getWindowDOM().style.tableLayout = "fixed";
	}
	if(w.getHeader()){
		w.getHeader().style.tableLayout = "fixed";
	}

	if(w.getTitle()){
		w.getTitle().style.overflow = "hidden";
		w.getTitle().style.textOverflow = "ellipsis";
	}

	return w;
}

function buildBody(data, id) {
	var viewSurveyLink = 'asmt_metric_type.do?sys_id=' + data[id].mt_id + '&sysparm_view=' + data[id].mt_evaluation_method;
	var takeSurveyLink = 'assessment_take2.do?sysparm_assessable_sysid=' + data[id].inst_id + '&amp;sysparm_assessable_type=' + data[id].mt_id;
	var takeSurveyButton = 'takeAssessment(\'' + data[id].inst_id + '\',\'' + data[id].mt_id + '\');';
	var desc = data[id].inst_description.escapeHTML();
	var name = '';
	var body = '';
	if (typeof desc  != "undefined") {
		desc = desc.replace(/\n/g, '<br/>');
		desc = desc.replace(/\r/g, '');

		var buttonText = "";
		var gwtMessage = new GwtMessage();
		if (data[id].mt_evaluation_method != "testplan") {
			var datum = data[id].mt_evaluation_method_display;
			if (data[id].inst_preview == "true")
				buttonText = gwtMessage.getMessage("Preview {0}", datum);
			else if (data[id].inst_state == "complete")
				buttonText = gwtMessage.getMessage("Modify {0}", datum);
			else
				buttonText = gwtMessage.getMessage("Take {0}", datum);
		}
		else if (data[id].inst_state == "complete")
			buttonText = getMessage("Retest");
		else if (data[id].inst_preview == "false")
			buttonText = getMessage("Start Testing");

		name += '<h4 style="margin: 0 0">' + data[id].mt_name.escapeHTML() + '</h4>';
		body += '<table style="width: 100%; vertical-align:top;">';
		body += '<tbody><tr style="vertical-align:top;">';
		body += '<td style="text-align: center; vertical-align: top;" class="homepage_category_only_image_cell">';
		body += '<span style="margin: 0px 10px 0px 0px; padding:10px 10px 10px 10px;">';
		body += '<img alt="" aria-label="'+data[id].mt_evaluation_method_display+' instance icon" onclick="document.location.href=\'assessment_list.do\'; return false;" src="images/assessment/checkClipboard' + data[id].inst_color + '.png" />';
		body += '</span>';
		body += '</td>';
		if ("${gs.getUserID()}" == data[id].inst_user) {
			body += '<td style="max-width: 0; overflow: hidden; text-overflow: ellipsis;vertical-align: top; white-space: nowrap; width: 100%" onclick="document.location.href=\'' + takeSurveyLink + '\'; return false;" class="homepage_category_only_description_cell">';
			body += '<span><a target="_self" style="color:#000;" href="' + takeSurveyLink + '">';
		} else {
			body += '<td style="max-width: 0; overflow: hidden; text-overflow: ellipsis;vertical-align: top; white-space: nowrap; width: 100%" onclick="document.location.href=\'' + viewSurveyLink + '\'; return false;" class="homepage_category_only_description_cell">';
			body += '<span><a target="_self" style="color:#000;" href="' + viewSurveyLink + '">';
		}
		body += '<h2 style="display: inherit; margin: 10px 0px 0px 0px; font-size: larger; font-weight: normal; overflow:hidden; text-overflow:ellipsis;">';
		body += data[id].mt_name.escapeHTML();
		body += '</h2>';
		body += '</a></span><br>';
		body += '<span style="color:#000; white-space:normal;">' + getMessage("Assigned to") + ': </span><span style="color:#888; white-space:normal;">' + data[id].user_name.escapeHTML() + '</span><br/>';
		body += '<span style="color:#000; white-space:normal;">' + getMessage("State") + ': </span><span style="color:#888; white-space:normal;">' + data[id].inst_state + '</span><br/>';
		body += '<span style="color:#000; white-space:normal;">' + getMessage("Due date") + ': </span><span style="color:' + data[id].inst_color + '; white-space:normal;">' + data[id].inst_due_date + '</span><br/>';
		if(data[id].inst_trigger_id){
		body += '<span style="color:#000; white-space:normal;">' + data[id].inst_trigger_table + ': </span><span style="color:#888; white-space:normal;">' + data[id].inst_trigger_id + '</span><br/>';
		}
		body += '<span style="color:#888; white-space:normal;">' + desc + '</span>';
		body += '</td></tr>';
		body += '<tr><td colspan="2" align="right">';
		if ("${gs.getUserID()}" == data[id].inst_user)
			body += '<button id="take_survey_button_'+ data[id].inst_id +'" class="btn btn-default" onClick="' + takeSurveyButton + '" style="font-size: inherit; margin: 3px 5px;" type="submit">' + buttonText + '</button>';
		else
			body += '&nbsp';
		body += '</td></tr></tbody></table>';
	}

	return {body: body, name: name};
}