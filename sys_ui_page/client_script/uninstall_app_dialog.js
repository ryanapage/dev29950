var UninstallAppDialog = Class.create({
  
  initialize: function () {
	if (typeof $('ok_button') === 'object' && $('ok_button') !== null)
	    $('ok_button').focus();
	else
	    $('cancel_button').focus();
  },

  uninstallApp: function () {
    if (!this.reconfirm(new GwtMessage().getMessage("To uninstall the application"))) {
      this.initialize();
      return false;
    }
    
    var deleteAll = gel('uninstall_app_dialog');
    var appId = '${JS:sysparm_sys_id}';
	var redirect = true;
	if ('${JS:sysparm_redirect}' === '0')
		redirect = false;
    var workerId;
    var refreshNav = false;
    
    var dd = new GlideModal("hierarchical_progress_viewer");
    dd.on("executionStarted", function(response) {
      workerId = response.responseXML.documentElement.getAttribute("answer");
      refreshNav = response.responseXML.documentElement.getAttribute("refresh_nav");
    });
    dd.on("beforeclose", function () {
      this.exitCompleted(workerId, appId, refreshNav, redirect);
    }.bind(this));
    
    dd.setTitle("Progress");
    dd.setPreference('sysparm_function', 'uninstallApplication');
    dd.setPreference('sysparm_sys_id', appId);
    var del = $('cb') ? !$('cb').checked : true; // if !retained, delete
    dd.setPreference('sysparm_delete_all', del);
    dd.setPreference('sysparm_progress_name', "Uninstalling application");
    dd.setPreference('sysparm_ajax_processor', 'com.snc.apps.AppsAjaxProcessor');
    dd.setPreference('sysparm_show_done_button', 'true');
    
    dd.render();
    GlideDialogWindow.get().destroy();
    
    return dd;
  },
  
  exit: function() {
    GlideDialogWindow.get().destroy();
  },
  
  singleExit: function() {
	  GlideDialogWindow.prototype.locate(this).destroy();
	  return false;
  },

  reconfirm: function (message) {
    var keyword = new GwtMessage().getMessage("uninstall").toLowerCase();
    var fieldMsg = new GwtMessage().format("Type the word \"{0}\" here", keyword);
    var name = prompt(message, fieldMsg);
    if (!name || name.toLowerCase() != keyword)
      return false;
    
    return true;
  },
  
  exitCompleted: function (workerId, appId, refreshNav, redirect) {
    // If we changed any menu structure, refresh the nav
    if(refreshNav)
      CustomEvent.fireTop('navigator.refresh');
    
    // Always refresh app picker when we uninstall an app
    var top = getTopWindow();
    if (typeof top.g_application_picker != 'undefined')
      top.g_application_picker.fillApplications();
    
	if (redirect) {
		var url = new GlideURL('$myappsmgmt.do'); // was sys_store_app_list.do
		window.location.replace(url.getURL());
	} else
		window.location.reload();
  }
});

$('uninstall_app_dialog_container').observe('keyup', test);
function test(event) {
  if (event.keyCode == 27) // escape
    gUninstallAppDialog.exit();
}

var gUninstallAppDialog = new UninstallAppDialog();