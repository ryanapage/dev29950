function setupStream(iframe) {
    var cs = new ChannelClient(iframe);
    cs.stream();
}

addLoadEvent(function() {
    // remove timer watch at bottom
	new CookieJar().set('g_startTime', 0);
});