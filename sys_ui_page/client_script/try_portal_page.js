function goThere(suffix, id) {
	var url = new GlideURL('/' + suffix + '/');
	url.addParam('id', id);
	var w = getTopWindow();
	GlideDialogWindow.get().destroy();
	w.popupOpenFocus(url.getURL(), 'try_portal_page', 950, 700, '', false, false);
}

