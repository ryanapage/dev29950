addLoadEvent(focusOnCorrectField);
setViewportForMobile();
$j('title').text("${gs.getMessage('Password Reset - Identify')}");

var wasSubmitted = false;

var displayCaptcha = false;
var googleCaptcha = true;

function focusOnCorrectField() {
	gel('sysparm_user_id').focus();

	if (gel('sysparm_captcha') != undefined) {
		displayCaptcha = true;
		googleCaptcha = false;
	} else {
		if (gel('google_captcha') != undefined) {
			displayCaptcha = true;
			googleCaptcha = true;
		}
	}
}

function verifyIdentity() {
	if (wasSubmitted) {
		return false;
	}
	wasSubmitted = true;
	
	var foundError = false;
	
	var user_id = gel('sysparm_user_id').value;
	
	if (user_id == "") {
		displayFieldError('retype_user_id', getMessage('Enter valid identity'), 'user_id_form_group');
		foundError = true;
	}
	
	if (displayCaptcha) {
		if (!googleCaptcha) {
	var captchaElem = gel('sysparm_captcha');
	if ((captchaElem != undefined) && (captchaElem.value == "")) {
		displayFieldError('retype_captcha', getMessage('Characters do not match, try again'), 'captcha_form_group');
		foundError = true;
	}
		}		
	}
	
	if (foundError) {
		wasSubmitted = false;
		return false;
	}
	
	var ga = new GlideAjax('PwdAjaxVerifyIdentity');
	
	ga.addParam('sysparm_name', 'verifyIdentity');
	ga.addParam('sysparm_process_id', gel('sysparm_process_id').value);
	ga.addParam('sysparm_user_id', user_id);
	
	if (displayCaptcha) {
		if (!googleCaptcha) {
			// Add built-in captcha
			var captchaElem = gel('sysparm_captcha');
			if (captchaElem != undefined && captchaElem!= null) {
				ga.addParam('sysparm_captcha', captchaElem.value);
			}
		}
	
		else {  // googleCaptcha
			// Add Google captcha
			var grc = grecaptcha.getResponse();
			if ((grc == "") || (grc == undefined)) {
				wasSubmitted = false;
				displayFieldError('retype_captcha', getMessage('Please select the checkbox above'), 'captcha_form_group');
				return false;
			}
			ga.addParam('sysparm_captcha', grc);
		}
	}
	
	// add csrf token for ajax
	ga.addParam('sysparam_pwd_csrf_token', findCSRFElement().value);
	
	ga.getXML(PwdAjaxIdentityParse);
	
	return false;
}

function PwdAjaxIdentityParse(response) {
	
	//Let's handle security
	handleSecurityFrom(response);
	
	var answer = response.responseXML.documentElement.getAttribute("answer");
	
	if (answer == "ok") {
		wasSubmitted = false;
 
		submitWithOK('$pwd_verify.do');
	}
	else if (answer == "locked") {
		wasSubmitted = false;
 
		submitWithBlock('$pwd_error.do',answer);
	}
	else if (answer == "block") {
		wasSubmitted = false;
 
		submitWithBlock('$pwd_error.do',answer);
	} else if(answer == "ldap_user") {
		wasSubmitted = false;
 
		submitWithBlock('$pwd_error.do',answer);
	} else {
		if (displayCaptcha) {
			var captchaElem = gel("retype_captcha");
			if (captchaElem != undefined) {

				refreshCaptcha();

				if (answer.indexOf("captcha") >= 0) {
					if (!googleCaptcha) {
						displayFieldError('retype_captcha', getMessage('Characters do not match, try again'), 'captcha_form_group');
					} else {
						displayFieldError('retype_captcha', getMessage('Invalid captcha response'), 'captcha_form_group');					
					}
				}
				else {
					clearFieldError('retype_captcha', 'captcha_form_group');
				}
			}
		}
			
		/* Invalid user. Redirect to the error page to be consistent with windows app.
		   Since we're obfuscating the granular user errors such as "user not in process"
		   for security reasons, just showing "Invalid user" may trigger a lot of incidents
		   By redirecting to the error page, we can show a detailed list of possible reasons
		   that the user was invalid. This is a tradeoff with UX since the user will have to 
		   go back to the previous page for every identification attempt. 
		 */
		if (answer.indexOf("user") != -1) 
			submitWithBlock('$pwd_error.do', answer);
		else 
			clearFieldError('retype_user_id', 'user_id_form_group');
		
		wasSubmitted = false;
	}
}

function isEmailAddress(str) {
	var email_pattern = /^\w+(\.\w+)*@\w+(\.\w+)+$/;
	return str.match(email_pattern);
}

function userIDRetype() {
	clearFieldError('retype_user_id', 'user_id_form_group');
}

function userCaptchaRetype() {
	clearFieldError('retype_captcha', 'captcha_form_group');
}

function refreshCaptcha() {
	if (!googleCaptcha) {
		// Built-in Captcha:
		
	var captchaElem = gel("captcha_image");
	if (captchaElem != undefined) {
		captchaElem.src = "pwd_jcaptcha.do?" + new Date().getTime();
	}
	} else {
		// Google Captcha:
		grecaptcha.reset();
	}
	return false;
}

function errorImage() {
	return '<img src="images/outputmsg_error.gifx" alt="Error Message" />';
}

function displayFieldError(field, message, form_group) {
	fieldElem = gel(field);
	if (fieldElem != undefined) {
		fieldElem.innerHTML = errorImage() + message;
		fieldElem.style.display = '';
	}
	
	$j('#' + form_group).addClass("has-error");
}

function clearFieldError(field, form_group) {
	fieldElem = gel(field);
	if (fieldElem != undefined) {
		fieldElem.innerHTML = '';
		fieldElem.style.display = 'none';
	}
	
	$j('#' + form_group).removeClass("has-error");

}

function switchButton(buttonToHideId, buttonToShowId) {
	var buttonToHide = gel(buttonToHideId);
	var buttonToShow = gel(buttonToShowId);
	if (buttonToHide != undefined) {
		buttonToHide.style.display = 'none';
	}
	if (buttonToShow != undefined) {
		buttonToShow.style.display = 'inline-block';
	}
}
