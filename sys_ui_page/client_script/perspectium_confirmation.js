var route;
var actionTitle;
var actionDescription;
var scriptName;
var scriptFunc;
var scriptAction;

onLoad(); // init

function onLoad() {}
    var caller = window.location.toString().toQueryParams().sysparm_action; // parse query string
    // description template: https://bitbucket.org/ryanapage/ryan-perspectium-dump/src/bb124641578eeedab682d41c125b7a79fa1151be/perspectium_confirmation_uipage_description.html?at=master&fileviewer=file-view-default 
	switch (caller) {
		case 'start_all_jobs':
            scriptName = "PerspectiumStartAllJobs";
            scriptFunc = "startAllJobs";
            $j("#action").text('Start All Jobs');
            $j("#description").html("<span>This will start the Perspectium scheduled jobs. These jobs can be found under \"All Scheduled Jobs\".<span/><br/><br/><span class=\"subtitle\"><b>What will be done</b></span><br/><ul><li>Enable all Sharing and Subscribing interactions with the Perspectium Message Broker Service</li><li>Enable various data cleaner, monitoring, and processing jobs including Observer.</li><li><i>Note: Running this while all jobs are active will have no effect.</i></li></ul><br/><span class=\"subtitle\"><b>What will not be done</b></span><br/><ul><li>Will not modify any Share or Subscribe configurations you have set up.</li><li>Will not modify any of the following:</li><ul><li>Shared Queues</li><li>Subscribed Queues</li><li>Dynamic Shares</li><li>Subscribes</li><li>Scheduled Bulk Shares</li></ul></ul>");
            break;
		case 'stop_all_jobs':
            scriptName = "PerspectiumStopAllJobs";
            scriptFunc = "stopAllJobs";
            $j('#action').text('Stop All Jobs');
            $j('#description').html("<span>This will stop the Perspectium Scheduled jobs. These jobs can be found under \"All Scheduled Jobs\".</span><br/><br/><span class=\"subtitle\"><strong>What will be done</strong></span><br/><ul><li>Disable all Sharing and Subscribing interactions with the Perspectium Message Broker Service.</li><li>Disable various data cleaner, monitoring, and processing jobs including Observer.</li><li>Running this while all jobs are inactive will have no effect.</li></ul><br/><span class=\"subtitle\"><strong>What will not be done</strong></span><br/><ul><li>Will not modify Share or Subscribe configurations that have been set up.</li><li>This will not modify any of the following: (i.e active configurations will remain active)</li><ul><li>Shared Queues</li><li>Subscribed Queues</li><li>Subscribes</li><li>Dynamic Shares</li><li>Scheduled Bulk Shares</li></ul><br/><span><i><strong>Note:</strong> This means that you may still be generating messages via Scheduled Bulk Shares and Dynamic Shares which will be queued up in the Outbound Message tables. Those messages will remain there until the Perspectium Outbound jobs are re-activated.</i></span></ul>");
            break;
		case 'finish_install':
            scriptName = "PerspectiumFinishInstall";
            scriptFunc = "finishInstall";
            $j('#action').text('Finish Install');
            $j("#description").html("<span>Running this will complete the processes for installing the Perspectium Update Sets onto this instance.</span><br/><br/><span class=\"subtitle\"><strong>What will be done</strong></span><br/><ul><li>Initialize Perspectium properties</li><li>Run some validation tests</li><li>Start Perspectium jobs</li></ul><br/><span class=\"subtitle\"><strong>When you should run this script</strong></span><ul><li>If you are installing multiple Perspectium Update Sets, this should be run at the end.</li><li>This is <strong>necessary</strong> for first time installation and following any Perspectium Update Set version upgrades.</li></ul>");
            break;
        case 'start_all_shares':
            scriptName = "PerspectiumToggleShares";
            scriptFunc = "start";
            $j('#action').text('Start All Dynamic/Scheduled Bulk Shares');
            $j('#description').html("<span>This will start all Dynamic Shares and Scheduled Bulk Shares<span/><br/><br/><span class=\"subtitle\"><b>What will be done</b></span><br/><ul><li>All Dynamic Shares will be set to an active state.</li><li>All Scheduled Bulk Shares will be set to an active state.</li></ul><br/><span class=\"subtitle\"><b>What will not be done</b></span><br/><ul><li>Dynamic Share configurations will not be changed.</li><li>Scheduled Bulk Share configurations will not be changed.</li></ul>");
            break;
        case 'stop_all_shares':
            scriptName = "PerspectiumToggleShares";
            scriptFunc = "stop";
            $j('#action').text('Stop All Dynamic/Scheduled Bulk Shares');
            $j('#description').html("<span>This will stop all Dynamic Shares and Scheduled Bulk Shares<span/><br/><br/><span class=\"subtitle\"><b>What will be done</b></span><br/><ul><li>All Dynamic Shares will be set inactive.</li><li>All Scheduled Bulk Shares will be set inactive.</li></ul><br/><span class=\"subtitle\"><b>What will not be done</b></span><br/><ul><li>Dynamic Share configurations will not be changed.</li><li>Scheduled Bulk Share configurations will not be changed.</li><li>Running Bulk Shares will not be stopped.</li></ul>");
            break;
		case 'reset_dynamic_share_rules':
			scriptName = "PerspectiumRunConfirmationPage";
			scriptFunc = "run";
			scriptAction = "resetDynamicShareRules";
			$j('#action').text('Reset Dynamic Share Rules');
			$j("#description").html("<span>This will delete and re-create all of the Business Rules corresponding to your Dynamic Sharing.<span/><br/><br/><span class=\"subtitle\"><b>What will be done</b></span><br/><ul><li>Delete all Business Rules corresponding to standard Dynamic Sharing</li><li>Delete all Business Rules corresponding to the Delete Audit Listener of Dynamic Sharing</li><li>Recreate all the Business Rules as necessary per active Dynamic Share</li></ul><br/><span class=\"subtitle\"><b>What will not be done</b></span><br/><ul><li>Will not create Business Rules for inactive Dynamic Shares</li><li>Will not modify your Dynamic Share configurations</li><br/><span><i><strong>Note:</strong> Due to ServiceNow's platform handling these Business Rules will be created in the current domain of the user executing this.  This means the Replication will only take effect in that domain.  If you are using a domain separated instance it is recommended to change your domain to \"global\" prior to executing this.</i></span></ul>");
			break;
}

/**
 * Go back one page.
 **/
function goBack() {
	window.history.back();
}

/**
 * onClick function for confirm button
 **/
function confirm() {
	// create ajax call
	var ga = new GlideAjax(scriptName);
	ga.addParam("sysparm_name", scriptFunc);
	
	if (scriptAction != undefined || scriptAction != 'undefined') {
		ga.addParam("sysparm_action", scriptAction);
	}
	
	ga.getXML(callback); // pass callback function
	
	// define alert element
	var mAlert = document.createElement("div");
	mAlert.setAttribute("id","alertID");
	mAlert.className = "notification notification-success";
	mAlert.innerHTML = "Processing action. Please wait...";
	mAlert.align = "center";
	document.getElementById("mAlert").appendChild(mAlert);
	
	// deactivate confirm button until response
	$j('#confirmBtn').attr('disabled', true);	
}

// callback for ajax call
function callback(response) {
	var status = JSON.parse(response.responseXML.documentElement.getAttribute("answer"));
	var SUCCESS = 200;
	// var FAILED = 500; // could be used if we need more than 2 status codes

	// reactivate confirm button
	$j('#confirmBtn').attr('disabled', false);

	if (status.code == SUCCESS) {
		alert('Processing action completed successfully.\n\n You will automatically be redirected to your previous page.');
		goBack();
	} else {
		// build string for displaying errors
		var errors = "\n\nErrors\n------------------------------\n";
		for (var i = 0; i < status.errors.length; i++) {
			errors += status.errors[i] + '\n\n';
		}

		alert('Something went wrong. Please try again...' + errors);
	}
}