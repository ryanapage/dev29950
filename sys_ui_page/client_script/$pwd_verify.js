// Show first form, others have display: none from the XML
document.forms[0].style.display = "";

setViewportForMobile();
$j('title').text("${gs.getMessage('Password Reset - Verify')}");

var wasSubmitted = false;
var currentFormNumber = 0;

function verifyResponse(formNumber) {
	
	currentFormNumber = parseInt(formNumber);
	var currentForm = document.forms[currentFormNumber];
	
	if (!_checkRequiredInputs(currentForm))
		return false;
	
	if (wasSubmitted) {
		return false;
	}
	wasSubmitted = true;
	
	var ga = new GlideAjax('PwdAjaxVerifyProcessor');
	ga.addParam('sysparm_name','checkVerification');
	ga.addParam('sysparm_verification_id', currentForm.id.split('.')[1]);
	ga.addParam('sysparm_silent_request', 'true');
	_addFormValuesToAjax(ga, currentForm);
	_addFormValuesToAjax(ga, gel("pwd_master_form"));
	
	// add CSRF token for AJAX handling.
	ga.addParam('sysparam_pwd_csrf_token', findCSRFElement().value);
	ga.getXML(PwdAjaxVerifyParse.bind(this), null, 'continue');
	
	return false;
}

function verifyResponses(formNumber) {
	
	currentFormNumber = parseInt(formNumber);
	var currentForm = document.forms[currentFormNumber];
	if (!_checkRequiredInputs(currentForm))
		return false;
	
	if (wasSubmitted) {
		return false;
	}
	wasSubmitted = true;
	
	var ga = new GlideAjax('PwdAjaxVerifyProcessor');
	ga.addParam('sysparm_name','pwdAjaxVerifyProcessor');
	for (var i = 0; i < document.forms.length; ++i)
		_addFormValuesToAjax(ga, document.forms[i]);
	
	// add CSRF token for AJAX handling.
	ga.addParam('sysparam_pwd_csrf_token', findCSRFElement().value);
	ga.addParam('sysparm_silent_request', 'true');
	ga.getXML(PwdAjaxVerifyParse.bind(this), null, 'submit');
	
	return false;
}

function _addFormValuesToAjax(ga, form) {	
	var form_inputs = form.getElementsByTagName('input');
	for (var i = 0; i < form_inputs.length; i++) {
		if (form_inputs[i].type == 'radio') {
			if (form_inputs[i].checked) {
				// Add only the value of the checked radio button
				ga.addParam(form_inputs[i].name, form_inputs[i].value);
			}
		} else {
			ga.addParam(form_inputs[i].id, form_inputs[i].value);
		}
	}
}

function PwdAjaxVerifyParse(response, action) {
	var answer = response.responseXML.documentElement.getAttribute("answer");
	
	//let's handle security from AJAX response.
	handleSecurityFrom(response);
	
	if (answer == "ok") {
		if (action == 'continue')
			_showNextForm();
		else if (action == 'submit') {
			if('$[jvar_email_reset_url]' == 'true') {
				var ga = new GlideAjax("PasswordResetUtil");
				ga.addParam("sysparm_name","sendResetEmail");
				ga.addParam('sysparam_pwd_csrf_token', findCSRFElement().value);
				ga.getXML(showConfirmationPage);
			} else
				submitWithOK('$pwd_new.do');
			
		}
	}
	else if (answer == "block") {
		submitWithBlock('$pwd_error.do', answer);
	}
	else {
		var retryResp = response.responseXML.getElementsByTagName("retry");
		var retry = "0";
		if ( ! (retryResp == undefined || retryResp == null || retryResp.length == 0))
			retry = retryResp[0].getAttribute("count");
		displayError(new GwtMessage().getMessage("Invalid response, cannot verify user ({0}). Number of attempts: {1}. Try again", answer, retry));
	}
	wasSubmitted = false;
}

function showConfirmationPage(response){
    handleSecurityFrom(response);
    
	var answer = response.responseXML.getElementsByTagName("result");
	var result = answer[0].getAttribute("result");
	if(result == "true")
		submitWithOK('$pwd_confirm.do');
	else
		submitWithBlock('$pwd_error.do');
}
	
function _showNextForm() {
	clearError();
	
	var currentForm = document.forms[currentFormNumber];
	currentForm.style.display = "none";
	var nextForm = document.forms[currentFormNumber + 1];
	nextForm.style.display = "";
}

function _checkRequiredInputs(formEle) {
	var requiredInputs = formEle.querySelectorAll("input[required]");
	for (var i = 0; i < requiredInputs.length; i++) {
		if (!requiredInputs[i].value.trim()) {
			displayError(new GwtMessage().getMessage('All fields must be filled'));
			requiredInputs[i].value = "";
			return false;    
		}
	}
	return true;
}
