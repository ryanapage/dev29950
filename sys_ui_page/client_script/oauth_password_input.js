function initiateOAuthFlow() {
	var username = gel("username").value;
	var password = gel("password").value;
	var requestor = gel("requestor").value;
	var requestor_context = gel("requestor_context").value;
	var oauth_provider_profile = gel("oauth_provider_profile").value;
	var oauth_provider_id = gel("oauth_provider_id").value;
	
	if(!username) {
		showError('Username is empty');
		return;
	}
	
	if(!password) {
		showError('Password is empty');
		return;
	}
	
	if(!requestor) {
		showError('Requestor is empty');
		return;
	}
	
	if(!requestor_context) {
		showError('Requestor Context is empty');
		return;
	}
	
	if(!oauth_provider_profile && !oauth_provider_id) {
		showError('OAuth Provider Profile is empty');
		return;
	}
	
	var gaOAuthPasswordGrantType = new GlideAjax('OAuthPasswordGrantType');
	gaOAuthPasswordGrantType.addParam('sysparm_name', 'initiateTokenFlow');
	gaOAuthPasswordGrantType.addParam('oauth_requestor', requestor);
	gaOAuthPasswordGrantType.addParam('oauth_requestor_context', requestor_context);
	gaOAuthPasswordGrantType.addParam('oauth_provider_profile', oauth_provider_profile);
	gaOAuthPasswordGrantType.addParam('oauth_provider_id', oauth_provider_id);
	gaOAuthPasswordGrantType.addParam('username', username);
	gaOAuthPasswordGrantType.addParam('password', password);
	
	// Invoke the GlideAjax with a callback function
	gaOAuthPasswordGrantType.getXML(parseAjaxResponse);
	return;
}

function parseAjaxResponse(response) {
	var result = response.responseXML.getElementsByTagName("result");
	var isToken = result[0].getAttribute("isToken");
	var responseCode = result[0].getAttribute("responseCode");
	var errorMessage = result[0].getAttribute("errorMessage");
	if(isToken == 'true') {
		showSuccess('OAuth token flow completed successfully');
		refreshOpener();
		return;
	} else {
		showError('OAuth flow failed. Verify the configurations and try again. Error detail:' + errorMessage);
		return;
	}
}

function closeSelf() {
	if (window.opener) {
		window.close();
	}
}

function refreshOpener() {
	if (window.opener) {
		window.opener.location.reload();
	}
}

function showError(msg){
	gel("divNotification").setAttribute('class', "notification notification-error");
    showNotification(msg);
}

function showSuccess(msg){
	gel("divNotification").setAttribute('class', "notification notification-success");
	showNotification(msg);
}

function showNotification(msg){
	gel("divNotification").style.display = "block";
	gel("divNotification").innerHTML = new GwtMessage().getMessage(msg);
	gel("output_messages").style.display = "none";//Clear the error from server if one exists so that we don't show the error twice
}