function setPageSize(pageSize) {
	_setUrl("sys_nav_count",pageSize);
}

function setStartRow(startRow) {
	_setUrl("sysparm_current_row",startRow);
}

function changeRowCount(element){
	setPageSize(element.value);
}

function changeRowCountFromMenu(value){
	setPageSize(value);
}

var timerHandle = null;
function gotoRow(pageSize,totalRows,element){
	if (timerHandle != null)
		clearTimeout ( timerHandle );
	timerHandle = setTimeout("_gotoRow("+pageSize+","+totalRows+",'"+element.id+"')",1000);
}

function _gotoRow(pageSize,totalRows,elementId){		
	var startRow = document.getElementById(elementId).value;
	if (isNaN(startRow))
		startRow = 1;	
	var pageStart = startRow;
	if (pageStart > totalRows)
		pageStart = totalRows - pageSize + 1;
	if (pageStart <= 0)
		pageStart = 1;
	setStartRow(pageStart);
}

function nextPage(pageSize,totalRows,startRow){
	var pageStart = startRow + pageSize;
	if (pageStart > totalRows)
		pageStart = totalRows - pageSize + 1;
	if (pageStart <= 0)
		pageStart = 1;
	setStartRow(pageStart);
}

function lastPage(pageSize,totalRows,startRow){
	var pageStart = totalRows - pageSize + 1;
	if (pageStart <= 0)
		pageStart = 1;
	setStartRow(pageStart);
}

function previousPage(pageSize,totalRows,startRow){
	var pageStart = startRow - pageSize;
	if (pageStart <= 0)
		pageStart = 1;
	setStartRow(pageStart);
}

function firstPage(pageSize,totalRows,startRow){
	setStartRow(1);
}

function _setUrl(key,value) {
	var url = _getUrlExcludingParam(key);
	if (url.hasParams)
		url.url += "&";
	else
		url.url += "?";
	url.url += key+'='+value;
	url.url += "&sysparm_rollbased="+roleBased;
	window.location.href = url.url;

}

function _getUrlExcludingParam(paramName) {
	var seperator = "";
	var hasParams = false;
	if (window.location.pathname.substring(0,1) != "/")
		seperator = "/";

	var baseURL = window.location.protocol + "//" + window.location.host + seperator + window.location.pathname;
	seperator = "?";
	var parts = window.location.href.replace(/[?&]+([^=&]+)=?([^&]*)/gi, function(full,key,value) {
		if ((key != paramName) && (key != "sysparm_rollbased")) {
			baseURL += seperator + key;
			if ((value != null) &&(value != ""))
				baseURL += "=" + value;
			seperator = "&";
			hasParams = true;
		}
	});
	return {
		url : baseURL,
		hasParams : hasParams
	};
}


