addLoadEvent(function() {
    // clear the slush bucket
    slush.clear();
    slush.ignoreDuplicates = true;

    var ga = new GlideAjax('MIDServerAjax');
    ga.addParam('sysparm_name','getSlushValues');
    ga.getXML(loadResponse);
});

function loadResponse(response) {
    // Process the return XML document and add groups to the left select
    var items = response.responseXML.getElementsByTagName("agent");
    if (items.length == 0) {
        errorNotification("No valid MID Servers are available");
        return;
    }

    // Loop through item elements and add each item to left slush bucket
    for (var i = 0; i < items.length; i++) {
        var item = items[i];
        slush.addLeftChoice(item.getAttribute('sys_id'), item.getAttribute('name'));
    }
}

function handleRedirect(statusId) {
    window.location.href = "/automation_status_set.do?sys_id=" + statusId;
}

function handleOK() {
    var selectedAgents = slush.getValues(slush.getRightSelect());
    if (selectedAgents.length <= 0) {
        errorNotification("You must select at least 1 MID Server");
        return;
    }

    var ga = new GlideAjax("MIDServerAjax");
    ga.addParam('sysparm_name', 'triggerSubnetDiscovery');
    ga.addParam('sysparm_agents', selectedAgents.join(","));
    ga.getXMLAnswer(handleRedirect);
}

function handleCancel() {
     window.location.href = document.referrer;
}

function errorNotification(msg) {
    $j('#mid_range_assign_error_span').text(getMessage(msg));
    $j('#mid_range_assign_error_div').show();
}

function closeNotification() {
    $j('#mid_range_assign_error_div').hide();
    return false;
}