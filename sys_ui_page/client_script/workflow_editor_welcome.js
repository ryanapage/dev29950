$j(document).ready(function(){
	$j('#page_timing_div').hide();
	$j('.wflistbutton').click(function(){
		var section = $j(this).attr('aria-controls');
		$j('.wflistbutton').removeClass('state-active');
		$j('.segmented-pane').hide();
		$j(this).addClass('state-active');
		$j('#'+section).show();
	});

	if (top && top.workflowIDE) {
		var lists = GlideList2.getListsForTable('wf_workflow_version');
		if (typeof top.workflowIDE.registerRefreshWelcome === "function")
			top.workflowIDE.registerRefreshWelcome(function(){
				lists.each(function(e){e.refreshWithOrderBy()})
			});
		$j('div.tabs2_list').delegate('tr[record_class="wf_workflow_version"]','click',function(){
			var wfv_id = $j(this).attr('sys_id');
			var wf_name = '' + $j(this).children("td.vt")[0].innerHTML;
			top.workflowIDE.newCanvas(wf_name, wfv_id);
		});
	 }
});
