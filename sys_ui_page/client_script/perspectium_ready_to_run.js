var perspectiumDisclaimer = "<!--This configuration file was automatically generated by the ServiceNow instance //the instance name//. Any modifications may cause the intended behavior to change.  Please contact Perspectium Support or consult the Perspectium wiki for any questions or concerns.-->";
var ENCODING = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"no\"?>";
var SERVICENOW_DIRECT = "servicenow_direct";
var PERSPECTIUM_READY_TO_RUN = "perspectium_ready_to_run";
var AGENT_NAME_PLACEHOLDER = "{AGENT_NAME_PLACEHOLDER}";
var INPUT_TAG = "INPUT";
var MIN_ENCRYPTION_LEN = 24;
var MYSQL_PORT = "3306";
var MSSQL_PORT = "1433";
var ORACLE_PORT = "1521";
var wrapperAgentName = "";
var wrapperFile;
var currentInstance;
var oracleActive = false;

// load in wrapper.conf files and JSON template for creating agent.xml
var snReadyToRunJSON;
var linuxWrapper;
var windowsWrapper;
var macWrapper;
ScriptLoader.getScripts(["psp_ready_to_run_agent.jsdbx", "psp_ready_to_run_wrapper.jsdbx"], function() {
	snReadyToRunJSON = psp_ready_to_run_agent.snReadyToRunJSON;
	linuxWrapper = psp_ready_to_run_wrapper.linuxWrapper;
	windowsWrapper = psp_ready_to_run_wrapper.windowsWrapper;
	macWrapper = psp_ready_to_run_wrapper.macWrapper;
});

// add any new checkboxes here
var CHECKBOXES = [
	document.getElementById("incident-checkbox")
];

var formType = window.location.toString().toQueryParams().sysparm_form_type;
var isDryRun = window.location.toString().toQueryParams().sysparm_is_dry_run;
var isEncrypted = false;
var formElements = getFormElements();
setEventListeners();
onLoad();

function onLoad() {
	correctParams = configureForm(formType);
	if (correctParams == false || correctParams == "false") {
		return;
	}
	loadFormValues();

	// use default key in replicator properties
	if (assertEncryptionKey()) {
		formElements.encryptionKeyElement.value = getEncryptionKey();
	}
}

function configureForm(formType) {
	switch (String(formType)) {
		case SERVICENOW_DIRECT:
			configureServiceNowDirectForm();
			return true;

		case PERSPECTIUM_READY_TO_RUN:
			configureReadyToRun();
			return true;

		default:
			showInvalidPage();
			return false;
	}
}

// SN Direct specific form configurations
function configureServiceNowDirectForm() {
	// hide mbs elements
	$j("#page-title").text("ServiceNow Direct");
	$j(".hide-servicenow-direct").hide();
}

// R2R specific form configurations
function configureReadyToRun() {
	// no specific configurations at this time
}

function showInvalidPage() {
	document.getElementById("main-form").style.display = "none";
	document.getElementById("invalid-params-display").style.display = "block";
}

function loadFormValues() {
	var ga = new GlideAjax("PerspectiumReadyToRun");
	var tableName = "u_psp_configuration";
	ga.addParam("sysparm_name", "loadConfiguration");
	ga.addParam("sysParm_formType", formType);
	ga.addParam("sysParm_tableName", tableName);

	ga.getXML(function(response) {
		loadResponseCallBack(response);
	});
}

function loadResponseCallBack(response) {
	var loadFormValues = response.responseXML.documentElement.getAttribute("answer");

	loadFormValues = loadFormValues.evalJSON();
	if (loadFormValues.isEmpty == true || loadFormValues.isEmpty == "true") {
		formElements.snInstanceElement.value = loadFormValues.instanceName;
		currentInstance = loadFormValues.instanceName;
		return;
	}
	switch (String(formType)) {
		case SERVICENOW_DIRECT:
			loadServiceNowDirectForm(loadFormValues);
			checkDBType();
			currentInstance = loadFormValues.instanceName;
			break;

		case PERSPECTIUM_READY_TO_RUN:
			loadReadyToRunForm(loadFormValues);
			checkDBType();
			currentInstance = loadFormValues.instanceName;
			break;
	}
	return;
}

function loadServiceNowDirectForm(loadFormValues) {
	formElements.snInstanceElement.value = loadFormValues.instanceName;
	formElements.snUserNameElement.value = loadFormValues.snUsername;
	formElements.snPasswordElement.value = loadFormValues.snPassword;
	formElements.dbTypeElement.value = loadFormValues.databaseType;
	formElements.dbServerUrlElement.value = loadFormValues.databaseUrl;
	formElements.dbPortElement.value = loadFormValues.databasePort;
	formElements.dbUserNameElement.value = loadFormValues.databaseUsername;
	formElements.dbPasswordElement.value = loadFormValues.databasePassword;
	formElements.dbNameElement.value = loadFormValues.databaseName;
}

function loadReadyToRunForm(loadFormValues) {
	formElements.snInstanceElement.value = loadFormValues.instanceName;
	formElements.snUserNameElement.value = loadFormValues.snUsername;
	formElements.snPasswordElement.value = loadFormValues.snPassword;
	formElements.mbsServerUrlElement.value = loadFormValues.mbsServerUrl;
	formElements.mbsUserNameElement.value = loadFormValues.mbsUserName;
	formElements.mbsPasswordElement.value = loadFormValues.mbsPassword;
	formElements.dbTypeElement.value = loadFormValues.databaseType;
	formElements.dbServerUrlElement.value = loadFormValues.databaseUrl;
	formElements.dbPortElement.value = loadFormValues.databasePort;
	formElements.dbUserNameElement.value = loadFormValues.databaseUsername;
	formElements.dbPasswordElement.value = loadFormValues.databasePassword;
	formElements.dbNameElement.value = loadFormValues.databaseName;
}

// clear current values in forms
function clearForms() {
	for (var element in formElements) {
		if (formElements[element].tagName == INPUT_TAG) {
			formElements[element].value = "";
		}
	}
}

function submitForm() {
	// validate field values are correct else return early
	if (!this.validateFormValues(formElements)) {
		return;
	}

	var formValues = getFormValues(formElements);
	wrapperAgentName = formValues.agentName;

	// define wrapper files
	wrapperFile = getWrapper(formValues.osType, wrapperAgentName);
	postFormValues(formValues);
}

function postFormValues(formValues) {
	var formValueString = JSON.stringify(formValues);
	var ga = new GlideAjax("PerspectiumReadyToRun");
	ga.addParam("sysparm_name", "run");
	ga.addParam("sysparm_formValues", formValueString);
	ga.addParam("sysparm_formType", formType);

	ga.getXML(function(response) {
		responseCallBack(response, formValues);
	});
}

function responseCallBack(response, formValues) {
	var schemas = response.responseXML.documentElement.getAttribute("answer");

	schemas = schemas.evalJSON();

	// backend error
	if (schemas.hasOwnProperty("error")) {
		alert(schemas.status);
		return;
	}

	// success message
	if (formType === PERSPECTIUM_READY_TO_RUN) {
		alert("Success! A Shared Queue and Dynamic Share have been created and a Bulk Share has been scheduled.");
	} else {
		alert("Success! A Dynamic Share has been created and a Bulk Share has been scheduled.");
	}

	generateAgentXMLByType(formValues, schemas);
}

function generateAgentXMLByType(formValues, schemas) {
	switch (String(formType)) {
		case SERVICENOW_DIRECT:
			populateServiceNowDirect(formValues);
			var xmlObj = formatXml(buildXml(snReadyToRunJSON));
			zipFolder(formValues, schemas, xmlObj);
			break;
		case PERSPECTIUM_READY_TO_RUN:
			populateReadyToRun(formValues);
			var xmlObj = formatXml(buildXml(snReadyToRunJSON));
			zipFolder(formValues, schemas, xmlObj);
			break;
	}
}

function zipFolder(formValues, schemas, xml_formatted) {
	var zip = new JSZip();
	zip.file("conf/agent.xml", xml_formatted);
	zip.file("conf/wrapper.conf", wrapperFile);
	for (var i = 0; i < schemas.length; i++) {
		zip.file("bin/" + formValues.snInstance + ".service-now.com.schemas_directory/" + schemas[i].tableName + ".xml", schemas[i].tableSchema);
	}
	zip.generateAsync({
			type: "blob"
		})
		.then(function(content) {
			// see FileSaver.js
			saveAs(content, formValues.agentName + ".zip");
		});
}

/**
 * return a JSON of all the form values
 */
function getFormElements() {
	var formElements = {
		tableNameElement: CHECKBOXES,
		encryptionKeyElement: document.getElementById("encryption-key"),
		snInstanceElement: document.getElementById("instance-sn"),
		snUserNameElement: document.getElementById("username-sn"),
		snPasswordElement: document.getElementById("password-sn"),
		mbsServerUrlElement: document.getElementById("server-mbs"),
		mbsUserNameElement: document.getElementById("username-mbs"),
		mbsPasswordElement: document.getElementById("password-mbs"),
		osTypeElement: document.getElementById("type-os"),
		dbTypeElement: document.getElementById("type-db"),
		dbServerUrlElement: document.getElementById("server-db"),
		dbPortElement: document.getElementById("port-db"),
		dbSIDElement: document.getElementById("sid-db"), // only used for Oracle
		dbUserNameElement: document.getElementById("username-db"),
		dbPasswordElement: document.getElementById("password-db"),
		dbNameElement: document.getElementById("name-db"),
		executeBulkShareElement: document.getElementById("bulkshare-checkbox"),
		saveReadyToRunFormElement: document.getElementById("save-checkbox")
	};

	return formElements;
}

function encryptValues(formValues) {
	formValues.mbsPassword = "encrypt:" + formValues.mbsPassword;
	formValues.snPassword = "encrypt:" + formValues.snPassword;
	formValues.dbPassword = "encrypt:" + formValues.dbPassword;
	return formValues;
}

function getFormValues(formElements) {
	// values in forms
	var formValues = {
		agentName: formElements.snInstanceElement.value + "_agent",
		tableName: formElements.tableNameElement.map(function(element) {
			return element.value;
		}),
		encryptionKey: formElements.encryptionKeyElement.value,
		snInstance: formElements.snInstanceElement.value,
		snUserName: formElements.snUserNameElement.value,
		snPassword: formElements.snPasswordElement.value,
		mbsServerUrl: formElements.mbsServerUrlElement.value,
		mbsUserName: formElements.mbsUserNameElement.value,
		mbsPassword: formElements.mbsPasswordElement.value,
		osType: formElements.osTypeElement.value,
		dbType: formElements.dbTypeElement.value,
		dbServerUrl: formElements.dbServerUrlElement.value,
		dbPort: formElements.dbPortElement.value,
		dbSID: formElements.dbSIDElement.value, // only used for Oracle
		dbUserName: formElements.dbUserNameElement.value,
		dbPassword: formElements.dbPasswordElement.value,
		dbName: formElements.dbNameElement.value,
		executeBulkShare: formElements.executeBulkShareElement.checked,
		saveReadyToRunForm: formElements.saveReadyToRunFormElement.checked
	};

	formValues = encryptValues(formValues);
	return formValues;
}

function populateReadyToRun(formValues) {
	updateReadyToRunValue("task_name", formValues.agentName + "_subscribe");
	updateReadyToRunValue("message_connection", formValues.mbsServerUrl);
	updateReadyToRunValue("instance_connection", "https://" + formValues.snInstance + ".service-now.com");
	updateReadyToRunValue("handler", "com.perspectium.replicator.sql.SQLSubscriber");
	updateReadyToRunValue("decryption_key", formValues.encryptionKey);
	updateReadyToRunValue("database_type", formValues.dbType);
	updateReadyToRunValue("database_server", formValues.dbServerUrl);
	updateReadyToRunValue("database_port", formValues.dbPort);
	updateReadyToRunValue("database_user", formValues.dbUserName);
	updateReadyToRunValue("database_password", formValues.dbPassword);
	updateReadyToRunValue("database_params", "characterEncoding=UTF-8");
	updateReadyToRunValue("database", formValues.dbName);
	updateReadyToRunValue("max_reads_per_connect", "4000");
	updateReadyToRunValue("polling_interval", "5");

	updateAttributeByTag("task", snReadyToRunJSON, "4", "instances");
	updateAttributeByTag("message_connection", snReadyToRunJSON, formValues.mbsPassword, "password");
	updateAttributeByTag("message_connection", snReadyToRunJSON, formValues.mbsUserName, "user");
	updateAttributeByTag("instance_connection", snReadyToRunJSON, formValues.snPassword, "password");
	updateAttributeByTag("instance_connection", snReadyToRunJSON, formValues.snUserName, "user");
	insertReadyToRunAttribute("message_connection", "queue", "psp.out.replicator." + formValues.agentName);

	if (oracleActive == true || oracleActive == "true") {
		updateReadyToRunValue("database_sid", formValues.dbSID);
		deleteByTag("database_params", snReadyToRunJSON);
	} else {
		deleteByTag("database_sid", snReadyToRunJSON);
		deleteByTag("skip_database_creation", snReadyToRunJSON);
	}
	deleteByTag("servicenow_subscriber", snReadyToRunJSON);
}

function populateServiceNowDirect(formValues) {
	updateReadyToRunValue("task_name", formValues.agentName + "_subscribe");
	updateReadyToRunValue("message_connection", "https://" + formValues.snInstance + ".service-now.com");
	updateReadyToRunValue("instance_connection", "https://" + formValues.snInstance + ".service-now.com");
	updateReadyToRunValue("handler", "com.perspectium.replicator.sql.SQLSubscriber");
	updateReadyToRunValue("decryption_key", formValues.encryptionKey);
	updateReadyToRunValue("database_type", formValues.dbType);
	updateReadyToRunValue("database_server", formValues.dbServerUrl);
	updateReadyToRunValue("database_port", formValues.dbPort);
	updateReadyToRunValue("database_user", formValues.dbUserName);
	updateReadyToRunValue("database_password", formValues.dbPassword);
	updateReadyToRunValue("database_params", "characterEncoding=UTF-8");
	updateReadyToRunValue("database", formValues.dbName);
	updateReadyToRunValue("servicenow_subscriber", "true");
	updateReadyToRunValue("max_reads_per_connect", "4000");
	updateReadyToRunValue("polling_interval", "5");

	updateAttributeByTag("task", snReadyToRunJSON, "1", "instances");
	updateAttributeByTag("message_connection", snReadyToRunJSON, formValues.snPassword, "password");
	updateAttributeByTag("message_connection", snReadyToRunJSON, formValues.snUserName, "user");
	updateAttributeByTag("instance_connection", snReadyToRunJSON, formValues.snPassword, "password");
	updateAttributeByTag("instance_connection", snReadyToRunJSON, formValues.snUserName, "user");

	if (oracleActive == true || oracleActive == "true") {
		updateReadyToRunValue("database_sid", formValues.dbSID);
		deleteByTag("database_params", snReadyToRunJSON);
	} else {
		deleteByTag("database_sid", snReadyToRunJSON);
		deleteByTag("skip_database_creation", snReadyToRunJSON);
	}
}

function deleteByTag(tagName, value) {
	for (var i = 0; i < value.length; i++) {
		if (value[i].tag == tagName) {
			value[i].tag = null;
			return;
		} else if (value[i].value != undefined) {
			deleteByTag(tagName, value[i].value);
		}
	}
}

function updateReadyToRunValue(tagName, value) {
	if (value == undefined) {
		return;
	}
	updateValueByTag(tagName, snReadyToRunJSON, value);
}

function updateValueByTag(tagName, value, newValue) {
	for (var i = 0; i < value.length; i++) {
		if (value[i].tag == tagName) {
			value[i].value[0] = newValue;
			return;
		} else if (value[i].value != undefined) {
			updateValueByTag(tagName, value[i].value, newValue);
		}
	}
}

function insertValueInTag(tagName, value) {
	for (var i = 0; i < value.length; i++) {
		if (value[i].tag == tagName) {
			value[i].value.push(newValue);
			return;
		} else if (value[i].value != undefined) {
			insertValueInTag(tagName, value[i].value, newValue);
		}
	}
}

function getValueByTag(tagName) {
	for (var i = 0; i < value.length; i++) {
		if (value[i].tag == tagName) {
			return value[i].value[0];
		} else if (value[i].value != undefined) {
			getValueByTag(tagName);
		}
	}
}

function getAttributeByTag(tagName, attributeName) {
	for (var i = 0; i < value.length; i++) {
		if (value[i].tag == tagName && value[i].attribute != undefined) {
			if (value[i].attribute[attributeName] != undefined) {
				return value[i].attribute[attributeName];
			}
		} else if (value[i].value != undefined) {
			getAttributeByTag(tagName, attributeName);
		}
	}
}

function updateAttributeByTag(tagName, value, newValue, attributeName) {
	for (var i = 0; i < value.length; i++) {
		if (value[i].tag == tagName && value[i].attribute != undefined) {
			if (value[i].attribute[attributeName] != undefined) {
				value[i].attribute[attributeName] = newValue;
			}
		} else if (value[i].value != undefined) {
			updateAttributeByTag(tagName, value[i].value, newValue, attributeName);
		}
	}
}

function deleteAttributeByTag(tagName, value, attributeName) {
	for (var i = 0; i < value.length; i++) {
		if (value[i].tag == tagName && value[i].attribute != undefined) {
			if (value[i].attribute[attributeName] != undefined) {
				delete value[i].attribute[attributeName];
			}
		} else if (value[i].value != undefined) {
			deleteAttributeByTag(tagName, value[i].value, attributeName);
		}
	}
}

function insertReadyToRunAttribute(tagName, attributeName, value) {
	if (value == undefined) {
		return;
	}
	insertAttributeByTag(tagName, snReadyToRunJSON, attributeName, value);
}

function insertAttributeByTag(tagName, value, attributeName, newValue) {
	for (var i = 0; i < value.length; i++) {
		if (value[i].tag == tagName) {
			value[i].attribute[attributeName] = newValue;
		} else if (value[i].value != undefined) {
			insertAttributeByTag(tagName, value[i].value, attributeName, newValue);
		}
	}
}

// value.length is 0, create combined tag
// value.length is 1, create opening tag and closing tag with value in the middle
// value.length is greater than 1, recurse
//author: Wizard Ryan
function buildXml(document) {
	perspectiumDisclaimer = perspectiumDisclaimer.replace("//the instance name//", formElements.snInstanceElement.value);
	var xmlDoc = ENCODING + perspectiumDisclaimer;
	for (var i = 0; i < document.length; i++) {
		xmlDoc += processElementValues(document[i]);
	}
	return xmlDoc;
}

function processElementValues(element) {
	if (element.tag == null) {
		return "";
	}
	if (element.value.length == 0) {
		return generateEmptyTag(element.tag);
	}
	if (element.value.length == 1 && typeof element.value[0] != "object") {
		if (element.tag == "task") {}
		return generateValueTag(element);
	}

	var mAttributes = generateAttributes(element.attribute);
	if (element.tag == "task") {}
	var childString = "<" + element.tag + mAttributes + ">";

	for (var i = 0; i < element.value.length; i++) {
		childString += processElementValues(element.value[i]);
	}

	childString += "</" + element.tag + ">";
	return childString;
}

function generateEmptyTag(tagName) {
	return "<" + tagName + "/>";
}

function generateValueTag(element) {
	var attributes = generateAttributes(element.attribute);
	return "<" + element.tag + attributes + ">" + element.value[0] + "</" + element.tag + ">";
}

function generateAttributes(mAttributes) {
	var attributes = "";
	if (mAttributes != undefined) {
		attributes = " ";
		for (var key in mAttributes) {
			attributes += key + "=" + "\"" + mAttributes[key] + "\" ";
		}
		attributes = attributes.slice(0, -1);
	}
	return attributes;
}

function formatXml(xml) {
	var formatted = "";
	var reg = /(>)(<)(\/*)/g;
	xml = xml.replace(reg, "$1\r\n$2$3");
	var pad = 0;
	jQuery.each(xml.split("\r\n"), function(index, node) {
		var indent = 0;
		if (node.match(/.+<\/\w[^>]*>$/)) {
			indent = 0;
		} else if (node.match(/^<\/\w/)) {
			if (pad != 0) {
				pad -= 1;
			}
		} else if (node.match(/^<\w[^>]*[^\/]>.*$/)) {
			indent = 1;
		} else {
			indent = 0;
		}

		var padding = "";
		for (var i = 0; i < pad; i++) {
			padding += "  ";
		}

		formatted += padding + node + "\r\n";
		pad += indent;
	});

	return formatted;
}

// hide Oracle elements if other database is selected
function showSID(show) {
	oracleDiv = document.getElementById("oracle-field");
	if (show) {
		oracleDiv.style.display = "block";
	} else {
		oracleDiv.style.display = "none";
	}
}

/* set event listeners to validate correctness of form inputs while user types in them */
function setEventListeners() {

	// validation event handlers for each form - add more checks as necessary
	var errors = document.querySelectorAll(".error");

	// add listeners for every table checkbox
	for (var i = 0; i < formElements.tableNameElement.length; i++) {
		formElements.tableNameElement[i].addEventListener("change", function(event) {
			var error = errors[0];
			if (!assertTableSelected()) {
				setTableNotSelectedError(error);
			} else {
				clearErrorMessage(error);
			}
		}, false);
	}

	formElements.snInstanceElement.addEventListener("blur", function(event) {
		var value = formElements.snInstanceElement.value;
		var error = errors[1];
		if (!assertFilled(value)) {
			setEmptyFieldsError(error);
		} else {
			clearErrorMessage(error);
		}
	}, false);

	formElements.snUserNameElement.addEventListener("blur", function(event) {
		var value = formElements.snUserNameElement.value;
		var error = errors[2];
		if (!assertFilled(value)) {
			setEmptyFieldsError(error);
		} else if (!assertRole(value)) {
			setInvalidUserOrRoleError(error);
		} else {
			clearErrorMessage(error);
		}
	}, false);

	formElements.snPasswordElement.addEventListener("blur", function(event) {
		var value = formElements.snPasswordElement.value;
		var error = errors[3];
		if (!assertFilled(value)) {
			setEmptyFieldsError(error);
		} else {
			clearErrorMessage(error);
		}
	}, false);

	formElements.encryptionKeyElement.addEventListener("blur", function(event) {
		var value = formElements.encryptionKeyElement.value;
		var error = errors[4];
		checkEncryptionKey(error);
		if (!assertFilled(value) && !assertEncryptionKey()) {
			setNoEncryptionKeyError(error);
		} else if (!assert24Chars(value)) {
			set24CharMinimumError(error);
		} else {
			clearErrorMessage(error);
		}
	}, false);

	formElements.mbsServerUrlElement.addEventListener("blur", function(event) {
		var value = formElements.mbsServerUrlElement.value;
		var error = errors[5];
		if (!assertFilled(value)) {
			setEmptyFieldsError(error);
		} else {
			clearErrorMessage(error);
		}
	}, false);

	formElements.mbsUserNameElement.addEventListener("blur", function(event) {
		var value = formElements.mbsUserNameElement.value;
		var error = errors[6];
		if (!assertFilled(value)) {
			setEmptyFieldsError(error);
		} else {
			clearErrorMessage(error);
		}
	}, false);

	formElements.mbsPasswordElement.addEventListener("blur", function(event) {
		var value = formElements.mbsPasswordElement.value;
		var error = errors[7];
		if (!assertFilled(value)) {
			setEmptyFieldsError(error);
		} else {
			clearErrorMessage(error);
		}
	}, false);

	formElements.dbServerUrlElement.addEventListener("blur", function(event) {
		var value = formElements.dbServerUrlElement.value;
		var error = errors[8];
		if (!assertFilled(value)) {
			setEmptyFieldsError(error);
		} else {
			clearErrorMessage(error);
		}
	}, false);

	formElements.dbTypeElement.addEventListener("change", function(event) {
		value = formElements.dbTypeElement.value;

		// add more default ports as necessary
		switch (value) {
			case "mysql":
				formElements.dbPortElement.value = MYSQL_PORT;
				oracleActive = false;
				showSID(false);
				formElements.dbUserNameElement.value = "";
				formElements.dbPasswordElement.value = "";
				break;

			case "mssql":
				formElements.dbPortElement.value = MSSQL_PORT;
				oracleActive = false;
				showSID(false);
				formElements.dbUserNameElement.value = "";
				formElements.dbPasswordElement.value = "";
				break;

			case "oracle":
				formElements.dbPortElement.value = ORACLE_PORT;
				oracleActive = true;
				showSID(true);
				formElements.dbUserNameElement.value = "";
				formElements.dbPasswordElement.value = "";
				break;

			default:
				formElements.dbPortElement.value = "";
				break;
		}
	}, false);

	formElements.dbPortElement.addEventListener("blur", function(event) {
		var value = formElements.dbPortElement.value;
		var error = errors[9];
		if (!assertFilled(value)) {
			setEmptyFieldsError(error);
		} else if (!assertNumber(value)) {
			setInvalidNumberError(error);
		} else {
			clearErrorMessage(error);
		}
	}, false);

	formElements.dbSIDElement.addEventListener("blur", function(event) {
		var value = formElements.dbSIDElement.value;
		var error = errors[10];
		if (!assertFilled(value)) {
			setEmptyFieldsError(error);
		} else {
			clearErrorMessage(error);
		}
	}, false);

	formElements.dbUserNameElement.addEventListener("blur", function(event) {
		var value = formElements.dbUserNameElement.value;
		var error = errors[11];
		if (!assertFilled(value)) {
			setEmptyFieldsError(error);
		} else {
			clearErrorMessage(error);
		}
	}, false);

	formElements.dbPasswordElement.addEventListener("blur", function(event) {
		var value = formElements.dbPasswordElement.value;
		var error = errors[12];
		if (!assertFilled(value)) {
			setEmptyFieldsError(error);
		} else {
			clearErrorMessage(error);
		}
	}, false);

	formElements.dbNameElement.addEventListener("blur", function(event) {
		value = formElements.dbNameElement.value;
		error = errors[13];
		if (!assertFilled(value)) {
			setEmptyFieldsError(error);
		} else {
			clearErrorMessage(error);
		}
	}, false);

	function checkEncryptionKey(event) {
		var value = formElements.encryptionKeyElement.value;
		if (!assertEncryptionKey()) {
			setNoEncryptionKeyError(error[4]);
		}
	}

	return formElements;
}

// set error message for empty field
function setEmptyFieldsError(error) {
	error.innerHTML = "Field cannot be empty.";
	error.className = "error active label label-danger";
}

// set error message for invalid number
function setInvalidNumberError(error) {
	error.innerHTML = "Field must be a number.";
	error.className = "error active label label-danger";
}

// set error message for invalid user or role
function setInvalidUserOrRoleError(error) {
	error.innerHTML = "User either does not exist or does not have the perspectium or admin role.";
	error.className = "error active label label-danger";
}

// set warning message for no encryption key set
function setNoEncryptionKeyError(error) {
	error.innerHTML = "Warning: No encryption key currently set in Perspectium Replicator Properties. " +
		"Please enter an encryption key.";
	error.className = "error active label label-warning";
}

// set error message for encryption key too short
function set24CharMinimumError(error) {
	error.innerHTML = "Encryption key must be at least 24 characters.";
	error.className = "error active label label-danger";
}

// set error message for no table selected
function setTableNotSelectedError(error) {
	error.innerHTML = "You must select at least one table.";
	error.className = "error active label label-danger";
}

// clear any error messages that have been set
function clearErrorMessage(error) {
	error.innerHTML = ""; // reset the content of the message
	error.className = "error"; // reset the visual state of the message
}

// validate all the forms on submit
function validateFormValues(formElements) {
	// check user has selected at least one table
	if (!assertTableSelected()) {
		setTableNotSelectedError(document.getElementById("table-error"));
		alert("You must select at least one table.");
		return false;
	}

	// add non-mandatory elements here
	optionalValues = [formElements.dbTypeElement,
		formElements.saveReadyToRunFormElement, formElements.encryptionKeyElement
	];
	optionalValues.push(formElements.tableNameElement);

	snDirectOptionalValues = [formElements.mbsServerUrlElement, formElements.mbsUserNameElement,
		formElements.mbsPasswordElement
	];

	// check if form values are filled
	for (var element in formElements) {
		if (optionalValues.indexOf(formElements[element]) != -1) {
			continue;
		}
		if (formType == "servicenow_direct") {
			if (snDirectOptionalValues.indexOf(formElements[element]) != -1) {
				continue;
			}
		}
		if ((oracleActive == false || oracleActive == "false") && formElements[element] === formElements.dbSIDElement) {
			continue;
		}
		if (!assertFilled(formElements[element].value)) {
			alert("You must fill out all mandatory fields.");
			return false;
		}
	}

	// check port is a number
	if (!assertNumber(formElements.dbPortElement.value)) {
		alert("Field must be a number.");
		return false;
	}

	// check valid user and role
	if (!assertRole(formElements.snUserNameElement.value)) {
		alert("User either does not exist or does not have the perspectium or admin role.");
		return false;
	}

	// check encryption key exists
	if (formElements.encryptionKeyElement.value === "") {
		alert("No default encryption key exists, you must set an encryption key.");
		return false;
	}

	// check encryption key length
	if (!assert24Chars(formElements.encryptionKeyElement.value)) {
		alert("Encryption key must be at least 24 characters.");
		return false;
	}
	return true;
}

// check if a form value is filled
function assertFilled(value) {
	if (value === undefined || value === "") {
		return false;
	}
	return true;
}

// check if a string or other value is a number
function assertNumber(value) {
	if (isNaN(value)) {
		return false;
	}
	return true;
}

// check if specified user exists and has role "perspectium" or "admin"
function assertRole(value) {
	// user_name field is unique so just check if user exists
	var ugr = new GlideRecord("sys_user");
	ugr.addQuery("user_name", value);
	ugr.query();
	if (!ugr.next()) {
		return false;
	}
	// check if user has role "perspectium" or "admin"
	rgr = new GlideRecord("sys_user_has_role");
	rgr.addQuery("user", ugr.sys_id);
	rgr.encodedQuery = "role.sys_name=perspectium^ORrole.sys_name=admin";
	rgr.query();
	if (!rgr.hasNext()) {
		return false;
	}
	return true;
}

// check if an encryption key exists in replicator properties
function assertEncryptionKey() {
	var gr = new GlideRecord("u_psp_properties");
	gr.addQuery("u_name", "com.perspectium.replicator.outbound.encryption_key");
	gr.query();
	if (gr.next() && gr.u_value === "") {
		return false;
	}
	return true;
}

// check if string is greater than 0 characters and less than 24 characters
function assert24Chars(value) {
	if (value != undefined && value.length > 0 && value.length < MIN_ENCRYPTION_LEN) {
		return false;
	}
	return true;
}

// check that at least one table is selected
function assertTableSelected() {
	for (var i = 0; i < CHECKBOXES.length; i++) {
		if (CHECKBOXES[i].checked === true) {
			return true;
		}
	}
	return false;
}

// if user saved database type as Oracle previously then need to show SID
function checkDBType() {
	if (formElements.dbTypeElement.value === "oracle") {
		oracleActive = true;
		showSID(true);
	}
}

// hide Oracle elements if other database is selected
function showSID(show) {
	oracleDiv = document.getElementById("oracle-field");
	if (show) {
		oracleDiv.style.display = "block";
	} else {
		oracleDiv.style.display = "none";
	}
}

// get the decrypted encryption key
function getEncryptionKey() {
	var gr = new GlideRecord("u_psp_properties");
	gr.addQuery("u_name", "com.perspectium.replicator.outbound.encryption_key");
	gr.query();
	gr.next();
	var key = gr.u_value;

	var ga = new GlideAjax("PerspectiumReadyToRun");
	var tableName = "u_psp_configuration";
	ga.addParam("sysparm_name", "decryptValue");
	ga.addParam("sysparm_key", key);

	ga.getXML(function(response) {
		formElements.encryptionKeyElement.value = response.responseXML.documentElement.getAttribute("answer");
	});
}

// get the correct type of wrapper.conf
function getWrapper(osType, wrapperAgentName) {
	if (osType == "Linux") {
		return linuxWrapper.replace(AGENT_NAME_PLACEHOLDER, wrapperAgentName);
	} else if (osType == "Windows") {
		return windowsWrapper.replace(AGENT_NAME_PLACEHOLDER, wrapperAgentName);
	} else {
		return macWrapper.replace(AGENT_NAME_PLACEHOLDER, wrapperAgentName);
	}
}