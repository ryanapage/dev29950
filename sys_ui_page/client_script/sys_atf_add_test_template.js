// a global variable that avoid click button twice quickly
var clicked = false;
var gm = new GwtMessage();
// Global variable to denote if table field is mandatory
var tableRequired = true;
function addTemplate(testId) {
	var tableId = gel('test_table_picker').value;
	var templateId = gel('test_template_picker').value;
	var testName = gel('test_name').value;

	if (!testName) {
		alertMessage("Please type a test name");
		return;
	}

	if (!tableId && tableRequired) {
		alertMessage("Please select a table");
		return;
	}

	if (!templateId) {
		alertMessage("Please select a template");
		return;
	}

	var ga = new GlideAjax('AddTestTemplateAjax');
	ga.addParam('sysparm_name', 'addTemplate');
	ga.addParam('sysparm_test_id', testId);
	ga.addParam('sysparm_table_id', tableId);
	ga.addParam('sysparm_template_id', templateId);
	ga.addParam('sysparm_test_name', testName);
	ga.getXML(afterAdd);
}

/**
 * Show or hide the table field when a different template is chosen
 */
function templateChanged() {
	var templateId = gel('test_template_picker').value;

	var ga = new GlideAjax("AddTestTemplateAjax");
	ga.addParam("sysparm_name", "doesTemplateRequireTable");
	ga.addParam("sysparm_template_id", templateId);
	ga.getXMLAnswer(function(response) {
		tableRequired = (response == "true");
		showOrHideTable(tableRequired);
	});
}

function showOrHideTable(show) {
	var tableElement = gel('tableDiv');

	if (show) {
		// make the table element visible and mandatory
		tableElement.setAttribute('style', '');
		tableElement.addClassName("is-required");
	} else {
		// hide the table element, remove is-required class so that it is not mandatory anymore
		// the existing value in the field is not cleared, it is just hidden
		tableElement.setAttribute('style', 'display:none;');
		tableElement.removeClassName("is-required");
	}
}

function afterAdd(response) {
	var testSysId = response.responseXML.documentElement.getAttribute("answer");
	if (!testSysId) {
		alertMessage("Please try it again");
		var addButton = gel('add_button');
		addButton.disabled = false;
		return;
	}
	window.location = "sys_atf_test.do?sys_id=" + testSysId;
}

function alertMessage(message) {
	alert(gm.getMessage(message));
	var addButton = gel('add_button');
	addButton.disabled = false;
}

function closeDialog() {
	GlideModal.get().destroy();
}

