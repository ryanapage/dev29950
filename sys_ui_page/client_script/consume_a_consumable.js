function cancel() {
	var c = gel('cancelled');
	c.value = "true";
	GlideDialogWindow.get().destroy();
}

function actionOK() {
	var total_quantity = gel('total_qty').value;
	var quantity = gel('qty').value;
	var asset_id = gel('alm_hardware').value;
	var user_id = gel('sys_user').value;

	total_quantity = parseInt(total_quantity, 10);
	quantity = parseInt(quantity, 10);

	if (quantity > total_quantity) {
		alert(getMessage("Please enter a number less than or equal to the number in stock"));
		return false;
	} else if (quantity < 1) {
		alert(getMessage("Please enter a number for the quantity"));
		return false;
	} else if (asset_id == '' && user_id == '') {
		alert(getMessage("Please select an asset or a user to attach the consumed items to"));
		return false;
	} else {
		var form = document.forms['form.' + '${sys_id}'];
		addInput(form, "HIDDEN", "asset", asset_id);
		addInput(form, "HIDDEN", "user", user_id);
		return true;
	}
}