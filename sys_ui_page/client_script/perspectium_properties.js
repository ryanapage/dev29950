function onSubmit() {
	$j('#propertiesMessage').empty();
    $j("#alert").remove();
    var MAX_BYTES = 150000000; // max bytes to share each post
    var MAX_CRYPT_LEN = 24; // max encrypt/decrypt length 
    // perspectium
	var maxBytes = $j("[name='com.perspectium.output_bytes_limit']").val();
	var maxOutputRows = $j("[name='com.perspectium.output_row_limit']").val();
    var maxMessages = $j("[name='com.perspectium.outbound.max_messages']").val();
    // perspectium replicator
    var replicatorMinMsgs = $j("[name='com.perspectium.replicator.large_share_value']").val();
    // perspectium observer
    var observerRecordLimit = $j("[name='com.perspectium.u_psp_observer_out_message.record_limit']").val();
	
	var receiptAlertThreshold = $j("[name='com.perspectium.receipt.error_alert_threshold']").val();
	
	
	if (maxBytes && maxBytes > MAX_BYTES) {
		showAlert("Maximum bytes is 15MB (15,000,000 bytes)");
		return false;
	} else if (maxBytes && (maxBytes < 0 || maxBytes.charAt(0) == '-')) {
		showAlert("Maximum bytes cannot be a negative value");
		return false;
	}
	
	if (maxOutputRows && (maxOutputRows < 0 || maxOutputRows.charAt(0) == '-')) {
		showAlert("Maximum number of output rows cannot be a negative number.");
		return false;
	}
	
	if (maxMessages && (maxMessages < 0 || maxMessages.charAt(0) == '-')) {
		showAlert("Maximum number of messages cannot be a negative value");
		return false;
	}
	
	if (replicatorMinMsgs && (replicatorMinMsgs < 0 || replicatorMinMsgs.charAt(0) == '-')) {
        showAlert("Minimum message number cannot be a negative value.");
        return false;
    }
    
    if (observerRecordLimit && (observerRecordLimit < 0 || observerRecordLimit.charAt(0) == '-')) {
        showAlert("Max number of outbound messages cannot be a negative value.");
        return false;
    }
	
	if (receiptAlertThreshold && receiptAlertThreshold < 0 || receiptAlertThreshold.charAt(0) == '-') {
        showAlert("Max number of outbound messages cannot be a negative value.");
        return false;
    }
	
	if (typeof $j("[name='com.perspectium.replicator.outbound.encryption_key']").val() == 'undefined')
		return true;
	
	var encryptLength = $j("[name='com.perspectium.replicator.outbound.encryption_key']").val().length;
	var decryptLength = $j("[name='com.perspectium.replicator.inbound.encryption_key']").val().length;
	
	if (encryptLength < MAX_CRYPT_LEN ) {
		var msg = "Please check that your encryption key is at least 24 characters long.";
		
		if (decryptLength != 0 && decryptLength < MAX_CRYPT_LEN) {
			msg = "Please check that your encryption and decryption keys are at least 24 characters long.";
		}
		
		showAlert(msg);
		return false;
	} else if (decryptLength != 0 && decryptLength < MAX_CRYPT_LEN) {
		showAlert("Please check that your decryption key is at least 24 characters long.");
		return false;
	} else {
		return true;
	}
}

/**
 * Display an alert message at the top of the page.
 **/
function showAlert(message) {
	$j('#alertID').remove(); // clear previous alert before creating new
	var alert = document.createElement("div");
	alert.setAttribute("id","alertID");
	alert.className = "outputmsg outputmsg_error notification notification-error";
	alert.innerHTML = "Unable to save properties. " + message;
	alert.align = "left";
	document.getElementById("header").appendChild(alert);
}