// When the page gets loaded, execute the event handler for on-load event.

$j('title').text("${gs.getMessage('Password Reset - Change Password')}");
addLoadEvent(
function() {
	populateProcessSelect('${gs.getUserID()}');
}
);

function goBack() {
    window.history.back();
}

function isPasswordValid(password) {
	var selectFieldElem = document.getElementById("processSelectId");
	var procIdentificationFieldElem = document.getElementById("processIdentificationId");
	var procId = procIdentificationFieldElem.options[selectFieldElem.selectedIndex].text;
	var funcName = "isPasswordValid_" + procId;
	var result = window[funcName](password);
	return result;
}

function calculatePasswordStrength(password) {
	var selectFieldElem = document.getElementById("processSelectId");
	var procIdentificationFieldElem = document.getElementById("processIdentificationId");
	var procId = procIdentificationFieldElem.options[selectFieldElem.selectedIndex].text;
	var funcName = "calculatePasswordStrength_" + procId;
	var result = window[funcName](password);
	return result;
}


var loadingDialog;
var startTime;
var new_password = null;
var old_password = null;
var wfCheckDoneFrequency = 500; // milliseconds (this is just a default value, the real value is set from a sys_property)
var wfTimeOut = 30000;          // milliseconds (this is just a default value, the real value is set from a sys_property)
var wfContextSysId;
var wasSubmitted = false;


function updatePwdRuleHint() {
	var selectFieldElem = document.getElementById("processSelectId");
	var ruleFieldElem = document.getElementById("processRuleId");
	var pwdRuleHintFieldElem = document.getElementById("pwdRuleHintId");
	
	if (selectFieldElem != undefined && ruleFieldElem != undefined && pwdRuleHintFieldElem != undefined) {
		var hintText = ruleFieldElem.options[selectFieldElem.selectedIndex].text;
		pwdRuleHintFieldElem.innerHTML = hintText;
	}
}

function updatePwdStrengthVisibility() {
	var selectFieldElem = document.getElementById("processSelectId");
	var pwdVisibilityFieldElem = document.getElementById("pwdStrengthVisibilityId");
	
	if ((selectFieldElem != undefined) && (pwdVisibilityFieldElem != undefined)) {
		var pwdStrengthContainer = document.getElementById("password_strength_container");
		var shouldShow = pwdVisibilityFieldElem.options[selectFieldElem.selectedIndex].value;
		if (shouldShow == "true") {
			pwdStrengthContainer.style.display="block";
		} else {
			pwdStrengthContainer.style.display="none";
		}
	}
}

// populate select box with processes available to user
function populateProcessSelect(user_id) {
	wfCheckDoneFrequency = gel('password_reset.wf.refresh_rate').value;
	wfTimeOut = gel('password_reset.wf.timeout').value;
	
	var sel = document.getElementById("processSelectId");
	var rules = document.getElementById("processRuleId");
	var identifications = document.getElementById("processIdentificationId");
	var pwdStrengthVisibilities = document.getElementById("pwdStrengthVisibilityId");
	
	var ga = new GlideAjax('PwdAjaxChangePassword');
	ga.addParam('sysparm_name', 'getProcessNames');
	ga.addParam('sysparm_user', user_id);
	ga.getXML(function(response) {
		var processes = response.responseXML.getElementsByTagName("process");
		var numProcesses = processes.length;
		for (var i = 0; i < processes.length; i++) {
			var name = processes[i].getAttribute("name");
			var pwdRuleHint = processes[i].getAttribute("pwdRuleHint");
			var procId = processes[i].getAttribute("procId");
			var ruleFunc = processes[i].getAttribute("pwdRule");
			var enablePasswordStrength = processes[i].getAttribute("enablePasswordStrength");
			var strengthFunc = processes[i].getAttribute("strengthRule");
			
			var selection = i + 1;
			if (numProcesses == 1) {
				selection = i;
			}
			sel[selection] = new Option(name, procId);
			rules[selection] = new Option(pwdRuleHint, pwdRuleHint);
			identifications[selection] = new Option(procId, procId);
			pwdStrengthVisibilities[selection] = new Option(name, enablePasswordStrength);
			
			// dynamically inject isPasswordValid_procId and calculatePasswordStrength_procId 
			// functions into the DOM
			var scriptElem = document.createElement('script');
			scriptElem.type = 'text/javascript';
			scriptElem.text = ruleFunc + "\n\n" + strengthFunc;
			document.body.appendChild(scriptElem);
		}
		
		if (numProcesses > 1) {
			//if there are more than one process available, show "select a process"
			sel[0] = new Option('${gs.getMessageS("-- None --")}', 0);
			sel.selectedIndex = 0;
			showPasswordChangeForm(numProcesses);
		} else if (numProcesses == 1) {
			sel.selectedIndex = 0;
//			sel.value = sel.options[0].value;
//			sel.options[0].selected = true;
			updatePwdRuleHint();
			updatePwdStrengthVisibility();
			showPasswordChangeForm(numProcesses);
		} else {
			document.location = "$pwd_change_error.do?sysparm_error=no_process";
//			var noProcErrMsg = '${gs.getMessage("Changing password is not yet configured in your environment. Contact your system administrator.")}';
//			document.write("<div id = \"noProcErrorMsgId\"> <h2>" + noProcErrMsg + "</h2> </div>");
		}
	});
}

function showPasswordChangeForm(numProcesses) {
	document.getElementById("passwordChangeFormId").style.display = "block";
	if (numProcesses > 1) {
		showHidePasswordChangeFormFields();
	}
}

function showHidePasswordChangeFormFields() {
	// Do not show the form fields for hint, old, new pwd, pwd button until user selects process
	var sel = document.getElementById("processSelectId");
	var processName = sel.options[sel.selectedIndex].text;
	
	if (processName == '${gs.getMessageS("-- None --")}') {
		// Hide form fields
		updateDisplayPasswordChangeFormFields("none");
	} else {
		// Show form fields
		updateDisplayPasswordChangeFormFields("block");
	}
}

function updateDisplayPasswordChangeFormFields(style) {
	gel('process_details').style.display = style;
	/*
	document.getElementById("pwdRuleHintId").style.display = style;
	document.getElementById("sysparm_old_pass_label").style.display = style;
	document.getElementById("sysparm_old_pass").style.display = style;
	document.getElementById("sysparm_new_pass_label").style.display = style;
	document.getElementById("sysparm_new_pass").style.display = style;
	document.getElementById("sysparm_confirm_pass_label").style.display = style;
	document.getElementById("sysparm_confirm_pass").style.display = style;
	document.getElementById("sysverb_pwd_change").style.display = style;
	*/
}

// change password event from the button handled
function changePassword(user_id) {
	if (wasSubmitted) {
		return false;
	}
	wasSubmitted = true;
	
	// clear existing error messages
	clearFieldError('error_msg_old_pass', 'old_pass_form_group');
	clearFieldError('error_msg_new_pass', 'new_pass_form_group');
	clearFieldError('error_msg_confirm_pass', 'confirm_pass_form_group');
	
	var sel = document.getElementById("processSelectId");
	var processName = sel.options[sel.selectedIndex].text;
    var procId = sel.options[sel.selectedIndex].value;
	var foundError = false;
	
	if (processName == '${gs.getMessageS("-- None --")}') {
		displayFieldError("error_msg_select", getMessage("Invalid selection"), "process_select_pass_form_group");
		wasSubmitted = false;
		foundError = true;
	}
	
	if (document.getElementById("sysparm_old_pass").value == '') {
		displayFieldError("error_msg_old_pass", getMessage("Cannot be empty"), "old_pass_form_group");
		wasSubmitted = false;
		foundError = true;
	}
	
	if (document.getElementById("sysparm_new_password").value == '') {
		displayFieldError("error_msg_new_pass", getMessage("Cannot be empty"), "new_pass_form_group");
		wasSubmitted = false;
		foundError = true;
	}
	
	if (foundError) {
		return false;
	}
	
	if (!isValidPassword(procId)) {
		wasSubmitted = false;
		return false;
	}
	
	var old_password = document.getElementById("sysparm_old_pass").value;
	var new_password = document.getElementById("sysparm_new_password").value;
	if (old_password == new_password) {
		displayFieldError("error_msg_new_pass", getMessage("Old and new password cannot be the same"), "new_pass_form_group");
		wasSubmitted = false;
		return false;
	}
	
	// Initate change password user ajax call:
	startChangePasswordWF(user_id, procId, old_password);
	return false;
}

// Triggers the workflow to change the user's password:
function startChangePasswordWF(userId, procId, old_pass) {
	startTime = new Date().getTime();
	
	var ga = new GlideAjax('PwdAjaxChangePassword');
	ga.addParam('sysparm_name', 'changePassword');
	ga.addParam('sysparm_user', userId);
	ga.addParam('sysparm_procId', procId);
	ga.addParam('sysparam_new_password', new_password);
	ga.addParam("ni.nolog.sysparam_new_password", "true"); 
	ga.addParam('sysparam_old_password', old_pass);
	ga.addParam("ni.nolog.sysparam_old_password", "true"); 
	ga.getXML(function(response) {
		var res =  response.responseXML.getElementsByTagName("response");
		var status = res[0].getAttribute("status");
		var message = res[0].getAttribute("message");
		wfContextSysId = res[0].getAttribute("value");
		if (status.match(/failure/i)) {
			submitWithBlock('$pwd_change_error.do', message);
			return;
		}
		else if (status.match(/block/i)) {
			submitWithBlock('$pwd_change_error.do', 'block');
			return;
		}
		showPwdLoadingDialog();
		
		// start the polling for the workflow result
		setTimeoutForChangePasswordRefresh();
	});
}

function setTimeoutForChangePasswordRefresh () {
	window.setTimeout(checkChangePwdWF, wfCheckDoneFrequency);
}

// poll the result of the workflow
function checkChangePwdWF(){
	var currTime = new Date().getTime();
	if (currTime - startTime > wfTimeOut) {
		resultCallback('timeout');
		return;
	}
	
	var ga = new GlideAjax('PwdAjaxChangePassword');
	ga.addParam('sysparm_name', 'checkChangePwdWFState');
	ga.addParam('sysparam_wf_context_sys_id', wfContextSysId);
	ga.getXML(function(response) {
		var res =  response.responseXML.getElementsByTagName("response");
		var status = res[0].getAttribute("status");
		var value = res[0].getAttribute("value");
		
		if (!status.match(/finished/i)) {
			setTimeoutForChangePasswordRefresh();
			return;
		}
		// everything done, finalize
		resultCallback(value);
	});
}

// finalize the result, and show to user.
function resultCallback(result) {
	loadingDialog.destroy();
	if (result.match(/success/i)) {
		submitWithOKAndSysParam('$pwd_change_success.do', 'sysparm_success', 'SUCCESS');
		return;
	} else if (result.match(/incorrect old password/i)) {
		submitWithBlock('$pwd_change_error.do', 'incorrect old password');
		return;
	} else if (result.match(/non fatal failure/i)) {
		submitWithBlock('$pwd_change_error.do', 'non fatal failure');
		return;
	}
	submitWithBlock('$pwd_change_error.do', '${gs.getMessageS("Change password request resulted in failure")}');
}

function isValidPassword(processId) {
	var foundProblem = false;
	var password = document.getElementById("sysparm_new_password");
	var retyped_password = document.getElementById("sysparm_confirm_pass");
	
	if (password.value != retyped_password.value) {
		displayFieldError("error_msg_confirm_pass", getMessage("New and retype password must be the same"), "confirm_pass_form_group");
		foundProblem = true;
	}
	try {
		var ga = new GlideAjax('PwdAjaxChangePassword');
		ga.addParam('sysparm_name','validatePassword');
		ga.addParam('sysparam_process_id', processId);
		ga.addParam('sysparam_new_password', password.value);
		ga.addParam("ni.nolog.sysparam_new_password", "true"); 
		ga.getXMLWait();
		var valid = ga.getAnswer();
		if (/failure/.test(valid)) {
			displayFieldError("error_msg_new_pass", getMessage("Invalid password"), "new_pass_form_group");
			foundProblem = true;
		}
	} catch (err) {
		// ignore err from isPasswordValid script, will pass to backend.
	}
	
	if (foundProblem) {
		return false;
	}
	
	new_password = password.value;
	return true;
}

function errorImage() {
	return '<img src="images/outputmsg_error.gifx" alt="Error Message" />';
}

function displayFieldError(field, message, formGroup) {
	fieldElem = gel(field);
	if (fieldElem != undefined) {
		fieldElem.innerHTML = message;
		//fieldElem.style.display = 'inline';
	}
	
	$j('#' + formGroup).removeClass("is-required");
	$j('#' + formGroup).addClass("has-error");	
}

function clearFormFields() {
	
	gel('sysparm_old_pass').value = "";
	gel('sysparm_new_password').value = "";
	gel('sysparm_confirm_pass').value = "";
	
	clearFieldError('error_msg_select', 'process_select_pass_form_group');
	clearFieldError('error_msg_old_pass', 'old_pass_form_group');
	clearFieldError('error_msg_new_pass', 'new_pass_form_group');
	clearFieldError('error_msg_confirm_pass', 'confirm_pass_form_group');
}

function clearFieldError(field, formGroup) {
	fieldElem = gel(field);
	if (fieldElem != undefined) {
		fieldElem.innerHTML = '';
		//fieldElem.style.display = 'none';
	}

	$j('#' + formGroup).addClass("is-required");
	$j('#' + formGroup).removeClass("has-error");
}

function clearErrorWhenNotEmpty(field, errorField, formGroup) {
	fieldElem = gel(field);
	if (fieldElem != undefined && fieldElem.text != '') {
		clearFieldError(gel(errorField), formGroup);
	}
}

function showPwdLoadingDialog() {
	loadingDialog = new GlideDialogWindow("dialog_loading", true);
	loadingDialog.setTitle(getMessage('Request in progress. This process can take up to a few minutes'));
}

function checkPasswordStrength() {
	var password = document.getElementById("sysparm_new_password").value;
	var password_strength = calculatePasswordStrength(password);
	updatePasswordStrengthBarColors(password_strength);
	updatePasswordStrengthMessage(password_strength);
}

function updatePasswordStrengthBarColors(password_strength) {
	var strengthMeter = document.getElementById("strength-meter");
	strengthMeter.style.width = (password_strength + '%');
	strengthMeter.setAttribute("aria-valuenow", password_strength);
	if (password_strength > 75) {
		strengthMeter.className = "progress-bar progress-bar-dark-green";
	} else if (password_strength > 50) {
		strengthMeter.className = "progress-bar progress-bar-success";
	} else if (password_strength > 25) {
		strengthMeter.className = "progress-bar progress-bar-warning";
	} else if (password_strength > 0) {
		strengthMeter.className = "progress-bar progress-bar-danger";
	}
}

function updatePasswordStrengthMessage(password_strength) {
	var strengthMessage = document.getElementById("pwd_strength_text");
	if (password_strength > 75) {
		strengthMessage.innerHTML = getMessage("Password Strength: Great");
	} else if (password_strength > 50) {
		strengthMessage.innerHTML = getMessage("Password Strength: Good");
	} else if (password_strength > 25) {
		strengthMessage.innerHTML = getMessage("Password Strength: Okay");
	} else if (password_strength > 0) {
		strengthMessage.innerHTML = getMessage("Password Strength: Weak");
	} else {
		strengthMessage.innerHTML = getMessage("Does not meet requirements");
	}
}