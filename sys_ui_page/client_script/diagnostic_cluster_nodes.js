function addValue(e, o) {
   var tr = cel("tr", e);
   var td = cel("td", tr);
   td.className = "label_left";
   td.noWrap = "true";
   if (o.flag)
      td.style.color = "red";
      
   td.innerHTML = o.label;
   td = cel("td", tr);
   td.noWrap = "true";
   if (o.flag)
      td.style.color = "red";
   if (o.bold)
      td.style.fontWeight = "bold";
      
   if (!o.value || o.value == "undefined")
      td.innerHTML = "?";
   else
      td.innerHTML = o.value;
}

function addNode(e, node) {
   var td = cel("td", e);
   td.style.verticalAlign = "top";
   var table = cel("table", td);
   table.cellSpacing = "2";
   table.cellPadding = "2";
   var tbody = cel("tbody", table);
   var o = {};
   o.label = "Name";
   o.value = node.name;
   o.bold = true;
   addValue(tbody, o);
   for (var i = 0; i < node.values.length; i++)
      addValue(tbody, node.values[i]);
}

var nodes_table = gel('$[jvar_nodes_table_id]');
var table = cel('table', nodes_table);
table.cellSpacing = "2";
table.cellPadding = "2";
var tbody = cel('tbody', table);
var tr;
for (var i = 0; i < g_nodes.length; i++) {
   if ((i % g_nodesAcross) == 0) 
      tr = cel("tr", tbody);

   addNode(tr, g_nodes[i]);
}