addLoadEvent(initializeVariables);
setViewportForMobile();
$j('title').text("${gs.getMessage('Password Reset - Reset')}");

var wfCheckDoneFrequency = 500; // milliseconds (this is just a default value, the real value is set from a sys_property)
var wfTimeOut = 30000;          // milliseconds (this is just a default value, the real value is set from a sys_property)
var loadingDialog;
var wasSubmitted = false;
var processId;
var requestId;
var userId;
var lockState;
var processSupportsUnlock;
var startTime;
var new_password = null;

function disableButton(btn) {
	btnElem = gel(btn);
	if (btnElem != undefined) {
		btnElem.style.display = 'none';
	}
}

// Onload:
function initializeVariables() {
	wfCheckDoneFrequency = gel('password_reset.wf.refresh_rate').value;
	wfTimeOut = gel('password_reset.wf.timeout').value;
	autoGenPassword = (gel('sysparm_auto_gen_password').value == "true");
	this.processId = gel('sysparm_process_id').value;
	this.requestId = gel('sysparm_request_id').value;
	this.userId = gel('sysparm_sys_user_id').value;
	this.lockState = gel('sysparm_lock_state').value;
	this.processSupportsUnlock = (gel('sysparm_process_supports_unlock').value == "true");
	
	this.startTime = new Date().getTime();
	updateLockStateImage(this.lockState);
}


// Update the lock state image & hint:
function updateLockStateImage(newLockState) {
	var hint;
	var image;
	
	this.lockState = newLockState;

	switch (newLockState) {
		case "-1":
			hint = '${gs.getMessageS("Unable to retrieve  account lock state")}';
			image = "icon-alert";
			break;
			
		case "0":
			hint = '${gs.getMessageS("Retrieving account lock state...")}';
			image = "icon-loading";
			break;
			
		case "1":
			hint = '${gs.getMessageS("Account is locked")}';
			image = "icon-locked";
			break;
			
		case "2":
			hint = '${gs.getMessageS("Account is not locked")}';
			image = "icon-unlocked";
			disableButton('sysverb_unlock_account');
			break;
			
		default:
			hint = '${gs.getMessageS("Error retrieving account lock state")}';
			image = "icon-alert";
	}

	gel('user_lock_image').className = image;
	gel('user_lock_message').innerHTML = hint;
	
	if (newLockState == "0") {
		setTimerForPollingLockState();
	}
}

function setTimerForPollingLockState() {
	window.setTimeout(this.pollForLockState.bind(this), this.wfCheckDoneFrequency);
}


// Ajax call for polling lock state from the request record:
function pollForLockState() {
	var currTime = new Date().getTime();
	if (currTime - this.startTime > this.wfTimeOut) {
		// update lock state to 'failure'
		updateLockStateImage("-1");
		return;
	}
	
	// Use ajax to retrieve the lock state from request record:
    var ga = new GlideAjax('PwdAjaxUserUnlockProcessor');
    ga.addParam('sysparm_name', 'pollLockStateFromRequest');
    ga.addParam('sysparam_request_id', this.requestId);
	ga.addParam('sysparam_pwd_csrf_token', findCSRFElement().value);
    ga.getXMLWait();
    
	//Let's handle the security.
    handleSecurityFrom(ga.requestObject);
	
    var state = ga.getAnswer();
	updateLockStateImage(state);
}


// Handle the reset password action:
function validatePassword(processIdIn, requestIdIn, userIdIn) {
	if (wasSubmitted) {
		return false;
	}
	wasSubmitted = true;
	
	if (!autoGenPassword) {
			
		var foundProblem = false;
		var password = document.getElementById("sysparm_new_password");
		var retyped_password = document.getElementById("sysparm_retype_password");
		if (password.value != retyped_password.value) {
			displayFieldError("retype_comment", getMessage("No match"), "retype_form_group");
			foundProblem = true;
		}
		try {
			if (!serversideValidatePassword(password.value)) {
				displayFieldError("password_comment", getMessage("Invalid password"), "password_form_group");
				foundProblem = true;
			}
		} catch (err) {
			// ignore err from isPasswordValid script, will pass to backend.
		}
			
		if (foundProblem) {
			wasSubmitted = false;
			return false;
		}
		
		new_password = password.value;
	}
		
	var requestId = requestIdIn;
	var processId = processIdIn;
	var userId = userIdIn;
		
	if ((lockState == "1") && !processSupportsUnlock) {
		var msgs = new GwtMessage();
		var dialog = new GlideDialogWindow('glide_confirm_standard');
		dialog.setTitle(msgs.getMessage('Account Locked Warning'));
		dialog.setPreference('warning', true);
		dialog.setPreference('title', msgs.getMessage('The account is locked, resetting the password alone will not enable access. Continue?'));
		dialog.setPreference('onPromptComplete', proceedWithReset);
		dialog.render();
		wasSubmitted = false;
	} else {
		proceedWithReset();
	}
	
	return false;
}

function serversideValidatePassword(password) {
	var ga = new GlideAjax('PwdAjaxWFRequestProcessor');
	ga.addParam('sysparm_name','validatePassword');
	ga.addParam('sysparam_process_id', processId);
	ga.addParam('sysparam_new_password', password);
	ga.addParam("ni.nolog.sysparam_new_password", 'true');
	ga.getXMLWait();
	var valid = ga.getAnswer();
	return /success/.test(valid);
}

function proceedWithReset() {
	wasSubmitted = true;
	var wf = new PwdWfManager(wfCheckDoneFrequency, wfTimeOut);
	wf.startWfViaAjax(processId, requestId, new_password);
	showPwdLoadingDialog();
}

// This is a callback function for handling the result of the "unlock account" workflow:
function resultCallbackForUnlockAccount(result) {
	if (result.match(/success/i)) {
		submitWithOKAndSysParam('$pwd_unlock_success.do', 'sysparm_success', 'SUCCESS');
		return;
	}

	submitWithBlock('$pwd_error.do', '${gs.getMessageS("Workflow resulted in failure")}');
}


function unlockAccount(processIdIn, requestIdIn, userIdIn) {
	// allow submit button to be pressed only once:
	if (wasSubmitted) {
		return false;
	}
	wasSubmitted = true;
	
	// Initate the unlock user ajax call:
	var wf = new PwdUserUnlockManager(wfCheckDoneFrequency, wfTimeOut);
	wf.initiateUnlockWF(processIdIn, requestIdIn, resultCallbackForUnlockAccount);
	showPwdLoadingDialog();
	
	return false;
}

function showPwdLoadingDialog() {
    loadingDialog = new GlideDialogWindow("dialog_loading", true);
    loadingDialog.setTitle(getMessage('Request in progress'));

    var msg = getMessage('This process can take up to a few minutes.');
    var altMsg = getMessage("Loading...");
    var body = "<center> " + msg + " <br/>" + altMsg + " <br/><img src='images/ajax-loader.gifx' alt='"+ altMsg +"' /></center>";
    loadingDialog.setBody(body);
}

function clearComments() {
	clearFieldError("retype_comment", "retype_form_group");
	clearFieldError("password_comment", "password_form_group");
}

function keyPassword() {
	clearComments();
}

function keyRetype() {
	clearComments();
}

function errorImage() {
	return '<img src="images/outputmsg_error.gifx" alt="Error Message" />';
}

function displayFieldError(field, message, formGroup) {
	fieldElem = gel(field);
	if (fieldElem != undefined) {
		fieldElem.innerHTML = errorImage() + message;
		fieldElem.style.display = '';
	}
	
	$j('#' + formGroup).removeClass("is-required");
	$j('#' + formGroup).addClass("has-error");	
}

function clearFieldError(field, formGroup) {
	fieldElem = gel(field);
	if (fieldElem != undefined) {
		fieldElem.innerHTML = '';
		//fieldElem.style.display = 'none';
	}

	$j('#' + formGroup).addClass("is-required");
	$j('#' + formGroup).removeClass("has-error");
}


function checkPasswordStrength() {
	var password = document.getElementById("sysparm_new_password").value;
	var password_strength = calculatePasswordStrength(password);
	updatePasswordStrengthBarColors(password_strength);
	updatePasswordStrengthMessage(password_strength);
}

function updatePasswordStrengthBarColors(password_strength) {
    var strengthMeter = document.getElementById("strength-meter");
    strengthMeter.style.width = (password_strength + '%');
    strengthMeter.setAttribute("aria-valuenow", password_strength);
    if (password_strength > 75) {
		strengthMeter.className = "progress-bar progress-bar-dark-green";
	} else if (password_strength > 50) {
        strengthMeter.className = "progress-bar progress-bar-success";
    } else if (password_strength > 25) {
        strengthMeter.className = "progress-bar progress-bar-warning";
    } else if (password_strength > 0) {
        strengthMeter.className = "progress-bar progress-bar-danger";
    }
}

function updatePasswordStrengthMessage(password_strength) {
	var strengthMessage = document.getElementById("pwd_strength_text");
	if (password_strength > 75) {
		strengthMessage.innerHTML = getMessage("Password Strength: Great");
	} else if (password_strength > 50) {
		strengthMessage.innerHTML = getMessage("Password Strength: Good");
	} else if (password_strength > 25) {
		strengthMessage.innerHTML = getMessage("Password Strength: Okay");
	} else if (password_strength > 0) {
		strengthMessage.innerHTML = getMessage("Password Strength: Weak");
	} else {
		strengthMessage.innerHTML = getMessage("Does not meet requirements");
	}
}