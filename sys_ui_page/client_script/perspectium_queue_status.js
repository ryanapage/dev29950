var ga = new GlideAjax('PerspectiumQueueAjax');	
ga.addParam('sysparm_name', 'getAllQueuesStatus');
ga.getXML(ajaxResponse);
 
function ajaxResponse(serverResponse) {  
	var queueResults = "<table id='queueResultsTable'><tr><th>Queue</th><th>Direction</th><th>URL</th><th>Status</th><th>Monitor</th><th>Connection Threshold</th><th>Records Threshold</th><th>Monitor Status</th></tr>";
 
	// get all queue items returned and display in table
	var queues = serverResponse.responseXML.getElementsByTagName("queue");
	for(var i = 0; i < queues.length; i++) {
		var name = queues[i].getAttribute("name");
		var direction = queues[i].getAttribute("direction");
		var url = queues[i].getAttribute("url");
		var status = queues[i].getAttribute("status");
		var monitor = queues[i].getAttribute("monitor");
		var recThreshold = queues[i].getAttribute("recThreshold");
		var connThreshold = queues[i].getAttribute("connThreshold");
		var monitorStatus = queues[i].getAttribute("monitorStatus");

		queueResults += "<tr><td>" + name + "</td><td>" + direction + "</td><td>" + url + "</td><td>" + status + "</td><td>" + monitor + "</td><td>" + connThreshold+ "</td><td>" + recThreshold + "</td><td>" + monitorStatus + "</td></tr>";

	}
 
	queueResults += "</table>";
	$j('#queueResults').html(queueResults);
}	

/*var qgr = new GlideRecord("u_psp_queues");
qgr.orderBy("u_direction");
qgr.query();
while (qgr.next()) {
	//queueResults += qgr.u_name;
	var ga = new GlideAjax('PerspectiumQueueAjax');	
	ga.addParam('sysparm_name', 'updateQueueStatus');
	ga.addParam('sysparm_queue_sysid', qgr.sys_id);
	ga.getXMLWait();
	var queueResult = ga.getAnswer();
	queueResults += "<tr><td>" + qgr.u_name + "</td><td>" + qgr.u_endpoint_url + "</td><td>" + queueResult + "</td></tr>";
	qgr.u_status = queueResult;
	qgr.update();		
}

queueResults += "</table>";
$j('#queueResults').html(queueResults);
*/