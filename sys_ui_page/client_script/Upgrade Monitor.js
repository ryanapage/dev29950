var msg = new GwtMessage();

var values = ["Failed","loading","fix","index","true","false","none","started","no thread","Complete","running","Your upgrade terminated abnormally. Please contact support", "Loading updated system data into the database. This step is not expected to take more than a few seconds.", "The system is applying a fix job to update a large quantity of system data. This step may, depending on the fix, take several minutes, although most fix jobs complete much more quickly than this.", "   The system is adding an index to a database table. On large tables, this step can take several minutes, and in extreme cases up to an hour. Most indexes complete much more quickly than this.", "Not running"];

var translated = msg.getMessages(values);

getUpgradeData();

function setTimer(value, threadStatus){
  if(value == "complete"
        || value == "none"
        || value == "no thread"
        || threadStatus == "false"&& value != "complete") {
           clearTimeout(setTimeout('getUpgradeData()', 5000));
  }

  else {
        setTimeout('getUpgradeData()', 5000);
  }
}



function getUpgradeData() {
  var ajax = new GlideAjax("UpgradeMonitor");
  ajax.addParam("sysparm_name", "get_status"); 
  ajax.getXML(getUpgradeDataParse);

}
 
function getUpgradeDataParse(response) {
  var items = response.responseXML.getElementsByTagName("item");
  if (items.length == 0){
           setTimer();
           return;
  }

  for (var i = 0; i < items.length; i++) {
     var item = items[i]; 
     var name = item.getAttribute("name");
     var value = item.getAttribute("value");

        var el = gel(name);

        if (name == "plugin_total") {
            var total_file = parseInt(value);
            el.innerHTML = value;

        }
        else if (name == "plugin_progress") {
             var progress = parseInt(value);
             el.innerHTML = value;
        }
        else if (name == "step_type") { 
             var stepType = value;
        } 

        else if (name == "thread_running") { 
              var threadRunning = value;
              translateOutput("thread_running", threadRunning); 
        }

        else if (name == "file_progress") {
              el.innerHTML = formatNumber(value);
        }
     
        else if(el)
                 el.innerHTML = value;  
  }

  var percentage_complete = (progress/total_file)*100;  


   if(stepType == "complete"
        || stepType == "no thread") {
           hideObject(gel("progress_table"));
           hideObject(gel("file_progress_field"));
           showObject(gel("refresh"));
           hideObject(gel("last_version_update"));
           showObject(gel("complete_update_message"));
           hideObject(gel("failed_update_message"));
           hideObject(gel("mainTable"));
   }

   else if(stepType == "loading") {
           showObject(gel("progress_table"));
           showObject(gel("file_progress_field"));
           hideObject(gel("refresh"));
           hideObject(gel("last_version_update"));
           hideObject(gel("complete_update_message"));
           showObject(gel("mainTable"));
   }

   else if(stepType == "none") {
           hideObject(gel("progress_table"));
           hideObject(gel("file_progress_field"));
           hideObject(gel("complete_update_message"));
           showObject(gel("last_version_update"));
           showObject(gel("refresh"));
           hideObject(gel("failed_update_message"));
           if(gel("from_version").innerHTML == "") {
              hideObject(gel("from_to_version"));
           }             
   }

   else if(threadRunning == "false") {
           showObject(gel("refresh"));
   }

   else {
         hideObject(gel("refresh"));
   }
        
   statusMessage(stepType,threadRunning);
   aboutStep(name, stepType, threadRunning);  
   progressbar(percentage_complete);
   setTimer(stepType, threadRunning);
}

//argument count: pass the percentage of the bar you want to be filled
function progressbar(count) {
   var progress = gel('progress');
   progress.style.width =  count+"%";
}

function statusMessage(valueStepType, valueThreadRunning) {
    var img_field = gel('img_status');

    if(valueStepType == "none") {
      translateOutput("img_status_text", "Not running");
      gel('img_status').innerHTML = "";
      hideObject(gel("startedended_field"));
    }

    else if(valueStepType == "complete") {
      translateOutput("img_status_text", "Complete");
      gel('img_status').innerHTML = " " + '<img src="images/outputmsg_success.gif"/>';
      showObject(gel("end_time_block"));
      showObject(gel("startedended_field"));
    }

    else if(valueStepType != "complete"&&
       valueThreadRunning == "false") {
       translateOutput("img_status_text", "Failed");
       gel('img_status').innerHTML = " "+ '<img src="images/outputmsg_error.gifx"/>';
       showObject(gel("progress_table"));
       showObject(gel("refresh"));
       hideObject(gel("end_time_block"));
       hideObject(gel("startedended_field"));
    }

    else {
       translateOutput("img_status_text", "running");
       gel('img_status').innerHTML = " " + '<img src="images/header_anim.gifx"/>';
       show("step_details");
       showObject(gel("last_message_row"));
       hideObject(gel("end_time_block"));
       showObject(gel("startedended_field"));
    }
}

function aboutStep(nameStepType, valueStepType, threadRunning) {
   
    if(valueStepType == "loading") {
      gel('about_step1').innerHTML = valueStepType + ": "; 
      translateOutput('about_step2',"Loading updated system data into the database. This step is not expected to take more than a few seconds.");
      hideObject(gel("failed_update_message"));
    }

    if(valueStepType == "fix") {
      gel('about_step1').innerHTML = valueStepType + ": "; 
      translateOutput('about_step2', "The system is applying a fix job to update a large quantity of system data. This step may, depending on the fix, take several minutes, although most fix jobs complete much more quickly than this.");
    }

    if(valueStepType == "index") {
      gel('about_step1').innerHTML = valueStepType + ": "; 
      translateOutput('about_step2', "The system is adding an index to a database table. On large tables, this step can take several minutes, and in extreme cases up to an hour. Most indexes complete much more quickly than this.");
    }

    if(valueStepType != "complete"&& threadRunning == "false") {
      translateOutput("about_step1", "Your upgrade terminated abnormally. Please contact support");
      gel('about_step2').innerHTML = "";
      showObject(gel("failed_update_message"));
    }

    if(valueStepType == "none") {
      hideObject(gel("failed_update_message"));
    }

    if(valueStepType == "complete") {
      gel('about_step1').innerHTML = "";
      gel('about_step2').innerHTML = "";
    }
}

function translateOutput(name, value) {
    gel(name).innerHTML = translated[value];
}