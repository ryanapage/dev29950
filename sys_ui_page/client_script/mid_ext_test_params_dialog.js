function done() {
	GlideDialogWindow.get().destroy();
	return false;
}

function testParameters() {
	var gdw = GlideDialogWindow.get();
	var context_sys_id = gdw.getPreference('context_sys_id');
	var ga = new GlideAjax('MIDExtensionTestParameters');
	ga.addParam('sysparm_name', 'testParameters');
	ga.addParam('sysparm_context_sys_id', gdw.getPreference('context_sys_id'));
	ga.addParam('sysparm_form_data', gdw.getPreference('form_data'));
	ga.getXMLWait();
	var ecc_message_sys_id = ga.getAnswer();
	if (ecc_message_sys_id.substring(0, 5) == 'error') {
		complete(ecc_message_sys_id);
		return;
	}

	checkProgress(ecc_message_sys_id);
}

function checkProgress(ecc_message_sys_id) {
	var ga = new GlideAjax('MIDExtensionTestParameters');
	ga.addParam('sysparm_name', 'checkProgress');
	ga.addParam('sysparm_ecc_message_sys_id', ecc_message_sys_id);
	ga.getXMLWait();
	result = ga.getAnswer();
	if (result.substring(0, 5) == 'ready') {
		setTimeout(function() { checkProgress(ecc_message_sys_id); }, 1000);
		return;
	}

	complete(result);
}

function complete(result) {
	$('test_progress').hide();
	$('button_cancel').hide();
	$('button_ok').show();
	if (result.substring(0, 9) == 'processed')
		$('test_complete').show();
	else {
		$('err_text').innerHTML = getErrorMessage(result);
		$('test_failed').show();
	}
}

function getErrorMessage(result) {
	var idx = result.indexOf(':');
	return (idx < 0) ? result : result.substring(idx + 1);
}