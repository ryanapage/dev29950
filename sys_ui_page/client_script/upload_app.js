var $jj = jQuery;

function refillPicker() {
	var top = getTopWindow();
	if (typeof top.g_application_picker != 'undefined')
		top.g_application_picker.fillApplications();
}

$jj('input[type=radio][name=upload_type]').change(function() {
	if (this.value == 'file') {
		$jj('form').attr("enctype", "multipart/form-data").attr("encoding", "multipart/form-data");
		$jj('.file_controls').show();
		$jj('.url_controls').hide();
		$jj('.repo_controls').hide();
		var val = $jj('#attachFile').val();
		if (val)
			$jj('.submit_controls').show();
		else
			$jj('.submit_controls').hide();
	} else if (this.value == 'url') {
		$jj('form').removeAttr("enctype").removeAttr("encoding");
		$jj('.file_controls').hide();
		$jj('.url_controls').show();
		var authenticated = $jj('#repo_authenticated').val() == 'true';
		if (authenticated)
			$jj('.repo_controls').show();
		else
			$jj('.repo_controls').hide();
		
		if ($jj('#artifact_id').val())
			$jj('.artifact_controls').show();
		else
			$jj('.artifact_controls').hide();
		
		if ($jj('#artifact_id').val() && $jj('#artifact_version').val())
			$jj('.submit_controls').show();
		else
			$jj('.submit_controls').hide();
	}
});

$jj("input:file").change(function () {
	$jj('.submit_controls').show();
});

$jj("#group_id").change(function() {
	$jj('.submit_controls').hide();
	$jj('.artifact_controls').hide();	
	populateArtifacts();
});

$jj("#artifact_id").change(function () {
	if ($jj('#artifact_id').val()) {
		$jj('.artifact_controls').show();
		populateArtifactVersions();
	} else
		$jj('.artifact_controls').hide();	
});

$jj("#artifact_id,#artifact_version").change(function () {
	if ($jj('#artifact_id').val() && $jj('#artifact_version').val())
		$jj('.submit_controls').show();
	else
		$jj('.submit_controls').hide();
});

$jj("#repo_username,#repo_password,#repo_url").change(function() {
	$jj('.repo_controls').hide();
	$jj('#artifact_id').empty();
	$jj('#artifact_version').empty();	
});

$jj("#repo_auth").click(function() {
	var ga = _newArtifactService('authenticate');
	ga.getXML(_cb);
	
	function _cb(response) {		
		var authenticated = response.responseXML.documentElement.getAttribute("answer");
		$jj('#repo_authenticated').val(authenticated);
		if (authenticated == 'true') {
			$jj('.repo_controls').show();
			ensureArtifacts();
		} else {
			$jj('.repo_controls').hide();
			alert("Invalid credentials, please try again");
		}
	}
});

function ensureArtifacts() {
	var isEmpty = $jj('#artifact_id > options').length <= 1;
	if (isEmpty)
		populateArtifacts();
}

function populateArtifacts() {
	$jj('#artifact_id').empty();
    $jj('#artifact_version').empty();
	
	var group_id = $jj('#group_id').val().trim();
	if (group_id === '')
		return;

	$jj('#artifact_id').append(new Option("Loading...", ""));
	
	var ga = _newArtifactService('getArtifacts');
	ga.addParam('sysparm_group_id', group_id);
	ga.getXML(_cb);
	
	function _cb(response) {		
		var responseString = response.responseXML.documentElement.getAttribute("answer");
		var artifacts = $jj.parseJSON(responseString)||[];
		var dd = $jj('#artifact_id');
		dd.empty();
		dd.append(new Option("--Select--", ""));
		for (var i = 0; i < artifacts.length; i++) {
			var artifactId = artifacts[i];
			dd.append(new Option(artifactId, artifactId));
		}
	}
}

function populateArtifactVersions() {
	$jj('#artifact_version').empty();
	$jj('.submit_controls').hide();
	
	var group_id = $jj('#group_id').val().trim();
	var artifact_id = $jj('#artifact_id').val();
	if (group_id === '' || artifact_id === '')
		return;

	$jj('#artifact_version').append(new Option("Loading...", ""));
	var ga = _newArtifactService('getArtifactVersions');
	ga.addParam('sysparm_group_id', group_id);
	ga.addParam('sysparm_artifact_id', artifact_id);	
	ga.getXML(_cb);
	
	function _cb(response) {		
		var responseString = response.responseXML.documentElement.getAttribute("answer");
		var versions = $jj.parseJSON(responseString)||[];
		var dd = $jj('#artifact_version');
		dd.empty();
		dd.append(new Option("--Select--", ""));
		for (var i = 0; i < versions.length; i++) {
			var version = versions[i];
			var text = version.text;			
			var value = version.resourceURI;
			dd.append(new Option(text, value));
		}		
	}
}

function _newArtifactService(name) {
	var repo_url = $jj('#repo_url').val();
	var repo_username = $jj('#repo_username').val();
	var repo_password = $jj('#repo_password').val();
	
	var ga = new GlideAjax('ArtifactServiceAjax');
	ga.addParam('sysparm_name', name);
	ga.addParam('sysparm_repo_url', repo_url);
	ga.addParam('sysparm_repo_username', repo_username);
	ga.addParam('sysparm_repo_password', repo_password);
	return ga;
}