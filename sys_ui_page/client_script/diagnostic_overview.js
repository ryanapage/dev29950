function addValue(e, o) {
   var tr = cel("tr", e);
   var td = cel("td", tr);
   td.className = "label_left";
   td.style.width = "25%";
   td.noWrap = "true";
   if (o.flag)
      td.style.color = "red";
      
   td.innerHTML = o.label;
   td = cel("td", tr);
   td.noWrap = "true";
   if (o.flag)
      td.style.color = "red";
      
   if (!o.value || o.value == "undefined")
      td.innerHTML = "?";
   else
      td.innerHTML = o.value;
}

var diag_table = gel('$[jvar_diag_table_id]');
var table = cel('table', diag_table);
table.cellSpacing = "2";
table.cellPadding = "2";
table.className = "wide";
var tbody = cel('tbody', table);
for (var i = 0; i < g_diagnostics.length; i++)
   addValue(tbody, g_diagnostics[i]);

