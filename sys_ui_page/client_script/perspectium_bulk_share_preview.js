document.observe("dom:loaded", function() { 	
	//convert back into arrays  
	var trendCounts = "${trendCounts}";  
	trendCounts = trendCounts.split(',');  
	var trendMonths = "${trendMonths}";  
	trendMonths = trendMonths.split(',');  

	if(trendCounts.length == 1 && trendCounts[0] == "")
		return;

	// convert counts to ints for highcharts to read properly  
	for (var i=0; i != trendCounts.length; i++) { 
		trendCounts[i] = parseInt(trendCounts[i], 10); 
	}  
    	
	var chart1 = new Highcharts.Chart({
        chart: {
			renderTo: 'trendChart', 
            type: 'line'
        },
        title: {
            text: 'Records Created by Month'
        },
        subtitle: {
            text: '${tableName} table'
        },
        xAxis: {
            categories: trendMonths
        },
        yAxis: {
            title: {
                text: 'Records Created'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [{
            name: 'Records',
			data: trendCounts
        }]
    });  
});