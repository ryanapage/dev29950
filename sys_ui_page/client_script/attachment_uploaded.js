newAttachmentUploaded();
 
function newAttachmentUploaded() {

	if ('${JS:sysparm_error}' != '')
		alert('${JS:sysparm_error}');

    parent.clearAttachmentFields();
 
    var fileName = fileNameStr.split('///');
    var canDelete = canDeleteStr.split(',');
    var createdBy = createdByStr.split(',');
    var createdOn = createdOnStr.split(',');
    var contentType = contentTypeStr.split(',');
    var encryption = encryptionStr.split(',');
    var iconPath = iconPaths.split(',');
 
    var newAttachmentId = newAttachmentIdString.split(',');
    for (var i = 0; i < newAttachmentId.length; ++i) {
        if (fileName[i]) {
            parent.refreshAttachments(newAttachmentId[i], fileName[i], canDelete[i], createdBy[i],
                createdOn[i], contentType[i], encryption[i], iconPath[i]);
        } else
            parent.refreshAttachments();
    }
}