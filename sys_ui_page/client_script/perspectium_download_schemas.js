var answer = '';
var ga = new GlideAjax('PerspectiumTables');
var zip = new JSZip();
ga.addParam('sysparm_name','getShareTables');
ga.getXML(getAjaxResponse);

function getSchemaCount() {
	var finalCount = findDuplicates(answer);
	$( "#schemaCounter" ).append('<b>Notice:</b> This will download ' + finalCount.length + ' table schemas.' );
}

function button() {	
	$('#download').prop('disabled',true); 
	$('#buttonText').remove();
	$('#download').append('<span id="buttonText">Downloading</span>')
	setTimeout(function(){
		var finalArray = findDuplicates(answer);
		getXML(finalArray);
	}, 500);
}

function getAjaxResponse(response) {
    answer = response.responseXML.documentElement.getAttribute("answer");
	answer = answer.replace(/[\[\]']+/g,'');
	answer = answer.split(',');
    return answer;
}

function getXML(combinedArray) {
	for(var i = 0; i < combinedArray.length; i++){
		$.ajax({
			async: false,
			type: "get",
			url: combinedArray[i] + ".do?PSP_SCHEMA",
			dataType: "xml",
			success: function(data) {
				/* handle data here */
				schema = new XMLSerializer().serializeToString(data);
				zip.file(combinedArray[i] + ".xml", schema);
			}
		});
}
	creatZip();
	$('#download').prop('disabled',false); 
	$('#buttonText').remove();
	$('#download').append('<span id="buttonText">Download</span>');
}

	function findDuplicates(arra1) {
		var i;
		var len = arra1.length;
		var result = [];
		var obj = {}; 
		for (i=0; i<len; i++) {
			obj[arra1[i]]=0;
		}
		for (i in obj) {
			result.push(i);
		}

		return result;
}

function creatZip() {
	zip.generateAsync({type:"blob"})
	.then(function(content) {
		// see FileSaver.js
		saveAs(content, "schemas.zip");
	});
}