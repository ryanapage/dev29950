document.observe("dom:loaded", function() { 	
	//convert back into arrays  
	var trendCounts = "${trendCounts}";  
	trendCounts = trendCounts.split(',');  
	var trendTimes = "${trendTimes}";  
	trendTimes = trendTimes.split(',');
	
	var daysAgo = "${daysAgo}";
	var tickCount = Math.round("${totalCount}" / 30);

	if(trendCounts.length == 1 && trendCounts[0] == ""){
		return;
	}
	
	// convert counts to ints for highcharts to read properly  
	for (var i = 0; i != trendCounts.length; i++) { 
		trendCounts[i] = parseInt(trendCounts[i], 10); 
	} 
	
	var chart1 = new Highcharts.Chart({
		chart: {
			renderTo: 'trendChart', 
			type: 'line'
		},
		title: {
			text: 'Queue Count by Time'
		},
		subtitle: {
		text: 'Tracked over the last ' + daysAgo + ' days'
		},
		xAxis: {
			categories: trendTimes,
			tickInterval: tickCount,
			title: {
				text: 'Timestamp'
			}
		},
		yAxis: {
			title: {
				text: 'Queue Count'
			}
		},
		plotOptions: {
			line: {
				dataLabels: {
					enabled: false
				}
			}
		},
		series: [{
			data: trendCounts		
		}]
	});  
});