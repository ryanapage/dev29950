var zip = new JSZip();

function button() {	
	getXML();
}

function getXML() {

	if (document.getElementById("bulkShares").checked){
		getTableRecords('psp_bulk_share');
	}
	
	if (document.getElementById("dynamicShares").checked){
		getTableRecords('psp_replicate_conf');
	}
	
	if (document.getElementById("scheduledShares").checked){
		getTableRecords('u_psp_scheduled_bulk_share');
	   
	}
	
	if (document.getElementById("tableMaps").checked){
		getTableRecords('u_psp_table_map');
		getTableRecords('u_psp_table_field_map');
	}
	
  createZip();
}

function getTableRecords(tableName) {
	$.ajax({
			async: false,
			type: "get",
			url: tableName + ".do?XML",
			dataType: "xml",
			success: function(data) {
				/* handle data here */
				schema = new XMLSerializer().serializeToString(data);
				zip.file(tableName + ".xml", schema);
			},
			error: function(xhr, status) {
				/* handle error here */
				$("#show_table").html(status);
			}
	});
}

function createZip() {
  zip.generateAsync({type:"blob"})
  .then(function(content) {
    // see FileSaver.js
    saveAs(content, "share_configurations.zip");
});
}