if (btn_clicked == 'ok') {
	if (signature_redirect_url)
		response.sendRedirect(signature_redirect_url);
	else 
		response.sendRedirect(gs.getSession().getStack().bottom());
	
	gs.addInfoMessage('The signature has been accepted');
}
if (btn_clicked == 'cancel' )
	current.setAbortAction(true);
