var bucket_sysid;
createRecords();

function createRecords() {
	var bucket = new GlideRecord("pa_bucket_groups");
	bucket.name = new_name;
	bucket_sysid = bucket.insert();
	
	var dim = new GlideRecord("pa_dimensions");
	dim.name = new_name;
	dim.facts_table = 'pa_buckets';
	dim.conditions = 'bucket_group=' + bucket_sysid;
	dim.insert();
}

response.sendRedirect('pa_bucket_groups.do?sys_id=' + bucket_sysid);