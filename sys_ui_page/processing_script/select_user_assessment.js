var sel_user = sys_user;
if (sel_user == null || sel_user == '')
    sel_user = asr_recent;

doAssessment(sel_user);

function doAssessment(new_user) {
	
	new SNC.AssessmentCreation().addRecentAssessor(new_user);
	
	var instanceId = "";
	if (type_id != "" && source_id != "") //This is an assessable record
		instanceId = new SNC.AssessmentCreation().createAssessments(type_id, source_id, new_user);
	else    // This is a survey or a metric type
	    instanceId = new SNC.AssessmentCreation().createAssessments(asmtr_sys_id, "", new_user);
	
	var gr = new GlideRecord("asmt_assessment_instance");
	var success = gr.get(instanceId.substring(0,32));
	if(success){
		var infoMsg = gs.getMessage("Instance created successfully");
		gs.addInfoMessage(infoMsg);
	}else{
		var errMsg = gs.getMessage("Instance could not be created");
		gs.addErrorMessage(errMsg);
	}
	var urlOnStack = GlideSession.get().getStack().bottom();
	response.sendRedirect(urlOnStack);
}